@extends('layouts.app')
<link rel="apple-touch-icon" href="{{ asset('iconbar/assets/images/apple-touch-icon.png') }}">
  <link rel="shortcut icon" href="{{ asset('iconbar/assets/images/favicon.ico') }}">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="{{ asset('iconbar/global/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/css/bootstrap-extend.min.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/assets/css/site.min.css') }}">
  <!-- Plugins -->
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/animsition/animsition.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/asscrollable/asScrollable.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/switchery/switchery.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/intro-js/introjs.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/slidepanel/slidePanel.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/flag-icon-css/flag-icon.css') }}">
  <!-- <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/waves/waves.css') }}"> -->
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/nestable/nestable.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/html5sortable/sortable.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/tasklist/tasklist.css') }}">
  <!-- Fonts -->
  <link rel="stylesheet" href="{{ asset('iconbar/global/fonts/material-design/material-design.min.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/fonts/brand-icons/brand-icons.min.css') }}">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <script src="{{ asset('iconbar/global/vendor/breakpoints/breakpoints.js') }}"></script>
  <script>
  Breakpoints();
  </script>
    <script src="{{ asset('iconbar/global/vendor/babel-external-helpers/babel-external-helpers.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/jquery/jquery.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/tether/tether.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/bootstrap/bootstrap.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/animsition/animsition.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/mousewheel/jquery.mousewheel.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/asscrollbar/jquery-asScrollbar.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/asscrollable/jquery-asScrollable.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/ashoverscroll/jquery-asHoverScroll.js') }}"></script>
  <!-- <script src="{{ asset('iconbar/global/vendor/waves/waves.js') }}"></script> -->
  <!-- Plugins -->
  <script src="{{ asset('iconbar/global/vendor/switchery/switchery.min.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/intro-js/intro.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/screenfull/screenfull.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/slidepanel/jquery-slidePanel.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/html5sortable/html.sortable.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/nestable/jquery.nestable.js') }}"></script>
  <!-- Scripts -->
  <script src="{{ asset('iconbar/global/js/State.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Component.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Base.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Config.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/Section/Menubar.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/Section/GridMenu.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/Section/Sidebar.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/Section/PageAside.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/Plugin/menu.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/config/colors.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/config/tour.js') }}"></script>
  <script>
  //Config.set('assets', '../../assets');
  </script>
  <!-- Page -->
  <script src="{{ asset('iconbar/assets/js/Site.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/asscrollable.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/slidepanel.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/switchery.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/html5sortable.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/nestable.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/tasklist.js') }}"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
    });
  })(document, window, jQuery);
  </script>

@section('content')

  
  <style>
    /* Always set the map height explicitly to define the size of the div
     * element that contains the map. */
    #map {
      max-height: 1200px;
      height:100% !important;
    }

    #mapEdit {
      min-height: 800px;
    }

    .modal-lg {
      max-width: 1200px !important;
    }

    .ul #listaParadas{
      overflow:auto;
      overflow-y: scroll;
      overflow-x: hidden;
      max-height:400px!important;
    }

    #listaParadas .form-control{
      padding-bottom: 15px !important;
      padding-top: 15px !important;
      font-size: 18px !important;
      line-height: 1.571429 !important;
      display: block !important;
      padding: 3px 3px !important;
    }
     #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }
      .pac-container {
          background-color: #FFF;
          z-index: 20;
          position: fixed;
          display: inline-block;
          float: left;
      }
      .modal{
          z-index: 20;   
      }
      .modal-backdrop{
          z-index: 10;        
      }​
      #type-selector{
        display:none;
      }
      #type-selector-Edit{
        display:none;
      }
      #onEnter{
        -webkit-box-shadow: 0px 3px 25px 23px rgba(0,0,0,0.22);
        -moz-box-shadow: 0px 3px 25px 23px rgba(0,0,0,0.22);
        box-shadow: 0px 3px 25px 23px rgba(0,0,0,0.22);
        z-index: 1000; 
      }
      .modal{
        top: 6%;
      }
      #label-special{
        padding: 0px;
        margin: 0px;
      }
      
      .select2-container--default .select2-selection--single{
        height: 2.287rem !important;
        line-height: 1.3;
      }
      #div_drivers{
        /*min-height: 700px;*/
        /*height: auto !important;*/
      }
      .list-group .card-block:hover {
          cursor: pointer !important;
          background-color: #eee!important;
      }

    /*#listaParadas  .li {
      display: block;
      height:50px;
      padding-bottom: :5px;
      padding-top:5px;
    }
    */
  </style>


  {{ csrf_field() }}
   <div class="page">
    
    <!-- <div class="page-content"> -->
      <!-- Panel -->
      <!-- <div class="panel"> -->
        <!-- <div class="panel-body"> -->

          <!-- <div class="row"> -->

            <div class="page-aside" style="width: 450px !important;">
            <!-- <div class="col-lg-4"> -->
              <div class="page-aside-inner">
                <div class="search-friends panel" style="margin-bottom:0px !important;">
                  <div class="panel-heading">
                    <div class="panel-actions">
                      <div class="input-search input-group-sm">
                        <button type="submit" class="input-search-btn">
                          <i class="icon wb-search" aria-hidden="true"></i>
                        </button>
                        <input type="text" class="form-control" name="searchDrivers" id="searchDrivers" placeholder="Buscar..." oninput="filterDrivers(this.value)";>
                      </div>
                    </div>
                    <h3 class="panel-title">Conductores</h3>
                  </div>
                  </hr>
                   <div class="row">
                    <div class="col-lg-1"> 
                    </div>
                    <div class="col-lg-5"> 
                      <div class="checkbox-custom checkbox-primary">
                        <input type="checkbox" value="1" id="checkBuses" onclick="filterDriversType()">
                        <label for="inputUnchecked">Buses</label>
                      </div>
                    </div>
                    <div class="col-lg-5">
                      <div class="checkbox-custom checkbox-primary">
                        <input type="checkbox" value="0"  id="checkWheels"  onclick="filterDriversType()">
                        <label for="inputUnchecked">Wheels</label>
                      </div> 
                    </div>
                    <div class="col-lg-1"> 
                    </div>
                  </div>
                </div>
                <div class="app-location-list page-aside-scroll" style="background-color: #f3f7f9 !important;height: 100%;" >
                  <div data-role="container">
                    <div data-role="content"  class="scrollable-content">
                      <div class="list-group flex-row flex-wrap" id="div_drivers" style="padding-top: 10px; padding-left: 7px;padding-right: 7px;">
                      </div>
                    </div>
                  </div>
                </div>
               
              </div>
            </div>

            <div class="page-main"  style="margin-left: 450px !important;">
             <!-- <div class="col-lg-8"> -->
              <div id="contactsContent" class="page-content page-content-table" data-plugin="selectable">
                
                <div id="map"></div>
              </div>
            </div>
            <!-- </div> -->
          <!-- </div> -->
        <!-- </div> -->
      <!-- </div> -->
      <!-- End Panel -->
    </div>
  <!-- </div> -->

  <div class="site-action" data-plugin="actionBtn" style="right: 60px !important;">
    <button type="button" class="site-action-toggle btn-raised btn btn-success btn-floating" onclick="refresh_transit();">
      <i class="front-icon wb-refresh animation-scale-up" aria-hidden="true"></i>
      <i class="back-icon wb-refresh animation-scale-up" aria-hidden="true"></i>
    </button>
  </div>



<script type="text/javascript">
  token = $("input[name*='_token']").val();
  if (window.location.host == 'localhost') {
    var baseurl = window.location.protocol +"//" + window.location.host + '/prontuz_backend/public';
  }else{
    var baseurl = window.location.protocol +"//" + window.location.host ;
  }
  locations= new Array();
  widthdiv=0
  inilocation=[]
  inilocation[0]={lat: 4.662767, lng: -74.0694308};
  inilocation[1]=15;
  inilocation[2]='';

  /**********************************************/

  $( document ).ready(function() {

    getRprincipals();
  


  });

  /**********************************************/
  
   // var markers[0] = {lat: 4.673417, lng: -74.0716269};
    // var markers[1] = {lat: 4.662767, lng: -74.0694308};
  
  function initMap() {

    var styledMapType = new google.maps.StyledMapType(
            [
              {elementType: 'geometry', stylers: [{color: '#ebe3cd'}]},
              {elementType: 'labels.text.fill', stylers: [{color: '#523735'}]},
              {elementType: 'labels.text.stroke', stylers: [{color: '#f5f1e6'}]},
              {
                featureType: 'administrative',
                elementType: 'geometry.stroke',
                stylers: [{color: '#c9b2a6'}]
              },
              {
                featureType: 'administrative.land_parcel',
                elementType: 'geometry.stroke',
                stylers: [{color: '#dcd2be'}]
              },
              {
                featureType: 'administrative.land_parcel',
                elementType: 'labels.text.fill',
                stylers: [{color: '#ae9e90'}]
              },
              {
                featureType: 'landscape.natural',
                elementType: 'geometry',
                stylers: [{color: '#dfd2ae'}]
              },
              {
                featureType: 'poi',
                elementType: 'geometry',
                stylers: [{color: '#dfd2ae'}]
              },
              {
                featureType: 'poi',
                elementType: 'labels.text.fill',
                stylers: [{color: '#93817c'}]
              },
              {
                featureType: 'poi.park',
                elementType: 'geometry.fill',
                stylers: [{color: '#a5b076'}]
              },
              {
                featureType: 'poi.park',
                elementType: 'labels.text.fill',
                stylers: [{color: '#447530'}]
              },
              {
                featureType: 'road',
                elementType: 'geometry',
                stylers: [{color: '#f5f1e6'}]
              },
              {
                featureType: 'road.arterial',
                elementType: 'geometry',
                stylers: [{color: '#fdfcf8'}]
              },
              {
                featureType: 'road.highway',
                elementType: 'geometry',
                stylers: [{color: '#f8c967'}]
              },
              {
                featureType: 'road.highway',
                elementType: 'geometry.stroke',
                stylers: [{color: '#e9bc62'}]
              },
              {
                featureType: 'road.highway.controlled_access',
                elementType: 'geometry',
                stylers: [{color: '#e98d58'}]
              },
              {
                featureType: 'road.highway.controlled_access',
                elementType: 'geometry.stroke',
                stylers: [{color: '#db8555'}]
              },
              {
                featureType: 'road.local',
                elementType: 'labels.text.fill',
                stylers: [{color: '#806b63'}]
              },
              {
                featureType: 'transit.line',
                elementType: 'geometry',
                stylers: [{color: '#dfd2ae'}]
              },
              {
                featureType: 'transit.line',
                elementType: 'labels.text.fill',
                stylers: [{color: '#8f7d77'}]
              },
              {
                featureType: 'transit.line',
                elementType: 'labels.text.stroke',
                stylers: [{color: '#ebe3cd'}]
              },
              {
                featureType: 'transit.station',
                elementType: 'geometry',
                stylers: [{color: '#dfd2ae'}]
              },
              {
                featureType: 'water',
                elementType: 'geometry.fill',
                stylers: [{color: '#b9d3c2'}]
              },
              {
                featureType: 'water',
                elementType: 'labels.text.fill',
                stylers: [{color: '#92998d'}]
              }
            ],
            {name: 'Styled Map'});
    // var map = new google.maps.Map(document.getElementById('map'), {
    //       center: {lat: 4.662767, lng: -74.0694308},
    //       zoom: 14,
    //       mapTypeControlOptions: {
    //         mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain',
    //                 'styled_map']
    //       }
    //     });
    var map = new google.maps.Map(document.getElementById('map'), {
          center: inilocation[0],
          zoom: inilocation[1],
          mapTypeControlOptions: {
            mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain',
                    'styled_map']
          }
        });

        //Associate the styled map with the MapTypeId and set it to display.
        map.mapTypes.set('styled_map', styledMapType);
        map.setMapTypeId('styled_map');

  

    setMarkers(map);
  }

  /**********************************************/

  // Data for the markers consisting of a name, a LatLng and a zIndex for the
  // order in which these markers should display on top of each other.

  // var beaches = [
  //   ['Coopamer', 4.673417, -74.0716269, 3],
  //   ['Taller del dragon', 4.662275, -74.0680328, 2],
  //   ['fundacion area andina', 4.657647, -74.0673567, 1]//,
    // ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
    // ['Maroubra Beach', -33.950198, 151.259302, 1]
  // ];

  /**********************************************/

  function setMarkers(map) {

    var image=new Array();

    var image1 = {
      url: baseurl+'/'+'parada.png',
      //url: baseurl+'/'+'car-wheels-2.png',
      // This marker is 20 pixels wide by 32 pixels high.
      size: new google.maps.Size(25, 32),
      //size: new google.maps.Size(25, 25),
      // The origin for this image is (0, 0).
      origin: new google.maps.Point(0, 0),
      // The anchor for this image is the base of the flagpole at (0, 32).
      anchor: new google.maps.Point(0, 25)
    };

    var image2 = {
      url: baseurl+'/'+'car-ruta.png',
      //url: baseurl+'/'+'car-wheels-2.png',
      // This marker is 20 pixels wide by 32 pixels high.
      size: new google.maps.Size(50, 50),
      //size: new google.maps.Size(25, 25),
      // The origin for this image is (0, 0).
      origin: new google.maps.Point(0, 0),
      // The anchor for this image is the base of the flagpole at (0, 32).
      anchor: new google.maps.Point(0, 25)
    };

    var image3 = {
      url: baseurl+'/'+'car-wheels-3.png',
      //url: baseurl+'/'+'car-wheels-2.png',
      // This marker is 20 pixels wide by 32 pixels high.
      size: new google.maps.Size(50, 50),
      //size: new google.maps.Size(25, 25),
      // The origin for this image is (0, 0).
      origin: new google.maps.Point(0, 0),
      // The anchor for this image is the base of the flagpole at (0, 32).
      anchor: new google.maps.Point(0, 25)
    };

    image[0]='';
    image[1]=image2;
    image[2]=image3;
    image[3]=image1;

    var shape = {
      coords: [1, 1, 1, 20, 18, 20, 18, 1],
      type: 'poly'
    };
    
    // console.log('locations');
    // console.log(locations);
    
    for (var i = 0; i < locations.length; i++) {
      var beach = locations[i];

      markLocation={lat: beach[1], lng: beach[2]};


      var marker = new google.maps.Marker({
        position: markLocation,
        map: map,
        icon: image[beach[3]],
        shape: shape,
        title: beach[0],
        zIndex: i
      });

      if(inilocation[2] != '' && beach[3] < 3){

     
        var x=JSON.stringify(inilocation[0]);
        var y=JSON.stringify(markLocation);

        if(x === y){

          console.log('inilocation');
          console.log(inilocation);
          console.log('beach');
          console.log(beach);

          var infowindow = new google.maps.InfoWindow({
            content: inilocation[2]
          });

          infowindow.open(map, marker);

          inilocation[2]='';

        }
        
      }
    }
  }
  
  /**********************************************/

  function getRprincipals(){
    
    var Rprincipals = '';
    var drivers = new Array();
    
    
    $.ajax({
          type: "GET",
          url:baseurl+"/routes/getRprincipals",
          dataType: 'JSON',
          success: function (data) {

            // console.log(data);

            $.each(data.ResponseData,function(k, v){

                if(Rprincipals == ''){
                  Rprincipals=v.routeprincipalsId;
                }else{
                  Rprincipals=Rprincipals+','+v.routeprincipalsId;
                }
                
            });


          var data= {_token: token,routewheelDirection:1,localizationLatitude:0,localizationLongitude:0,routewheel_routeprincipalsId:Rprincipals};

          $.ajax({
                type: "POST",
                url: baseurl+'/routes/findWheel',
                data: data,
                dataType: 'JSON',
                success: function (data) {

                  if(data.ResponseCode == 1){

                    var i=0;
                    $.each(data.ResponseData,function(k, v){  
                      
                      print_drivers(v);
                      i=i+1;
                    });
                  }
                  var data= {_token: token,routewheelDirection:0,localizationLatitude:0,localizationLongitude:0,routewheel_routeprincipalsId:Rprincipals};

                    $.ajax({
                          type: "POST",
                          url: baseurl+'/routes/findWheel',
                          data: data,
                          dataType: 'JSON',
                          success: function (data) {

                            if(data.ResponseCode == 1){

                              var i=0;
                              $.each(data.ResponseData,function(k, v){  
                                 print_drivers(v);
                              });
                            }

                            $.ajax({
                                type: "POST",
                                url: baseurl+'/routes/findRoutes',
                                data: {_token: token},
                                dataType: 'JSON',
                                success: function (data) {


                                  if(data.ResponseCode == 1){

                                    var i=0;
                                    $.each(data.ResponseData,function(k, v){  
                                       print_drivers(v);
                                      i=i+1;
                                    });

                                  }

                                  getLocations(); 

                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                          });
                            

                          },
                          error: function (data) {
                              console.log('Error:', data);
                          }
                    });
               
                },
                error: function (data) {
                    console.log('Error:', data);
                }
          });

          

            
          
        },
        error: function (data) {
            console.log('Error:', data);
        }
      });


  



     
    

     

  }

  /**********************************************/

  function print_drivers(drivers){

    var locations2= new Array();
   
                    //0 => Rumbo Universidad, 1=> Desde universidad


    if(drivers.routeName !== undefined){

      locations2[0]=drivers.userFirstname+' '+drivers.userLastname;
      locations2[1]=parseFloat(drivers.vehicleLatitude);
      locations2[2]=parseFloat(drivers.vehicleLongitude);
      locations2[3]=1;

      
      

      if(drivers.routeDirection == 0){
        var routeDirection='Rumbo Universidad';
      }else{
        var routeDirection='Desde universidad';
      }


                var add='<div class="list-group-item col-sm-12 col-md-6 friend-info dBus dDrivers" data-friend-id="f_'+locations.length+'" style="min-height:209px; max-height:215px !important;background-color: #f3f7f9 !important;padding:15px 10px 10px 10px;" onclick="mark_event(\'f_'+locations.length+'\')" id="f_'+locations.length+'">'+
                          '<div class="card card-shadow w-full"  style="min-height:204px;margin-top:18px;">'+
                            '<div class="card-block text-center bg-white p-10"><br>'+
                              '<a href="#" class="avatar mb-5" style="width: 60px !important;">'+
                                '<img class="img-fluid" src="'+drivers.userImage+'" alt="Adam_photo" style="height: 70px;"  onerror="imgError(this);">'+
                              '</a>'+
                              '<div class="friend-name">'+drivers.routeName+'</div>'+
                              '<div class="friend-title mb-7 blue-grey-400">'+routeDirection+'</div>'+
                              '<div class="friend-title mb-7 blue-grey-400 dName" id="f_'+locations.length+'_name">'+
                                drivers.userFirstname+' '+drivers.userLastname+
                              '</div>'+
                              '<div class="friend-title mb-7 blue-grey-400 dPlaque" id="f_'+locations.length+'_plaque">'+
                                drivers.vehiclePlaque+
                              '</div>'+
                               '<div class="friend-title mb-7 blue-grey-400">'+
                                'Cupos: '+drivers.vehicleDifference+
                              '</div>'+
                            '</div>'+
                          '</div>'+
                        '</div>';

      locations[locations.length]=locations2;

      $('#div_drivers').append(add);
    }else{

      //
      locations2[0]=drivers.userFirstname+' '+drivers.userLastname;
      locations2[1]=parseFloat(drivers.vehicleLatitude);
      locations2[2]=parseFloat(drivers.vehicleLongitude);
      locations2[3]=2;

      
      if(drivers.routewheelDirection == 1){
        var routeDirection='Rumbo Casa';
      }else{
        var routeDirection='Rumbo universidad';
      }

         var add='<div class="list-group-item col-sm-12 col-md-6 friend-info dWheel dDrivers" data-friend-id="f_'+locations.length+'" style="min-height:209px; max-height:215px !important;background-color: #f3f7f9 !important;padding:15px 10px 10px 10px;" onclick="mark_event(\'f_'+locations.length+'\')" id="f_'+locations.length+'">'+
                          '<div class="card card-shadow w-full" style="min-height:204px;margin-top:18px;">'+
                            '<div class="card-block text-center bg-white p-10"><br>'+
                              '<a href="#" class="avatar mb-5" style="width: 60px !important;">'+
                                '<img class="img-fluid" src="'+drivers.userImage+'" alt="Adam_photo" style="height: 70px;"  onerror="imgError(this);">'+
                              '</a>'+
                              '<div class="friend-title mb-7 blue-grey-400">'+routeDirection+'</div>'+
                              '<div class="friend-title mb-7 blue-grey-400 dName" id="f_'+locations.length+'_name">'+
                                drivers.userFirstname+' '+drivers.userLastname+
                              '</div>'+
                              '<div class="friend-title mb-7 blue-grey-400 dPlaque" id="f_'+locations.length+'_plaque">'+
                                drivers.vehiclePlaque+
                              '</div>'+
                               '<div class="friend-title mb-7 blue-grey-400">'+
                                'Cupos: '+drivers.vehicleDifference+
                              '</div>'+
                            '</div>'+
                          '</div>'+
                        '</div>';
        
        locations[locations.length]=locations2;

        $('#div_drivers').append(add);
    }

    if(locations.length % 2 == 0) {

    

        var widthdiv2=((locations.length % 2)*210);

        if(widthdiv2 > widthdiv){

          $('#div_drivers').css('height',widthdiv2+'px');
          widthdiv=widthdiv2;
        }
        
      
    }else{
        var widthdiv2=(((locations.length % 2)*210)+210);
         
         if(widthdiv2 > widthdiv){

          $('#div_drivers').css('height',widthdiv2+'px');
          
          widthdiv=widthdiv2;
        }
    }

  }

  /**********************************************/

  function getLocations(){

    // console.log('locations.length');
    // console.log(locations.length);
    $.ajax({
            type: "GET",
            url: baseurl+'/routes/bringlocalization',
            dataType: 'JSON',
            success: function (data) {

              if(data.ResponseCode == 1){

                var i=0;
                $.each(data.ResponseData,function(k, v){  

                  var locations2= new Array();
                  
                  locations2[0]=v.localizationAddress;
                  locations2[1]=parseFloat(v.localizationLatitude);
                  locations2[2]=parseFloat(v.localizationLongitude);
                  locations2[3]=3;

                  locations[locations.length]=locations2;
                });
                // console.log(data);
              }
           
            },
            error: function (data) {
                console.log('Error:', data);
            }
    });

    setTimeout(function(){
      initMap();
    },4000);
  }
  /**********************************************/

  function mark_event(idmarket){
    //console.log(idmarket);

    var res = idmarket.split("_");
    
    var themarket=res[1];

    inilocation[0] = {lat: locations[themarket][1], lng: locations[themarket][2]};
    inilocation[1] = 18;
    inilocation[2] = locations[themarket][0];

    console.log('inilocation');
    console.log(inilocation);


     initMap();


  }

  /**********************************************/

    function imgError(image) {
      image.onerror = "";
  
      var routeImage='icon_user_2.png';
      image.src = baseurl+'/'+routeImage;
      image.style.height = '60px';
      return true;
    }

  /**********************************************/

    function filterDrivers(dText){

      if(dText == ''){
        $( "#div_drivers" ).find('.dDrivers').each(function() {
            var idDriverP=this.id;
            $( "#"+idDriverP ).css('display','flex');
        });
      }else{
          $( "#div_drivers" ).find('.dDrivers .dName').each(function() {

              var cadena = "hola mundo";

              if(this.innerText.indexOf(dText) > -1 == false){
                var idDriver=this.id;
                var idDriver2=idDriver.split('_');

                idDriver=idDriver2[0]+'_'+idDriver2[1]+'_plaque';
                idDriverP=idDriver2[0]+'_'+idDriver2[1];

                var dtext=document.getElementById(idDriver).innerHTML;
                
                if(dtext.indexOf(dText) > -1 == false){

                  $( "#"+idDriverP ).css('display','none');

                }else{
                  $( "#"+idDriverP ).css('display','flex');
                }
              }

          });
    
     //$( "#div_drivers" ).find('.dDrivers .dPlaque');
      }

    

    }

  /**********************************************/

    function filterDriversType(){

      var checkBuses=$('#checkBuses').prop('checked');
      var checkWheels=$('#checkWheels').prop('checked');


      if(checkWheels == false){
        $( "#div_drivers" ).find('.dWheel').css('display','none');
      }else{
        $( "#div_drivers" ).find('.dWheel').css('display','flex');
      }if(checkBuses == false){
        $( "#div_drivers" ).find('.dBus').css('display','none');
      }else{
        $( "#div_drivers" ).find('.dBus').css('display','flex');
      }


    }
  /**********************************************/

  function refresh_transit(){
    location.reload();
  }

  /***************************************/

  $('#profile_a').click(function(){
      //Some code
      // console.log('here');
   
      $('#dropdown-menu').show();
      //$('#profile_a').show();
  });
   $("#btn-perfil").click(function() {
        $("#newpassword").val('');
        $("#renewpassword").val('');
        $('#profile').modal('show');
    });
  /**********************************************/

  </script>

  <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCAcMphZ5OU_4iZl30i-gjD_NDmZsZ0TrQ" async defer></script> -->
  <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAsPNQHqMIginuMT9QgHu_7LXVzV-dW0Es&libraries=places"></script> -->
   <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAsPNQHqMIginuMT9QgHu_7LXVzV-dW0Es&libraries=places"></script>
@endsection
