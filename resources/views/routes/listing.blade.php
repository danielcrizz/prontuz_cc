@extends('layouts.app')
<link rel="apple-touch-icon" href="{{ asset('iconbar/assets/images/apple-touch-icon.png') }}">
  <link rel="shortcut icon" href="{{ asset('iconbar/assets/images/favicon.ico') }}">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="{{ asset('iconbar/global/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/css/bootstrap-extend.min.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/assets/css/site.min.css') }}">
  <!-- Plugins -->
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/animsition/animsition.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/asscrollable/asScrollable.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/switchery/switchery.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/intro-js/introjs.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/slidepanel/slidePanel.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/flag-icon-css/flag-icon.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/waves/waves.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/nestable/nestable.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/html5sortable/sortable.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/tasklist/tasklist.css') }}">
  <!-- Fonts -->
  <link rel="stylesheet" href="{{ asset('iconbar/global/fonts/material-design/material-design.min.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/fonts/brand-icons/brand-icons.min.css') }}">
  <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <script src="{{ asset('iconbar/global/vendor/breakpoints/breakpoints.js') }}"></script>
  <script>
  Breakpoints();
  </script>
    <script src="{{ asset('iconbar/global/vendor/babel-external-helpers/babel-external-helpers.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/jquery/jquery.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/tether/tether.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/bootstrap/bootstrap.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/animsition/animsition.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/mousewheel/jquery.mousewheel.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/asscrollbar/jquery-asScrollbar.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/asscrollable/jquery-asScrollable.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/ashoverscroll/jquery-asHoverScroll.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/waves/waves.js') }}"></script>
  <!-- Plugins -->
  <script src="{{ asset('iconbar/global/vendor/switchery/switchery.min.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/intro-js/intro.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/screenfull/screenfull.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/slidepanel/jquery-slidePanel.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/html5sortable/html.sortable.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/nestable/jquery.nestable.js') }}"></script>
  <!-- Scripts -->
  <script src="{{ asset('iconbar/global/js/State.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Component.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Base.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Config.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/Section/Menubar.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/Section/GridMenu.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/Section/Sidebar.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/Section/PageAside.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/Plugin/menu.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/config/colors.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/config/tour.js') }}"></script>
  <script>
  //Config.set('assets', '../../assets');
  </script>
  <!-- Page -->
  <script src="{{ asset('iconbar/assets/js/Site.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/asscrollable.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/slidepanel.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/switchery.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/html5sortable.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/nestable.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/tasklist.js') }}"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
    });
  })(document, window, jQuery);
  </script>

@section('content')

  
  <style>
    /* Always set the map height explicitly to define the size of the div
     * element that contains the map. */
    #map {
      height: 400px;
    }

    #mapEdit {
      height: 400px;
    }

    .modal-lg {
      max-width: 1200px !important;
    }

    .ul #listaParadas{
      overflow:auto;
      overflow-y: scroll;
      overflow-x: hidden;
      max-height:400px!important;
    }

    #listaParadas .form-control{
      padding-bottom: 15px !important;
      padding-top: 15px !important;
      font-size: 18px !important;
      line-height: 1.571429 !important;
      display: block !important;
      padding: 3px 3px !important;
    }
     #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }
      .pac-container {
          background-color: #FFF;
          z-index: 20;
          position: fixed;
          display: inline-block;
          float: left;
      }
      .modal{
          z-index: 20;   
      }
      .modal-backdrop{
          z-index: 10;        
      }​
      #type-selector{
        display:none;
      }
      #type-selector-Edit{
        display:none;
      }
      #onEnter{
        -webkit-box-shadow: 0px 3px 25px 23px rgba(0,0,0,0.22);
        -moz-box-shadow: 0px 3px 25px 23px rgba(0,0,0,0.22);
        box-shadow: 0px 3px 25px 23px rgba(0,0,0,0.22);
        z-index: 1000; 
      }
      .modal{
        top: 6%;
      }
      #label-special{
        padding: 0px;
        margin: 0px;
      }
      
      .select2-container--default .select2-selection--single{
        height: 2.287rem !important;
        line-height: 1.3;
      }
      

    /*#listaParadas  .li {
      display: block;
      height:50px;
      padding-bottom: :5px;
      padding-top:5px;
    }
    */
  </style>


  {{ csrf_field() }}
  <div class="page">
    <div class="page-content">
      <!-- Panel -->
      <div class="panel">
        <div class="panel-body">
          <div id="contactsContent" class="page-content page-content-table" data-plugin="selectable">
            <!-- Contacts -->
            <table id='route_list' width="100%" class="table is-indent" data-plugin="animateList" data-animate="fade" data-child="tr"
            data-selectable="selectable">
              <thead>
              <!--  border-bottom: 3px solid #4fb933; -->
                <tr>
                  <th width="75%" scope="col">Todas las Rutas</th>
                  <th width="15%" scope="col">Estado</th>
                  <th width="10%" scope="col">Acción</th>
                </tr>
              </thead>
            </table>
            
          </div>
        
        </div>
      </div>
      <!-- End Panel -->
    </div>
  </div>


  <!-- Site Action -->
  <div class="site-action" data-plugin="actionBtn">
    <button type="button" class="site-action-toggle btn-raised btn btn-success btn-floating">
      <i class="front-icon wb-plus animation-scale-up" aria-hidden="true"></i>
      <i class="back-icon wb-close animation-scale-up" aria-hidden="true"></i>
    </button>
    <div class="site-action-buttons">
      <button type="button" data-action="trash" class="btn-raised btn btn-success btn-floating animation-slide-bottom">
        <i class="icon wb-trash" aria-hidden="true"></i>
      </button>
      <button type="button" data-action="folder" class="btn-raised btn btn-success btn-floating animation-slide-bottom">
        <i class="icon wb-folder" aria-hidden="true"></i>
      </button>
    </div>
  </div>
  <!-- End Site Action -->


  <!-- Add User Form -->
  <div class="modal fade modal-3d-flip-vertical" id="addUserForm"  data-keyboard="false" data-backdrop="staticaria-labelledby="addUserForm" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
           <h4 class="modal-title">Crear Nueva Ruta</h4>
        </div>
        <div class="modal-body">

        <div id="loader" style="display:none;">
          <center>
              <br><br>
               <img width="48px" src="{{ asset('iconbar/loading.gif') }}" /> &nbsp; Enviando ...
              <br><br>
          </center>
        </div>

        <form id="data_crt" method="post" action="{{ url('routes/store') }}">
        <div id="data">

          <div class="form-group">
              <div id="error">
                <div role="alert" id="lalert" class="alert dark alert-danger alert-dismissible"> 
                  <button aria-label="Close" data-dismiss="alert" class="close" type="button"> 
                  </button>
                </div>
                <!-- error will be shown here ! -->
              </div>
          </div>
          {{ csrf_field() }}

            <div class="row">
              <div class="col-md-4 col-xs-4">
                  <span class="checkbox-custom checkbox-success checkbox-md">
                      <input type="radio" name="routeDirection" checked value="0"  class="contacts-radio" id="selDirRumbo"
                      />
                    <label for="selDirRumbo">Rumbo Universidad</label>
                  </span>
              </div>
              <div class="col-md-4 col-xs-4">
                  <span class="checkbox-custom checkbox-success checkbox-md">
                      <input type="radio" name="routeDirection" value="1" class="contacts-radio" id="selDirDesde"
                      />
                    <label for="selDirDesde">Desde la Universidad</label>
                  </span>
              </div>
              <div class="col-md-3 col-xs-6">
                  <div class="form-group">
                    <label class="label" id="label-special">Precio:</label>
                    <div class="input-group input-group-sm" id="DroutePrice">
                      <input class="form-control" placeholder="Valor" id="routePrice" name="routePrice" type="text" maxlength="7"  onclick="validatePrice()" onchange="validatePrice()"/>
                    </div>
                  </div>
              </div>
            </div>
            <br>
            <div class="row" style="margin-left: 10px; margin-right: 10px;">
              <div class="col-md-4 col-xs-12">
                  <div class="form-group form-material floating" data-plugin="formMaterial">
                    <input required type="text" class="form-control" id="routeName" name="routeName"  onclick="validateName()" onchange="validateName()"/>
                    <input id="routeNamehidden" type="hidden" value="0"/>
                    <label class="floating-label">Nombre de Ruta</label>
                  </div>
              </div>
              <div class="col-md-8 col-xs-12">
                  <div class="form-group">
                    <label class="floating-label">Avenidas Principales (separadas por coma)</label>
                    <div class="select2-success">
                    <select class="form-control" onchange="validate_rp();" required id="route_routeprincipalsId" name="route_routeprincipalsId[]" multiple="multiple" data-plugin="select2" size="3">
                    </select>
                    </div>
                  </div>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-md-4 col-xs-12">
                  <div class="example-wrap">
                    <h4 class="example-title">Añadir Paradas:</h4>
                    <p>( Clic en el mapa para ir añadiendo paradas )</p>
                    <ul id="listaParadas" class="" style="overflow-y:scroll;overflow-x:hidden; height:400px;">
                    </ul>
                  </div>
              </div>
              <div class="col-md-8 col-xs-12">
                  <div id="map_message"></div>
                  <div class="form-group">
                    <div class="input-group" style="float:left">
                      <input class="form-control" name="" placeholder="Ingrese la dirección a buscar" type="text" id="pac-input" style="width:60%">
                      <datalist id="pac-input-list"></datalist>
                    </div>
                  </div>
                  <div id="type-selector" class="controls">
                    <input type="radio" name="type" id="changetype-all" checked="checked">
                    <label for="changetype-all">All</label>

                    <input type="radio" name="type" id="changetype-establishment">
                    <label for="changetype-establishment">Establishments</label>

                    <input type="radio" name="type" id="changetype-address">
                    <label for="changetype-address">Addresses</label>

                    <input type="radio" name="type" id="changetype-geocode">
                    <label for="changetype-geocode">Geocodes</label>
                  </div>

                  <!-- <input id="pac-input" class="controls" type="text" placeholder="Search Box"> -->
                
                  <div id="map"></div>
              </div>
            </div>
            <div class="form-group col-xl-12 text-right padding-top-m">
                <button class="btn btn-success" type="button" id="crt_route" onclick="crt_routef();">Crear</button>
                <a class="btn btn-sm btn-white" onclick="close()" data-dismiss="modal" href="javascript:void(0)">Cancelar</a>
            </div>
          <div>
          </form>

        </div>
      </div>
    </div>
  </div>

  <!--- prueba -->
</div>
</div>
  <!-- End Add User Form -->

  <!-- edit User Form -->
  <div class="modal fade modal-3d-flip-vertical" id="editUserForm" aria-hidden="true" aria-labelledby="editUserForm" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Editar Ruta</h4>
        </div>
        <div class="modal-body">

          <div id="loaderEdit" style="display:none;">
              <center>
                  <br><br>
                   <img width="48px" src="{{ asset('iconbar/loading.gif') }}" /> &nbsp; Enviando ...
                  <br><br>
              </center>
          </div>

        <form id="dataEdit" method="put" action="{{ url('routes/put') }}">

          <div class="form-group">
              <div id="errorEdit">
              <div role="alert" id="lalertedit" class="alert dark alert-danger alert-dismissible">    <button aria-label="Close" data-dismiss="alert" class="close" type="button"> 
                  </button>
                </div>
              <!-- error will be shown here ! -->
              </div>
          </div>
          {{ csrf_field() }}

          <input type="hidden" id="routeIdEdit" name="routeId">

            <div class="row">
              <div class="col-md-3 col-xs-6">
                  <span class="checkbox-custom checkbox-success checkbox-md">
                      <input type="radio" name="routeDirection" value="0"  class="contacts-radio" id="selDirRumboEdit"
                      />
                    <label for="selDirRumboEdit">Rumbo Universidad</label>
                  </span>
              </div>
              <div class="col-md-3 col-xs-6">
                  <span class="checkbox-custom checkbox-success checkbox-md">
                      <input type="radio" name="routeDirection" value="1" class="contacts-radio" id="selDirDesdeEdit"
                      />
                    <label for="selDirDesdeEdit">Desde la Universidad</label>
                  </span>
              </div>
              <div class="col-md-3 col-xs-6">
                  <div class="form-group">
                    <label class="label" id="label-special">Precio:</label>
                    <div class="input-group input-group-sm" id="DroutePrice" name="DroutePrice">
                      <input class="form-control" placeholder="Valor" id="routePriceEdit"  name="routePrice" type="text" maxlength="7">
                    </div>
                  </div>
              </div>
              <div class="col-md-3 col-xs-6">
                  <div class="form-group">
                  <label class="label" id="label-special">Estado:</label>
                    <select required class="form-control-sm" id="routeStateEdit" name="routeState" data-plugin="select2">
                      <option value="0">Inactiva</option>
                      <option value="1">Activa</option>
                    </select>
                  </div>
              </div>
            </div>
            <br>
            <div class="row" style="margin-left: 10px; margin-right: 10px;">
              <div class="col-md-4 col-xs-12">
                  <div class="form-group form-material floating">
                    <input required type="text" class="form-control" id="routeNameEdit" name="routeName" onclick="validateNameEdit()" onchange="validateNameEdit()" />
                    <label class="floating-label">Nombre de Ruta</label>
                    <input id="routeNamehiddenedit" type="hidden" value="0"/>
                  </div>
              </div>
              <div class="col-md-8 col-xs-12">
                  <div class="form-group">
                    <label class="floating-label">Avenidas Principales (separadas por coma)</label>
                    <div class="select2-success">
                    <select required class="form-control" id="route_routeprincipalsIdEdit" name="route_routeprincipalsId[]" multiple="multiple" data-plugin="select2">
                    </select>
                    </div>
                  </div>
              </div>
            </div>
            <br>
             <div class="row">
              <div class="col-md-4 col-xs-12">
                  <div class="example-wrap">
                    <h4 class="example-title">Editar Paradas:</h4>
                    <p>( Clic en el mapa para ir añadiendo paradas )</p>
                    <ul id="listaParadasEdit" class="" style="overflow-y:scroll;overflow-x:hidden; height:400px;">
                    </ul>
                  </div>
              </div>
              <div class="col-md-8 col-xs-12">
                  <div id="mapedit_message"></div>
                  <div class="form-group">
                    <div class="input-group" style="float:left">
                      <input class="form-control" name="" placeholder="Ingrese la dirección a buscar" type="text" id="pac-inputEdit" style="width:60%">
                    </div>
                  </div>
                  <div id="type-selector-Edit" class="controls">
                    <input type="radio" name="type" id="changetype-all" checked="checked">
                    <label for="changetype-all">All</label>

                    <input type="radio" name="type" id="changetype-establishment">
                    <label for="changetype-establishment">Establishments</label>

                    <input type="radio" name="type" id="changetype-address">
                    <label for="changetype-address">Addresses</label>

                    <input type="radio" name="type" id="changetype-geocode">
                    <label for="changetype-geocode">Geocodes</label>
                  </div>
                  <!-- <input class="controls" type="text" placeholder="Search Box"> -->
                  <div id="mapEdit"></div>
              </div>
            </div>
            <div class="form-group col-xl-12 text-right padding-top-m">
                <button class="btn btn-success" id="edt_route" onclick="edt_route2()" type="button" >Guardar Cambios</button>
                <a class="btn btn-sm btn-white" data-dismiss="modal" href="javascript:void(0)">Cancelar</a>
            </div>

          </form>


        </div>
      </div>
    </div>
  </div>
  <!-- End edit User Form -->


  <!-- edit User Form -->
  <div class="modalAsig modal asignacion fade modal-3d-flip-vertical" id="asignacionForm" aria-hidden="true" aria-labelledby="asignacionForm" role="dialog" tabindex="-1" >
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Asignación de Conductores y Vehiculos a la Ruta</h4>
        </div>
        <div class="modal-body">

          <div id="loaderAssignment" style="display:none;">
              <center>
                  <br><br>
                   <img width="48px" src="{{ asset('iconbar/loading.gif') }}" /> &nbsp; Enviando ...
                  <br><br>
              </center>
          </div>

          <form id="datasaveAssignment" method="POST" action="{{ url('routes/saveAssignment') }}">

          <div class="form-group">
              <div id="errorAssignment">
              <!-- error will be shown here ! -->
              </div>
          </div>
          {{ csrf_field() }}

          <input type="hidden" name="routeassignmentFirst_routeId" id="routeassignmentFirst_routeId">
          <input type="hidden" name="routeassignmentActive" value="1">
            <div class="row" style="margin-left: 10px; margin-right: 10px;">
              <div class="col-md-4 col-xs-12">
                  <div class="form-group">
                    <label class="floating-label">Conductores: </label>
                    <div class="select2-success">
                    <select required class="form-control" id="routeassignment_userId" name="routeassignment_userId" data-plugin="select2">
                    </select>
                    </div>
                  </div>
              </div> 
              <div class="col-md-4 col-xs-12">
                  <div class="form-group">
                    <label class="floating-label">Vehiculos: </label>
                    <div class="select2-success">
                    <select required class="form-control" id="routeassignment_vehicleId" name="routeassignment_vehicleId" data-plugin="select2">
                    </select>
                    </div>
                  </div>
              </div>
              
              <div class="col-md-4 col-xs-12">
                  <div class="form-group">
                    <label class="floating-label">Rutas Vuelta: </label>
                    <div class="select2-success">
                    <select required class="form-control" id="routeassignmentLast_routeId" name="routeassignmentLast_routeId" data-plugin="select2">
                    </select>
                    </div>
                  </div>
              </div>
            </div>

            <br>

            <div class="form-group col-xl-12 text-right padding-top-m">
                <button class="btn btn-success" type="submit">Crear Asignación</button>
            </div>

          </form>
          



          <div id="loaderAsignados" style="display:none;">
              <center>
                  <br><br>
                   <img width="48px" src="{{ asset('iconbar/loading.gif') }}" /> &nbsp; Cargando ...
                  <br><br>
              </center>
          </div>

          <table class="table table-bordered table-hover table-striped" cellspacing="0" id="dataTable"><thead><tr><th>Conductor</th><th>Placa</th><th>Ruta Vuelta</th><th>Accion</th></tr></thead><tbody id="tableAsignados">
          </tbody></table>


        </div>
      </div>
    </div>
  </div>
  <iput type="hidden" id="clickMap" value="0"/>



  <!-- Modal -->
  


  <!-- End edit User Form -->

  <script type="text/javascript">

var service =''; 
//datalist
$("#pac-input").on('keyup', function (e) {
    if (e.keyCode == 13) {
        do_lalert(true,'Error! De click a una de las opciones de la lista del mapa');
    }else{
      do_lalert(false,'');
    }
});
$("#pac-inputEdit").on('keyup', function (e) {
    if (e.keyCode == 13) {
      do_lalertedit(true,'Error! De click a una de las opciones de la lista del mapa');
    }else{
      do_lalertedit(false,'');
    }
});



   if (window.location.host == 'localhost') {
            var baseurl = window.location.protocol +"//" + window.location.host + '/prontuz_backend/public';
        }else{
            var baseurl = window.location.protocol +"//" + window.location.host ;
        }
      var token = $("input[name*='_token']").val();

      function eliminarAsignacion(routeassignmentId){

          //enviar eliminar asignacion 
          $.ajax({
              type: "POST",
              url: baseurl+'/routes/destroyAssignment',
              data: {routeassignmentId: routeassignmentId, _token: token},
              dataType: 'JSON',
              beforeSend: function(){ 
                $("#tableAsignados").fadeOut();
                $("#loaderAsignados").fadeIn();
              },
              success: function (data) {
                  //buscar informacion de la ruta
                  var routeId = $('#routeassignmentFirst_routeId').val();
                  $.ajax({
                      type: "GET",
                      url: baseurl+'/routes/find',
                      data: {routeId: routeId},
                      dataType: 'JSON',
                      success: function (data) {
                          
                          
                          $.each(data.ResponseData,function(k, v){

                            routeDirection = v.routeDirection; 
                            $('#routeassignmentFirst_routeId').val(v.routeId); 

                            //consumir lista de conductores
                            $.ajax({
                                  type: "GET",
                                  url: baseurl+'/routes/bringDrivers',
                                  dataType: 'JSON',
                                  success: function (data) {

                                    var toAppend = '';
                                    $('#routeassignment_userId').empty().trigger("select2:updated");
                                    $('#routeassignment_userIdEdit').empty().trigger("select2:updated");
                                    $.each(data.ResponseData,function(k, v){
                                        //console.log(v.doctypeName);
                                        toAppend += '<option value="'+v.userId+'">'+v.userName+'</option>';
                                    });

                                     $('#routeassignment_userId').append(toAppend);
                                     $('#routeassignment_userId').trigger("select2:updated");

                                     $('#routeassignment_userIdEdit').append(toAppend);
                                     $('#routeassignment_userIdEdit').trigger("select2:updated");

                                  },
                                  error: function (data) {
                                      console.log('Error:', data);
                                  }
                            });

                            //consumir lista de vehiculos
                            $.ajax({
                                  type: "GET",
                                  url: baseurl+'/routes/bringVavailable',
                                  dataType: 'JSON',
                                  success: function (data) {

                                    var toAppend = '';
                                    $('#routeassignment_vehicleId').empty().trigger("select2:updated");
                                    $('#routeassignment_vehicleIdEdit').empty().trigger("select2:updated");
                                    $.each(data.ResponseData,function(k, v){
                                        //console.log(v.doctypeName);
                                        toAppend += '<option value="'+v.vehicleId+'">'+v.vehiclePlaque+'</option>';
                                    });

                                     $('#routeassignment_vehicleId').append(toAppend);
                                     $('#routeassignment_vehicleId').trigger("select2:updated");

                                     $('#routeassignment_vehicleIdEdit').append(toAppend);
                                     $('#routeassignment_vehicleIdEdit').trigger("select2:updated");

                                  },
                                  error: function (data) {
                                      console.log('Error:', data);
                                  }
                            });

                            //consumir lista de rutas de vuelta bringRlast
                            $.ajax({
                                  type: "GET",
                                  url: baseurl+'/routes/bringRlast',
                                  data: {routeId: routeId, routeDirection: routeDirection},
                                  dataType: 'JSON',
                                  success: function (data) {

                                    var toAppend = '';
                                    $('#routeassignmentLast_routeId').empty().trigger("select2:updated");
                                    $('#routeassignmentLast_routeIdEdit').empty().trigger("select2:updated");
                                    $.each(data.ResponseData,function(k, v){
                                        //console.log(v.doctypeName);
                                        toAppend += '<option value="'+v.routeId+'">'+v.routeName+'</option>';
                                    });

                                     $('#routeassignmentLast_routeId').append(toAppend);
                                     $('#routeassignmentLast_routeId').trigger("select2:updated");

                                     $('#routeassignmentLast_routeIdEdit').append(toAppend);
                                     $('#routeassignmentLast_routeIdEdit').trigger("select2:updated");

                                  },
                                  error: function (data) {
                                      console.log('Error:', data);
                                  }
                            });                      
                              
                              
                          });



                      },
                      error: function (data) {
                          console.log('Error:', data);
                      }
                  });
                  
                  //consumir lista de asignaciones
                  $.ajax({
                      type: "POST",
                      url: baseurl+'/routes/bringAssignment',
                      data: {routeId: routeId, _token: token},
                      dataType: 'JSON',
                      beforeSend: function(){ 
                        $("#tableAsignados").fadeOut();
                        $("#loaderAsignados").fadeIn();
                      },
                      success: function (data) {

                        var toAppend = '';
                        $('#tableAsignados').html('');
                        $("#loaderAsignados").fadeOut();

                        $.each(data.ResponseData,function(k, v){
                        

                            toAppend += '<tr class="gradeA"><td>'+v.userName+'</td><td>'+v.vehiclePlaque+'</td><td>'+v.routeName+'</td><td class="actions"><button onclick="eliminarAsignacion('+v.routeassignmentId+')" value="'+v.routeassignmentId+'" class="remove-row-eliminar btn btn-sm btn-icon btn-pure btn-default on-default remove-row" data-original-title="Remover"><i class="icon wb-trash" aria-hidden="true"></i></button></td></tr>';

                            
                        });

                     $('#tableAsignados').append(toAppend);
                     $("#tableAsignados").fadeIn(); 

                      },
                      error: function (data) {
                          console.log('Error:', data);
                      }
                  });
                   

              },
              error: function (data) {
                  console.log('Error:', data);
              }
          });       


        }


  </script>

  <script type="text/javascript">
    $(document).ready(function() {
      $('#lalert').hide();
      $('#lalertedit').hide();
      $('#onEnter').hide();
      tableName='route_list';
      urlName='/routes/listing';
      columnsName=[
              { data: 'route'},
              {
                data: 'routeState2',
                render: function (data){
                        if(data == 'Activo'){
                          return '<span class="badge badge-outline badge-success">'+data+'</span>';
                        }else{
                          return '<span class="badge badge-outline badge-danger">'+data+'</span>';
                        }
                        
                  }
                },
                {
                data: 'routeId',
                render: function (data){
                  
                  return '<button data-toggle="modal" value="'+data+'" data-target="#editUserForm" type="button" class="btnEditar btn btn-primary btn-sm">Editar</button> <button data-toggle="modal" value="'+data+'" data-target="#asignacionForm" type="button" class="btnAsignar btn btn-primary btn-sm">Asignar</button>';
                }
              },
            ];
        //var token = $("input[name*='_token']").val();
      list_datatable(tableName,urlName,columnsName);

      $("#data").trigger( "reset" );

  


        $("#addUserForm").on("shown.bs.modal", function () {
            google.maps.event.trigger(map, "resize");

            $('#pac-input').val('');
        });

        $("#editUserForm").on("shown.bs.modal", function () {
            google.maps.event.trigger(mapEdit, "resize");
            
            $('#pac-inputEdit').val('');
        });


        $('body').on('click', '[data-toggle="modal"]', function(){
            //console.log($(this).val());


            if ($(this).data("target") == '#editUserForm') {

              $('#listaParadasEdit').empty();
              deleteMarkers();
              var routeId = $(this).val();

                $.ajax({
                    type: "GET",
                    url: baseurl+'/routes/find',
                    data: {routeId: routeId},
                    dataType: 'JSON',
                    success: function (data) {
                        
                        // console.log(data.ResponseData);
                        // console.log(data.ResponseData[0]);
                        
                        $.each(data.ResponseData,function(k, v){
                            if (v.routeDirection == 0) {
                              $('#selDirRumboEdit').attr("checked", "checked");
                            }else{
                              $('#selDirDesdeEdit').attr("checked", "checked");
                            }
                            $('#routeNameEdit').val(v.routeName);
                            $('#routePriceEdit').val(v.routePrice);
                            $('#routeIdEdit').val(v.routeId);

                            $('#routeStateEdit').val(v.routeState).trigger('change');
                            
                            
                        });

                        var routesPrin = [];

                        $.each(data.ResponseData[0].route_routeprincipalsId,function(k, v){
                            routesPrin = routesPrin.concat(v.routeprincipalsId);
                        });

                        $('#route_routeprincipalsIdEdit').val(routesPrin).trigger('change');



                        $.each(data.ResponseData[0].localization,function(k, v){

                            ubicacion = {
                              lat: parseFloat(v.localizationLatitude),
                              lng: parseFloat(v.localizationLongitude)
                            }
                            editgeocodeLatLng(mapEdit, ubicacion, v.localizationId, v.localizationAddress);
                        });




                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });

            }else if($(this).data("target") == '#asignacionForm'){

              var routeId = $(this).val();
              var routeDirection = 0;

              //buscar informacion de la ruta
              $.ajax({
                    type: "GET",
                    url: baseurl+'/routes/find',
                    data: {routeId: routeId},
                    dataType: 'JSON',
                    success: function (data) {
                        
                        
                        $.each(data.ResponseData,function(k, v){

                          routeDirection = v.routeDirection; 
                          $('#routeassignmentFirst_routeId').val(v.routeId); 

                          //consumir lista de conductores
                          $.ajax({
                                type: "GET",
                                url: baseurl+'/routes/bringDrivers',
                                dataType: 'JSON',
                                success: function (data) {

                                  var toAppend = '';
                                  $('#routeassignment_userId').empty().trigger("select2:updated");
                                  $('#routeassignment_userIdEdit').empty().trigger("select2:updated");
                                  $.each(data.ResponseData,function(k, v){
                                      //console.log(v.doctypeName);
                                      toAppend += '<option value="'+v.userId+'">'+v.userName+'</option>';
                                  });

                                   $('#routeassignment_userId').append(toAppend);
                                   $('#routeassignment_userId').trigger("select2:updated");

                                   $('#routeassignment_userIdEdit').append(toAppend);
                                   $('#routeassignment_userIdEdit').trigger("select2:updated");

                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                          });

                          //consumir lista de vehiculos
                          $.ajax({
                                type: "GET",
                                url: baseurl+'/routes/bringVavailable',
                                dataType: 'JSON',
                                success: function (data) {

                                  var toAppend = '';
                                  $('#routeassignment_vehicleId').empty().trigger("select2:updated");
                                  $('#routeassignment_vehicleIdEdit').empty().trigger("select2:updated");
                                  $.each(data.ResponseData,function(k, v){
                                      //console.log(v.doctypeName);
                                      toAppend += '<option value="'+v.vehicleId+'">'+v.vehiclePlaque+'</option>';
                                  });

                                   $('#routeassignment_vehicleId').append(toAppend);
                                   $('#routeassignment_vehicleId').trigger("select2:updated");

                                   $('#routeassignment_vehicleIdEdit').append(toAppend);
                                   $('#routeassignment_vehicleIdEdit').trigger("select2:updated");

                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                          });

                          //consumir lista de rutas de vuelta bringRlast
                          $.ajax({
                                type: "GET",
                                url: baseurl+'/routes/bringRlast',
                                data: {routeId: routeId, routeDirection: routeDirection},
                                dataType: 'JSON',
                                success: function (data) {

                                  var toAppend = '';
                                  $('#routeassignmentLast_routeId').empty().trigger("select2:updated");
                                  $('#routeassignmentLast_routeIdEdit').empty().trigger("select2:updated");
                                  $.each(data.ResponseData,function(k, v){
                                      //console.log(v.doctypeName);
                                      toAppend += '<option value="'+v.routeId+'">'+v.routeName+'</option>';
                                  });

                                   $('#routeassignmentLast_routeId').append(toAppend);
                                   $('#routeassignmentLast_routeId').trigger("select2:updated");

                                   $('#routeassignmentLast_routeIdEdit').append(toAppend);
                                   $('#routeassignmentLast_routeIdEdit').trigger("select2:updated");

                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                          });                      
                            
                            
                        });



                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });

              //consumir lista de asignaciones
              //var routeId = $('#routeassignmentFirst_routeId').val();
              $.ajax({
                  type: "POST",
                  url: baseurl+'/routes/bringAssignment',
                  data: {routeId: routeId, _token: token},
                  dataType: 'JSON',
                  beforeSend: function(){ 
                    $("#tableAsignados").fadeOut();
                    $("#loaderAsignados").fadeIn();
                  },
                  success: function (data) {

                    var toAppend = '';
                    $('#tableAsignados').html('');
                    $("#loaderAsignados").fadeOut();

                    $.each(data.ResponseData,function(k, v){
                    

                    toAppend += '<tr class="gradeA"><td>'+v.userName+'</td><td>'+v.vehiclePlaque+'</td><td>'+v.routeName+'</td><td class="actions"><button onclick="eliminarAsignacion('+v.routeassignmentId+')" value="'+v.routeassignmentId+'" class="remove-row-eliminar btn btn-sm btn-icon btn-pure btn-default on-default remove-row" data-original-title="Remover"><i class="icon wb-trash" aria-hidden="true"></i></button></td></tr>';

                    
                });

                 $('#tableAsignados').append(toAppend);
                 $("#tableAsignados").fadeIn();

                   
 

                  },
                  error: function (data) {
                      console.log('Error:', data);
                  }
              });

              

            }

            
        });

   
        //enviar data registro
        $("form#data").submit(function() {
            var formData = new FormData($(this)[0]);

            $.ajax({
                type: $(this).attr("method"),
                url: $(this).attr("action"),
                data: $(this).serialize(),
                dataType: 'JSON',
                beforeSend: function(){ 
                  $("#error").fadeOut();
                  $("#data").fadeOut();
                  $("#loader").fadeIn();
                },
                success: function (response) {
                  $("#loader").fadeOut();
                  if(response.ResponseCode == 0){

                    $("#data").fadeIn();
                    $("#error").fadeIn(1000, function(){      
                    $("#error").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> '+response.ResponseMessage+'</div>');
                       });
                  
                  }else{

                    setTimeout(' window.location.href = "'+baseurl+'/routes"; ',2000);                     
                    
                  }

                },
                error: function (response) {
                    $("#error").fadeIn(1000, function(){      
                    $("#error").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> Error de comunicación, revise su conexion a internet.</div>');
                    });
                }
              });

            return false;
        });

        //enviar data editar
        // $("form#dataEdit").submit(function() {
        //     var formData = new FormData($(this)[0]);

        //     $.ajax({
        //         type: 'PUT',
        //         url: $(this).attr("action"),
        //         data: $(this).serialize(),
        //         dataType: 'JSON',
        //         beforeSend: function(){ 
        //           $("#errorEdit").fadeOut();
        //           $("#dataEdit").fadeOut();
        //           $("#loaderEdit").fadeIn();
        //         },
        //         success: function (response) {
        //           $("#loaderEdit").fadeOut();
        //           if(response.ResponseCode == 0){

        //             $("#dataEdit").fadeIn();
        //             $("#errorEdit").fadeIn(1000, function(){      
        //             $("#errorEdit").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> '+response.ResponseMessage+'</div>');
        //                });
                  
        //           }else{

        //             setTimeout(' window.location.href = "'+baseurl+'/routes"; ',2000);                     
                    
        //           }

        //         },
        //         error: function (response) {
        //             $("#errorEdit").fadeIn(1000, function(){      
        //             $("#errorEdit").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> Error de comunicación, revise su conexion a internet.</div>');
        //             });
        //         }
        //       });

        //     return false;
        // });

        //enviar data registro asignacion
        $("form#datasaveAssignment").submit(function() {

            $.ajax({
                type: $(this).attr("method"),
                url: $(this).attr("action"),
                data: $(this).serialize(),
                dataType: 'JSON',
                beforeSend: function(){ 
                  $("#errorAssignment").fadeOut();
                  $("#datasaveAssignment").fadeOut();
                  $("#loaderAssignment").fadeIn();
                },
                success: function (response) {
                  $("#loaderAssignment").fadeOut();

                  $("#datasaveAssignment").fadeIn();
                  if(response.ResponseCode == 0){

                    $("#datasaveAssignment").fadeIn();
                    $("#errorAssignment").fadeIn(1000, function(){      
                    $("#errorAssignment").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> '+response.ResponseMessage+'</div>');
                       });
                  
                  }else{



                    //buscar informacion de la ruta
                    var routeId = $('#routeassignmentFirst_routeId').val();
              $.ajax({
                    type: "GET",
                    url: baseurl+'/routes/find',
                    data: {routeId: routeId},
                    dataType: 'JSON',
                    success: function (data) {
                        
                        
                        $.each(data.ResponseData,function(k, v){

                          routeDirection = v.routeDirection; 
                          $('#routeassignmentFirst_routeId').val(v.routeId); 

                          //consumir lista de conductores
                          $.ajax({
                                type: "GET",
                                url: baseurl+'/routes/bringDrivers',
                                dataType: 'JSON',
                                success: function (data) {

                                  var toAppend = '';
                                  $('#routeassignment_userId').empty().trigger("select2:updated");
                                  $('#routeassignment_userIdEdit').empty().trigger("select2:updated");
                                  $.each(data.ResponseData,function(k, v){
                                      //console.log(v.doctypeName);
                                      toAppend += '<option value="'+v.userId+'">'+v.userName+'</option>';
                                  });

                                   $('#routeassignment_userId').append(toAppend);
                                   $('#routeassignment_userId').trigger("select2:updated");

                                   $('#routeassignment_userIdEdit').append(toAppend);
                                   $('#routeassignment_userIdEdit').trigger("select2:updated");

                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                          });

                          //consumir lista de vehiculos
                          $.ajax({
                                type: "GET",
                                url: baseurl+'/routes/bringVavailable',
                                dataType: 'JSON',
                                success: function (data) {

                                  var toAppend = '';
                                  $('#routeassignment_vehicleId').empty().trigger("select2:updated");
                                  $('#routeassignment_vehicleIdEdit').empty().trigger("select2:updated");
                                  $.each(data.ResponseData,function(k, v){
                                      //console.log(v.doctypeName);
                                      toAppend += '<option value="'+v.vehicleId+'">'+v.vehiclePlaque+'</option>';
                                  });

                                   $('#routeassignment_vehicleId').append(toAppend);
                                   $('#routeassignment_vehicleId').trigger("select2:updated");

                                   $('#routeassignment_vehicleIdEdit').append(toAppend);
                                   $('#routeassignment_vehicleIdEdit').trigger("select2:updated");

                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                          });

                          //consumir lista de rutas de vuelta bringRlast
                          $.ajax({
                                type: "GET",
                                url: baseurl+'/routes/bringRlast',
                                data: {routeId: routeId, routeDirection: routeDirection},
                                dataType: 'JSON',
                                success: function (data) {

                                  var toAppend = '';
                                  $('#routeassignmentLast_routeId').empty().trigger("select2:updated");
                                  $('#routeassignmentLast_routeIdEdit').empty().trigger("select2:updated");
                                  $.each(data.ResponseData,function(k, v){
                                      //console.log(v.doctypeName);
                                      toAppend += '<option value="'+v.routeId+'">'+v.routeName+'</option>';
                                  });

                                   $('#routeassignmentLast_routeId').append(toAppend);
                                   $('#routeassignmentLast_routeId').trigger("select2:updated");

                                   $('#routeassignmentLast_routeIdEdit').append(toAppend);
                                   $('#routeassignmentLast_routeIdEdit').trigger("select2:updated");

                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                          });                      
                            
                            
                        });



                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });

              //consumir lista de asignaciones
              
              $.ajax({
                  type: "POST",
                  url: baseurl+'/routes/bringAssignment',
                  data: {routeId: routeId, _token: token},
                  dataType: 'JSON',
                  beforeSend: function(){ 
                    $("#tableAsignados").fadeOut();
                    $("#loaderAsignados").fadeIn();
                  },
                  success: function (data) {

                    var toAppend = '';
                    $('#tableAsignados').html('');
                    $("#loaderAsignados").fadeOut();

                    $.each(data.ResponseData,function(k, v){
                    

                    toAppend += '<tr class="gradeA"><td>'+v.userName+'</td><td>'+v.vehiclePlaque+'</td><td>'+v.routeName+'</td><td class="actions"><button onclick="eliminarAsignacion('+v.routeassignmentId+')" value="'+v.routeassignmentId+'" class="remove-row-eliminar btn btn-sm btn-icon btn-pure btn-default on-default remove-row" data-original-title="Remover"><i class="icon wb-trash" aria-hidden="true"></i></button></td></tr>';

                    
                });

                 $('#tableAsignados').append(toAppend);
                 $("#tableAsignados").fadeIn();

                   
 

                  },
                  error: function (data) {
                      console.log('Error:', data);
                  }
              });

                     
                 
                    
                  }

                },
                error: function (response) {
                    $("#errorAssignment").fadeIn(1000, function(){      
                    $("#errorAssignment").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> Error de comunicación, revise su conexion a internet.</div>');
                    });
                }
              });

            return false;
        });
        
        //av principales
        $.ajax({
            type: "GET",
            url: baseurl+'/routes/bringRprincipals',
            dataType: 'JSON',
            success: function (data) {
                

                var toAppend = '';
                $.each(data.ResponseData,function(k, v){
                    //console.log(v.doctypeName);
                    toAppend += '<option value="'+v+'">'+k+'</option>';
                });

                 $('#route_routeprincipalsId').append(toAppend);
                 $('#route_routeprincipalsId').trigger("select2:updated");

                 $('#route_routeprincipalsIdEdit').append(toAppend);
                 $('#route_routeprincipalsIdEdit').trigger("select2:updated");

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
        
        

        $("#listaRoutes").html('');
          $.ajax({
            type: "GET",
            url: baseurl+'/routes/find',
            dataType: 'JSON',
            beforeSend: function(){ 
              $("#listaRoutes").fadeOut();
              $("#loaderlist").fadeIn();
            },
            success: function (data) {
                
                $("#loaderlist").fadeOut();
                var toAppend = '';
                $.each(data.ResponseData,function(k, v){
                    //console.log(v.userEmail);
                    var rutasprincipales = '';
                    var i = 0;
                    $.each(v.route_routeprincipalsId,function(kl, va){
                      if (i == 0) {
                        rutasprincipales = va.routeprincipalsName;
                      }else{
                        rutasprincipales = rutasprincipales+' - '+va.routeprincipalsName;
                      }
                      i++;
                    });

                    var direccion = '';
                        if (v.routeDirection == 0) {
                          direccion = 'Rumbo Universidad';
                        }else{
                          direccion = 'Desde Universidad';
                        }

                    toAppend += '<li class="list-group-item" > <div class="media"> <div class="media-body"> <h4 class="mt-0 mb-5"> '+v.routeName+' <small>'+direccion+'</small> </h4><span class="text-green">'+rutasprincipales+'</span> <br>Paradas: <span class="text-orange">'+v.numLocations+' Paradas </span> <br> Asignados: <spam class="text-orange"> '+v.numvehicle+' Vehiculos </spam> </p></div><div class="pl-20 align-self-center"> <button data-toggle="modal" value="'+v.routeId+'" data-target="#editUserForm" type="button" class="btnEditar btn btn-primary btn-sm">Editar</button> <button data-toggle="modal" value="'+v.routeId+'" data-target="#asignacionForm" type="button" class="btnAsignar btn btn-primary btn-sm">Asignar</button> </div></div></li>';

                    
                });

                 $('#listaRoutes').append(toAppend);
                 $("#listaRoutes").fadeIn();
                 

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });


        //validar name route
        // $("#routeName").focusout(function() {
        //     var value = $(this).val().toUpperCase();
        //     $(this).val(value);
        //     if (value != '') {
        //         $.ajax({
        //           type: "POST",
        //           url: baseurl+'/routes/validateRname',
        //           data: {routeName: value, _token: token},
        //           dataType: 'JSON',
        //           success: function (response) {
                      
        //               if (response.ResponseCode == 0) {
        //                 $("#routeName").val('');
        //                 $("#error").fadeIn(1000, function(){      
        //                 $("#error").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button>'+response.ResponseMessage+'</div>');
        //                 });
        //                 $('#crt_route').attr('disabled','disabled');

        //               }else{
        //                 $("#error").fadeOut();
        //                 $("#error").html('');
        //                 enable_map();
        //               }

        //           },
        //           error: function (data) {
        //               console.log('Error:', data);
        //           }
        //         });
        //     }

        // });






    });
  </script>

  <script>
    

    if (window.location.host == 'localhost') {
        var baseurl = window.location.protocol +"//" + window.location.host + '/prontuz_backend/public';
    }else{
        var baseurl = window.location.protocol +"//" + window.location.host ;
    }

    var map;
    var mapEdit;
    var image = baseurl+'/parada.png';
    var toAppendPar = '';
    var markers = [];
    var uniqueId = 1;

   

    function editgeocodeLatLng(map, ubicacion, localizationId, localizationAddress) {
        //console.log(localizationAddress);

        var infowindow = new google.maps.InfoWindow;

        var dir = localizationAddress;

        if (localizationId == null) {
          localizationId = '';
        }

        var marker = new google.maps.Marker({
                  map: map,
                  icon: image,
                  position: ubicacion,
                  mapTypeId: 'terrain'
              });


              //Set unique id
              marker.id = uniqueId;
              uniqueId++;

              marker.addListener("click", function (e) {
                  var content = 'Parada: ' + dir;
                  content += "<br/><center><input class='btn btn-success btn-small' type = 'button' onclick = 'DeleteMarkerEdit(" + marker.id + ");' value = 'Eliminar' /></center>";

                  var infoWindow = new google.maps.InfoWindow({
                      content: content
                  });
                  infoWindow.open(map, marker);
              });
   
              //Add marker to the array.
              markers.push(marker);

              map.setZoom(15);
              map.panTo(marker.getPosition());

               toAppendPar = '<li id="editlistN'+marker.id+'" class="list-group-item row" style="height:60px;padding:0px"><div class="col-md-2 col-xs-2"><a href="javascript:void(0)"  class="rt_lst_success_a" ><i onclick = "DeleteMarkerEdit(' + marker.id + ');" class="icon wb-trash" aria-hidden="true"></i></a></div><div class="col-md-10 col-xs-10"><div class="form-group form-material floating">'
                  +'<input ldata="'+dir+'*'+ubicacion.lat+'*'+ubicacion.lng+'*'+marker.id+'" onfocusout="validateAddress(this)" type="text" class="form-control rt_lst_success" required  id="editlocalizationAddress'+marker.id+'" name="localization_localizationId[' + marker.id + '][localizationAddress] " value="'+ dir +'"></div></div></li>'
                  +'<input type="hidden" id="editlocalizationLatitude'+marker.id+'" name="localization_localizationId[' + marker.id + '][localizationLatitude] " value="'+ ubicacion.lat +'">'
                  +'<input type="hidden" id="editlocalizationLongitude'+marker.id+'" name="localization_localizationId[' + marker.id + '][localizationLongitude] " value="'+ ubicacion.lng +'">'
                  +'<input type="hidden" id="editlocalizationId'+marker.id+'" name="localization_localizationId[' + marker.id + '][localizationId] " value="'+ localizationId +'">';


              // toAppendPar = '<li id="editlistN'+marker.id+'" class="list-group-item row" style="height:60px;padding:0px"><div class="col-md-2 col-xs-2"><a href="javascript:void(0)"  ><i onclick = "DeleteMarkerEdit(' + marker.id + ');" class="icon wb-trash" aria-hidden="true"></i></a></div><div class="col-md-10 col-xs-10">'+dir+'</div></li><input type="hidden" id="editlocalizationId'+marker.id+'" name="localization_localizationId[' + marker.id + '][localizationId] " value="'+ localizationId +'"><input type="hidden" id="editlocalizationLatitude'+marker.id+'" name="localization_localizationId[' + marker.id + '][localizationLatitude] " value="'+ ubicacion.lat +'"><input type="hidden" id="editlocalizationLongitude'+marker.id+'" name="localization_localizationId[' + marker.id + '][localizationLongitude] " value="'+ ubicacion.lng +'"><input type="hidden"  id="editlocalizationAddress'+marker.id+'" name="localization_localizationId[' + marker.id + '][localizationAddress] " value="'+ dir +'">';

              //$('#listaParadasEdit').append(toAppendPar);
              $('#listaParadasEdit').append(toAppendPar);

        
    };


    function DeleteMarker(id) {

      // console.log('here');

      // console.log(markers);
        //Find and remove the marker from the Array
        for (var i = 0; i < markers.length; i++) {
            if (markers[i].id == id) {
                //Remove the marker from Map                  
                markers[i].setMap(null);
                document.getElementById("listN"+id).remove();
                
                document.getElementById("localizationLatitude"+id).remove();
                document.getElementById("localizationLongitude"+id).remove();
                // document.getElementById("localizationAddress"+id).remove();
 
                //Remove the marker from array.
                markers.splice(i, 1);
                return;
            }
        }
    };

    function DeleteMarkerEdit(id) {
        //Find and remove the marker from the Array
        for (var i = 0; i < markers.length; i++) {
            if (markers[i].id == id) {
                //Remove the marker from Map                  
                markers[i].setMap(null);
                document.getElementById("editlistN"+id).remove();
                document.getElementById("editlocalizationId"+id).remove();
                document.getElementById("editlocalizationLatitude"+id).remove();
                document.getElementById("editlocalizationLongitude"+id).remove();
                //document.getElementById("editlocalizationAddress"+id).remove();
 
                //Remove the marker from array.
                markers.splice(i, 1);
                return;
            }
        }
    };


    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
      for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
      }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
      setMapOnAll(null);
    }

    // Shows any markers currently in the array.
    function showMarkers() {
      setMapOnAll(map);
    }

    // Deletes all markers in the array by removing references to them.
    function deleteMarkers() {
      clearMarkers();
      markers = [];
      uniqueId = 1;
    }


    
    $(window).load(function(){

    // Create the search box and link it to the UI element.
       


    function initMap() {
      map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 4.6880573, lng: -74.0664687},
        zoom: 18,
        mapTypeId: 'terrain'
      });

      mapEdit = new google.maps.Map(document.getElementById('mapEdit'), {
        center: {lat: 4.6880573, lng: -74.0664687},
        zoom: 15,
        mapTypeId: 'terrain'
      });


      ////////

        var input = document.getElementById('pac-input');
        var types = document.getElementById('type-selector');

        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);


        
        var autocomplete = new google.maps.places.Autocomplete(input);

        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();

          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            //window.alert("No details available for input: '" + place.name + "'");
            return;
          }else{
            do_lalert(false,'');
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setIcon(/** @type {google.maps.Icon} */({
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
          }));
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }
        });

        // Sets a listener on a radio button to change the filter type on Places
        // Autocomplete.
        function setupClickListener(id, types) {
          var radioButton = document.getElementById(id);
          radioButton.addEventListener('click', function() {
            autocomplete.setTypes(types);
          });
        }

        setupClickListener('changetype-all', []);
        setupClickListener('changetype-address', ['address']);
        setupClickListener('changetype-establishment', ['establishment']);
        setupClickListener('changetype-geocode', ['geocode']);
      


      ////////

        var input = document.getElementById('pac-inputEdit');
        var types = document.getElementById('type-selector-Edit');

        var searchBoxEdit = new google.maps.places.SearchBox(input);
        mapEdit.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        mapEdit.controls[google.maps.ControlPosition.TOP_LEFT].push(types);


        
        var autocompleteEdit = new google.maps.places.Autocomplete(input);

        autocompleteEdit.bindTo('bounds', mapEdit);

        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
          map: mapEdit,
          anchorPoint: new google.maps.Point(0, -29)
        });

        autocompleteEdit.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocompleteEdit.getPlace();

          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            //window.alert("No details available for input: '" + place.name + "'");
            return;
          }else{
            do_lalertedit(false,'');
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            mapEdit.fitBounds(place.geometry.viewport);
          } else {
            mapEdit.setCenter(place.geometry.location);
            mapEdit.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setIcon(/** @type {google.maps.Icon} */({
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
          }));
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }
        });

        function setupClickListener(id, types) {
          var radioButton = document.getElementById(id);
          radioButton.addEventListener('click', function() {
            autocompleteEdit.setTypes(types);
          });
        }

        setupClickListener('changetype-all', []);
        setupClickListener('changetype-address', ['address']);
        setupClickListener('changetype-establishment', ['establishment']);
        setupClickListener('changetype-geocode', ['geocode']);
      
      ////////


      var geocoder = new google.maps.Geocoder;
      var infowindow = new google.maps.InfoWindow;

      //add
      map.addListener('click', function(e) {

          //var clickMap=$('#clickMap').val();

          disabled_map();

          ubicacion = {
            lat: e.latLng.lat(),
            lng: e.latLng.lng()
          }
          
          //geocodeLatLng(map, ubicacion);

          geocoder.geocode({'location': ubicacion}, function(results, status) {

            // console.log(results);
            
            if (status === 'OK') {
              if (results[1]) {


                // var dir = results[1].address_components[0].long_name+', '+results[1].address_components[1].long_name;
                var dir = results[0].address_components[0].long_name+', '+results[0].address_components[1].long_name;


                 var token = $("input[name*='_token']").val();

                 var marker = new google.maps.Marker({
                    map: map,
                    icon: image,
                    position: ubicacion,
                    mapTypeId: 'terrain'
                });


                //Set unique id
                marker.id = uniqueId;
                uniqueId++;

                marker.addListener("click", function (e) {
                    var content = 'Parada: ' + dir;
                    content += "<br/><center><input class='btn btn-success btn-small' type = 'button' onclick = 'DeleteMarker(" + marker.id + ");' value = 'Eliminar' /></center>";

                    var infoWindow = new google.maps.InfoWindow({
                        content: content
                    });
                    infoWindow.open(map, marker);
                });
     
                //Add marker to the array.
                markers.push(marker);

                var content = 'Parada: ' + dir;
                    content += "<br/><center><input class='btn btn-success btn-small' type = 'button' onclick = 'DeleteMarker(" + marker.id + ");' value = 'Eliminar' /></center>";


                map.setZoom(18);
                map.panTo(marker.getPosition());
                infowindow.setContent(content);
                infowindow.open(map, marker);

            
                 toAppendPar = '<li id="listN'+marker.id+'" class="list-group-item row" style="height:60px;padding:0px"><div class="col-md-2 col-xs-2"><a href="javascript:void(0)"  class="rt_lst_success_a" ><i onclick = "DeleteMarker(' + marker.id + ');" class="icon wb-trash" aria-hidden="true"></i></a></div><div class="col-md-10 col-xs-10"><div class="form-group form-material floating">'
                  +'<input ldata="'+dir+'*'+ubicacion.lat+'*'+ubicacion.lng+'" poison="validatelocalization(this)" type="text" class="form-control rt_lst_success" required  id="localizationAddress'+marker.id+'" name="localization_localizationId[' + marker.id + '][localizationAddress] " value="'+ dir +'"></div></div></li>'
                  +'<input type="hidden" id="localizationLatitude'+marker.id+'" name="localization_localizationId[' + marker.id + '][localizationLatitude] " value="'+ ubicacion.lat +'">'
                  +'<input type="hidden" id="localizationLongitude'+marker.id+'" name="localization_localizationId[' + marker.id + '][localizationLongitude] " value="'+ ubicacion.lng +'">';

                  validate_nl();

                  $.ajax({
                      type: "POST",
                      async:false,
                      url: baseurl+'/routes/validatelocalization',
                      data: {localizationLatitude: ubicacion.lat, localizationLongitude: ubicacion.lng, _token: token},
                      dataType: 'JSON',
                      success: function (data) {

                          
                          if (data.ResponseCode == 1) {

                              /* (OJO) Se omite temporalmente validacion de direccion */

                              // $.ajax({
                              //   type: "POST",
                              //   async:false,
                              //   url: baseurl+'/routes/validatelocalization',
                              //   data: {localizationAddress: dir, _token: token},
                              //   dataType: 'JSON',
                              //   success: function (data) {
                                    
                              //       if (data.ResponseCode == 0) {

                              //         $("#error").fadeIn(1000, function(){  

                              //           do_lalert(true,data.ResponseMessage+', puede editarla en la lista y colocarle un nombre personalisado !!!');

                              //           // $("#error").html('<div role="alert" id="lalert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> '+data.ResponseMessage+', puede editarla en la lista y colocarle un nombre personalisado !!!</div>');
                              //         });

                              //         toAppendPar = '<li id="listN'+marker.id+'" class="list-group-item row" style="height:60px;padding:0px" ><div class="col-md-2 col-xs-2"><a href="javascript:void(0)"  class="rt_lst_error_a" ><i onclick = "DeleteMarker(' + marker.id + ');" class="icon wb-trash" aria-hidden="true"></i></a></div><div class="col-md-10 col-xs-10"><div class="form-group form-material floating">'
                              //         +'<input ldata="'+dir+'*'+ubicacion.lat+'*'+ubicacion.lng+'" poison="validatelocalization(this)" type="text" class="form-control rt_lst_error" required  id="localizationAddress'+marker.id+'" name="localization_localizationId[' + marker.id + '][localizationAddress] " value="'+ dir +'"></div></div></li>'
                              //         +'<input type="hidden" id="localizationLatitude'+marker.id+'" name="localization_localizationId[' + marker.id + '][localizationLatitude] " value="'+ ubicacion.lat +'">'
                              //         +'<input type="hidden" id="localizationLongitude'+marker.id+'" name="localization_localizationId[' + marker.id + '][localizationLongitude] " value="'+ ubicacion.lng +'">';


                              //        // $('#listaParadas').append(toAppendPar);
                              //          enable_map(false);
                              //       }else{
                                       enable_map(true);
                                    //  // $('#listaParadas').append(toAppendPar);

                                    // }
                                    print_list(toAppendPar);
                                    
                                   


                              //   },
                              //   error: function (data) {
                              //       alert('Error: compruebe su conexion a internet');
                              //   }
                              // });

                          }else{

                            $('#listaParadas').append(toAppendPar);
                            enable_map(true);

                          }
                          

                      },
                      error: function (data) {
                          console.log('Error:', data);
                      }
                  });


                

                


              } else {
                window.alert('No results found');
              }
            } else {
              window.alert('Error de comunicación, intentelo de nuevo mas tarde');
            }
          });

          
      });

      
      //edit
      mapEdit.addListener('click', function(e) {
          ubicacion = {
            lat: e.latLng.lat(),
            lng: e.latLng.lng()
          }

           disabled_mapedit();
          
          //editgeocodeLatLng(mapEdit, ubicacion, null);

          geocoder.geocode({'location': ubicacion}, function(results, status) {
            if (status === 'OK') {
              if (results[1]) {


                var dir = results[0].address_components[0].long_name+', '+results[0].address_components[1].long_name;


                 var token = $("input[name*='_token']").val();

                 var marker = new google.maps.Marker({
                    map: mapEdit,
                    icon: image,
                    position: ubicacion,
                    mapTypeId: 'terrain'
                });


                //Set unique id
                marker.id = uniqueId;
                uniqueId++;

                marker.addListener("click", function (e) {
                    alert('here');
                    var content = 'Parada: ' + dir;
                    content += "<br/><center><input class='btn btn-success btn-small' type = 'button' onclick = 'DeleteMarkerEdit(" + marker.id + ");' value = 'Eliminar' /></center>";

                    var infoWindow = new google.maps.InfoWindow({
                        content: content
                    });
                    infoWindow.open(mapEdit, marker);
                });
     
                //Add marker to the array.
                markers.push(marker);

                var content = 'Parada: ' + dir;
                    content += "<br/><center><input class='btn btn-success btn-small' type = 'button' onclick = 'DeleteMarkerEdit(" + marker.id + ");' value = 'Eliminar' /></center>";


                map.setZoom(18);
                map.panTo(marker.getPosition());
                infowindow.setContent(content);
                infowindow.open(mapEdit, marker);

          
                 toAppendPar = '<li id="editlistN'+marker.id+'" class="list-group-item row" style="height:60px;padding:0px"><div class="col-md-2 col-xs-2"><a href="javascript:void(0)"  class="rt_lst_success_a" ><i onclick = "DeleteMarkerEdit(' + marker.id + ');" class="icon wb-trash" aria-hidden="true"></i></a></div><div class="col-md-10 col-xs-10"><div class="form-group form-material floating">'
                  +'<input ldata="'+dir+'*'+ubicacion.lat+'*'+ubicacion.lng+'*'+marker.id+'" poison="validatelocalization(this)" type="text" class="form-control rt_lst_success" required  id="localizationAddress'+marker.id+'" name="localization_localizationId[' + marker.id + '][localizationAddress] " value="'+ dir +'"></div></div></li>'
                  +'<input type="hidden" id="editlocalizationLatitude'+marker.id+'" name="localization_localizationId[' + marker.id + '][localizationLatitude] " value="'+ ubicacion.lat +'">'
                  +'<input type="hidden" id="editlocalizationLongitude'+marker.id+'" name="localization_localizationId[' + marker.id + '][localizationLongitude] " value="'+ ubicacion.lng +'">'
                  //+'<input type="hidden" id="editlocalizationId'+marker.id+'">';
                   +'<input type="hidden" id="editlocalizationId'+marker.id+'" name="localization_localizationId[' + marker.id + '][localizationId] " value=" ">'

                  $.ajax({
                      type: "POST",
                      url: baseurl+'/routes/validatelocalization',
                      data: {localizationLatitude: ubicacion.lat, localizationLongitude: ubicacion.lng, _token: token},
                      dataType: 'JSON',
                      success: function (data) {
                          
                          if (data.ResponseCode == 1) {

                              /* (OJO) Se omite temporalmente validacion de direccion */

                              // $.ajax({
                              //   type: "POST",
                              //   url: baseurl+'/routes/validatelocalization',
                              //   data: {localizationAddress: dir, _token: token},
                              //   dataType: 'JSON',
                              //   success: function (data) {
                                    
                              //       if (data.ResponseCode == 0) {

                              //         do_lalertedit(true,data.ResponseMessage+', puede editarla en la lista y colocarle un nombre personalisado !!!');

                              //         $("#errorEdit").fadeIn(1000, function(){      
                              //         $("#errorEdit").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> '+data.ResponseMessage+', puede editarla en la lista y colocarle un nombre personalisado !!!</div>');
                              //            });
                              //          toAppendPar = '<li id="editlistN'+marker.id+'" class="list-group-item row" style="height:60px;padding:0px"><div class="col-md-2 col-xs-2"><a href="javascript:void(0)"  class="rt_lst_error_a" ><i onclick = "DeleteMarkerEdit(' + marker.id + ');" class="icon wb-trash" aria-hidden="true"></i></a></div><div class="col-md-10 col-xs-10"><div class="form-group form-material floating">'
                              //           +'<input ldata="'+dir+'*'+ubicacion.lat+'*'+ubicacion.lng+'*'+ marker.id +'" poison="validatelocalization(this)" type="text" class="form-control rt_lst_error" required  id="localizationAddress'+marker.id+'" name="localization_localizationId[' + marker.id + '][localizationAddress] " value="'+ dir +'"></div></div></li>'
                              //           +'<input type="hidden" id="editlocalizationLatitude'+marker.id+'" name="localization_localizationId[' + marker.id + '][localizationLatitude] " value="'+ ubicacion.lat +'">'
                              //           +'<input type="hidden" id="editlocalizationLongitude'+marker.id+'" name="localization_localizationId[' + marker.id + '][localizationLongitude] " value="'+ ubicacion.lng +'">'
                              //           //+'<input type="hidden" id="editlocalizationId'+marker.id+'">';
                              //            +'<input type="hidden" id="editlocalizationId'+marker.id+'" name="localization_localizationId[' + marker.id + '][localizationId] " value=" ">'



                              //         $('#listaParadasEdit').append(toAppendPar);

                              //         disabled_mapedit();

                              //       }else{

                                      $('#listaParadasEdit').append(toAppendPar);
                                      do_lalertedit(false,'');
                                      enable_mapedit(true);
                              //       }
                                    

                              //   },
                              //   error: function (data) {
                              //       alert('Error: compruebe su conexion a internet');
                              //   }
                              // });

                          }else{

                            $('#listaParadasEdit').append(toAppendPar);

                          }
                          

                      },
                      error: function (data) {
                          console.log('Error:', data);
                      }
                  });


              } else {
                window.alert('No results found');
              }
            } else {
              window.alert('Error de comunicación, intentelo de nuevo mas tarde');
            }
          });

      });

    };



    initMap();

    });

  /***************************************/

  function edt_route2(){

    $('#edt_route').attr('type','submit');

    $("form#dataEdit").submit(function() {
        var formData = new FormData($(this)[0]);
        //console.log($(this).serialize());

        $.ajax({
            type: 'PUT',
            url: $(this).attr("action"),
            data: $(this).serialize(),
            dataType: 'JSON',
            beforeSend: function(){ 
              $("#errorEdit").fadeOut();
              $("#dataEdit").fadeOut();
              $("#loaderEdit").fadeIn();
            },
            success: function (response) {
              $("#loaderEdit").fadeOut();
              if(response.ResponseCode == 0){

                $("#dataEdit").fadeIn();
                $("#errorEdit").fadeIn(1000, function(){      
                $("#errorEdit").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> '+response.ResponseMessage+'</div>');
                   });
              
              }else{

                setTimeout(function(){

                  $("#editUserForm").html('<div role="alert" class="alert dark alert-success alert-dismissible" style="z-index: 20;"> <button style="width:120px;" aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button>Se ha editado la ruta exitosamente</div>');

                    setTimeout(' window.location.href = "'+baseurl+'/routes"; ',2000);
                },2000);
                                     
                
              }

            },
            error: function (response) {
                $("#errorEdit").fadeIn(1000, function(){      
                $("#errorEdit").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> Error de comunicación, revise su conexion a internet.</div>');
                });
            }
          });

        return false;
    });

  }

  /***************************************/

  function enable_map(op){
    
    if(op == true){
       $('#map').removeClass('disableddiv');
    }
   
    $('#map').show();
    $('#map_message').html('');
    $('#crt_route').attr('disabled',false);
  }
  /***************************************/

  function disabled_map(){
    $('#map').addClass('disableddiv');
    $('#map').hide();
    var img="{{ asset( 'iconbar/loading.gif ') }}";
    $('#map_message').html('<center><img width="35px" src="'+img+'" /><br>Cargando configuración de la parada...</center>');
    $('#crt_route').attr('disabled','disabled');

  }
  /***************************************/

  function disabled_mapedit(){
    $('#mapEdit').addClass('disableddiv');
    $('#mapEdit').hide();
    var img="{{ asset( 'iconbar/loading.gif ') }}";
    $('#mapedit_message').html('<center><img width="35px" src="'+img+'" /><br>Cargando configuración de la parada...</center>');
    $('#edt_route').attr('disabled','disabled');

  }
  
  /***************************************/
  
  function enable_mapedit(op){
    
    if(op == true){
       $('#mapEdit').removeClass('disableddiv');
    }
   
    $('#mapEdit').show();
    $('#mapedit_message').html('');
    //$('#edt_route').attr('disabled',false);
  }

  /***************************************/
  
  $( "#route_routeprincipalsId" ).change(function() {
    var valores=0;
    var ulIndex=false;

    valores=$(this).val();
    
    if(valores != null && valores != undefined){
      valores2=valores.length;

      if(valores2 > 3){
        $('option:selected:Last', this).attr('selected',false);
      }
    }
    
  });

  /***************************************/

  function print_list(toAppendPar,state){
    $('#listaParadas').append(toAppendPar);
  }

  /***************************************/
  
  function validateName(){
    var res=false;
    var valu=$('#routeName').val();


    if (valu != '') {
        $.ajax({
          type: "POST",
          url: baseurl+'/routes/validateRname',
          data: {routeName: valu, _token: token},
          dataType: 'JSON',
          success: function (response) {
              
              if (response.ResponseCode == 0) {
                
                do_lalert(true,response.ResponseMessage);
                $('#crt_route').attr('disabled','disabled');
                $('#routeNamehidden').val(0);
                
              }else{
                $('#routeNamehidden').val(1);
                var res=true;
                do_lalert(false,'');
              }

          },
          error: function (data) {
              console.log('Error:', data);
          }
        });
    }else{
       do_lalert(true,'Ingrese el nombre de la ruta');
    }
    return res;
  }
  
  /***************************************/
  
  function validateNameEdit(){
    var res=false;
    var valu=$('#routeNameEdit').val();
    var id=$('#routeIdEdit').val();


    if (valu != '') {
        $.ajax({
          type: "POST",
          url: baseurl+'/routes/validateRname',
          data: {routeName: valu,op: 2,routeId: id, _token: token},
          dataType: 'JSON',
          success: function (response) {
              
              //console.log('Errorname');
              //console.log(response);

              if (response.ResponseCode == 0) {
                
                do_lalertedit(true,response.ResponseMessage);
                $('#routeNamehiddenedit').val(0);
                
              }else{
                $('#routeNamehiddenedit').val(1);
                var res=true;
                do_lalertedit(false,'');
              }

          },
          error: function (data) {
              console.log('Error:', data);
          }
        });
    }else{
       do_lalertedit(true,'Ingrese el nombre de la ruta');
    }
    return res;
  }

  /***************************************/
    
  function validatelocalization(inp){
    var res=false;

    var ldata=$(inp).attr('ldata');
    var lval=$(inp).val();
    var lid=$(inp).attr('id')
    var ldata2=ldata.split("*");
    var la=lid.split("localizationAddress");
    var laedit=lid.split("editlocalizationAddress");
    laedit=laedit[1];
    la=la[1];

    if(ldata2[0] != lval){
      $.ajax({
        type: "POST",
        async:false,
        url: baseurl+'/routes/validatelocalization',
        data: {localizationAddress: lval, _token: token},
        dataType: 'JSON',
        success: function (data) {
          
          if (data.ResponseCode == 1) {

            hide_alert(lid,la);
            do_lalert(false,'');
            enable_map(true);
            /**edt**/
            hide_alertedit(lid,la);
            do_lalertedit(false,'');
            enable_mapedit(true);
            var res=true;
          }else{
            show_alert(lid,la);
            show_alertedit(lid,laedit);

          }
          
        },
        error: function (data) {
            do_lalert(true,data);
            do_lalertedit(true,data.ResponseMessage);
            console.log('Error:', data);
        }
      });
    }
    return res;
  }

  /***************************************/
    
  function validateAddress(inp){
    
    var res=false;
    var ldata=$(inp).attr('ldata');
    var lval=$(inp).val();
    var lid=$(inp).attr('id')
    var ldata2=ldata.split("*");
    var laedit=lid.split("editlocalizationAddress");
    laedit=laedit[1];

    if(ldata2[0] != lval){
      $.ajax({
        type: "POST",
        async:false,
        url: baseurl+'/routes/validateAddress',
        data: {localizationAddress: lval,localizationId: ldata2[3], _token: token},
        dataType: 'JSON',
        success: function (data) {
          
          if (data.ResponseCode == 1) {

            hide_alertedit(lid,laedit);
            do_lalertedit(false,'');
            enable_mapedit(true);
            var res=true;

          }else{
           
            disabled_mapedit();
            show_alertedit(lid,laedit);
            do_lalertedit(true,data.ResponseMessage);
          }
          
        },
        error: function (data) {
            do_lalertedit(true,data.ResponseMessage);
            console.log('Error:', data);
        }
      });
    }
    return res;
  }

  /***************************************/

  function validate_rp(){
    
    var count = $('#route_routeprincipalsId option:selected').length;

    if(count < 1 || count > 3){
      var lp_messg="Error! Se deben seleccionar entre 1 y 3 avenidas principales";
      // console.log('here2');
    }else{
      var lp_messg="";
      do_lalert(false,'');
    }

    return lp_messg;
  }

  /***************************************/

  function validate_nl(){


    var lp=$('#listaParadas').find( "li" );
    var lp1=new Array();
    var lp2 = new Object();
    var lp_messg='';
    var i=0;
    
    $('#listaParadas').find( "li" ).each(function() {
      lp3=$(this).find("input.rt_lst_success").val();
      lp1[i]=lp3;
      i++;
    });
    
    for(var j=0; j<lp1.length;j++){
      var v=lp1[j];

      if(lp2[v]  == undefined){
        lp2[v]=1;
      }else{
        lp2[v]=lp2[v]+1;
      }
    }
    
    $.each( lp2, function( key, value ) {

      if( value != 1){
          lp_messg=lp_messg+' Nombre repetido : '+key;
          $('#listaParadas').find("li input[value='"+key+"']").each(function() {
            $(this).attr('onkeypress','do_lalert()');
          });
          
      }
    });
    
    if(lp1[1] == undefined){
      var lp_messg=" Error debe seleccionar minimo dos paradas para crear una ruta";
     
    }
    if(lp_messg==''){
      do_lalert(false,'');
    }
    
    return lp_messg;
  }

  /***************************************/

  function crt_routef(){

    var lp_messg=validate_nl();
    var lp_messg2=validate_rp();

    if(lp_messg == '' && lp_messg2 ==''){

      var res=$('#routeNamehidden').val();

        //console.log(res);
      
      if(res != 0){
        //console.log(res);
        store_rt();
      }
      
    }else{

      //console.log('nan 2');

      if(lp_messg2 != ''){
          lp_messg=lp_messg2;
      }
      do_lalert(true,'Error! '+lp_messg);
      
      $('#crt_route').attr('disabled','disabled');
      
    }
 }

  /***************************************/

  function store_rt(){
    
    if(($('#routePrice').val() == '')|| ($('#routePrice').val() < 100) ){

      do_lalert(true,'Error! '+'Ingrese el precio de la ruta');
      
      $('#crt_route').attr('disabled','disabled');
    
    }else{
      $('#crt_route').attr('type','submit');

      $("form#data_crt").submit(function() {
    
        console.log($(this).serialize());
        var formData = new FormData($(this)[0]);
        
         $.ajax({
            type: $(this).attr("method"),
            url: $(this).attr("action"),
            data: $(this).serialize(),
            dataType: 'JSON',
            beforeSend: function(){ 
              $("#error").fadeOut();
              $("#data").fadeOut();
              $("#loader").fadeIn();
            },
            success: function (response) {
              $("#loader").fadeOut();
              if(response.ResponseCode == 0){

                $("#data").fadeIn();
                do_lalert(true,response.ResponseMessage);
                $('#crt_route').attr('type','button');
              
              }else{

                //setTimeout(' window.location.href = "'+baseurl+'/routes"; ',2000); 
                setTimeout(function(){

                  $("#addUserForm").html('<div role="alert" class="alert dark alert-success alert-dismissible" style="z-index: 20;"> <button style="width:120px;" aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button>Se ha creado la ruta exitosamente</div>');

                    setTimeout(' window.location.href = "'+baseurl+'/routes"; ',2000);
                },2000); 
              }

            },
            error: function (response) {

                do_lalert(true,'Error de comunicación, revise su conexion a internet.');
            }
          });

        return false;
      });
    }

  }

/***************************************/

  function do_lalert(state,textl){
    
    if(state == true){

      $('#crt_route').attr('disabled','disabled');
      $('#lalert').text(textl);
      $('#lalert').show();
    
    }else{
      $('#lalert').hide();
      $('#crt_route').attr('disabled',false);
    }
    
  }
  /***************************************/

  function do_lalertedit(state,textl){

    if(state == true){
      //console.log(textl);

      $('#edt_route').attr('disabled','disabled');
      $('#lalertedit').text(textl);
      $('#errorEdit').show();
      $('#lalertedit').show();
    
    }else{
      $('#errorEdit').hide();
      $('#lalertedit').hide();
      $('#edt_route').attr('disabled',false);
    }
    
  }

  /***************************************/

  function hide_alert(lid,la){

    $('#'+lid).removeClass('rt_lst_error');
    $('#'+lid).addClass('rt_lst_success');
    $('#listN'+la+' a').removeClass('rt_lst_error_a');
    $('#listN'+la+' a').addClass('rt_lst_success_a');
   
  }

  /***************************************/

   function hide_alertedit(lid,la){

    $('#'+lid).removeClass('rt_lst_error');
    $('#'+lid).addClass('rt_lst_success');
    $('#editlistN'+la+' a').removeClass('rt_lst_error_a');
    $('#editlistN'+la+' a').addClass('rt_lst_success_a');
   
  }

  /***************************************/

  function show_alert(lid,la){

    $('#'+lid).removeClass('rt_lst_success');
    $('#'+lid).addClass('rt_lst_error');
    $('#listN'+la+' a').removeClass('rt_lst_success_a');
    $('#listN'+la+' a').addClass('rt_lst_error_a');
   
  }
  /***************************************/

  function show_alertedit(lid,la){

    $('#'+lid).removeClass('rt_lst_success');
    $('#'+lid).addClass('rt_lst_error');
    $('#editlistN'+la+' a').removeClass('rt_lst_success_a');
    $('#editlistN'+la+' a').addClass('rt_lst_error_a');
   
  }

  /***************************************/

  function close(){
    hide_alert();
    hide_alertedit();
    do_lalert(false);
    do_lalertedit(false);
  }

  /***************************************/
  function closeonEnter(){
    $('#onEnter').hide();
  }
  /***************************************/

  $(function() {
    $('#DroutePrice').on('keydown', '#routePrice', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
  })

  /***************************************/

  $('#profile_a').click(function(){
      //Some code
      // console.log('here');
   
      $('#dropdown-menu').show();
      //$('#profile_a').show();
  });
   $("#btn-perfil").click(function() {
        $("#newpassword").val('');
        $("#renewpassword").val('');
        $('#profile').modal('show');
    });
/**********************************************/

function validatePrice(){

  if(($('#routePrice').val() != '') && ($('#routePrice').val() > 99) ){

      do_lalert(false);
    
    }
}

/**********************************************/

// $("#routePrice").on({
//   "focus": function(event) {
//     $(event.target).select();
//   },
//   "keyup": function(event) {
//     $(event.target).val(function(index, value) {
//       return value.replace(/\D/g, "")
//         .replace(/([0-9])([0-9]{0})$/, '$1$2')
//         .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
//     });
//   }
// });
//
  </script>

  <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCAcMphZ5OU_4iZl30i-gjD_NDmZsZ0TrQ" async defer></script> -->
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAsPNQHqMIginuMT9QgHu_7LXVzV-dW0Es&libraries=places"></script>
@endsection
