<header class="slidePanel-header overlay" style="height: 130px; background-image: url('iconbar/global/photos/placeholder.jpg');')">
  <div class="overlay-panel overlay-background vertical-align">
    <div class="slidePanel-actions">
      <div class="btn-group">
        <button type="button" id="deleteVehicle" class="btn btn-pure btn-inverse icon wb-trash" aria-hidden="true"></button>
        <button type="button" class="btn btn-pure btn-inverse slidePanel-close icon wb-close"
          aria-hidden="true"></button>
      </div>
    </div>
    <button type="button" value="0" class="edit btn btn-success btn-floating" data-toggle="edit">
      <i class="icon wb-pencil animation-scale-up" aria-hidden="true"></i>
      <i class="icon wb-close animation-scale-up" aria-hidden="true"></i>
    </button>
  </div>
</header>

<div class="slidePanel-inner">
  <br>
  <h4 class="modal-title">Editar Datos del Vehiculo</h4>
  <table class="user-info">
    <tbody>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Wheel:</td>
        <td>
          <span id="driverTypeEdit"></span>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Placa:</td>
        <td>
          <span id="vehiclePlaque1"></span>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Marca:</td>
        <td>
          <span id="vehicle_vehiclebrandId1"></span>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Linea:</td>
        <td>
          <span id="vehicle_lineId1"></span>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Modelo (Año):</td>
        <td>
          <span id="vehicleModel1"></span>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Clase:</td>
        <td>
          <span id="vehicle_vehicletypeId1"></span>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Capacidad:</td>
        <td>
          <span id="vehicleCapacity1"></span>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Nro Interno:</td>
        <td>
          <span id="vehicleNinside1"></span>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Nro Operación:</td>
        <td>
          <span id="vehicleNoperation1"></span>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Nro SOAT:</td>
        <td>
          <span id="vehicleNsoat1"></span>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">SOAT Vence:</td>
        <td>
          <span id="vehicleExpiration1"></span>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Tecnomecanica:</td>
        <td>
          <span id="vehicleTecnomecanica1"></span>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Status:</td>
        <td>
          <span id="vehicleState1"></span>
        </td>
      </tr>
      
    </tbody>
  </table>


  <div class="form-group">
      <div id="errorEdit">
      <!-- error will be shown here ! -->
      </div>
  </div>

  <div id="loaderEdit" style="display:none;">
    <center>
        <br><br>
         <img width="48px" src="{{ asset('iconbar/loading.gif') }}" /> &nbsp; Enviando ...
        <br><br>
    </center>
  </div>


  <form id="dataEdit" style="display:none;" method="PUT" action="{{ url('vehicles/put') }}">
  <input type="hidden" id="vehicleIdedit" name="vehicleId" value="{!! $vehicleId !!}">
  <input type="hidden" name="vehicleOcupation" value="0">
    {{ csrf_field() }}
  <table>
    <tbody>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Wheel:</td>
        <td>
          <div class="form-group form-material floating">
            <input id="driverTypeEdit2" name="driverTypeEdit2" type="radio" />
          </div>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Placa:</td>
        <td>
          <div class="form-group form-material floating">
            <input required type="text" class="form-control" name="vehiclePlaque" id="vehiclePlaqueEdit" maxlength="6"/>
          </div>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Marca:</td>
        <td>
          <div class="form-group form-material floating">
            <select required class="form-control"  id="vehicle_vehiclebrandIdEdit" name="vehicle_vehiclebrandId" data-plugin="select2">
          </div>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Linea:</td>
        <td>
          <div class="form-group form-material floating">
            <select required class="form-control form-select"  id="vehicle_lineIdEdit" name="vehicle_lineId" data-plugin="select2">
          </div>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Modelo (Año):</td>
        <td>
          <div class="form-group form-material floating">
             <select id="vehicleModelEdit"  name="vehicleModel"  class="form-control form-select" data-plugin="select2">
              @for ($i = $leasttoday; $i <= $moretoday; $i++)
                <option value="{{ $i }}">{{ $i }}</option>
              @endfor
            </select>
            <!-- <input required type="text" class="form-control" name="vehicleModel" id="vehicleModelEdit"> -->
          </div>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Clase:</td>
        <td>
          <div class="form-group form-material floating">
            <select required class="form-control form-select"  id="vehicle_vehicletypeIdEdit" name="vehicle_vehicletypeId" data-plugin="select2">
              </select>
          </div>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Capacidad:</td>
        <td>
          <div class="form-group form-material floating">
            <select required class="form-control form-select"  id="vehicleCapacityEdit" name="vehicleCapacity" data-plugin="select2">
              </select>
          </div>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Nro Interno:</td>
        <td>
          <div class="form-group form-material floating">
            <input required type="text" class="form-control" name="vehicleNinside" id="vehicleNinsideEdit">
          </div>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Nro Operación:</td>
        <td>
          <div class="form-group form-material floating">
            <input required type="text" class="form-control" name="vehicleNoperation" id="vehicleNoperationEdit">
          </div>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Nro SOAT:</td>
        <td>
          <div class="form-group form-material floating">
            <input required type="text" class="form-control" name="vehicleNsoat" id="vehicleNsoatEdit">
          </div>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">SOAT Vence:</td>
        <td>
          <div class="form-group form-material floating">
            <input required type="text" class="form-control" data-plugin="datepicker" data-format="dd/mm/yyyy" name="vehicleExpiration" id="vehicleExpirationEdit">
          </div>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Tecnomecanica:</td>
        <td>
          <div class="form-group form-material floating">
            <input required type="text" class="form-control" data-plugin="datepicker" data-format="dd/mm/yyyy" name="vehicleTecnomecanica" id="vehicleTecnomecanicaEdit">
          </div>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Status:</td>
        <td>
          <div class="form-group form-material floating">
            <select required class="form-control form-select"  id="vehicleStateEdit" name="vehicleState" data-plugin="select2">

            <option value="0">Inactivo</option>
            <option value="1">Activo</option>

            </select>
          </div>
        </td>
      </tr>
    </tbody>
  </table>
  <hr>
  <center>
    <button id="guardar" class="btn btn-animate btn-animate-vertical btn-success" type="submit">
      <span>
      <i class="icon wb-download" aria-hidden="true"></i>
      Guardar Cambios
      </span>
    </button>
  </center>
  <br><br><br>
  </form>

</div>

<script type="text/javascript">
    $(document).ready(function() {
        $("#dataEdit").trigger( "reset" );
        $(".edit").val(0);
        var toAppend = '';
        var y = 0;
        for (var i = 40; i > 0; i--) {
            y++;
            toAppend += '<option value="'+y+'">'+y+'</option>';
        }

        $('#vehicleCapacityEdit').append(toAppend);
        $('#vehicleCapacityEdit').trigger("select2:updated");

        if (window.location.host == 'localhost') {
            var baseurl = window.location.protocol +"//" + window.location.host + '/prontuz_backend/public';
        }else{
            var baseurl = window.location.protocol +"//" + window.location.host ;
        }

        var vehicleId = $('#vehicleIdedit').val();
        var token = $("input[name*='_token']").val();

        $.ajax({
            type: "GET",
            url: baseurl+'/vehicles/bringVType',
            dataType: 'JSON',
            success: function (data) {
                

                var toAppend = '';
                $.each(data.ResponseData,function(k, v){
                    toAppend += '<option value="'+v+'">'+k+'</option>';
                });

                 $('#vehicle_vehicletypeIdEdit').append(toAppend);
                 $('#vehicle_vehicletypeIdEdit').trigger("select2:updated");

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });

        $.ajax({
            type: "GET",
            url: baseurl+'/vehicles/bringVBrand',
            dataType: 'JSON',
            success: function (data) {
                

                var toAppend = '';
                $.each(data.ResponseData,function(k, v){
                    toAppend += '<option value="'+v+'">'+k+'</option>';
                });

                 $('#vehicle_vehiclebrandIdEdit').append(toAppend);
                 $('#vehicle_vehiclebrandIdEdit').trigger("select2:updated");

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });

        $.ajax({
              type: "POST",
              url: baseurl+'/vehicles/bringVLine',
              data: {line_vehiclebrandId: 1, _token: token},
              dataType: 'JSON',
              success: function (data) {
                  

                  var toAppend = '';
                  $.each(data.ResponseData,function(k, v){
                      toAppend += '<option value="'+v.lineId+'">'+v.lineName+'</option>';
                  });

                 $('#vehicle_lineIdEdit').append(toAppend);
                 $('#vehicle_lineIdEdit').trigger("select2:updated");

              },
              error: function (data) {
                  console.log('Error:', data);
              }
          });

        $('#vehicle_vehiclebrandIdEdit').change(function() {
          var value = $('#vehicle_vehiclebrandIdEdit').val();
          $.ajax({
              type: "POST",
              url: baseurl+'/vehicles/bringVLine',
              data: {line_vehiclebrandId: value, _token: token},
              dataType: 'JSON',
              success: function (data) {
                  
                  $('#vehicle_lineIdEdit').empty().trigger('change');
                 
                  var toAppend = '';
                  $.each(data.ResponseData,function(k, v){
                      toAppend += '<option value="'+v.lineId+'">'+v.lineName+'</option>';
                  });

                 $('#vehicle_lineIdEdit').append(toAppend);
                 $('#vehicle_lineIdEdit').trigger("select2:updated");

              },
              error: function (data) {
                  console.log('Error:', data);
              }
          });
            
        });

        $.ajax({
            type: "GET",
            url: baseurl+'/vehicles/find',
            data: {dataSearch: vehicleId, vehicleId: vehicleId, vehicleState: 1},
            dataType: 'JSON',
            success: function (data) {
                
                
                $.each(data.ResponseData,function(k, v){
                    if(v.routeassignment_vehicleId == 1){
                      var dothis2=true;
                    }else{
                      var dothis2=false;
                    }
                    $('#driverTypeEdit').html(v.userName);
                    $('#driverTypeEdit2').attr("checked", dothis2);
                    $('#driverTypeEdit2').attr("disabled", true);
                   
                    $('#vehiclePlaque1').html(v.vehiclePlaque);
                    $('#vehiclePlaqueEdit').val(v.vehiclePlaque);
                    $('#vehicle_vehiclebrandId1').html(v.vehiclebrandName);
                    $('#vehicle_vehiclebrandIdEdit').val(v.vehicle_vehiclebrandId).trigger('change');
                    $('#vehicle_lineId1').html(v.lineName);
                    $('#vehicle_lineIdEdit').val(v.vehicle_lineId).trigger('change');
                    $('#vehicleModel1').html(v.vehicleModel);
                    // $('#vehicleModelEdit').val(v.vehicleModel);
                    $('#vehicleModelEdit option[value='+v.vehicleModel+']').attr('selected','selected');
                    //<span class="select2-selection__rendered" id="select2-vehicleModelEdit-container" title="1992">1992</span>
                    $('#select2-vehicleModelEdit-container').attr('title',v.vehicleModel);
                    $('#select2-vehicleModelEdit-container').html(v.vehicleModel);
                    $('#vehicle_vehicletypeId1').html(v.vehicletypeName);
                    $('#vehicle_vehicletypeIdEdit').val(v.vehicle_vehicletypeId).trigger('change');
                    $('#vehicleCapacity1').html(v.vehicleCapacity);
                    $('#vehicleCapacityEdit').val(v.vehicleCapacity).trigger('change');
                    $('#vehicleNinside1').html(v.vehicleNinside);
                    $('#vehicleNinsideEdit').val(v.vehicleNinside);
                    $('#vehicleNoperation1').html(v.vehicleNoperation);
                    $('#vehicleNoperationEdit').val(v.vehicleNoperation);
                    $('#vehicleNsoat1').html(v.vehicleNsoat);
                    $('#vehicleNsoatEdit').val(v.vehicleNsoat);

                    $('#vehicleExpiration1').html(v.vehicleExpiration);
                    $('#vehicleExpirationEdit').val(v.vehicleExpiration);
                    $('#vehicleTecnomecanica1').html(v.vehicleTecnomecanica);
                    $('#vehicleTecnomecanicaEdit').val(v.vehicleTecnomecanica);
                    if (v.vehicleState == 1) {
                        $('#vehicleState1').html('Activo');
                    }else{
                        $('#vehicleState1').html('Inactivo');
                    }
                    $('#vehicleStateEdit').val(v.vehicleState).trigger('change');
                    
                    
                    
                });


            },
            error: function (data) {
                console.log('Error:', data);
            }
        });

        $(".edit").on("click", function() {
          var $button = $(this);
          if ($button[0].value == 0) {
              $('.user-info').fadeOut();
              $('#dataEdit').fadeIn();
              $button[0].value = 1;
          }else{
              $('#dataEdit').fadeOut();
              $('.user-info').fadeIn();
              $button[0].value = 0;
          }
        });

       

        $('#deleteVehicle').on("click", function() {
          swal({
              title: "Esta Seguro de Eliminar este Vehiculo?",
              text: "el status cambiara a inactivo!",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "btn-danger",
              confirmButtonText: 'Si, eliminar!',
              cancelButtonText: "No, cancelar!",
              closeOnConfirm: false,
              closeOnCancel: true
            },
            function(isConfirm) {
              if (isConfirm) {

                $.ajax({
                    type: "DELETE",
                    url: baseurl+'/vehicles/destroy',
                    data: {vehicleId: vehicleId},
                    dataType: 'JSON',
                    success: function (data) {
                        
                        if (data.ResponseCode == 1) {
                            swal("Status Inactivo!", data.ResponseMessage, "success"); 
                            setTimeout(' window.location.href = "'+baseurl+'/vehicles"; ',2000);
                        }else{
                            swal("Error!", data.ResponseMessage, "error"); 
                        }
                        


                    },
                    error: function (data) {
                        swal("Error!", data.ResponseMessage, "error"); 
                    }
                });


                
              }
            });
        });
        
        
        $("form#dataEdit").submit(function() {
            $.ajax({
                type: 'PUT',
                url: $(this).attr("action"),
                data: $(this).serialize(),
                dataType: 'JSON',
                beforeSend: function(){ 
                  $("#errorEdit").fadeOut();
                  $("#dataEdit").fadeOut();
                  $("#loaderEdit").fadeIn();
                },
                success: function (response) {
                  $("#loaderEdit").fadeOut();
                  if(response.ResponseCode == 0){

                    $("#dataEdit").fadeIn();
                    $("#errorEdit").fadeIn(1000, function(){      
                    $("#errorEdit").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> '+response.ResponseMessage+'</div>');
                       });
                  
                  }else{

                    setTimeout(' window.location.href = "'+baseurl+'/vehicles"; ',2000);                     
                    
                  }

                },
                error: function (response) {
                    $("#error").fadeIn(1000, function(){      
                    $("#error").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> Error de comunicación, revise su conexion a internet.</div>');
                    });
                }
              });

            return false;
        });
        

    });
  </script>