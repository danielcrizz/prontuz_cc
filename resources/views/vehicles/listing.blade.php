@extends('layouts.app')

@section('content')
  <link rel="stylesheet" href="{{ asset('classic/global/vendor/webui-popover/webui-popover.css') }}">
  <link rel="stylesheet" href="{{ asset('classic/global/vendor/toolbar/toolbar.css') }}">

  <script src="{{ asset('classic/global/vendor/webui-popover/jquery.webui-popover.min.js') }}"></script>
  <script src="{{ asset('classic/global/vendor/toolbar/jquery.toolbar.js') }}"></script>
  <script src="{{ asset('classic/global/js/Plugin/toolbar.js') }}"></script>
 

  <div class="page">
    <div class="page-content">
      <!-- Panel -->
      <div class="panel">
        <div class="panel-body">
          <div class="nav-tabs-horizontal nav-tabs-animate" data-plugin="tabs">
          <!-- Contacts Content -->
          <div id="contactsContent" class="page-content page-content-table" data-plugin="selectable">
            <!-- Contacts -->
            <table id='vehicle_list' width="100%" class="table is-indent" data-plugin="animateList" data-animate="fade" data-child="tr"
            data-selectable="selectable">
              <thead>
                <tr>
                  <th width="75%" scope="col">Vehiculos</th>
                  <th width="15%" scope="col">Estado</th>
                  <th width="10%" scope="col">Acción</th>
                </tr>
              </thead>
             </table>
            
          </div>
         
          </div>
        </div>
      </div>
      <!-- End Panel -->
    </div>
  </div>


  <!-- Site Action -->
  <div class="site-action" data-plugin="actionBtn">
    <button type="button" class="btn-raised btn btn-success btn-floating" onclick="openOpt()" value="0">
      <i class="front-icon wb-plus animation-scale-up" aria-hidden="true"></i>
      <i class="back-icon wb-close animation-scale-up" aria-hidden="true"></i>
    </button>
    <div class="site-action-buttons" id="Opt">
      <button type="button" data-action="sliders" class="btn-raised btn btn-success btn-floating animation-slide-bottom" onclick="openModal2()">
        <i class="icon ion-android-options" aria-hidden="true" style="font-size: 30px;"></i>
      </button>
      <button type="button" data-action="bus" class="btn-raised btn btn-success btn-floating animation-slide-bottom" onclick="openModal()">
        <i class="icon ion-android-bus" aria-hidden="true" style="font-size: 30px;"></i>
      </button>
    </div>
  </div>
  <!-- End Site Action -->


  <!-- Add User Form -->
  <div class="modal fade modal-3d-flip-vertical" id="alertModal"  style="display:none" role="dialog" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
          <h4 class="modal-title">Crear Nuevo Vehiculo</h4>
        </div>
        <div class="modal-body">
          <div class="alert dark alert-icon alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
              <i class="icon wb-check" aria-hidden="true"></i>Creaccion exitosa
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade modal-3d-flip-vertical" id="addUserForm"  style="display:none" role="dialog" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
          <h4 class="modal-title">Crear Nuevo Vehiculo</h4>
        </div>
        <div class="modal-body">

        <div id="loader" style="display:none;">
          <center>
              <br><br>
               <img width="48px" src="{{ asset('iconbar/loading.gif') }}" /> &nbsp; Enviando ...
              <br><br>
          </center>
        </div>

        <form id="data" method="post" action="{{ url('vehicles/store') }}">
        <input type="hidden" id="routeassignment_userId" value=""/>
            <div class="form-group">
                <div id="error">
                <!-- error will be shown here ! -->
                </div>
            </div>
          {{ csrf_field() }}
            <div class="radio-custom radio-primary">
              <input id="driverType" name="driverType" type="radio" />
              <!--  checked="" -->
              <label for="driverType">Wheel</label>
            </div>
            <div class="col-md-12 col-xs-12" id="userDatadiv" style="display:none">
                <div class="form-group form-material floating" data-plugin="formMaterial">
                  <div class="form-group">
                      <div class="input-group">
                        <input class="form-control"  id="userIdV" name="userId" placeholder="Ingrese la identificacion..." type="text">
                        <span class="input-group-btn">
                          <button type="button" class="btn btn-primary" onclick="load_userinfo()"><i class="icon wb-search" aria-hidden="true"></i></button>
                        </span>
                      </div>
                    </div>
                </div>
                <div class="form-group form-material floating" id="user_info" >
                   
                </div>
            </div>
             
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="text" required class="form-control" id="vehiclePlaque" name="vehiclePlaque" maxlength="6" onkeyup="valida(this.value)"  data-toggle="tooltip" data-placement="right" data-trigger="click" data-original-title="Ingrese minimo 6 digitos" onclick="hideTooltip()"/>
              <label class="floating-label" >Placa</label>
            </div>

            <div class="form-group">
              <label class="floating-label">Marca</label>
              <select required class="form-control"  id="vehicle_vehiclebrandId" name="vehicle_vehiclebrandId" data-plugin="select2">
              </select>
            </div>

            <div class="form-group">
              <label class="floating-label">Linea</label>
              <select required class="form-control"  id="vehicle_lineId" name="vehicle_lineId" data-plugin="select2">
              </select>
            </div>

            <div class="form-group">
              <label class="floating-label">Modelo (Año)</label>
               <select id="vehicleModel" name="vehicleModel" class="form-control" >
                @for ($i = $leasttoday; $i <= $moretoday; $i++)
                  <option value="{{ $i }}">{{ $i }}</option>
                @endfor
              </select>
            </div>

            <div class="form-group">
              <label class="floating-label">Clase</label>
              <select required class="form-control"  id="vehicle_vehicletypeId" name="vehicle_vehicletypeId" data-plugin="select2">
              </select>
            </div>

            <div class="form-group">
              <label class="floating-label">Capacidad (Pasajeros)</label>
              <select required class="form-control"  id="vehicleCapacity" name="vehicleCapacity" data-plugin="select2">
              </select>
            </div>

            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="text" required class="form-control" id="vehicleNinside" name="vehicleNinside" />
              <label class="floating-label">Nro Interno</label>
            </div>

            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="text" required class="form-control" id="vehicleNoperation" name="vehicleNoperation" />
              <label class="floating-label">Nro de Tarjeta de Operación</label>
            </div>

            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="text" required class="form-control" id="vehicleNsoat" name="vehicleNsoat" />
              <label class="floating-label">Nro SOAT</label>
            </div>

            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input required type="text" class="form-control" id="vehicleExpiration" name="vehicleExpiration" data-plugin="datepicker" data-format="dd/mm/yyyy"/>
              <label class="floating-label">SOAT Fecha de Vencimiento</label>
            </div>

            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="text" required class="form-control" id="vehicleTecnomecanica" name="vehicleTecnomecanica" data-plugin="datepicker" data-format="dd/mm/yyyy"/>
              <label class="floating-label">Tecnomecanica</label>
            </div>
            
            <input type="hidden" name="vehicleOcupation" value="0">
            <input type="hidden" name="vehicleState" value="1">
            <br>
            <div class="form-group col-xl-12 text-right padding-top-m">
                <button class="btn btn-success" type="button" onclick="crtVehicle();" id="crtVehicleId">Crear</button>
                <a class="btn btn-sm btn-white" data-dismiss="modal" href="javascript:void(0)">Cancelar</a>
            </div>

          </form>

        </div>
      </div>
    </div>
  </div>
  <!-- Add User Form -->
  <div class="modal fade modal-3d-flip-vertical" id="addVsetting"  style="display:none" role="dialog" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Administrar Marca/Linea</h4>
        </div>
        <ul class="nav nav-tabs nav-tabs-line" role="tablist">
          <li class="nav-item" role="presentation"  onclick="fexampleLine()"><a class="nav-link active" data-toggle="tab" href="#exampleLine1" aria-controls="exampleLine1" role="tab" aria-expanded="false">Marca</a></li>
          <li class="nav-item" role="presentation"  onclick="fexampleLine2()"><a class="nav-link" data-toggle="tab" href="#exampleLine2" aria-controls="exampleLine2" role="tab
          " aria-expanded="true"  onclick="fexampleLine2()">Linea</a></li>
         </ul>
        <div class="modal-body">
        {{ csrf_field() }}
          <input type="hidden" value="0" id="Vselected"/>
          <div class="alert alert-danger alert-dismissible" role="alert" id="errorAlert" style="display:none">
          </div>
          <div class="tab-pane" id="exampleLine1" role="tabpanel" aria-expanded="false">
              <div class="form-group form-material floating" data-plugin="formMaterial">
                  <input type="text" required class="form-control" id="vehiclebrandName" name="vehiclebrandName" onchange="turnOffAlert()"/>
                  <label class="floating-label">Nombre de la Marca</label>
              </div>
          </div>
          <div class="tab-pane" id="exampleLine2" role="tabpanel" aria-expanded="false" style="display:none">
              <div class="form-group">
                <label class="floating-label">Marca a la que pertenece</label>
                <select required class="form-control"  id="vehiclebrandId" name="vehiclebrandId" data-plugin="select2">
                </select>
              </div>
              <div class="form-group form-material floating" data-plugin="formMaterial">
                  <input type="text" required class="form-control" id="lineName" name="lineName" onchange="turnOffAlert()"/>
                  <label class="floating-label">Nombre de Linea</label>
              </div>
          </div>
          <div class="form-group col-xl-12 text-right padding-top-m">
            <button class="btn btn-success" type="submit" onclick="crt_Vsetting()" id="crt_VsettingId">Crear</button>
            <a class="btn btn-sm btn-white" data-dismiss="modal" href="javascript:void(0)">Cancelar</a>
          </div>
        </div>
      </div>
    </div>
  </div>
   
  <!-- End Add User Form -->
    <script type="text/javascript">
    token = $("input[name*='_token']").val();
    $(document).ready(function() {

      /****  Load Table****/
      
      tableName='vehicle_list';
      urlName='/vehicles/listing';
      columnsName=[
              { data: 'vehicle'},
              {
                data: 'vehicleState2',
                render: function (data){
                        if(data == 'Activo'){
                          return '<span class="badge badge-outline badge-success">'+data+'</span>';
                        }else{
                          return '<span class="badge badge-outline badge-danger">'+data+'</span>';
                        }
                  }
                },
                {
                data: 'vehicleId',
                render: function (data){
                        return ' <button data-url="vehicles/'+data+'/card" data-toggle="slidePanel" type="button" class="btnEditar btn btn-primary btn-sm">Editar</button>';
                  }
                },
           ];

      list_datatable(tableName,urlName,columnsName);
      /****  Load Table****/


        $("#data").trigger( "reset" );
        var toAppend = '';
        var y = 0;
        for (var i = 40; i > 0; i--) {
            y++;
            toAppend += '<option value="'+y+'">'+y+'</option>';
        }

        $('#vehicleCapacity').append(toAppend);
        $('#vehicleCapacity').trigger("select2:updated");

        var token = $("input[name*='_token']").val();

        if (window.location.host == 'localhost') {
            var baseurl = window.location.protocol +"//" + window.location.host + '/prontuz_backend/public';
        }else{
            var baseurl = window.location.protocol +"//" + window.location.host ;
        }

      


      
        
        $.ajax({
            type: "GET",
            url: baseurl+'/vehicles/bringVType',
            dataType: 'JSON',
            success: function (data) {
                

                var toAppend = '';
                $.each(data.ResponseData,function(k, v){
                    toAppend += '<option value="'+v+'">'+k+'</option>';
                });

                 $('#vehicle_vehicletypeId').append(toAppend);
                 $('#vehicle_vehicletypeId').trigger("select2:updated");

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });

        $.ajax({
            type: "GET",
            url: baseurl+'/vehicles/bringVBrand',
            dataType: 'JSON',
            success: function (data) {
                

                var toAppend = '';
                $.each(data.ResponseData,function(k, v){
                    toAppend += '<option value="'+v+'">'+k+'</option>';
                });

                 $('#vehicle_vehiclebrandId').append(toAppend);
                 $('#vehicle_vehiclebrandId').trigger("select2:updated");
                 $('#vehiclebrandId').append(toAppend);
                 $('#vehiclebrandId').trigger("select2:updated");

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });

        $.ajax({
              type: "POST",
              url: baseurl+'/vehicles/bringVLine',
              data: {line_vehiclebrandId: 1, _token: token},
              dataType: 'JSON',
              success: function (data) {
                  

                  var toAppend = '';
                  $.each(data.ResponseData,function(k, v){
                      toAppend += '<option value="'+v.lineId+'">'+v.lineName+'</option>';
                  });

                 $('#vehicle_lineId').append(toAppend);
                 $('#vehicle_lineId').trigger("select2:updated");

              },
              error: function (data) {
                  console.log('Error:', data);
              }
          });

        $('#vehicle_vehiclebrandId').change(function() {
          var value = $('#vehicle_vehiclebrandId').val();
          $.ajax({
              type: "POST",
              url: baseurl+'/vehicles/bringVLine',
              data: {line_vehiclebrandId: value, _token: token},
              dataType: 'JSON',
              success: function (data) {
                  
                  $('#vehicle_lineId').empty().trigger('change');
                 
                  var toAppend = '';
                  $.each(data.ResponseData,function(k, v){
                      toAppend += '<option value="'+v.lineId+'">'+v.lineName+'</option>';
                  });

                 $('#vehicle_lineId').append(toAppend);
                 $('#vehicle_lineId').trigger("select2:updated");

              },
              error: function (data) {
                  console.log('Error:', data);
              }
          });
            
        });

        

        //valida placa
        $("#vehiclePlaque").focusout(function() {
            var value = $(this).val();
            if (value != '') {
                $.ajax({
                  type: "POST",
                  url: baseurl+'/vehicles/validateVplaque',
                  data: {vehiclePlaque: value, _token: token},
                  dataType: 'JSON',
                  success: function (response) {
                      
                      if (response > 0) {
                        $("#vehiclePlaque").val('');
                        $("#error").fadeIn(1000, function(){      
                        $("#error").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> Placa ya se registro anteriormente.</div>');
                        });

                      }else{
                        $("#error").fadeOut();
                        $("#error").html('');
                      }

                  },
                  error: function (data) {
                      console.log('Error:', data);
                  }
                });
            }

        });

        

        $("#listaVehicles").html('');
        $.ajax({
          type: "GET",
          url: baseurl+'/vehicles/find',
          dataType: 'JSON',
          beforeSend: function(){ 
            $("#listaVehicles").fadeOut();
            //$("#loaderlist").fadeIn();
          },
          success: function (data) {
              
              //$("#loaderlist").fadeOut();
              var toAppend = '';
              $.each(data.ResponseData,function(k, v){
                  //console.log(v.userEmail);

                  if (v.vehicleState == 1) {

                    toAppend += '<li class="list-group-item"> <div class="media"> <div class="media-body"> <h4 class="mt-0 mb-5"> '+v.vehiclePlaque+' </h4><span class="text-green">'+v.vehiclebrandName+' - '+v.lineName+' - '+v.vehicletypeName+' - '+v.vehicleModel+'</span> <br>Nº Interno: '+v.vehicleNinside+' - Nº Operación: '+v.vehicleNoperation+' <br> Capacidad (Pasajeros) : <spam class="text-orange"> '+v.vehicleCapacity+' </spam> <br>SOAP: '+v.vehicleNsoat+' - Vence: <span class="text-orange">'+v.vehicleExpiration+'</span> <br>TECNICOMECANICA: <span class="text-orange">'+v.vehicleTecnomecanica+'</span> </p></div><div class="pl-20 align-self-center"> <button data-url="vehicles/'+v.vehicleId+'/card" data-toggle="slidePanel" type="button" class="btnEditar btn btn-primary btn-sm">Editar</button> <button type="button" class="btn btn-primary btn-sm">Activo</button> </div></div></li>';

                  }else{
                    toAppend += '<li class="list-group-item"> <div class="media"> <div class="media-body"> <h4 class="mt-0 mb-5"> '+v.vehiclePlaque+' </h4><span class="text-green">'+v.vehiclebrandName+' - '+v.lineName+' - '+v.vehicletypeName+' - '+v.vehicleModel+'</span> <br>Nº Interno: '+v.vehicleNinside+' - Nº Operación: '+v.vehicleNoperation+' <br> Capacidad (Pasajeros) : <spam class="text-orange"> '+v.vehicleCapacity+' </spam> <br>SOAP: '+v.vehicleNsoat+' - Vence: <span class="text-orange">'+v.vehicleExpiration+'</span> <br>TECNICOMECANICA: <span class="text-orange">'+v.vehicleTecnomecanica+'</span> </p></div><div class="pl-20 align-self-center"> <button data-url="vehicles/'+v.vehicleId+'/card" data-toggle="slidePanel" type="button" class="btnEditar btn btn-primary btn-sm">Editar</button> <button type="button" class="btn btn-outline btn-primary btn-sm">Inactivo</button> </div></div></li>';
                  }

                  
              });

               $('#listaVehicles').append(toAppend);
               $("#listaVehicles").fadeIn();
               

          },
          error: function (data) {
              console.log('Error:', data);
          }
      });


      $('#driverType').click(function(event) {
        $('#routeassignment_userId').val(0);
        $('#user_info').empty("");
        $('#userIdV').val("");


        if($('#driverType').attr("checked") == "checked" || $('#driverType').attr("checked") == true){
          $('#driverType').attr("checked", false);
          $('#userIdV').attr("required", false);
          $('#userDatadiv').css("display","none");

          
        }else{
          $('#driverType').attr("checked",true);
          $('#userDatadiv').css("display","block");
          $('#userIdV').attr("required","required");
        }
      });
   
    });



/************************************/

function openOpt(){
  
  if($('#Opt').val() == 0){

    document.getElementById("Opt").value=1;
    $('#Opt').show();
  }else{
    document.getElementById("Opt").value=0;
    $('#Opt').hide('slow');
  }
  
}
/************************************/

function openModal(){
  
  openOpt();
  $('#addUserForm').modal('show');
  
}
/************************************/

function openModal2(){
  
  openOpt();
  $('#addVsetting').modal('show');
  doClear();
  fexampleLine();
}
/************************************/

function fexampleLine(){
  
  $('#exampleLine1').show();
  $('#exampleLine2').hide();
  $('#Vselected').val(0);
  
}
/************************************/

function fexampleLine2(){
  
  $('#exampleLine2').show();
  $('#exampleLine1').hide();
  $('#Vselected').val(1);
}
/************************************/

function hideTooltip(){
    
    $('.tooltip-inner').hide();
  }

/************************************/

function valida(e){
    if(e.length <= 5){
     
      $('.tooltip-inner').show();
      $('#crtVehicleId').attr('disabled',true);
    }else{
      
      $('.tooltip-inner').hide();
      $('#crtVehicleId').attr('disabled',false);
    }
  }

/************************************/

function crt_Vsetting(){

  var Vselected=$('#Vselected').val();

  if(Vselected == 0){

    var vehiclebrandName=$('#vehiclebrandName').val();
        $.ajax({
          type: "post",
          url: baseurl+'/vehicles/validateVbrand',
          data: {_token: token,vehiclebrandName:vehiclebrandName},
          dataType: 'JSON',
          success: function (data) {
              
              if(data.ResponseCode == 0){
                printAlert(data.ResponseMessage);
              }else{
                $.ajax({
                    type: "post",
                    url: baseurl+'/vehicles/storeVbrand',
                    data: {_token: token,vehiclebrandName:vehiclebrandName},
                    dataType: 'JSON',
                    success: function (data) {
                        
                        if(data.ResponseCode == 0){
                          printAlert(data.ResponseMessage);
                        }else{
                        
                          $('#addVsetting').modal('hide');
                          $('#alertModal').modal('show');
                          setTimeout(function(){
                            location.reload(); 
                          },2000);
                          
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
              }
          },
          error: function (data) {
              console.log('Error:', data);
          }
      });


    
  }else if(Vselected == 1){
    var vehiclebrandId=$('#vehiclebrandId').val();
    var lineName=$('#lineName').val();
     $.ajax({
        type: "post",
        url: baseurl+'/vehicles/validateVLine',
        data: {_token: token,lineName:lineName},
        dataType: 'JSON',
        success: function (data) {
            
            if(data.ResponseCode == 0){
              printAlert(data.ResponseMessage);
            }else{
               $.ajax({
                  type: "post",
                  url: baseurl+'/vehicles/storeVline',
                  data: {_token: token,vehiclebrandId:vehiclebrandId,lineName:lineName},
                  dataType: 'JSON',
                  success: function (data) {
                      
                      if(data.ResponseCode == 0){
                        printAlert(data.ResponseMessage);
                      }else{
                      
                        $('#addVsetting').modal('hide');
                        $('#alertModal').modal('show');
                        setTimeout(function(){
                          location.reload(); 
                        },2000);
                        
                      }
                  },
                  error: function (data) {
                      console.log('Error:', data);
                  }
              });
          }
        },
        error: function (data) {
            console.log('Error:', data);
        }
    });
  }

}
/**************************************************/
  function printAlert(messg){
    $('#errorAlert').text(messg);
    $('#errorAlert').show();
    $('#crt_VsettingId').attr('disabled',true);
  }

/**************************************************/

function turnOffAlert(){

    $('#errorAlert').text('');
    $('#errorAlert').hide();
    $('#crt_VsettingId').attr('disabled',false);
}

/**************************************************/

function doClear(){

  $('#vehiclebrandId').find($('option')).attr('selected',false)
  $('#vehiclebrandName').val('');
  $('#lineName').val('');
  turnOffAlert();
}
/************************************/
  function crtVehicle(){

    var driverType=$('#driverType').attr("checked");
    var pass = false;
    $('#crtVehicleId').attr('type','button');  


    if((driverType == true) || (driverType == 'checked')){
      pass=true;
    }else if((driverType == undefined)|| (driverType == false)){
      pass=true;
      $("#routeassignment_userId").val(0);
    }

    if(pass == true){

      $('#crtVehicleId').attr('type','submit');  
      $("form#data").submit(function() {

        var formData = new FormData($(this)[0]);

        $('#crtVehicleId').attr('disabled','disabled');  
        $.ajax({
            type: $(this).attr("method"),
            url: $(this).attr("action"),
            data: formData,
            dataType: 'JSON',
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){ 
              $("#error").fadeOut();
              $("#data").fadeOut();
              $("#loader").fadeIn();
            },
            success: function (response) {
                  $("#loader").fadeOut();
                  if(response.ResponseCode == 0){

                    $("#data").fadeIn();
                    $("#error").fadeIn(1000, function(){      
                    $("#error").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> '+response.ResponseMessage+'</div>');
                         $("#btn-login").html('<span class="icon ion-android-lock"></span> &nbsp; Entrar');
                       });

                    $('#crtVehicleId').attr('disabled',false);  
                  
                  }else{
                    var routeassignment_vehicleId=response.ResponseData;

                    if((routeassignment_vehicleId > 0) && $('#routeassignment_userId').val() >0){
                      
                      var routeassignment_userId=$("#routeassignment_userId").val();
                      
                      var data= {_token: token,routeassignment_userId:routeassignment_userId,routeassignment_vehicleId:routeassignment_vehicleId};
      
                        $.ajax({
                              type: "POST",
                              url: baseurl+'/vehicles/storeAssignmentWheel',
                              data: data,
                              async: false,
                              dataType: 'JSON',
                              success: function (data) {

                                console.log('here2');

                                if(data.ResponseCode == 0){

                                  $("#data").fadeIn();
                                  $("#error").fadeIn(1000, function(){      
                                  $("#error").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> '+response.ResponseMessage+'</div>');
                                       $("#btn-login").html('<span class="icon ion-android-lock"></span> &nbsp; Entrar');
                                     });

                                }
                                setTimeout(' window.location.href = "'+baseurl+'/vehicles"; ',2000);
                             
                              },
                              error: function (data) {
                                  console.log('Error:', data);
                              }
                          });
                    }
                    setTimeout(' window.location.href = "'+baseurl+'/vehicles"; ',2000);    
                  }

                },
                error: function (response) {
                    $("#error").fadeIn(1000, function(){      
                    $("#error").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> Error de comunicación, revise su conexion a internet.</div>');
                    });
                }
              });

            return false;
        });
      }else{

        $("#data").fadeIn();
        $("#error").fadeIn(1000, function(){      
        $("#error").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button>Ingrese un conductor Wheel valido o desseleccione la opción</div>');
           $("#btn-login").html('<span class="icon ion-android-lock"></span> &nbsp; Entrar');
         });
      }
    
  }
/**************************************************/

function load_userinfo(){
    
    $('#user_info').empty();
    e=$('#userIdV').val();
    $.ajax({
        type: "POST",
        url: baseurl+'/vehicles/getWheel',
        data: {_token: token,userIdentificacion:e},
        dataType: 'JSON',
        success: function (data) {

          console.log(data);
           
                  if(data.ResponseCode == 0){
                  $('#user_info').html('<div role="alert" class="alert dark alert-danger alert-dismissible">'+data.ResponseMessage+' o ya se encuentra asignado</div>');
                  $('#userIdV').val('');
                  //$('#userIdV').removeAttr('required');
                  
                  }else if(data.ResponseCode == 1){
                      v=data.ResponseData;
                      userId = v[0].userId; 
                      userFirstname = v[0].userFirstname; 
                      userLastname = v[0].userLastname; 
                      userIdentificacion = v[0].userIdentificacion; 
                    var routeassignment_userId=$("#routeassignment_userId").val(userId);

                    var add='<table class="table table-bordered">'+
                            '<thead>'+
                              '<tr>'+
                                '<th>Usuario</th>'+
                                '<th>Identificacion</th>'+
                                '<th>Rol</th>'+
                              '</tr>'+
                            '</thead>'+
                            '<tbody id="recharge_info_tbody">'+
                              '<tr>'+
                                '<td>'+userFirstname+userLastname+'</td>'+
                                '<td>'+userIdentificacion+'</td>'+
                                '<td>Wheel</td>'+
                              '</tr>'+
                            '</tbody>'+
                          '</table>';

                  }
                $('#user_info').append(add);
                $('#user_info').show();
         },
        error: function (data) {
            console.log('Error:', data);
        }
    });

 
}

/************************************/





  </script>

@endsection
