@extends('layouts.login')

@section('content')

<link rel="stylesheet" href="{{ asset('iconbar/global/vendor/bootstrap-sweetalert/sweetalert.css') }}">
<!-- Page -->
<div class="page" data-animsition-in="fade-in" data-animsition-out="fade-out">
    <div class="page-content">
      <div class="page-brand-info">
        <div class="brand">
          <img class="brand-img" src="{{ asset('LogoHeader.svg') }}" alt="logo" width="200px">
        </div>
        <p class="font-size-20"><b>Bienvenido al administrador general del portal Prontuz U</b></p>
      </div>



      <div class="page-login-main animation-slide-right animation-duration-1">
        <div class="brand hidden-md-up">
          <img class="brand-img" src="{{ asset('LogoHeader.svg') }}" alt="logo" width="200px">
        </div>
        <h3 class="font-size-24 font-white"><b>Ingresar</b></h3>
        <p>Ingrese acá datos de usuario para iniciar sesion.</p>
        <br>

        <form id="data"  method="post" action="{{ url('logIn') }}">

        {{ csrf_field() }}

            <div class="form-group">
                <div id="error">
                <!-- error will be shown here ! -->
                </div>
            </div>

            

            <div class="form-group">
                <label class="sr-only" for="email">Email</label>
                <input type="email" required class="form-control" id="userEmail" name="userEmail" placeholder="Email">
            </div>

            <div class="form-group">
                <label class="sr-only" for="password">Password</label>
                <input type="password" required class="form-control" id="password" name="password" placeholder="Password">
            </div>

            <input type="hidden" name="typePlataform" value="web">

          <div class="form-group clearfix">
            <a class="float-right font-white" href="#" id='newpassword_btn'>Olvide mi password</a>
          </div>
          <button type="submit" id="btn-login" class="btn btn-danger btn-block">
            <span class="icon ion-android-lock"></span> &nbsp; Entrar
          </button>
        </form>

        <footer class="page-copyright font-white">
          <p>Design & Powered by <a target="_black" class="font-black" href="http://crizz.com.co"><i><u>CRIZZ</u></i></a></p>
          <p>© 2017. All RIGHT RESERVED.</p>
        </footer>
      </div>
    </div>
</div>
<!-- End Page -->
<!-- profile -->
  <div class="modal fade" id="newPassword_modal"  role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
          <h4 class="modal-title">Solicitud de nueva contraseña</h4>
        </div>
     
        <div class="modal-body">
        <div id="loader" style="display:none;">
            <center>
                <br><br>
                <img width="48px" src="{{ asset('iconbar/loading.gif') }}" /> &nbsp; Enviando ...
                <br><br>
            </center>
        </div>           
        <div class="form-group">
            <div id="error_modal" align='center'>
            <!-- error will be shown here ! -->
            </div>
        </div>
        <form class="form-horizontal" id="form_newPassword" action="javascript:newPass(2)">
            {{ csrf_field() }}
            <div class="row" style='margin-left: 6.929%;    margin-right: 6%;'>
                <div class="col-lg-12">
                    <div class="form-group form-material floating" data-plugin="formMaterial">
                        <input  required='required'  type="text"  class="form-control" id="email" name="email" />
                        <label class="floating-label">Correo electronico</label>
                    </div>
                </div>
                <div  style='display:none;' class="col-lg-12 " id='code_div'>
                    <div class="form-group form-material floating" data-plugin="formMaterial">
                        <input required='required' type="text"  class="form-control" id="code" name="code" />
                        <label class="floating-label">Codigo de confirmación</label>
                    </div> 
                </div>
                <div style='display:none;' class="col-lg-12 " id='new_pass'>
                    <div class="form-group form-material floating" data-plugin="formMaterial">
                        <input required='required' title="8 a 16 characters" type="password" pattern=".{8,16}" class="form-control" id="newpassword" name="newpassword" maxlength="16" />
                        <label class="floating-label">Nueva contraseña</label>
                    </div> 
                </div>
                <div style='display:none;' class="col-lg-12 " id='re_pass'>
                    <div class="form-group form-material floating" data-plugin="formMaterial">
                        <input required='required' title="8 a 16 characters" type="password" pattern=".{8,16}" class="form-control" id="renewpassword" name="renewpassword" maxlength="16" />
                        <label class="floating-label">Repetir contraseña</label>
                    </div> 
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" onclick="newPass(1)" class="btn btn-success" id='btn_correo'>Solicitar Codigo</button>
            <button type="submit"  class="btn btn-success" id='btn_sent' style='display:none'>Guardar</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal" href="javascript:void(0)">Cancelar</button> 
        </div>
        </form>
      </div>
    </div>
  </div>
  <script src="{{ asset('iconbar/global/vendor/bootstrap-sweetalert/sweetalert.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {

        if (window.location.host == 'localhost') {
            var baseurl = window.location.protocol +"//" + window.location.host + '/prontuz_backend/public';
        }else{
            var baseurl = window.location.protocol +"//" + window.location.host ;
        }

        $("form#data").submit(function() {

            $.ajax({
              type: $(this).attr("method"),
              url: $(this).attr("action"),
              data: $(this).serialize(),
              dataType: 'JSON',
              beforeSend: function(){ 
                $("#error").fadeOut();
                $("#btn-login").html('<img width="24px" src="'+baseurl+'/iconbar/loading.gif" /> &nbsp; Autenticando ...');
              },
              success: function (response) {

    

                if(response.ResponseCode != 0){
                  
                  $("#btn-login").html('<img width="24px" src="'+baseurl+'/iconbar/loading.gif" /> &nbsp; Cargando ...');
                  setTimeout(' window.location.href = "'+baseurl+'/index"; ',2000);
                
                }else{
                     
                  $("#error").fadeIn(1000, function(){      
                  $("#error").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> '+response.ResponseMessage+'</div>');
                       $("#btn-login").html('<span class="icon ion-android-lock"></span> &nbsp; Entrar');
                     });
                }
                  

              },
              error: function (response) {
                  $("#error").fadeIn(1000, function(){      
                  $("#error").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> Error de comunicación, revise su conexion a internet.</div>');
                       $("#btn-login").html('<span class="icon ion-android-lock"></span> &nbsp; Entrar');
                     });
                  console.log(response);
              }
            });


            return false;
        });
        $("#newpassword_btn").click(function() {
            $("#code_div").fadeOut();
            $("#new_pass").fadeOut();
            $("#re_pass").fadeOut();
            $("#btn_sent").fadeOut();
            $("#btn_correo").fadeIn();
            $("#email").val('');
            $("#code").val('');
            $("#newpassword").val('');
            $("#renewpassword").val('');
            $("#newPassword_modal").modal('show');
        });

    });
    function newPass(op) 
    { 
        var url = '';
        var a = true;
        var b = true;
        if(op ==  1 && $("#email").val()=='')
        {
            b = false;
        }
        switch(op)
        {
            case 1:
                url='/user/resetPassword'
            break;
            case 2:
                url='/user/newPassword'
            break;           
        }
        if(parseInt(op) === 2 )
        {
          if($("#newpassword").val() =='' || $("#renewpassword").val() =='' || $("#newpassword").val() != $("#renewpassword").val())
          {
              a=false;
          }
        }
        if(a==true && b==true)
        {
            $.ajax({
                      type: "POST",
                      url: url,
                      data: {userEmail:$("#email").val(), userCode:$("#code").val(), password:$("#renewpassword").val(),_token: $("input[name*='_token']").val()},
                      dataType: 'JSON',
                      beforeSend: function(){ 
                        $("#loader").fadeIn();
                      },
                      success: function (resp) {
                          var data = resp;
                          $("#loader").fadeOut();
                          if (data.ResponseCode > 0) {
                            switch(op)
                            {
                                case 1:
                                swal({
                                      title: "ÉXITO!",
                                      text: data.ResponseMessage,
                                      type: "success",
                                      timer: 2500,
                                      showConfirmButton: false
                                    });
                                $("#btn_correo").fadeOut();
                                $("#btn_correo").fadeOut();
                                $("#code_div").fadeIn();
                                $("#new_pass").fadeIn();
                                $("#re_pass").fadeIn();
                                $("#btn_sent").fadeIn();
                                $("#email").attr('disabled',true);
                                break;
                                case 2:
                                    swal({
                                      title: "ÉXITO!",
                                      text: data.ResponseMessage,
                                      type: "success",
                                      timer: 2500,
                                      showConfirmButton: false
                                    });
                                    $("#newPassword_modal").modal('hide');
                                break;           
                            }
                          }else{
                            swal("Error!",data.ResponseMessage,'error');
                            $("#error").fadeOut();
                            $("#error").html('');
                          }

                      },
                      error: function (jqXHR, exception) {
                        $("#loader").fadeOut();
                        if(jqXHR.status===500)
                        {
                            swal("Error!","ERROR INTERNO DEL SERVIDOR",'error');
                        }                 
                        if(jqXHR.status===401)
                        {
                            $("#logOut").click();
                        }
                        if(jqXHR.status===404)
                        {
                            swal("Error!","RUTA NO ENCONTRADA (404 NOT FOUND)",'error');
                        }
                      }
                    });
        }
        if(a == false && b === true)
        {
            swal("Error!","La contraseña no coincide o esta vacio los campos",'error');
        }      
        if(a == true && b === false)
        {
            swal("Error!","El correo es obligatorio",'error');
        }
    }
  </script>
@endsection
