<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">

  <title>Prontuz U | Administrador</title>

  <link rel="apple-touch-icon" href="{{ asset('iconbar/assets/images/apple-touch-icon.png') }}"> <link rel="shortcut icon" href="{{ asset('iconbar/assets/images/favicon.ico') }}">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="{{ asset('iconbar/global/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/css/bootstrap-extend.min.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/assets/css/site.css') }}">
  <!-- Plugins -->
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/animsition/animsition.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/asscrollable/asScrollable.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/switchery/switchery.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/intro-js/introjs.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/slidepanel/slidePanel.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/flag-icon-css/flag-icon.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/assets/examples/css/uikit/buttons.css') }}">

  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/formvalidation/formValidation.css') }}">

  <link rel="stylesheet" href="{{ asset('iconbar/assets/examples/css/apps/contacts.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/assets/examples/css/pages/user.css') }}">

  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/slidepanel/slidePanel.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/chartist/chartist.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/jquery-selective/jquery-selective.css') }}">
  

  <link rel="stylesheet" href="{{ asset('iconbar/assets/examples/css/forms/masks.css') }}">

 

  <!-- otros plugin -->
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/datatables/datatables.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/datatables-fixedheader/dataTables.fixedHeader.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/datatables-responsive/dataTables.responsive.css') }}">
 

  <!-- Fonts -->
  <link rel="stylesheet" href="{{ asset('iconbar/global/fonts/ionicons/ionicons.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/fonts/web-icons/web-icons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/fonts/brand-icons/brand-icons.min.css') }}">

  <link rel="stylesheet" href="{{ asset('iconbar/assets/examples/css/apps/work.css') }}">

  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/select2/select2.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/bootstrap-sweetalert/sweetalert.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/assets/examples/css/uikit/modals.css') }}">
  <link rel="stylesheet" href="{{ asset('css/datables_personalized.css') }}">
  <link rel="stylesheet" href="{{ asset('classic/global/vendor/footable/footable.bootstrap.css') }}">
  <link rel="stylesheet" href="{{ asset('classic/global/vendor/footable/footable.core.css') }}">
  <link rel="stylesheet" href="{{ asset('classic/center/assets/examples/css/tables/footable.css') }}">


  <link rel="stylesheet" href="{{ asset('css/app.css') }}">



  
  


  <style type="text/css">

    @font-face {
      font-family: 'Prometo';
      src: url('{{ asset('iconbar/global/fonts/Prometo Regular.otf') }}');
    }

    html, body {
        font-family: Prometo !important;
        font-weight: 500 !important;
    }
    

    input[type="file"] {
        display: none;
    }

    .bg-green-nuevo{
        background-color: #4FB933 !important;
    }

    .btn-success {
      background-color: #4FB933 !important;
      border-color: #4FB933 !important;
    }

    .text-green {
      color: #4FB933 !important;
    }

    .text-orange {
      color: #FF7A36 !important;
    }
    
  </style>
  <!--[if lt IE 9]>
    <script src="{{ asset('iconbar/global/vendor/html5shiv/html5shiv.min.js') }}"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="{{ asset('iconbar/global/vendor/media-match/media.match.min.js') }}"></script>
    <script src="{{ asset('iconbar/global/vendor/respond/respond.min.js') }}"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="{{ asset('iconbar/global/vendor/breakpoints/breakpoints.js') }}"></script>
  <script>
  Breakpoints();
  </script>

  <!-- Core  -->
  <script src="{{ asset('iconbar/global/vendor/babel-external-helpers/babel-external-helpers.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/jquery/jquery.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/tether/tether.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/bootstrap/bootstrap.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/animsition/animsition.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/mousewheel/jquery.mousewheel.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/asscrollbar/jquery-asScrollbar.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/asscrollable/jquery-asScrollable.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/ashoverscroll/jquery-asHoverScroll.js') }}"></script>
</head>
<body class="animsition app-contacts page-user page-aside-left">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega navbar-inverse bg-green-nuevo"
  role="navigation">
    <div class="navbar-header">
      <button type="button" class="navbar-toggler hamburger hamburger-close navbar-toggler-left hided"
      data-toggle="menubar">
        <span class="sr-only">Toggle navigation</span>
        <span class="hamburger-bar"></span>
      </button>
      <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse"
      data-toggle="collapse">
        <i class="icon wb-more-horizontal" aria-hidden="true"></i>
      </button>
      <div class="navbar-brand navbar-brand-center">
        <img class="navbar-brand-logo" src="{{ asset('LogoHeader.svg') }}" title="Prontuz">
      </div>
    </div>
    <div class="navbar-container container-fluid">
      <!-- Navbar Collapse -->
      <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
        <!-- Navbar Toolbar -->
        <ul class="nav navbar-toolbar">
          <li class="nav-item hidden-float" id="toggleMenubar">
            <a class="nav-link" data-toggle="menubar" href="#" role="button">
              <i class="icon hamburger hamburger-arrow-left">
                  <span class="sr-only">Toggle menubar</span>
                  <span class="hamburger-bar"></span>
                </i>
            </a>
          </li>
          <li class="nav-item hidden-sm-down" id="toggleFullscreen">
            <a class="nav-link icon icon-fullscreen" data-toggle="fullscreen" href="#" role="button">
              <span class="sr-only">Toggle fullscreen</span>
            </a>
          </li>
        </ul>
        <!-- End Navbar Toolbar -->
        <!-- Navbar Toolbar Right -->
        <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
          <li class="nav-item dropdown">
            <a id="profile_a" class="nav-link navbar-avatar" data-toggle="dropdown" href="#" aria-expanded="false"
            data-animation="scale-up" role="button">
              <span class="avatar avatar-online">
                <img src="{!! Auth::user()->userImage !!}" alt="...">
                <i></i>
              </span>
            </a>
            <div class="dropdown-menu" role="menu" id="dropdown-menu">
              @if(Auth::user()->userId!= '')
                <input type="hidden" id="userId" value="{!! Auth::user()->userId !!}">
              @else
                <script>
                  window.location.href = baseurl+'/login';
                </script>
              @endif
              
              <a class="dropdown-item" href="javascript:void(0)" role="menuitem" id='btn-perfil'><i class="icon wb-user" aria-hidden="true"></i> Perfil</a>
              <a class="dropdown-item" href="javascript:void(0)" role="menuitem" id='btn-sysparameters'><i class="icon wb-settings" aria-hidden="true"></i> Parametros del sistema</a>
              <div class="dropdown-divider" role="presentation"></div>
              <a class="dropdown-item" id="logOut" href="javascript:void(0)" role="menuitem"><i class="icon wb-power" aria-hidden="true"></i> Logout</a>
            </div>
          </li>
        </ul>
        <!-- End Navbar Toolbar Right -->
      </div>
      <!-- End Navbar Collapse -->
    </div>
  </nav>
<!-- profile -->
  <div class="modal fade" id="profile"  role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
          <h4 class="modal-title">Perfil de Usuario</h4>
        </div>
     <form id="FormProfile" action="javascript:sendPassword()">
        <div class="modal-body">

            <div id="loader_modal" style="display:none;">
              <center>
                  <br><br>
                   <img width="48px" src="{{ asset('iconbar/loading.gif') }}" /> &nbsp; Enviando ...
                  <br><br>
              </center>
            </div>           
              <div class="form-group">
                    <div id="error_modal" align='center'>
                    <!-- error will be shown here ! -->
                    </div>
                </div>

              {{ csrf_field() }}

              <input type="hidden" id="rol" value="">          
                <div class="form-group"> 
                    <center>
                      <div class='img-circle img-responsive'>
                        <a href="javascript:void(0)">
                          <img class='img-circle img-responsive' id='img_modal_user' src="{!! Auth::user()->userImage !!}" style="position: relative; display: inline-block; width: 128px; white-space: nowrap; border-radius: 1000px; vertical-align: bottom; align-content: center;"/>
                        </a>
                      </div>
                    </center>
                </div>
                <center>
                    <h4 class="profile-user">{!! Auth::user()->FirstName !!} {!! Auth::user()->LastName !!}</h4>  
                </center> 
                <center>
                    <p class="profile-job">{!! Auth::user()->userEmail !!}</p>
                </center>    

                <div class="form-group form-material floating" data-plugin="formMaterial">
                  <input required='required' title="8 a 16 characters" type="password" pattern=".{8,16}" class="form-control" id="newpassword" name="newpassword" maxlength="16" />
                  <label class="floating-label">Contraseña</label>
                </div>

                <div class="form-group form-material floating" data-plugin="formMaterial">
                  <input type="hidden" name='userIdPhoto' id="userId" value="{!! Auth::user()->userId !!}">
                  <input required='required' title="8 a 16 characters" type="password" pattern=".{8,16}" class="form-control" id="renewpassword" name="renewpassword" maxlength="16" />
                  <label class="floating-label">Repetir Contraseña</label>
                </div> 
         </div>
        <div class="modal-footer">
            <input type="file" name="new_img" id = 'new_img_modal' class='hidden'>    
            <button type="submit" class="btn btn-success">Guardar</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal" href="javascript:void(0)">Cancelar</button> 
        </div>
        </form>
      </div>
    </div>
  </div>

  <!-- sysparameters -->
  <div class="modal fade" id="sysparameters"  role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
          <h4 class="modal-title">Parametros del sistema</h4>
        </div>
     <form id="FormParameters" action="{{ url('user/putSysparameters') }}" method="POST">
        <div class="modal-body">
          {{ csrf_field() }}
            <div id="loader_modal" style="display:none;">
              <center>
                  <br><br>
                   <img width="48px" src="{{ asset('iconbar/loading.gif') }}" /> &nbsp; Enviando ...
                  <br><br>
              </center>
            </div>           
              <div class="form-group">
                <div id="error_modal_parameters" align='center'>
                <!-- error will be shown here ! -->
                </div>
              </div>
              
              <div id="bodyParameters">
              </div>
         </div>
        <div class="modal-footer">
            <input type="file" name="new_img" id = 'new_img_modal' class='hidden'>    
            <button type="button" class="btn btn-success" onclick="putSysparameters()" id="putSysparametersId">Guardar</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal" href="javascript:void(0)">Cancelar</button> 
        </div>
        </form>
      </div>
    </div>
  </div>
  @include('layouts.sidebar',['some' => 'data'])

  <!-- Page -->

  @yield('content')

  <!-- End Page -->
  <!-- Footer -->
  <footer class="site-footer">
    <div class="site-footer-legal">© 2017 <a href="">Prontuz</a></div>
    <div class="site-footer-right">
      Design & Powered by <a target="_black" class="font-white" href="http://crizz.com.co"><i><u>CRIZZ</u></i></a>
    </div>
  </footer>
  
  <!-- Plugins -->
  <script src="{{ asset('iconbar/global/vendor/switchery/switchery.min.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/intro-js/intro.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/screenfull/screenfull.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/slidepanel/jquery-slidePanel.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/aspaginator/jquery.asPaginator.min.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/skycons/skycons.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/asprogress/jquery-asProgress.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/draggabilly/draggabilly.pkgd.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/raty/jquery.raty.js') }}"></script>
  

  <script src="{{ asset('iconbar/global/vendor/datatables/jquery.dataTables.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/datatables-buttons/dataTables.buttons.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/datatables-buttons/buttons.html5.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/datatables-buttons/buttons.flash.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/datatables-buttons/buttons.print.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/asrange/jquery-asRange.min.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/bootbox/bootbox.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/formatter/jquery.formatter.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/select2/select2.full.min.js') }}"></script>


  <!-- Scripts -->
  <script src="{{ asset('iconbar/global/js/State.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Component.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Base.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Config.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/Section/Menubar.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/Section/Sidebar.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/Section/PageAside.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/Plugin/menu.js') }}"></script>
  <!-- Config -->
  <script src="{{ asset('iconbar/global/js/config/colors.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/config/tour.js') }}"></script>
  <script>
  Config.set('assets', 'assets');
  </script>
  <!-- Page -->
  <script src="{{ asset('iconbar/assets/js/Site.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/asscrollable.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/slidepanel.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/switchery.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/sticky-header.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/action-btn.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/asselectable.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/editlist.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/aspaginator.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/animate-list.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/jquery-placeholder.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/material.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/selectable.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/bootbox.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/BaseApp.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/App/Contacts.js') }}"></script>
  <script src="{{ asset('iconbar/assets/examples/js/apps/contacts.js') }}"></script>

  <script src="{{ asset('iconbar/global/js/Plugin/responsive-tabs.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/tabs.js') }}"></script>

  <script src="{{ asset('iconbar/global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.min.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>

  <script src="{{ asset('iconbar/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/bootstrap-datepicker/bootstrap-datepicker.es.min.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/magnific-popup.js') }}"></script>

  <script src="{{ asset('iconbar/global/vendor/formvalidation/formValidation.min.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/formvalidation/framework/bootstrap4.min.js') }}"></script>

  <script src="{{ asset('iconbar/global/js/Plugin/select2.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/bootstrap-datepicker.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/bootstrap-sweetalert/sweetalert.js') }}"></script>

  <script src="{{ asset('iconbar/global/vendor/datatables/jquery.dataTables.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/datatables.js') }}"></script>
  <script src="{{ asset('js/datables_personalized.js') }}"></script>




  <script type="text/javascript">
    if (window.location.host == 'localhost') {
            var baseurl = window.location.protocol +"//" + window.location.host + '/prontuz_backend/public';
        }else{
            var baseurl = window.location.protocol +"//" + window.location.host ;
        }
    (function(document, window, $) {
      'use strict';

      var Site = window.Site;

      $(document).ready(function($) {
        Site.run();

        $("#logOut").click(function() {
            var userId = $("#userId").val();
            $.ajax({
              type: 'GET',
              url: baseurl+'/logOut',
              data: {userId: userId, typePlataform: 'web', user_rolId:1},
              dataType: 'JSON',
              success: function (response) {

                if(response.ResponseCode != 0){
                  
                  window.location.href = baseurl+'/login';
                
                }else{
                  alert(response.ResponseMessage); 
                }
                  

              },
              error: function (response) {
                  alert('Error de comunicación, revise su conexion a internet.'); 
              }
            });


            return false;
        });



      });
    })(document, window, jQuery);
    $("#btn-perfil").click(function() {
        $("#newpassword").val('');
        $("#renewpassword").val('');
        $('#profile').modal('show');
    });

    /**********************************************/

    $("#btn-sysparameters").click(function() {
        bringParameters();
        setTimeout(function(){
          $('#sysparameters').modal('show');
        },1000);
    });

    /**********************************************/

    function bringParameters(){

      $('#bodyParameters').empty();
      $('#error_modal_parameters').hide();

      var data= {_token: $("input[name*='_token']").val()};
    
      $.ajax({
            type: "POST",
            url: baseurl+'/user/bringParameters',
            data: data,
            dataType: 'JSON',
            success: function (data) {

              console.log(data);

              if(data.ResponseCode == 1){
                
                var add='';

                $.each(data.ResponseData ,function(k, v){
                  sysparametersTitle=v.sysparametersTitle;
                  sysparametersName=v.sysparametersName;
                  sysparametersValue=v.sysparametersValue;
            
                  add += '<div class="form-group form-material floating" data-plugin="formMaterial">'+
                            '<label class="label"><b>'+sysparametersTitle+'</b></label>'+
                            '<input required="required" class="form-control empty" id="'+sysparametersName+'" name="'+sysparametersName+'" type="text" value="'+sysparametersValue+'" />'+
                          '</div>';
                });

                $('#bodyParameters').append(add);
                
                
                
              }else if(data.ResponseCode == 0){
                $('#error_modal_parameters').html(data.ResponseMessage);
                $('#error_modal_parameters').show();
              }
           
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
      }

    /**********************************************/

      function putSysparameters(){

        $('#putSysparametersId').attr('type','submit');

        $("form#FormParameters").submit(function() {
      
          var formData = new FormData($(this)[0]);
          
           $.ajax({
              type: $(this).attr("method"),
              url: $(this).attr("action"),
              data: $(this).serialize(),
              dataType: 'JSON',
              beforeSend: function(){ 
                $("#error").fadeOut();
                $("#data").fadeOut();
                $("#loader").fadeIn();
              },
              success: function (response) {
                $("#loader").fadeOut();
                if(response.ResponseCode == 0){

                  $('#sysparameters').modal('hide');
                  swal({
                    title: "Error!",
                    text: "No se pudo realizar la operación!",
                    type: "danger",
                    timer: 1000,
                    showConfirmButton: false
                  });
                  setTimeout(function () {
                    location.reload()
                  },1500);
                
                }else{
                  $('#sysparameters').modal('hide');
                  swal({
                    title: "ÉXITO!",
                    text: "Operación realizada!",
                    type: "success",
                    timer: 1000,
                    showConfirmButton: false
                  });
                  setTimeout(function () {
                    location.reload()
                  },1500);
                }

              },
              error: function (response) {

                  //do_lalert(true,'Error de comunicación, revise su conexion a internet.');
              }
            });

          return false;
        });
      }

    /**********************************************/

        function sendPassword() 
        {
            if($("#newpassword").val() === $("#renewpassword").val())
            {
                swal({
                    title: "CONFIRMAR",
                    text: "Esta segura de realizar la operación?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#2A67BD",
                    confirmButtonText: "Confirmar",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    showLoaderOnConfirm: true
                },
                function(isConfirm){
                    if (isConfirm)
                    {
                      $.ajax({
                          type: "POST",
                          url: baseurl+'/users/sendPassword',
                          data: {password: $("#newpassword").val(),  userId:$('#userId').val(),_token: $("input[name*='_token']").val()},
                          dataType: 'JSON',
                          success: function (resp) {
                              var data = resp;
                              if (data.ResponseCode > 0) {
                                $("#newpassword").val('');
                                $("#renewpassword").val('');
                                $('#profile').modal('hide');
                                swal({
                                      title: "ÉXITO!",
                                      text: "Operación Exitosa!",
                                      type: "success",
                                      timer: 1000,
                                      showConfirmButton: false
                                    });
                              }else{
                                $("#error").fadeOut();
                                $("#error").html('');
                              }

                          },
                          error: function (jqXHR, textStatus, errorThrow) {
                            if(jqXHR.status===500)
                            {
                                swal("Error!","ERROR INTERNO DEL SERVIDOR",'error');
                            }                 
                            if(jqXHR.status===401)
                            {
                                $("#logOut").click();
                            }
                            if(jqXHR.status===404)
                            {
                                swal("Error!","RUTA NO ENCONTRADA (404 NOT FOUND)",'error');
                            }
                          }
                        });
                    }
                    else
                    {
                       swal({
                                      title: "ÉXITO!",
                                      text: "Operación Cancelada!",
                                      type: "success",
                                      timer: 1000,
                                      showConfirmButton: false
                                    });
                    } 
                });           
            }
            else
            {
                swal("Error!", 'Las Contraseñas no coincide', 'error');
            }
        }
    $('#img_modal_user').click(function(event) {
           $('#new_img_modal').click();
        });
    $('#new_img_modal').change(function()
    {
         var reader = new FileReader();
        reader.onload = function (e) 
        {
            $('#img_modal_user').attr('src', e.target.result);
        }
        reader.readAsDataURL(this.files[0]);
        var formData = new FormData($("#FormProfile")[0]);
        $('#loader_modal').show();
        $.ajax({
            type: 'POST',
            url: 'users/updatePhoto',
            data: formData,
            contentType: false,
            processData: false,
            success: function(datos)
            {
                $('#loader_modal').hide();
                data = $.parseJSON(datos);
               if(parseInt(data.ResponseCode) ===1)
               {
                    $("#error_modal").fadeIn(1000, function()
                    {      
                        $("#error_modal").html('<div role="alert" class="alert dark alert-success alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> '+data.ResponseMessage+'</div>');
                    });                  
               }
               else
               {
                    $("#error_modal").fadeIn(1000, function()
                    {      
                        $("#error_modal").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> '+data.ResponseMessage+'</div>');
                    });
               }
            },
            error: function (jqXHR, textStatus, errorThrow) 
            {
                $('#loader_modal').hide();
                if(jqXHR.status===500)
                {
                    swal("Error!","ERROR INTERNO DEL SERVIDOR",'error');
                }                 
                if(jqXHR.status===401)
                {
                    $("#logOut").click();
                }
                if(jqXHR.status===404)
                {
                    swal("Error!","RUTA NO ENCONTRADA (404 NOT FOUND)",'error');
                }
            }
        });          
    });
  </script>
  <script src="{{ asset('iconbar/assets/examples/js/tables/datatable.js') }}"></script>
  <script src="{{ asset('classic/global/vendor/moment/moment.min.js') }}"></script>
<script src="{{ asset('classic/global/vendor/footable/footable.min.js') }}"></script>
<!-- <script src="{{ asset('classic/global/js/Plugin/switchery.js') }}"></script> -->
<script src="{{ asset('classic/center/assets/examples/js/tables/footable.js') }}"></script>
  

</body>
</html>