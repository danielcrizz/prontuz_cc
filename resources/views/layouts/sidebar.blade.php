@if(Auth::check())
<div class="site-menubar site-menubar-light">
    <div class="site-menubar-body">
      <ul class="site-menu" data-plugin="menu">
              
        @foreach(Session::get('userMenu') as $item)
          <li class="site-menu-item">
            <a href="{!! url($item['menuPrefix']) !!}">
              <i class="site-menu-icon {{ $item['menuIcon'] }}" aria-hidden="true"></i>
              <span class="site-menu-title">{{ $item['menuName'] }}</span>
            </a>
          </li>
 
        @endforeach


        <!-- <li class="site-menu-item">
          <a href="javascript:void(0)">
            <i class="site-menu-icon ion-android-car" aria-hidden="true"></i>
            <span class="site-menu-title">Wheels</span>
          </a>
        </li>  -->
      </ul>
    </div>
  </div>
  @else
  <script>window.location.href = '/login';</script>
  @endif