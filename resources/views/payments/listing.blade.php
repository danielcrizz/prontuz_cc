@extends('layouts.app')

@section('content')

{{ csrf_field() }}
<meta charset="utf-8">
<style>
    #sec_list_points{
      height: 230px !important;
      overflow-y: scroll !important;
      overflow-x: hidden !important;
    }
    #sec_list_points2{
      height: 230px !important;
      overflow-y: scroll !important;
      overflow-x: hidden !important;
    }
   
   
</style>

<div class="page bg-white">
    <div class="page-aside">
      <!-- Contacts Sidebar -->
      <div class="page-aside-switch">
        <i class="icon wb-chevron-left" aria-hidden="true"></i>
        <i class="icon wb-chevron-right" aria-hidden="true"></i>
      </div>
      <div class="page-aside-inner page-aside-scroll">
        <div data-role="container">
          <div data-role="content">
            <div class="page-aside-section">
              <div class="list-group">
                <a class="list-group-item justify-content-between" id="listAllUsers" href="javascript:void(0)">
                  <span>
                    <i class="icon wb-user" aria-hidden="true" id="cusers"></i> Conductores Wheel
                  </span>
                  <span class="item-right" id="totalUsers"></span>
                </a>
              </div>
            </div>
            <div class="">
              <section id="sec_list_points">
                <h5 class="page-aside-title">Estadisticas</h5>
                <div class="list-group" id="statistics">
                </div>
              </section>
            </div>
            
          </div>
        </div>
      </div>
    </div>
    <!-- Contacts Content -->
    <div class="page-main" style="padding:20px;">
      <!-- Contacts Content Header -->
      <div class="page-header">
        <h1 class="page-title">Wheels y pagos</h1>
      </div>
      <div class="panel">
        <div class="panel-body">
          <table class="table table-hover toggle-circle" id="exampleNoHeaders" data-show-header="false"
          data-paging="true" data-filtering="true" data-page-size="4">
          </table>
        </div>
      </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="modalPay" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
             <h4 class="modal-title">Pago de salgo </h4>
          </div>
          <div class="modal-body">
              <div class="alert alert-danger alert-dismissible" role="alert" id="errorAlert" style="display:none">
              </div>
            <div class="row" style="margin-left: 10px; margin-right: 10px;">
              <div class="col-md-12 col-xs-12">
                <div class="form-group form-material floating" data-plugin="formMaterial">
                  <label class="label">Usuario Wheel:</label>
                  <input id="userName" name="userName" class="form-control empty" readonly="readonly" />
                </div>
                <div class="form-group form-material floating" data-plugin="formMaterial">
                  <label class="label">Pago pendiente:</label>
                  <input id="paymentPay" name="paymentPay" class="form-control empty" readonly="readonly" />
                </div>
                <input id="payment_wheel_userId" name="payment_wheel_userId" type="hidden" />
              </div>
            </div>
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="doClear()">Cancelar</button>
            <button type="button" class="btn btn-primary" onclick="doPay()" id="doPayId">Pagar</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="modalHistory" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
             <h4 class="modal-title">Historico de viajes</h4>
          </div>
          <div class="modal-body">
              <div class="alert alert-danger alert-dismissible" role="alert" id="errorAlert" style="display:none">
              </div>
              <table id='pay_list' width="98%" class="table is-indent" data-plugin="animateList" data-animate="fade" data-child="tr"
        data-selectable="selectable">
                <thead>
                  <tr>
                    <th class="cell-100" scope="col">Fecha transacción</th>
                    <th class="cell-150" scope="col">Usuario</th>
                    <th class="cell-100" scope="col">Direccion</th>
                    <th class="cell-100" scope="col">Valor</th>
                    <th class="cell-100" scope="col">Estado</th>
                    
                    </tr>
                </thead>
                <tbody id="bodytable">
                  <div id="loaderlist" style="display:none;">
                  <center>
                      <br><br><br><br>
                       <img width="48px" src="{{ asset('iconbar/loading.gif') }}" /> &nbsp; Cargando información ...
                      <br><br>
                  </center>
                </div>
                </tbody>
              </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="doClear()">Cerrar</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  </div>
  <div class="site-action">
    <button type="button" id="actionBtn" class="site-action-toggle btn-raised btn btn-success btn-floating">
      <i class="front-icon wb-plus animation-scale-up" aria-hidden="true"></i>
      <i class="back-icon wb-close animation-scale-up" aria-hidden="true"></i>
    </button>
    <div class="site-action-buttons">
      <button type="button" data-action="trash" class="btn-raised btn btn-success btn-floating animation-slide-bottom">
        <i class="icon wb-trash" aria-hidden="true"></i>
      </button>
      <button type="button" data-action="folder" class="btn-raised btn btn-success btn-floating animation-slide-bottom">
        <i class="icon wb-folder" aria-hidden="true"></i>
      </button>
    </div>
  </div>

  <script type="text/javascript">
    $(document).ready(function() {
      // $('#crt_recharge').modal('hide');
      $('#modalPay').hide();
      token = $("input[name*='_token']").val();
      recharge_userId=0;
      doClear();
      getRusers();
      print_footable();
      getRallUsers();
      sumTransactions();
       
    });

/**************************************************/

    function getRusers(){

       $.ajax({
          type: "POST",
          url: baseurl+'/payments/getRusers',
          data: {_token: token},
          dataType: 'JSON',
          success: function (data) {
            $('#totalUsers').html(data);
          },
          error: function (data) {
              console.log('Error:', data);
          }
        });
    }

/**************************************************/

    function print_footable(){
        $.ajax({
          type: "POST",
          url: baseurl+'/payments/getWheelinfo',
          data: {_token: token},
          dataType: 'JSON',
          success: function (data) {

          var columns=[
            { "name": "userId", "visible": false },
            { "name": "FullName", "title": "Wheel" },
            { "name": "vehiclePlaque", "title": "Placa" },
            { "name": "Balance", "title": "Saldo" },
            { "name": "Utility", "title": "Utilidad", "breakpoints": "xs" },
            { "name": "Income", "title": "ingreso", "breakpoints": "xs sm" },
            { "name": "History", "title": "historico", "breakpoints": "xs sm" },
            { "name": "pay", "title": "pagar", "breakpoints": "xs sm" },
            { "name": "UserData", "title": " ", "style": "width:98%","breakpoints": "all" }
          
          ];

            var rows=[];
            var i=0;

            $.each(data,function(k, v){

            var subrows=[];

            var add='<table style="width:100%;" class="table table-hover">';
            var userName='';

              $.each(v.data,function(k1, v1){

                userName=v1.UserFullName;
                add+='<tr><td>Usuario :'+v1.UserFullName+'</td><td>Fecha :'+v1.date +'</td></tr>';
              });
              add+='</table>';
              //
              if(v.Balance == null){
                v.Balance=0;
              }
              
              rows[i]={ "userId": v.userId, "FullName": v.FullName, "vehiclePlaque": 'Placa: '+v.vehiclePlaque, "Balance": 'Saldo: '+formatThousands(parseInt(v.Balance)), "Utility": 'Utilidad: '+formatThousands(parseInt(v.Utility)),"Income": 'Ingresos: '+formatThousands(parseInt(v.Income)),"History": '<button type="button" class="btn btn-icon btn-info btn-round" onclick="showHistory('+v.userId +')"><i class="icon wb-clipboard" aria-hidden="true"></i></button>',"pay": '<button type="button" class="btn btn-icon btn-success btn-round" onclick="showPay('+v.userId+',\''+userName+'\','+v.Balance+')"><i class="icon wb-check-circle" aria-hidden="true"></i></button>', "UserData": add};
              i++;
            });

     
             $('#exampleNoHeaders').footable({
              "paging": {
                "enabled": true,
                "container": "#paging-ui-container",
                "current": 1,
                "size": 15
              },
              "expandFirst": false,
              "columns": columns,
              "rows": rows
            });
          },
          error: function (data) {
              console.log('Error:', data);
          }
        });
    }
    
/**************************************************/

function showHistory(userId){
  $('#modalHistory').modal('show');

    tableName='pay_list';
    urlName='/payments/listing?userId='+userId;
    columnsName=[
            { data: 'date'},
            { data: 'UserFullName'},
            { data: 'routeDirection2'},
            { data: 'reservePrice',
                      "render" : function (data) {
                      return formatThousands(parseInt(data));
                      }},
            { data: "reserveState",
                    "render" : function ( data) {
                          if(data == 5)
                          {
                              return '<span class="badge badge-outline badge-success">Cancelado</span>';
                          }
                          else
                          {
                              return '<span class="badge badge-outline badge-danger">Pendiente</span>';
                          }
                      }}
          ];
    list_datatable(tableName,urlName,columnsName);
}
/**************************************************/

function showPay(userId,userName,Balance){

  if(Balance==0){
      swal({
        title: "Sin saldo pendiente",
        type: "warning",
        timer: 2500,
        showConfirmButton: false
      }); 
  }else{
     $('#userName').val(userName);
    $('#paymentPay').val(Balance);
    $('#payment_wheel_userId').val(userId);
    $('#modalPay').modal('show');
  }
  
}
//modalPay
/**************************************************/
  
  $('#actionBtn').click(function(){
      $('#crt_recharge').modal('show');
  });

/**************************************************/

function getRallUsers(){

  $.ajax({
      type: "POST",
      url: baseurl+'/recharge/getRallUsers',
      data: {_token: token},
      dataType: 'JSON',
      success: function (data) {
          
          var toAppend = '';
          $.each(data,function(k, v){
            
              toAppend += '<option val2="'+v.userId+'" value="'+v.userFirstname+' '+v.userLastname+'">'+v.userFirstname+' '+v.userLastname+'</option>';
          });

           $('#recharge_userId').append('<select>'+toAppend+'</select>');
           $('#recharge_userId').trigger("select2:updated");


      },
      error: function (data) {
          console.log('Error:', data);
      }
  });
}

/**************************************************/

function doClear(){

  $('#rechargeValue').find($('option')).attr('selected',false)
  $('#recharge_userIdI').val('');
  $('#rechargeValue').val('');
  $('#recharge_info').empty();
  turnOffAlert();
}

/**************************************************/

function doPay(){
  
  var paymentPay=$('#paymentPay').val();
  var payment_wheel_userId=$('#payment_wheel_userId').val();
  var data= {_token: token,paymentPay:paymentPay,payment_wheel_userId:payment_wheel_userId};
    
    $.ajax({
          type: "POST",
          url: baseurl+'/payments/store',
          data: data,
          dataType: 'JSON',
          success: function (data) {

            if(data.ResponseCode == 0){

              printAlert(data.ResponseMessage);

              setTimeout(function(){ 
                $('#modalPay').modal('hide');
              }, 3000);
              
              
              
            }else if(data.ResponseCode == 1){
              $('#modalPay').modal('hide');
              swal({
                title: "ÉXITO!",
                text: "Operación Exitosa!",
                type: "success",
                timer: 2500,
                showConfirmButton: false
              }); 
              print_footable();
            }
         
          },
          error: function (data) {
              console.log('Error:', data);
          }
      });

  
}
/**************************************************/

function sumTransactions(){
  $('#statistics').empty();
  $.ajax({
      type: "POST",
      url: baseurl+'/payments/statistics',
      data: {_token: token},
      dataType: 'JSON',
      success: function (data) {

        var add = '';
        var add2 = '';
        
            
          add += '<a class="list-group-item justify-content-between" href="javascript:void(0)">'+
              '<span class="item-right" id="numAdmin" style="font-size:18px;">$'+formatThousands(parseInt(data.Income))+'</span>'+
              '<span>Ingresos</span>'+
            '</a>';

          add += '<a class="list-group-item justify-content-between" href="javascript:void(0)">'+
              '<span class="item-right" id="numAdmin" style="font-size:18px;">$'+formatThousands(parseInt(data.Utility))+'</span>'+
              '<span >Utilidad</span>'+
            '</a>';

          add += '<a class="list-group-item justify-content-between" href="javascript:void(0)">'+
              '<span class="item-right" id="numAdmin" style="font-size:18px;">$'+formatThousands(parseInt(data.Balance))+'</span>'+
              '<span ">Saldo</span>'+
            '</a>';
    

           $('#statistics').append(add);
           
      },
      error: function (data) {
          console.log('Error:', data);
      }
  });
}
/**************************************************/

$("#rechargeValue").on({
  "focus": function(event) {
    $(event.target).select();
  },
  "keyup": function(event) {
    $(event.target).val(function(index, value) {
      return value.replace(/\D/g, "")
        .replace(/([0-9])([0-9]{0})$/, '$1$2')
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
    });
  }
});
/**************************************************/
function formatThousands(n, dp) {
  var s = ''+(Math.floor(n)), d = n % 1, i = s.length, r = '';
  while ( (i -= 3) > 0 ) { r = '.' + s.substr(i, 3) + r; }
  return s.substr(0, i + 3) + r + (d ? ',' + Math.round(d * Math.pow(10,dp||2)) : '');
}

/**************************************************/
  function printAlert(messg){
    $('#errorAlert').text(messg);
    $('#errorAlert').show();
    //$('#doRechargeId').attr('disabled',true);
  }

/**************************************************/

function turnOffAlert(){
    $('#errorAlert').text('');
    $('#errorAlert').hide();
    $('#doRechargeId').attr('disabled',false);

}
/**************************************************/

  </script>


@endsection
