<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap admin template">
  <meta name="author" content="">
  <title>404 | Prontuz U</title>
  <link rel="apple-touch-icon" href="{{ asset('iconbar/assets/images/apple-touch-icon.png') }}">
  <link rel="shortcut icon" href="{{ asset('iconbar/assets/images/favicon.ico') }}">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="{{ asset('iconbar/global/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/css/bootstrap-extend.min.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/assets/css/site.min.css') }}">
  <!-- Plugins -->
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/animsition/animsition.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/asscrollable/asScrollable.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/switchery/switchery.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/intro-js/introjs.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/slidepanel/slidePanel.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/flag-icon-css/flag-icon.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/assets/examples/css/pages/errors.css') }}">
  <!-- Fonts -->
  <link rel="stylesheet" href="{{ asset('iconbar/global/fonts/web-icons/web-icons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/fonts/brand-icons/brand-icons.min.css') }}">

  <style type="text/css">

    @font-face {
      font-family: 'Prometo';
      src: url('{{ asset('iconbar/global/fonts/Prometo Regular.otf') }}');
    }

    body {
        font-family: Prometo !important;
        font-weight: 500 !important;
    }
  </style>
  <!--[if lt IE 9]>
    <script src="{{ asset('iconbar/global/vendor/html5shiv/html5shiv.min.js') }}"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="{{ asset('iconbar/global/vendor/media-match/media.match.min.js') }}"></script>
    <script src="{{ asset('iconbar/global/vendor/respond/respond.min.js') }}"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="{{ asset('iconbar/global/vendor/breakpoints/breakpoints.js') }}"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition page-error page-error-404 layout-full">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <!-- Page -->
  <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
    <div class="page-content vertical-align-middle">
      <header>
        <h1 class="animation-slide-top">404</h1>
        <p>Acceso no autorizado !</p>
      </header>
      <p class="error-advise">Acceso no autorizado, o su sesion a finalizado intente iniciando sesion nuevamente</p>
      <a class="btn btn-primary btn-round" href="{!! url('login') !!}">IR A LOGIN</a>
      <footer class="page-copyright">
        <p>PRONTUZ U</p>
        <p>© 2017. All RIGHT RESERVED.</p>
        <div class="social">
          <a href="javascript:void(0)">
            <i class="icon bd-twitter" aria-hidden="true"></i>
          </a>
          <a href="javascript:void(0)">
            <i class="icon bd-facebook" aria-hidden="true"></i>
          </a>
          <a href="javascript:void(0)">
            <i class="icon bd-dribbble" aria-hidden="true"></i>
          </a>
        </div>
      </footer>
    </div>
  </div>
  <!-- End Page -->
  <!-- Core  -->
  <script src="{{ asset('iconbar/global/vendor/babel-external-helpers/babel-external-helpers.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/jquery/jquery.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/tether/tether.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/bootstrap/bootstrap.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/animsition/animsition.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/mousewheel/jquery.mousewheel.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/asscrollbar/jquery-asScrollbar.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/asscrollable/jquery-asScrollable.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/ashoverscroll/jquery-asHoverScroll.js') }}"></script>
  <!-- Plugins -->
  <script src="{{ asset('iconbar/global/vendor/switchery/switchery.min.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/intro-js/intro.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/screenfull/screenfull.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/slidepanel/jquery-slidePanel.js') }}"></script>
  <!-- Scripts -->
  <script src="{{ asset('iconbar/global/js/State.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Component.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Base.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Config.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/Section/Menubar.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/Section/Sidebar.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/Section/PageAside.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/Plugin/menu.js') }}"></script>
  <!-- Config -->
  <script src="{{ asset('iconbar/global/js/config/colors.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/config/tour.js') }}"></script>
  <script>
  Config.set('assets', 'assets');
  </script>
  <!-- Page -->
  <script src="{{ asset('iconbar/assets/js/Site.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/asscrollable.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/slidepanel.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/switchery.js') }}"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
    });
  })(document, window, jQuery);
  </script>
</body>
</html>