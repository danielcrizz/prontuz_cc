<div class="slidePanel slidePanel-right slidePanel-show" style="transform: translate3d(0%, 0px, 0px); width: 550px;" id="userSlidePanel">
    <div class="slidePanel-scrollable scrollable is-enabled scrollable-vertical" style="position: relative;">
        <div class="scrollable-container" style="height: 96%; width: 550px;">
            <div class="slidePanel-content scrollable-content" style="width: 535px;">
                <form id="dataEdit" method="POST" action="{{ url('offers/put') }}">
                    <header class="slidePanel-header overlay" style="height: 200px; background-size: 545px 300px; background-image: url('iconbar/global/photos/placeholder.jpg');')">
                        
                        {{ csrf_field() }}
                          <div class="overlay-panel overlay-background vertical-align">
                            <div class="form-group" style="margin-top:-140px;"> 
                                <center>
                                  <label for="offerImageEdit">
                                      <img id="imgperfilEdit" src="{{ asset('iconbar/assets/images/006-meta-A.jpg') }}" style="border-radius: 150px; width: 150px; height: 150px; background-repeat: no-repeat; background-position: 50%; border-radius: 50%; background-size: 100% auto; border:1px solid #999;" />
                                  </label>
                                  <input type="file" name="offerImageEdit" id="offerImageEdit" style="display:none"/>
                                  <p>* Clic para cargar imagen</p>
                                </center>
                            </div>
                            <div class="slidePanel-actions">
                              <div class="btn-group">
                                <!--button type="button" id="deleteUsers" class="btn btn-pure btn-inverse icon wb-trash" aria-hidden="true"></button-->
                                <button type="button" class="btn btn-pure btn-inverse slidePanel-close icon wb-close"
                                  aria-hidden="true" onclick="closeSlide()"></button>
                            </div>
                            </div>    
                          </div>
                        
                    </header>
                    <center>
                        <div class="loader loader-default" id="charging" style="margin-top: 150px;"></div>
                    </center>
                    
                    <div class="slidePanel-inner" id="slideBody" style="display:none" >
                        <br>
                        <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closeSlide()">
                                <span aria-hidden="true">×</span>
                              </button>
                              <h4 class="modal-title">Editar Datos de la oferta</h4>
                        </div>
                        
                        <input type="hidden" id="offerIdEdit" name="offerIdEdit">
                        <input type="hidden" id="offerAvailableEdit" name="offerAvailableEdit">
                        <input type="hidden" id="offerQuantity2Edit" name="offerQuantity2Edit">
                        <div class="form-group">
                            <div id="errorEdit">
                            <!-- error will be shown here ! -->
                            </div>
                        </div>
                        <div class="form-group form-material floating col-xl-8" data-plugin="formMaterial">
                            <label class="label" >Titulo</label>
                            <input type="text" required class="form-control" id="offerTitleEdit" name="offerTitleEdit"/>
                        </div>
                        <div class="form-group form-material floating col-xl-8" data-plugin="formMaterial">
                            <label class="label" >Empresa</label>
                          <input type="text" required class="form-control" id="offerCompanyEdit" name="offerCompanyEdit"/>
                          
                        </div>
                        <div class="form-group form-material floating col-xl-8" data-plugin="formMaterial">
                            <label class="label" >Contenido</label>
                            <textarea rows="4" cols="50" required class="form-control" id="offerContentEdit" name="offerContentEdit" style="height:150px !important"></textarea>
                            <!-- <label class="label" >Contenido</label>
                             <input type="text" required class="form-control" id="offerContentEdit" name="offerContentEdit"/> -->
                        </div>
                        <div class="form-group form-material floating col-xl-8" data-plugin="formMaterial">
                            <label class="label" >Email</label>
                            <input type="email" required class="form-control" id="offerEmailEdit" name="offerEmailEdit"/>
                        </div>
                        <div class="form-group form-material floating col-xl-8" data-plugin="formMaterial">
                            <label class="label" >Cantidad disponible</label>
                            <input type="text" required class="form-control" id="offerQuantityEdit" name="offerQuantityEdit" onchange="validateAvailable(this.value)"/>
                        </div>
                        <div class="form-group form-material floating col-xl-8" data-plugin="formMaterial">
                            <label class="label" >Valor</label>
                            <input type="text" required class="form-control" id="offerValueEdit" name="offerValueEdit"/>
                        </div>
                        <div class="form-group form-material floating col-xl-8">
                            <label class="label" >Estado</label>
                            <select required  class="form-control form-select"  id="offerStateEdit" name="offerStateEdit">
                                <option value="0">Inactivo</option>
                                <option value="1">Activo</option>
                            </select>
                        </div>
                        <hr>
                        <center>
                            <button id="guardar" class="btn btn-animate btn-animate-vertical btn-success" type="button" onclick="savePut()">
                                <span>
                                <i class="icon wb-download" aria-hidden="true"></i>
                                Actualizar Cambios
                                </span>
                            </button>
                        </center>
                        
                        <div class="loaderSend" style="display: none;">
                            <center>
                                 <img src="{{ asset('iconbar/loading.gif') }}" width="48px"> &nbsp; Cargando ...
                            </center>
                        </div>
                        <div id="loaderEdit" style="display:none;">
                            <center>
                                <br><br>
                                 <img src="{{ asset('iconbar/loading.gif') }}" width="48px"> &nbsp; Enviando ...
                                <br><br>
                            </center>
                        </div>
                    </div>
                </form>
            </div>
        </div>
          <div class="scrollable-bar scrollable-bar-vertical is-disabled scrollable-bar-hide" draggable="false">
          <div class="scrollable-bar-handle">
          </div>
          </div>
    </div>
    <div class="slidePanel-handler"></div>
    <div class="slidePanel-loading">
        <div class="loader loader-default"></div>
    </div>
</div>