@extends('layouts.app')

@section('content')
  <link rel="stylesheet" href="{{ asset('classic/global/vendor/webui-popover/webui-popover.css') }}">
  <link rel="stylesheet" href="{{ asset('classic/global/vendor/toolbar/toolbar.css') }}">

  <script src="{{ asset('classic/global/vendor/webui-popover/jquery.webui-popover.min.js') }}"></script>
  <script src="{{ asset('classic/global/vendor/toolbar/jquery.toolbar.js') }}"></script>
  <script src="{{ asset('classic/global/js/Plugin/toolbar.js') }}"></script>
 

  <div class="page">
    <div class="page-content">
      <!-- Panel -->
      <div class="panel">
        <div class="panel-body">
          <div class="nav-tabs-horizontal nav-tabs-animate" data-plugin="tabs">
          <!-- Contacts Content -->
          <div id="contactsContent" class="page-content page-content-table" data-plugin="selectable">
            <!-- Contacts -->
            <table id='offer_list' width="100%" class="table is-indent" data-plugin="animateList" data-animate="fade" data-child="tr"
            data-selectable="selectable">
              <thead>
                <tr>
                  <th width="25%" scope="col">Todas las ofertas</th>
                  <th width="50%" scope="col"></th>
                  <th width="15%" scope="col"></th>
                  <th width="10%" scope="col"></th>
                </tr>
              </thead>
             </table>
            
          </div>
         
          </div>
        </div>
      </div>
      <!-- End Panel -->
    </div>
  </div>


  <!-- Site Action -->
  <div class="site-action" data-plugin="actionBtn">
    <button type="button" class="btn-raised btn btn-success btn-floating" onclick="openOpt()" value="0">
      <i class="front-icon wb-plus animation-scale-up" aria-hidden="true"></i>
      <i class="back-icon wb-close animation-scale-up" aria-hidden="true"></i>
    </button>
    <div class="site-action-buttons" id="Opt">
      <button type="button" data-action="sliders" class="btn-raised btn btn-success btn-floating animation-slide-bottom" onclick="openModal2()">
        <i class="icon ion-android-options" aria-hidden="true" style="font-size: 30px;"></i>
      </button>
      <button type="button" data-action="bus" class="btn-raised btn btn-success btn-floating animation-slide-bottom" onclick="openModal()">
        <i class="icon ion-android-bus" aria-hidden="true" style="font-size: 30px;"></i>
      </button>
    </div>
  </div>
  <!-- End Site Action -->


  <!-- Add User Form -->
  <div class="modal fade modal-3d-flip-vertical" id="alertModal"  style="display:none" role="dialog" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
          <h4 class="modal-title">Crear Nuevo Vehiculo</h4>
        </div>
        <div class="modal-body">
          <div class="alert dark alert-icon alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
              <i class="icon wb-check" aria-hidden="true"></i>Creaccion exitosa
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade modal-3d-flip-vertical" id="addUserForm"  style="display:none" role="dialog" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
          <h4 class="modal-title">Crear Nueva Oferta</h4>
        </div>
        <div class="modal-body">

        <div id="loader" style="display:none;">
          <center>
              <br><br>
               <img width="48px" src="{{ asset('iconbar/loading.gif') }}" /> &nbsp; Enviando ...
              <br><br>
          </center>
        </div>

        <form id="data" method="POST" action="{{ url('offers/store') }}" >
            <div class="form-group">
                <div id="errorOffer">
                <!-- error will be shown here ! -->
                </div>
            </div>
          {{ csrf_field() }}
            <div class="form-group"> 
              <center>
                <label for="offerImage">
                    <img id="imgoffer" src="{{ asset('iconbar/assets/images/006-meta-A.jpg') }}" style="border-radius: 150px; width: 150px; height: 150px; background-repeat: no-repeat; background-position: 50%; border-radius: 50%; background-size: 100% auto; border:1px solid #999;" />
                </label>
                <input type="file" name="offerImage" id="offerImage" style="display:none" required/>
                <p>* Clic para cargar imagen</p>
              </center>
            </div>
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="text" required class="form-control" id="offerTitle" name="offerTitle"/>
              <label class="floating-label" >Titulo</label>
            </div>
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="text" required class="form-control" id="offerCompany" name="offerCompany"/>
              <label class="floating-label" >Empresa</label>
            </div>
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <label class="label" >Contenido</label>
              <textarea rows="4" cols="50" required class="form-control" id="offerContent" name="offerContent" style="height:150px !important"></textarea>

            <!--   <input type="text" required class="form-control" id="offerContent" name="offerContent"/>
              <label class="floating-label" >Contenido</label> -->
            </div>
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="email" required class="form-control" id="offerEmail" name="offerEmail"/>
              <label class="floating-label" >Email</label>
            </div>
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="text" required class="form-control" id="offerQuantity" name="offerQuantity"/>
              <label class="floating-label" >Cantidad disponible</label>
            </div>
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="text" required class="form-control" id="offerValue" name="offerValue"/>
              <label class="floating-label" >Valor</label>
            </div>
            <input type="hidden" name="offerAvailable" value="0">
            <input type="hidden" name="offerState" value="1">
            <br>
            <div class="form-group col-xl-12 text-right padding-top-m">
                <button class="btn btn-success" type="button" onclick="crtOffer();" id="crtOfferId">Crear</button>
                <a class="btn btn-sm btn-white" data-dismiss="modal" href="javascript:void(0)">Cancelar</a>
            </div>

          </form>

        </div>
      </div>
    </div>
  </div>
  <!-- Add User Form -->
  <div class="modal fade modal-3d-flip-vertical" id="addVsetting"  style="display:none" role="dialog" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Administrar Marca/Linea</h4>
        </div>
        <ul class="nav nav-tabs nav-tabs-line" role="tablist">
          <li class="nav-item" role="presentation"  onclick="fexampleLine()"><a class="nav-link active" data-toggle="tab" href="#exampleLine1" aria-controls="exampleLine1" role="tab" aria-expanded="false">Marca</a></li>
          <li class="nav-item" role="presentation"  onclick="fexampleLine2()"><a class="nav-link" data-toggle="tab" href="#exampleLine2" aria-controls="exampleLine2" role="tab
          " aria-expanded="true"  onclick="fexampleLine2()">Linea</a></li>
         </ul>
        <div class="modal-body">
        {{ csrf_field() }}
          <input type="hidden" value="0" id="Vselected"/>
          <div class="alert alert-danger alert-dismissible" role="alert" id="errorAlert" style="display:none">
          </div>
          <div class="tab-pane" id="exampleLine1" role="tabpanel" aria-expanded="false">
              <div class="form-group form-material floating" data-plugin="formMaterial">
                  <input type="text" required class="form-control" id="vehiclebrandName" name="vehiclebrandName" onchange="turnOffAlert()"/>
                  <label class="floating-label">Nombre de la Marca</label>
              </div>
          </div>
          <div class="tab-pane" id="exampleLine2" role="tabpanel" aria-expanded="false" style="display:none">
              <div class="form-group">
                <label class="floating-label">Marca a la que pertenece</label>
                <select required class="form-control"  id="vehiclebrandId" name="vehiclebrandId" data-plugin="select2">
                </select>
              </div>
              <div class="form-group form-material floating" data-plugin="formMaterial">
                  <input type="text" required class="form-control" id="lineName" name="lineName" onchange="turnOffAlert()"/>
                  <label class="floating-label">Nombre de Linea</label>
              </div>
          </div>
          <div class="form-group col-xl-12 text-right padding-top-m">
            <button class="btn btn-success" type="submit" onclick="crt_Vsetting()" id="crt_VsettingId">Crear</button>
            <a class="btn btn-sm btn-white" data-dismiss="modal" href="javascript:void(0)">Cancelar</a>
          </div>
        </div>
      </div>
    </div>

  </div>
    @include('offers.panel')
   
  <!-- End Add User Form -->
    <script type="text/javascript">
    token = $("input[name*='_token']").val();
    $(document).ready(function() {
      $('#userSlidePanel').hide();

      /****  Load Table****/
    print_datatable();
      /****  Load Table****/


        $("#data").trigger( "reset" );
        var toAppend = '';
        var y = 0;
        for (var i = 40; i > 0; i--) {
            y++;
            toAppend += '<option value="'+y+'">'+y+'</option>';
        }

        $('#vehicleCapacity').append(toAppend);
        $('#vehicleCapacity').trigger("select2:updated");

        var token = $("input[name*='_token']").val();

        if (window.location.host == 'localhost') {
            var baseurl = window.location.protocol +"//" + window.location.host + '/prontuz_backend/public';
        }else{
            var baseurl = window.location.protocol +"//" + window.location.host ;
        }
   
    });

  /**************************************************/

  function print_datatable(){

      tableName='offer_list';
      urlName='/offers/listing';
      columnsName=[
              { data: 'offerImage',
                render: function (data){
 
                  return '<img width="150px"  style="max-height:130px !important" src="'+data+'" />';
                    
                  }
              },
              { data: 'offer'},
              {
                data: 'offerState2',
                render: function (data){
                        if(data == 'Activo'){
                          return '<span class="badge badge-outline badge-success">'+data+'</span>';
                        }else{
                          return '<span class="badge badge-outline badge-danger">'+data+'</span>';
                        }
                  }
                },
                {
                data: 'offerId',
                render: function (data){
                        return ' <button onclick="edtOffer('+data+')" type="button" class="btnEditar btn btn-primary btn-sm">Editar</button>';
                  }
                },
           ];

      list_datatable(tableName,urlName,columnsName);
  }
  /**************************************************/
  
  $("#offerImage").change(function(){
      var reader = new FileReader();
      reader.onload = function (e) {
       $('#imgoffer').attr('src', e.target.result);
      }
      reader.readAsDataURL(this.files[0]);
  });

/**************************************************/

  $("#offerImageEdit").change(function(){
      var reader = new FileReader();
      reader.onload = function (e) {
       $('#imgperfilEdit').attr('src', e.target.result);
      }
      reader.readAsDataURL(this.files[0]);
  });
/**************************************************/

$("#offerQuantity").on({
  "focus": function(event) {
    $(event.target).select();
  },
  "keyup": function(event) {
    $(event.target).val(function(index, value) {
      return value.replace(/\D/g, "")
        .replace(/([0-9])([0-9]{0})$/, '$1$2')
    });
    var offerQuantity=$('#offerQuantity').val();
    $('#offerAvailable').val(offerQuantity);
  }

});
//$('#offerAvailable').val(value);
/**************************************************/

$("#offerQuantityEdit").on({
  "focus": function(event) {
    $(event.target).select();
  },
  "keyup": function(event) {
    $(event.target).val(function(index, value) {
        return value.replace(/\D/g, "")
        .replace(/([0-9])([0-9]{0})$/, '$1$2')
      
    });

  }

});


/**************************************************/

function validateAvailable(e){

  var offerAvailableEdit=$('#offerAvailableEdit').val();

  if(parseInt(e) < parseInt(offerAvailableEdit)){

    $("#offerQuantityEdit").val(offerAvailableEdit);
  
  }
}
/**************************************************/

$("#offerValue").on({
  "focus": function(event) {
    $(event.target).select();
  },
  "keyup": function(event) {
    $(event.target).val(function(index, value) {
      return value.replace(/\D/g, "")
        .replace(/([0-9])([0-9]{0})$/, '$1$2')
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
    });
  }
});
/**************************************************/

$("#offerValueEdit").on({
  "focus": function(event) {
    $(event.target).select();
  },
  "keyup": function(event) {
    $(event.target).val(function(index, value) {
      return value.replace(/\D/g, "")
        .replace(/([0-9])([0-9]{0})$/, '$1$2')
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
    });
  }
});
/**************************************************/

var typingTimer;              
var doneTypingInterval = 1500; 
$input = $('#offerEmail');

$input.on('keyup', function () {
  clearTimeout(typingTimer);
  typingTimer = setTimeout(doneTyping, doneTypingInterval);
});

$input.on('keydown', function () {
  clearTimeout(typingTimer);
});

/**************************************************/

function doneTyping () {
 

  if($("#offerEmail").val().indexOf('@', 0) == -1 || $("#offerEmail").val().indexOf('.', 0) == -1) {

    $("#errorOffer").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button>Ingrese un correo correcto</div>');
    $("#errorOffer").show();
      
  }else{

     $("#errorOffer").html(' ');
     $("#errorOffer").hide();
  }
}
/************************************/
  
  function edtOffer(offerId){


    document.getElementById("offerImageEdit").value = "";
    $( "#charging" ).show();
    //$('#userSlidePanel').show();
    $( "#userSlidePanel" ).show( "slow" );

    $('#offerIdedit').val(offerId);

    $.ajax({
        type: 'GET',
        url: "offers/find",
        data: {_token: token,offerId:offerId },
        dataType: 'JSON',
        beforeSend: function(){   
        },
        success: function (response) {

            if(response.ResponseCode == 0){
                $( "#charging" ).hide();
                $( "#userSlidePanel" ).hide( "slow" ); 
              setTimeout( function(){
                        swal({
                            title: "Error!",
                            text: response.ResponseMessage,
                            type: "danger",
                            timer: 3000,
                            showConfirmButton: false
                        });    
                    },1000); 


            }else{
          
              $('#offerIdEdit').val(offerId);
              $('#offerTitleEdit').val(response.ResponseData[0].offerTitle);
              $('#offerCompanyEdit').val(response.ResponseData[0].offerCompany);
              $('#offerContentEdit').val(response.ResponseData[0].offerContent);
              $('#offerEmailEdit').val(response.ResponseData[0].offerEmail);
              $('#offerQuantityEdit').val(response.ResponseData[0].offerQuantity);
              $('#offerQuantity2Edit').val(response.ResponseData[0].offerQuantity);
              $('#offerAvailableEdit').val(response.ResponseData[0].offerAvailable);
              $('#offerValueEdit').val(response.ResponseData[0].offerValue);
              $('#offerStateEdit option[value="'+response.ResponseData[0].offerState+'"]').attr("selected", "selected");
              $('#imgperfilEdit').attr("src",response.ResponseData[0].offerImage);
              //$('#companyImageEdit').attr("src",baseurl+'/'+data.ResponseData.companyImage);
              
              $( "#charging" ).hide();
              $( "#slideBody" ).show();
            }

        },error: function (response) {
              $("#errorEdit").fadeIn(1000, function(){      
              $("#errorEdit").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> Error de comunicación, revise su conexion a internet.</div>');
              });
          }
    });
     
  }
/************************************/

function closeSlide(){
  
  $( "#userSlidePanel" ).hide( "slow" );
  
}
/************************************/

function openModal(){
  
  openOpt();
  $('#addUserForm').modal('show');
  
}
/************************************/

function openModal2(){
  
  openOpt();
  $('#addVsetting').modal('show');
  doClear();
  fexampleLine();
}
/************************************/

function fexampleLine(){
  
  $('#exampleLine1').show();
  $('#exampleLine2').hide();
  $('#Vselected').val(0);
  
}
/************************************/

function fexampleLine2(){
  
  $('#exampleLine2').show();
  $('#exampleLine1').hide();
  $('#Vselected').val(1);
}
/************************************/

function hideTooltip(){
    
    $('.tooltip-inner').hide();
  }

/************************************/

function valida(e){
    if(e.length <= 5){
     
      $('.tooltip-inner').show();
      $('#crtVehicleId').attr('disabled',true);
    }else{
      
      $('.tooltip-inner').hide();
      $('#crtVehicleId').attr('disabled',false);
    }
  }


/**************************************************/
  function printAlert(messg){
    $('#errorAlert').text(messg);
    $('#errorAlert').show();
    $('#crt_VsettingId').attr('disabled',true);
  }

/**************************************************/

function turnOffAlert(){

    $('#errorAlert').text('');
    $('#errorAlert').hide();
    $('#crt_VsettingId').attr('disabled',false);
}

/**************************************************/

function doClear(){

  $('#vehiclebrandId').find($('option')).attr('selected',false)
  $('#vehiclebrandName').val('');
  $('#lineName').val('');
  turnOffAlert();
}
/************************************/

  function crtOffer(){

    var errorOffer=$('#errorOffer').html();
    var pass = false;
    $('#crtOfferId').attr('type','button');  

    var offerImageEdit=document.getElementById('offerImage');

    offerImageEdit=validateImage(offerImageEdit);

    if((errorOffer == ' ')){
      pass=true;

    }


    if( $('#offerImage').val() == '' || $('#offerImage').val() == undefined ){

      pass=false;
    }else{
      pass=true;
      $('#errorOffer').html(' ');
      $('#errorOffer').hide();
    }

    


    if(pass == true){

      $('#crtOfferId').attr('type','submit');  

      
      $("form#data").submit(function() {

        var formData = new FormData($(this)[0]);

        $('#crtOfferId').attr('disabled','disabled');  
        $.ajax({
            type: 'POST',
            url: $(this).attr("action"),
            headers:{'x-csrf-token':token},
            data: formData,
            dataType: 'JSON',
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){ 
              $("#error").fadeOut();
              $("#data").fadeOut();
              $("#loader").fadeIn();
            },
            success: function (response) {
                  $("#loader").fadeOut();
                  if(response.ResponseCode == 0){

                   setTimeout( function(){
                        swal({
                            title: "Error!",
                            text: response.ResponseMessage,
                            type: "danger",
                            timer: 3000,
                            showConfirmButton: false
                        });    
                    },1000);  
                  
                  }else{
                    $('#addUserForm').modal('hide');
                    print_datatable();

                    setTimeout( function(){
                        swal({
                            title: "Exito!",
                            text: "Oferta creada correctamente",
                            type: "success",
                            timer: 3000,
                            showConfirmButton: false
                        });    
                    },1000);

                  }
                },
                error: function (response) {
                    $("#errorOffer").fadeIn(1000, function(){      
                    $("#errorOffer").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> Error de comunicación, revise su conexion a internet.</div>');
                    });
                }
              });

            return false;
        });
    }else{

      $("#data").fadeIn();
      $("#errorOffer").fadeIn(1000, function(){      
      $("#errorOffer").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button>Ingrese los datos requeridos incluyendo la imagen</div>');
         $("#btn-login").html('<span class="icon ion-android-lock"></span> &nbsp; Entrar');
       });
    }
    
  }
/**************************************************/

function validateImage(offerImageEdit){

  if(offerImageEdit.value != ''){
    var uploadFile = offerImageEdit.files[0];
  
    if (!(/\.(jpg|jpeg|png|gif)$/i).test(uploadFile.name)) {
        return false;
    }else{
        return true;
    } 
  }else{
    return true;
  }                
}

/**************************************************/


function savePut(){

    var offerImageEdit=document.getElementById('offerImageEdit');

    offerImageEdit=validateImage(offerImageEdit);

    if( offerImageEdit == true){
        $("#errorEdit").hide();
        $('#guardar').attr('type','submit');  

        $("form#dataEdit").submit(function() {

            var formData = new FormData($(this)[0]);
        
                $.ajax({
                    type: 'POST',
                    url: baseurl+'/offers/put',
                    headers:{'x-csrf-token':token},
                    data: formData,
                    dataType: 'JSON',
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function(){ 
                      $("#errorEdit").fadeOut();
                      $("#slideBody").fadeOut();
                      $("#loaderEdit").fadeIn();
                    },
                    success: function (response) {
                        $("#loaderEdit").fadeOut();
                        if(response.ResponseCode == 0){

                            $("#slideBody").fadeIn();
                            $("#errorEdit").fadeIn(1000, function(){      
                            $("#errorEdit").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> '+response.ResponseMessage+'</div>');
                               });
                        }else{
                            setTimeout( function(){
                                swal({
                                    title: "ÉXITO!",
                                    text: "Operación Exitosa!",
                                    type: "success",
                                    timer: 3000,
                                    showConfirmButton: false
                                });    
                                setTimeout(function(){
                                  $( "#charging" ).hide();
                                  $( "#userSlidePanel" ).hide( "slow" );
                                  print_datatable();

                                },1500);                     
                            },1000);     
                       }

                    },
                    error: function (response) {
                        $("#error").fadeIn(1000, function(){      
                        $("#error").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> Error de comunicación, revise su conexion a internet.</div>');
                        });
                    }
                });
            return false;
        });
    }else{
       $("#slideBody").fadeIn();
      $("#errorEdit").fadeIn(1000, function(){      
        $("#errorEdit").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button>Formato de Imagen no valido.</div>');
      });
    }
  
}
  
/************************************/

function openOpt(){

    $('#addUserForm').modal('show');
  
}
/************************************/





  </script>

@endsection
