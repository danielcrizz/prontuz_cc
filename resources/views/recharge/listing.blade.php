@extends('layouts.app')

@section('content')

{{ csrf_field() }}
<meta charset="utf-8">
<style>
    #sec_list_points{
      height: 230px !important;
      overflow-y: scroll !important;
      overflow-x: hidden !important;
    }
    #sec_list_points2{
      height: 230px !important;
      overflow-y: scroll !important;
      overflow-x: hidden !important;
    }
   
   
</style>
<div class="page bg-white">
    <div class="page-aside">
      <!-- Contacts Sidebar -->
      <div class="page-aside-switch">
        <i class="icon wb-chevron-left" aria-hidden="true"></i>
        <i class="icon wb-chevron-right" aria-hidden="true"></i>
      </div>
      <div class="page-aside-inner page-aside-scroll">
        <div data-role="container">
          <div data-role="content">
            <div class="page-aside-section">
              <div class="list-group">
                <a class="list-group-item justify-content-between" id="listAllUsers" href="javascript:void(0)">
                  <span>
                    <i class="icon wb-inbox" aria-hidden="true" id="cusers"></i> Todos los usuarios
                  </span>
                  <span class="item-right" id="totalUsers"></span>
                </a>
              </div>
            </div>
            <div class="">
              <section id="sec_list_points">
              <h5 class="page-aside-title">IMPORTES RECARGAS</h5>
              <div class="list-group" id="sumTransactionsImport">
              </div>
              </section>
            </div>
            <div class="">
              <section id="sec_list_points">
              <h5 class="page-aside-title">IMPORTES OFERTAS</h5>
              <div class="list-group" id="sumTransactionsImportOffers">
              </div>
              </section>
            </div>
            <div class="">
              <section id="sec_list_points2">
              <h5 class="page-aside-title">MEDIOS DE PAGO</h5>
              <div class="list-group" id="sumTransactionsId">
              </div>
              </section>
            </div>
          </div>
        </div>
      </div>
    </div>
     <!-- Contacts Content -->
    <div class="page-main" style="padding:20px;">
      <!-- Contacts Content Header -->
      <div class="page-header">
        <h1 class="page-title">Lista de Pagos de Usuarios</h1>
      </div>
      <!-- Contacts Content -->
      <div id="contactsContent" class="page-content page-content-table" data-plugin="selectable">
        <!-- Contacts -->
        <table id='recharge_list' width="98%" class="table is-indent" data-plugin="animateList" data-animate="fade" data-child="tr"
        data-selectable="selectable">
          <thead>
            <tr>
              <th class="cell-150" scope="col">Nombre</th>
              <th class="cell-100" scope="col">Saldo actual</th>
              <th class="cell-100" scope="col">Préstamo actual</th>
              <th class="cell-100" scope="col">Transacción</th>
              <th class="cell-100" scope="col">Categoria</th>
              <th class="cell-100" scope="col">Valor transacción</th>
              <th class="cell-100" scope="col">Fecha transacción</th>
              <th class="cell-100" scope="col">Estado de transacción</th>
            </tr>
          </thead>
          <tbody id="bodytable">
            <div id="loaderlist" style="display:none;">
            <center>
                <br><br><br><br>
                 <img width="48px" src="{{ asset('iconbar/loading.gif') }}" /> &nbsp; Cargando información ...
                <br><br>
            </center>
          </div>
          </tbody>
        </table>
        
      </div>
    </div>
  <div class="modal fade" tabindex="-1" role="dialog" id="crt_recharge" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
           <h4 class="modal-title">Crear nuevo pago</h4>
        </div>
        <div class="modal-body">
            <div class="alert alert-danger alert-dismissible" role="alert" id="errorAlert" style="display:none">
            </div>
          <div class="row">
            <div class="col-md-2 col-xs-6"></div>
            <div class="col-md-4 col-xs-6">
                  <div class="radio-custom radio-primary">
                    <input id="transactionType1" name="transactionType" type="radio" onclick="showRecharge()">
                    <!--  checked="" -->
                    <label for="transactionType1">Recarga</label>
                  </div>
                </div>
                <div class="col-md-4 col-xs-6">
                  <div class="radio-custom radio-primary">
                    <input id="transactionType2" name="transactionType" type="radio" onclick="showOffer()">
                    <label for="transactionType2">Oferta</label>
                  </div>
                </div>
          </div>
         <div class="row" style="margin-left: 10px; margin-right: 10px;">
            <div class="col-md-12 col-xs-12">
                <div class="form-group form-material floating" data-plugin="formMaterial">
                    <label class="label">Identificación de usuario:</label>
                    <div class="select2-success">
                    <input list="recharge_userId" id="recharge_userIdI" name="recharge_userId" class="form-control empty" onfocusout="load_userinfo(this.value)"/>
                    <!-- <datalist id="recharge_userId">
                    </datalist> -->
                    </div>
                </div>
            </div>
          <div class="col-md-12 col-xs-12">
            <div class="form-group form-material form-offer" style="display:none">
              <select required class="form-control"  id="offerId" name="offerId" onclick="print_offerValue(this.value);" onchange="print_offerValue(this.value);">
              </select>
            </div>
          </div>
          <div class="col-md-12 col-xs-12">
            <div class="form-group form-material form-offer" style="display:none">
              <input class="form-control" placeholder="Valor" id="offerValue" name="offerValue" type="text" maxlength="7" readonly="readonly">
            </div>
          </div>
        </div>
          <div class="row form-recharge" style="margin-left: 10px; margin-right: 10px;">
            <div class="col-md-12 col-xs-12">
                <div class="form-group form-material floating" data-plugin="formMaterial">
                  <input required="" class="form-control empty" id="rechargeValue" name="rechargeValue" maxlength="7" type="text" onkeyup="turnOffAlert()">
                  <label class="floating-label">Valor</label>
                </div>
            </div>
          </div>
          <div class="example table-responsive" id="recharge_info">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal" onclick="doClear()">Cancelar</button>
          <button type="button" class="btn btn-primary" onclick="doRecharge()" id="doRechargeId">Recargar</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  </div>
  <div class="site-action">
    <button type="button" id="actionBtn" class="site-action-toggle btn-raised btn btn-success btn-floating">
      <i class="front-icon wb-plus animation-scale-up" aria-hidden="true"></i>
      <i class="back-icon wb-close animation-scale-up" aria-hidden="true"></i>
    </button>
    <div class="site-action-buttons">
      <button type="button" data-action="trash" class="btn-raised btn btn-success btn-floating animation-slide-bottom">
        <i class="icon wb-trash" aria-hidden="true"></i>
      </button>
      <button type="button" data-action="folder" class="btn-raised btn btn-success btn-floating animation-slide-bottom">
        <i class="icon wb-folder" aria-hidden="true"></i>
      </button>
    </div>
  </div>

  <script type="text/javascript">
    $(document).ready(function() {
      loadOffer();
      document.getElementById("transactionType1").checked = true;
      document.getElementById("transactionType2").checked = false;
      // $('#crt_recharge').modal('hide');
      $('#recharge_info').hide();
      token = $("input[name*='_token']").val();
      transactionValue=0;
      transactionTypeAd=0;
      recharge_userId=0;
      doClear();
      getRusers();
      print_datatable();
      getRallUsers();
      sumTransactions();
    });
/**************************************************/

  function showOffer(){
    $('.form-recharge').hide();
    $('.form-offer').show();
    print_offerValue($('#offerId').val());

  }

  /**************************************************/

  function showRecharge(){
    $('.form-recharge').show();
    $('.form-offer').hide();

  }

/**************************************************/

function print_offerValue(offerdata){

  console.log('22');
  
  offerdata=offerdata.split('_');

  var offV = offerdata[1].replace(/\D/g, "");
      offV = offV.replace(/([0-9])([0-9]{0})$/, '$1$2');
      offV = offV.replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");


  console.log(offV);

  $('#offerValue').val(offV);

}
/**************************************************/

function loadOffer(){
  
  
  $.ajax({
        type: "GET",
        url:"offers/find",
        dataType: 'JSON',
        success: function (data) {


          if(data.ResponseCode == 0){

            printAlert(data.ResponseMessage);

          }else if(data.ResponseCode == 1){

          var toAppend = '';
            $.each(data.ResponseData,function(k, v){
                toAppend += '<option value="'+v.offerId+'_'+v.offerValue+'">'+v.offerTitle+'</option>';
            });

             $('#offerId').append(toAppend);
          }
       
        },
        error: function (data) {
            console.log('Error:', data);
        }
    });

}

/**************************************************/

    function getRusers(){

       $.ajax({
          type: "POST",
          url: baseurl+'/recharge/getRusers',
          data: {_token: token},
          dataType: 'JSON',
          success: function (data) {
            $('#totalUsers').html(data);
          },
          error: function (data) {
              console.log('Error:', data);
          }
        });
    }

/**************************************************/

    function print_datatable(){

      tableName='recharge_list';
      urlName='/recharge/listing';
      columnsName=[
              { data: 'userName'},
              { data: 'rechargeValue',
                render: function (data){
                  return '$ '+formatThousands(parseInt(data));
                }},
              { data: 'rechargeLoan',
                render: function (data){
                  return '$ '+formatThousands(parseInt(data));
                }},
              { data: 'transactionType2'},
              { data: 'transactionCategory2'},
              { data: 'transactionValue',
                render: function (data){
                  return '$ '+formatThousands(parseInt(data));
                }
              },
              { data: 'transactionOperationdate'},
              // { data: 'transactionState2'},
              { data: 'transactionState2'},
            ];
      list_datatable(tableName,urlName,columnsName);

    }
    
/**************************************************/
  
  $('#actionBtn').click(function(){
      $('#crt_recharge').modal('show');
  });

/**************************************************/

function getRallUsers(){

  $.ajax({
      type: "POST",
      url: baseurl+'/recharge/getRallUsers',
      data: {_token: token},
      dataType: 'JSON',
      success: function (data) {
          
          var toAppend = '';
          $.each(data,function(k, v){
            
              toAppend += '<option val2="'+v.userId+'" value="'+v.userFirstname+' '+v.userLastname+'">'+v.userFirstname+' '+v.userLastname+'</option>';
          });

           $('#recharge_userId').append('<select>'+toAppend+'</select>');
           $('#recharge_userId').trigger("select2:updated");


      },
      error: function (data) {
          console.log('Error:', data);
      }
  });
}
/**************************************************/

function load_userinfo(e){

   $('#recharge_info').empty();
    
    
    $.ajax({
        type: "POST",
        url: baseurl+'/recharge/getIduser',
        data: {_token: token,userIdentificacion:e},
        dataType: 'JSON',
        success: function (data) {

            if(data.ResponseCode == 0){
                $('#recharge_info').html('<div role="alert" class="alert dark alert-danger alert-dismissible">El usuario no existe</div>');
                $('#recharge_info').show();

            }else{
              recharge_userId=data;
               $.ajax({
                type: "POST",
                url: baseurl+'/recharge/getRcValue',
                data: {_token: token,recharge_userId:recharge_userId},
                dataType: 'JSON',
                success: function (data) {

                  if(data.ResponseCode == 0){

                
                    
                    $('#recharge_info').html('<div role="alert" class="alert dark alert-danger alert-dismissible">'+data.ResponseMessage+'</div>');

                  
                  }else{
                      v=data.ResponseData;
                      rechargeValue = v[0].rechargeValue; 
                      rechargeLoan = v[0].rechargeLoan; 
                      updated_at = v[0].updated_at; 

                    var add='<table class="table table-bordered">'+
                            '<thead>'+
                              '<tr>'+
                                '<th>Saldo actual</th>'+
                                '<th>Prestamo Actual</th>'+
                                '<th>Fecha Ultima Recarga</th>'+
                              '</tr>'+
                            '</thead>'+
                            '<tbody id="recharge_info_tbody">'+
                              '<tr>'+
                                '<td>'+rechargeValue+'</td>'+
                                '<td>'+rechargeLoan+'</td>'+
                                '<td>'+updated_at+'</td>'+
                              '</tr>'+
                            '</tbody>'+
                          '</table>';

                  }

                  if($('#transactionType1').is(':checked')) { 
                    $('#recharge_info').append(add);
                    $('#recharge_info').show();
                  } 
               
                  
                    

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
            }
            
         },
        error: function (data) {
            console.log('Error:', data);
        }
    });

 
}

/**************************************************/

function doClear(){

  $('#rechargeValue').find($('option')).attr('selected',false)
  $('#recharge_userIdI').val('');
  $('#rechargeValue').val('');
  $('#recharge_info').empty();
  turnOffAlert();
}

/**************************************************/

function doRecharge(){

  e = $('#recharge_userIdI').val();

  var offerId=0; 
  var ban = false;
  userId ='{{ $userId }}';
 
  console.log(recharge_userId);

  if($('#transactionType1').is(':checked')) { 
    transactionTypeAd=1;
  } 

  if(transactionTypeAd == 1){
     rechargeValue=$('#rechargeValue').val();

    if(rechargeValue !=''){
      rechargeValue2=rechargeValue.split(".");
      rechargeValue='';
      for (var i = 0; i < rechargeValue2.length; i++) {
        rechargeValue=rechargeValue+rechargeValue2[i];
      }
      rechargeValue=parseInt(rechargeValue);
      transactionValue=parseInt(rechargeValue);
    }

    if(rechargeValue<20000){
      ban=true;
    }
  }else{
    transactionTypeAd=0;

    offerValue=$('#offerValue').val();
    var offerId2=$('#offerId').val();
    var offerId2=offerId2.split("_");
    offerId=offerId2[0];


    if(offerValue !=''){
      offerValue2=offerValue.split(".");
      offerValue='';
      for (var i = 0; i < offerValue2.length; i++) {
        offerValue=offerValue+offerValue2[i];
      }
      offerValue=parseInt(offerValue);
      transactionValue=parseInt(offerValue);
    }
  }
  

  if(ban == true){
    printAlert('Valor mínimo de recarga 1000 COP');
  }else{
    //if( transactionType!=99){
    var data= {_token: token,recharge_userId:recharge_userId,offerId:offerId,transactionTypeAd:transactionTypeAd,transactionType:99,rechargeValue:transactionValue,offerValue:transactionValue,method:'cashPay',userId:userId};

    setTimeout(function(){
        $.ajax({
          type: "POST",
          url: baseurl+'/recharge/store',
          data: data,
          dataType: 'JSON',
          success: function (data) {

            console.log(data);

            if(data.ResponseCode == 0){

              printAlert(data.ResponseMessage);

              setTimeout(function(){ 
                $('#crt_recharge').modal('hide');
              }, 3000);
              
              
              
            }else if(data.ResponseCode == 1){
              $('#crt_recharge').modal('hide');
              doClear();
              print_datatable();
            }
         
          },
          error: function (data) {
              console.log('Error:', data);
          }
      });
    },2000);
  
    //}
  }

  
}
/**************************************************/

function sumTransactions(){
  $('#sumTransactionsId').empty();
  $.ajax({
      type: "POST",
      url: baseurl+'/recharge/sumTransactions',
      data: {_token: token},
      dataType: 'JSON',
      success: function (data) {

          
          var add = '';
          var add2 = '';
          $.each(data.recharge,function(k, v){
            
              add += '<a class="list-group-item justify-content-between" id="'+v.transactionType+'" href="javascript:void(0)">'+
                  '<span>'+
                    v.transactionType2+
                  '</span>'+
                  '<span class="item-right" id="numAdmin">'+v.transactioncount+'</span>'+
                '</a>';

              add2 += '<a class="list-group-item justify-content-between" id="'+v.transactionType+'_gets" href="javascript:void(0)">'+
                  '<span>'+
                    v.transactionType2+
                  '</span>'+
                  '<span class="item-right" id="numAdmin">$ '+formatThousands(parseInt(v.transactionGets))+'</span>'+
                '</a>';
          });

          $('#sumTransactionsImport').append(add2);
           $('#sumTransactionsId').append(add);

          var add = '';
          var add2 = '';
          $.each(data.offer,function(k, v){
            
              add2 += '<a class="list-group-item justify-content-between" id="'+v.transactionType+'_gets" href="javascript:void(0)">'+
                  '<span>'+
                    v.transactionType2+
                  '</span>'+
                  '<span class="item-right" id="numAdmin">$ '+formatThousands(parseInt(v.transactionGets))+'</span>'+
                '</a>';
          });

          $('#sumTransactionsImportOffers').append(add2);
      },
      error: function (data) {
          console.log('Error:', data);
      }
  });
}
/**************************************************/

$("#rechargeValue").on({
  "focus": function(event) {
    $(event.target).select();
  },
  "keyup": function(event) {
    $(event.target).val(function(index, value) {
      return value.replace(/\D/g, "")
        .replace(/([0-9])([0-9]{0})$/, '$1$2')
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
    });
  }
});
/**************************************************/
function formatThousands(n, dp) {
  var s = ''+(Math.floor(n)), d = n % 1, i = s.length, r = '';
  while ( (i -= 3) > 0 ) { r = '.' + s.substr(i, 3) + r; }
  return s.substr(0, i + 3) + r + (d ? ',' + Math.round(d * Math.pow(10,dp||2)) : '');
}

/**************************************************/
  function printAlert(messg){
    $('#errorAlert').text(messg);
    $('#errorAlert').show();
    $('#doRechargeId').attr('disabled',true);
  }

/**************************************************/

function turnOffAlert(){
    $('#errorAlert').text('');
    $('#errorAlert').hide();
    $('#doRechargeId').attr('disabled',false);

}
/**************************************************/

  </script>
@endsection

