<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap admin template">
  <meta name="author" content="">
  <title>Prontuz U | Recargas</title>
  <link rel="apple-touch-icon" href="{{ asset('iconbar/assets/images/apple-touch-icon.png') }}">
  <link rel="shortcut icon" href="{{ asset('iconbar/assets/images/favicon.ico') }}">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="{{ asset('iconbar/global/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/css/bootstrap-extend.min.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/assets/css/site.min.css') }}">
  <!-- Plugins -->
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/animsition/animsition.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/asscrollable/asScrollable.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/switchery/switchery.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/intro-js/introjs.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/slidepanel/slidePanel.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/flag-icon-css/flag-icon.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/assets/examples/css/pages/login-v2.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/assets/examples/css/structure/alerts.css') }}">
  <!-- Fonts -->
  <link rel="stylesheet" href="{{ asset('iconbar/global/fonts/ionicons/ionicons.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/fonts/web-icons/web-icons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/fonts/brand-icons/brand-icons.min.css') }}">

  <!--[if lt IE 9]>
    <script src="{{ asset('iconbar/global/vendor/html5shiv/html5shiv.min.js') }}"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="{{ asset('iconbar/global/vendor/media-match/media.match.min.js') }}"></script>
    <script src="{{ asset('iconbar/global/vendor/respond/respond.min.js') }}"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="{{ asset('iconbar/global/vendor/breakpoints/breakpoints.js') }}"></script>


  <!-- Core  -->
  <script src="{{ asset('iconbar/global/vendor/babel-external-helpers/babel-external-helpers.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/jquery/jquery.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/tether/tether.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/bootstrap/bootstrap.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/animsition/animsition.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/mousewheel/jquery.mousewheel.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/asscrollbar/jquery-asScrollbar.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/asscrollable/jquery-asScrollable.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/ashoverscroll/jquery-asHoverScroll.js') }}"></script>
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->

  <style type="text/css">

    @font-face {
      font-family: 'Prometo';
      src: url('{{ asset('iconbar/global/fonts/Prometo Light.otf') }}');
    }

    body {
        font-family: Prometo !important;
        font-weight: 500 !important;
        /*background: #4fb933 none repeat scroll 0 0  !important;*/
        background: #7dc138 url("{{ ('Prontuz_files/pagos-Bg.jpg') }}") no-repeat fixed center !important; 
    }
    #formId{
      background: #fff !important;
    }
    .brand p{
      max-width:380px !important;
      width:98%;
    }
    #table_info h4,h5,h6{
       color: #76838f!important;
       font-weight:normal !important;
    }
    #table_info small{
      color: #4fb933!important;
    }
    #table_info img{
      width: 60px !important;
    }
    #formId2{
      text-align: center;
    }

    #popUpModal .modal-content {
      background-color: #4D5D5D !important;
    } 
    .modal-content #popUpModal{
      background-color: #4D5D5D !important;
    }
    #popUpModal table{
      color:#7F8F8F !important;
      font-size: 12px;
    }
    #popUpModal .modal-header{
      color: #fff !important;
      text-align: center !important;
    }
    #popUpModal .modal-body{
      padding-left: 0px !important;
    }
  </style>

  <link rel="stylesheet" href="{{ asset('iconbar/assets/examples/css/structure/alerts.css') }}">
  
</head>
<body class="animsition page-register-v3 layout-full site-menubar-unfold" style="animation-duration: 800ms; opacity: 1;">
{{ csrf_field() }}
   <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">&gt;
       
    <div class="page-content vertical-align-middle animation-slide-top animation-duration-1">
      <div class="brand">
        <img class="brand-img" src="{{ ('Prontuz_files/pagos-logo prontuz.png') }}" alt="Prontuz U"/>
      </div> 
      <br>
      <div class="panel">
        <div class="panel-body">
          <div class="brand" id="brand_text">
            <h2 class="brand-text font-size-18">Recarga tu cuenta Prontuz U</h2>
            <p>Es fácil, solo escoge la opción y el valor, tendrás varias opciones de pago disponibles. Una vez el pago sea validado se abonara en tu App</p>
          </div>
          <div class="alert alert-danger alert-dismissible" role="alert" id="errorAlert" style="width:390px !important;display:none">
          </div>
          <form method="post" id="formId">
          <div id="loaderEdit" style="display:none">
             <img width="48px" src="{{ asset('iconbar/loading.gif') }}" /> &nbsp; Validando ...
          </div>
          <div class="row">
                <div class="col-md-5 col-xs-6">
                  <div class="radio-custom radio-primary">
                    <input id="transactionType1" name="transactionType" type="radio" onclick="showRecharge()">
                    <!--  checked="" -->
                    <label for="transactionType1">Recarga</label>
                  </div>
                </div>
                <div class="col-md-5 col-xs-6">
                  <div class="radio-custom radio-primary">
                    <input id="transactionType2" name="transactionType" type="radio" onclick="showOffer()">
                    <label for="transactionType2">Oferta</label>
                  </div>
                </div>
            </div>
            <div class="form-group form-offer" style="display:none">
              <select required class="form-control"  id="offerId" name="offerId" data-plugin="select2" onclick="print_offerValue(this.value);">
              </select>
            </div>
            <div class="form-group form-offer" style="display:none">
              <div class="input-group" id="">
                <span class="input-group-addon">$</span>
                <input class="form-control" placeholder="Valor" id="offerValue" name="offerValue" type="text" maxlength="7" readonly="readonly">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group input-group-icon" id="DuserIdentificacion">
                <span class="input-group-addon">
                  <span class="icon wb-user" aria-hidden="true"></span>
                </span>
                <input class="form-control" placeholder="Documento" id="userIdentificacion" type="text" onkeyup="turnOffAlert()">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group input-group-icon" >
                <span class="input-group-addon">
                  <span class="icon wb-envelope" aria-hidden="true"></span>
                </span>
                <input class="form-control" placeholder="Email" id="userEmail" type="text">
              </div>
            </div>
            <div class="form-group form-recharge">
              <div class="input-group" id="DrechargeValue">
                <span class="input-group-addon">$</span>
                <input class="form-control" placeholder="Valor" id="rechargeValue" type="text" maxlength="7" onkeyup="turnOffAlert()">
              </div>
            </div>
            <button type="button" class="btn btn-primary btn-block" onclick="validateUser()" id="validateUserId">Continuar</button>
          </form>
          <form method="post" id="formId2">
          </form>
          
        <form method="post" action="https://sandbox.gateway.payulatam.com/ppp-web-gateway/"  accept-charset="UTF-8" style="display:none" id="formId3">
        <!-- <form method="post" action="https://gateway.payulatam.com/ppp-web-gateway/" accept-charset="UTF-8" style="display:none" id="formId3" > -->
            <input name="test"          id="test"         type="hidden"/>
            <input name="buttonId" type="hidden" value="O47EgiOrkel7s6HTJkmhmeP1OBSzp5VBIcV0fsiG1a4daiV9ePZk5g=="/>
            <input name="merchantId"    id="merchantId"   type="hidden"/>
            <input name="accountId"     id="accountId"    type="hidden"/>
            <input name="description"   id="description"  type="hidden"/>
            <input name="signature"     id="signature"    type="hidden"/>
            <input name="amount"        id="amount"       type="hidden"/>
            <input name="referenceCode" id="referenceCode" type="hidden"/>
            <input name="currency"      id="currency"     type="hidden"/>
            <input name="buyerEmail"    id="buyerEmail"   type="hidden"/>
            <input name="telephone"     id="telephone"    type="hidden"/>
            <input name="buyerFullName" id="buyerFullName" type="hidden" value="REJECTED"/>
            <input name="responseUrl"   id="responseUrl"  type="hidden"/>
            <input name="tax" type="hidden" value="0"/>
            <input name="expirationDate" id="expirationDate" type="hidden"/>
            <input name="taxReturnBase" type="hidden" value="0"/>
            <input name="shipmentValue" value="0" type="hidden"/>
            <input name="lng" type="hidden" value="es"/>
            <input name="displayShippingInformation" type="hidden" value="NO"/>
            <input name="sourceUrl" id="urlOrigen" value="" type="hidden"/>
            <div class="example example-buttons">
              <button type="button" class="btn btn-primary" onclick="sendPayU()" id="sendPayUId">Pagar</button>
              <button type="button" class="btn btn-default" data-dismiss="modal" onclick="goBack3()">Atrás</button>
            </div>
            <!-- <input type="image" border="0" alt="" src="http://www.payulatam.com/img-secure-2015/boton_pagar_pequeno.png" onClick="this.form.urlOrigen.value = window.location.href;"/> -->
          </form>
        </div>
      </div>
      <br> 
      <footer class="page-copyright page-copyright-inverse">
      <div class="brand">
        <img class="brand-img" src="{{ ('Prontuz_files/pagos-logo-coopamer.svg') }}" alt="Prontuz U"/>
      </div> 
        <!-- <p>WEBSITE BY amazingSurge</p>
        <p>© 2017. All RIGHT RESERVED.</p>
        <div class="social">
          <a class="btn btn-icon btn-pure" href="javascript:void(0)">
            <i class="icon bd-twitter" aria-hidden="true"></i>
          </a>
          <a class="btn btn-icon btn-pure" href="javascript:void(0)">
            <i class="icon bd-facebook" aria-hidden="true"></i>
          </a>
          <a class="btn btn-icon btn-pure" href="javascript:void(0)">
            <i class="icon bd-google-plus" aria-hidden="true"></i>
          </a>
        </div> -->
      </footer>
    </div>
    <div class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" id="popUpModal" style="top: 20% !important;" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel" style="color:#fff;font-weight: bold">BIENVENIDO A PRONTUZ U
            <p style="font-size:0.8em;color:#fff;width:65%;margin-left: 18%;font-weight: normal">Aquí podrás hacer las recargas de tu cuenta Prontuz U para ser utilizado en tus viajes a la universidad</p>
            </h4>
            <br>
            
          </div>
          <div class="modal-body">
            <table style="width:100%">
              <tr>
                <td rowspan="2" width="50%"><img class="brand-img" src="{{ ('Prontuz_files/popup-grafico.svg') }}" alt="Prontuz U"/></td>
                <td  width="50%" style="font-size:1.1em ;font-weight: bold">Recarga desde 20.00 COP en cualquier de nuestros medios de pago disponibles.</td>
              </tr>
              <tr>
                <td>Tarjetas de crédito.
                <img class="brand-img" src="{{ ('Prontuz_files/popup-TC.svg') }}" alt="Prontuz U"/>
                </td>
              </tr>
              <tr>
                <td></td>
                <td>
                 PSE, Efecty, Baloto, Grupo Éxito.<br>
                <img class="brand-img" src="{{ ('Prontuz_files/popup-PAYU.svg') }}" alt="Prontuz U"/>
                </td>
              </tr>
            </table>
          </div>
          <div class="modal-footer" style="display:inline">
            <center>
              <button type="button" class="btn btn-primary" style="color:#4D5D5D;font-weight: bold;background-color:#8eff01;" data-dismiss="modal">Entendido</button>
            </center>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Page -->
  <!-- Core  -->
  <script src="{{ asset('iconbar/global/vendor/bootstrap-sweetalert/sweetalert.js') }}"></script>
<script type="text/javascript">

if (window.location.host == 'localhost') {
  var baseurl = window.location.protocol +"//" + window.location.host + '/prontuz_backend/public';
}else{
  var baseurl = window.location.protocol +"//" + window.location.host ;
}

    
  $(document).ready(function() {
      loadOffer();
       document.getElementById("transactionType1").checked = true;
       document.getElementById("transactionType2").checked = false;

      $('#popUpModal').modal('show');
      ban=false;
      usData='';
      transactionValue=0;
      transactionTypeAd=0;
      token = $("input[name*='_token']").val();
      $('#formId').find(':input').each(function(){
        $(this).val('');
      })

      $('#errorAlert').hide();
      $('#loaderEdit').hide();
      $('#formId2').hide();

      if( '{{ $response }}' != ''){
        $('#formId').hide();
        $('#brand_text h2').text('Pago confirmado exitosamente');
        $('#brand_text p').text('Ahora puedes ver el movimiento en tu aplicacion ProntuzU');
        //pagos-check.svg
        $('#formId2').empty();
        var fimg="{{ ('Prontuz_files/pagos-check.svg') }}";
        $('#formId2').append('<div class="brand"><img class="brand-img" src="'+fimg+'" alt="Prontuz U"/></div>'+
          '<br><button type="button" class="btn btn-primary btn-block" onclick="reloadPge()" >volver al inicio</button>');
        $('#formId2').show();
      }else{
        $('#formId3').find(':input').each(function(){
          $(this).val('');
        })
      }
  });
  
/**********************************************/

function reloadPge(){
  window.location.href=baseurl;
}

/**************************************************/

  function showOffer(){
    $('.form-recharge').hide();
    $('.form-offer').show();
    print_offerValue($('#offerId').val());

  }

  /**************************************************/

  function showRecharge(){
    $('.form-recharge').show();
    $('.form-offer').hide();

  }

/**************************************************/

function validateUser(){

  
  var ban=false;
  userIdentificacion=$('#userIdentificacion').val();
  userEmail=$('#userEmail').val();

  if($('#transactionType1').is(':checked')) { 
    transactionTypeAd=1;
  } 


  if(transactionTypeAd == 1){
     rechargeValue=$('#rechargeValue').val();

    if(rechargeValue !=''){
      rechargeValue2=rechargeValue.split(".");
      rechargeValue='';
      for (var i = 0; i < rechargeValue2.length; i++) {
        rechargeValue=rechargeValue+rechargeValue2[i];
      }
      rechargeValue=parseInt(rechargeValue);
      transactionValue=parseInt(rechargeValue);
    }

    if(rechargeValue<20000){
      ban=true;
    }
  }else{
    transactionTypeAd=0;

    offerValue=$('#offerValue').val();

    if(offerValue !=''){
      offerValue2=offerValue.split(".");
      offerValue='';
      for (var i = 0; i < offerValue2.length; i++) {
        offerValue=offerValue+offerValue2[i];
      }
      offerValue=parseInt(offerValue);
      transactionValue=parseInt(offerValue);
    }
  }
 
  


  if(ban == true){
    printAlert('Valor mínimo de recarga 20.000 COP');
  }else{

    $('#loaderEdit').show();

    $.ajax({
      type: "POST",
      url: baseurl+'/validateRuser',
      data: {_token: token,userIdentificacion:userIdentificacion,userEmail:userEmail,transactionValue:transactionValue},
      dataType: 'JSON',
      success: function (data) {

        $('#loaderEdit').hide();

        if(data.ResponseCode < 1){
          printAlert(data.ResponseMessage);
        }else{
          usData = data;
          var img=data.ResponseData.userImage;

          if( transactionTypeAd == 1){
            var tval=$('#rechargeValue').val();
            var msg="Recarga";
          }else{
            var tval=$('#offerValue').val();
            var msg="Oferta";
          }

          $('#formId').hide();
          $('#brand_text h2').text('Confirma tu compra');
          $('#brand_text p').text('Si todo está perfecto entonces procede a pagar, de lo contrario vuelve atrás y edita la información.');
          $('#formId2').empty();
          $('#formId2').append('<br><center><table style="text-align:left;border:0px" id="table_info">'+
                    '<thead>'+
                      '<tr>'+
                        '<th rowspan="5" style="vertical-align:top;"><img class="avatar avatar-lg" src="'+baseurl+'/'+img+'" data-toggle="tooltip" data-original-title="Crystal Bates" data-container="body" title="" style="width: 90px !important;padding:5px;"></th>'+
                      '</tr>'+
                      '<tr><td><h3>'+data.ResponseData.userFirstname+' '+data.ResponseData.userLastname+'</h3></td></tr>'+
                      '<tr><td><h4>'+data.ResponseData.userIdentificacion+'</h4></td></tr>'+
                      '<tr><td><h5>Correo: <small>'+data.ResponseData.userEmail+'</small></h5></td></tr>'+
                      '<tr><td><h5>Valor '+msg+': <small>'+tval+' COP</small></h5></td></tr>'+
                      '</tr>'+
                     '</thead>'+
                   '</table></center>');
          $('#formId2').append('<div class="example example-buttons">'+
            '<button type="button" class="btn btn-primary" onclick="doRecharge('+transactionValue+')" id="doRechargeId">Continuar</button>'+
            '<button type="button" class="btn btn-default" data-dismiss="modal" onclick="goBack()">Atrás</button>'+
            '</div>');


          $('#formId2').show();
          
        }

      },
      error: function (data) {
          console.log('Error:', data);
      }
    });
  }

  


}
/**************************************************/

  function printAlert(messg){
    ban=true;
    $('#errorAlert').text(messg);
    $('#errorAlert').show();
    $('#validateUserId').attr('disabled',true);
  }
/**************************************************/

function turnOffAlert(){

  if( $('#errorAlert').text() != 'Email incorrecto'){

    if( ban == true ){
      $('#errorAlert').text('');
      $('#errorAlert').hide();
      $('#validateUserId').attr('disabled',false);
      ban = false;
    }
  } 
}
/**************************************************/

var typingTimer;              
var doneTypingInterval = 1500; 
$input = $('#userEmail');

$input.on('keyup', function () {
  clearTimeout(typingTimer);
  typingTimer = setTimeout(doneTyping, doneTypingInterval);
});

$input.on('keydown', function () {
  clearTimeout(typingTimer);
});


function doneTyping () {
  if($("#userEmail").val().indexOf('@', 0) == -1 || $("#userEmail").val().indexOf('.', 0) == -1) {
    printAlert('Email incorrecto');
  }else{
    printAlert('Email incorrecto.');
    turnOffAlert();
  }
}

/**************************************************/

function goBack(){
  $('#formId2').empty();
  $('#formId2').hide();
   $('#formId').find(':input').each(function(){
        $(this).val('');
      })
  $('#formId').show();
}
/**************************************************/

function goBack3(){
  $('#formId2').empty();
  $('#formId2').hide();
  $('#formId3').hide();
  turnOffAlert();
  $('#formId').show();
}

/**************************************************/

function doRecharge(transactionValue){
  $('#formId2').empty();
  $('#formId2').hide();
  // $('#formId').find(':input').each(function(){
  //       $(this).val('');
  //     })
  //$('#formId3').append('');

 
  if(usData !=''){


    $('#merchantId').val(usData.ResponseData.merchantId);
    $('#accountId').val(usData.ResponseData.accountId);
    $('#description').val(usData.ResponseData.description);
    $('#referenceCode').val(usData.ResponseData.referenceCode);
    //$('#buyerFullName').val(usData.ResponseData.userFirstname+' '+usData.ResponseData.userLastname);
    $('#amount').val(transactionValue);
    $('#currency').val(usData.ResponseData.currency);
    $('#buyerEmail').val(usData.ResponseData.userEmail);
    $('#telephone').val(usData.ResponseData.userPhone);
    $('#signature').val(usData.ResponseData.signature);
    $('#responseUrl').val(usData.ResponseData.responseUrl);
    $('#test').val(usData.ResponseData.test);
    $('#expirationDate').val('{{ $tomorrow }}');

  }
 
  $('#formId3').show();
}

/**************************************************/

function print_offerValue(offerdata){
  
  offerdata=offerdata.split('_');

  var offV = offerdata[1].replace(/\D/g, "");
      offV = offV.replace(/([0-9])([0-9]{0})$/, '$1$2');
      offV = offV.replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");


  $('#offerValue').val(offV);


}
/**************************************************/

function loadOffer(){
  
  
  $.ajax({
        type: "GET",
        url:baseurl+"/Ffind",
        dataType: 'JSON',
        success: function (data) {


          if(data.ResponseCode == 0){

            printAlert(data.ResponseMessage);

          }else if(data.ResponseCode == 1){

          var toAppend = '';
            $.each(data.ResponseData,function(k, v){
                toAppend += '<option value="'+v.offerId+'_'+v.offerValue+'">'+v.offerTitle+'</option>';
            });

             $('#offerId').append(toAppend);
          }
       
        },
        error: function (data) {
            console.log('Error:', data);
        }
    });

}

/**************************************************/

function sendPayU(){
  
  //if( transactionType!=99){#ojo ofertas
  var data= {_token: token,userId:usData.ResponseData.userId,rechargeValue:transactionValue,offerValue:transactionValue,method:'webPay',transactionrReferencecode:usData.ResponseData.referenceCode,transactionTypeAd:transactionTypeAd};
  
   $.ajax({
        type: "POST",
        url: baseurl+'/Rstore',
        data: data,
        dataType: 'JSON',
        success: function (data) {


          if(data.ResponseCode == 0){
            printAlert(data.ResponseMessage);

          }else if(data.ResponseCode == 1){

            $('#formId').empty();
            $('#formId2').empty();
            $('#sendPayUId').attr('type','submit');
            document.getElementById("formId3").submit(); 
          }
       
        },
        error: function (data) {
            console.log('Error:', data);
        }
    });

}

/**************************************************/


$(function() {
  $('#DuserIdentificacion').on('keydown', '#userIdentificacion', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
})
$(function() {
  $('#DrechargeValue').on('keydown', '#rechargeValue', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
})

  </script>


  
  <!-- Plugins -->
  <script src="{{ asset('iconbar/global/vendor/switchery/switchery.min.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/intro-js/intro.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/screenfull/screenfull.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/slidepanel/jquery-slidePanel.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/jquery-placeholder/jquery.placeholder.js') }}"></script>
  <!-- Scripts -->
  <script src="{{ asset('iconbar/global/js/State.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Component.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Base.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Config.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/Section/Menubar.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/Section/Sidebar.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/Section/PageAside.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/Plugin/menu.js') }}"></script>
  <!-- Config -->
  <script src="{{ asset('iconbar/global/js/config/colors.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/config/tour.js') }}"></script>
 
  <!-- Page -->
  <script src="{{ asset('iconbar/assets/js/Site.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/asscrollable.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/slidepanel.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/switchery.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/jquery-placeholder.js') }}"></script>

  <script>
  Config.set('assets', 'assets');
  Breakpoints();
  </script>
  <script>
  (function(document, window, $) {
    'use strict';
    $(document).ready(function() {
      var Site = window.Site;
      Site.run();
    });
  })(document, window, jQuery);
  </script>
</body>
</html>