@extends('layouts.app')
<link rel="apple-touch-icon" href="{{ asset('iconbar/assets/images/apple-touch-icon.png') }}">
  <link rel="shortcut icon" href="{{ asset('iconbar/assets/images/favicon.ico') }}">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="{{ asset('iconbar/global/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/css/bootstrap-extend.min.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/assets/css/site.min.css') }}">
  <!-- Plugins -->
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/animsition/animsition.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/asscrollable/asScrollable.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/switchery/switchery.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/intro-js/introjs.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/slidepanel/slidePanel.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/flag-icon-css/flag-icon.css') }}">
  <!-- <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/waves/waves.css') }}"> -->
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/nestable/nestable.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/html5sortable/sortable.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/vendor/tasklist/tasklist.css') }}">
  <!-- Fonts -->
  <link rel="stylesheet" href="{{ asset('iconbar/global/fonts/material-design/material-design.min.css') }}">
  <link rel="stylesheet" href="{{ asset('iconbar/global/fonts/brand-icons/brand-icons.min.css') }}">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <script src="{{ asset('iconbar/global/vendor/breakpoints/breakpoints.js') }}"></script>
  <script>
  Breakpoints();
  </script>
    <script src="{{ asset('iconbar/global/vendor/babel-external-helpers/babel-external-helpers.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/jquery/jquery.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/tether/tether.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/bootstrap/bootstrap.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/animsition/animsition.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/mousewheel/jquery.mousewheel.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/asscrollbar/jquery-asScrollbar.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/asscrollable/jquery-asScrollable.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/ashoverscroll/jquery-asHoverScroll.js') }}"></script>
  <!-- <script src="{{ asset('iconbar/global/vendor/waves/waves.js') }}"></script> -->
  <!-- Plugins -->
  <script src="{{ asset('iconbar/global/vendor/switchery/switchery.min.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/intro-js/intro.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/screenfull/screenfull.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/slidepanel/jquery-slidePanel.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/html5sortable/html.sortable.js') }}"></script>
  <script src="{{ asset('iconbar/global/vendor/nestable/jquery.nestable.js') }}"></script>
  <!-- Scripts -->
  <script src="{{ asset('iconbar/global/js/State.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Component.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Base.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Config.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/Section/Menubar.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/Section/GridMenu.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/Section/Sidebar.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/Section/PageAside.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/Plugin/menu.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/config/colors.js') }}"></script>
  <script src="{{ asset('iconbar/assets/js/config/tour.js') }}"></script>
  <script>
  //Config.set('assets', '../../assets');
  </script>
  <!-- Page -->
  <script src="{{ asset('iconbar/assets/js/Site.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/asscrollable.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/slidepanel.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/switchery.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/html5sortable.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/nestable.js') }}"></script>
  <script src="{{ asset('iconbar/global/js/Plugin/tasklist.js') }}"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
    });
  })(document, window, jQuery);
  </script>

@section('content')

  {{ csrf_field() }}
   

   <div class="page">
    <div class="page-content container-fluid">
      <div class="row" data-plugin="matchHeight" data-by-row="true">                
        <div class="col-xxl-6 col-lg-12" style="">
          <!-- Widget OVERALL VIEWS -->
          <div class="card card-shadow card-responsive" id="widgetOverallViews">
            <div class="card-block p-30">
              <div class="row pb-30" style="height:calc(100% - 250px);">
                <div class="col-sm-4">
                  <div class="counter counter-md text-left">
                    <div class="counter-label">SERVICIOS TOTALES</div>
                    <div class="counter-number-group text-truncate">
                      <span class="counter-number-related red-600">$</span>
                      <span class="counter-number red-600">432,852</span>
                    </div>
                    <div class="counter-label">2% más que el mes anterior</div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="counter counter-sm text-left inline-block">
                    <div class="counter-label">RUTA 1</div>
                    <div class="counter-number-group">
                      <span class="counter-number-related">$</span>
                      <span class="counter-number">12,346</span>
                    </div>
                  </div>                  
                </div>
                <div class="col-sm-4">
                  <div class="counter counter-sm text-left inline-block">
                    <div class="counter-label">RUTA 2</div>
                    <div class="counter-number-group">
                      <span class="counter-number-related">$</span>
                      <span class="counter-number">17,555</span>
                    </div>
                  </div>                
                </div>
              </div>
              <div class="ct-chart line-chart h-250"><div class="chartist-tooltip" style="display: none; top: 287px; left: 243px;"></div><svg xmlns:ct="http://gionkunz.github.com/chartist-js/ct" width="100%" height="100%" class="ct-chart-line" style="width: 100%; height: 100%;"><g class="ct-grids"><line y1="220" y2="220" x1="40" x2="600" class="ct-grid ct-vertical"></line><line y1="176" y2="176" x1="40" x2="600" class="ct-grid ct-vertical"></line><line y1="132" y2="132" x1="40" x2="600" class="ct-grid ct-vertical"></line><line y1="88" y2="88" x1="40" x2="600" class="ct-grid ct-vertical"></line><line y1="44" y2="44" x1="40" x2="600" class="ct-grid ct-vertical"></line><line y1="0" y2="0" x1="40" x2="600" class="ct-grid ct-vertical"></line></g><g><g class="ct-series ct-series-a"><path d="M40,202.4L133.333,176L226.667,158.4L320,123.2L413.333,132L506.667,44L600,17.6" class="ct-line"></path></g><g class="ct-series ct-series-b"><path d="M40,176L133.333,149.6L226.667,96.8L320,105.6L413.333,88L506.667,123.2L600,79.2" class="ct-line"></path></g></g><g class="ct-labels"><foreignObject style="overflow: visible;" x="40" y="225" width="93.33333333333333" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 93px; height: 20px;">SUN</span></foreignObject><foreignObject style="overflow: visible;" x="133.33333333333331" y="225" width="93.33333333333333" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 93px; height: 20px;">MON</span></foreignObject><foreignObject style="overflow: visible;" x="226.66666666666666" y="225" width="93.33333333333334" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 93px; height: 20px;">TUE</span></foreignObject><foreignObject style="overflow: visible;" x="320" y="225" width="93.33333333333331" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 93px; height: 20px;">WED</span></foreignObject><foreignObject style="overflow: visible;" x="413.3333333333333" y="225" width="93.33333333333331" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 93px; height: 20px;">THU</span></foreignObject><foreignObject style="overflow: visible;" x="506.66666666666663" y="225" width="93.33333333333337" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 93px; height: 20px;">FRI</span></foreignObject><foreignObject style="overflow: visible;" x="600" y="225" width="30" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 30px; height: 20px;">SAT</span></foreignObject><foreignObject style="overflow: visible;" y="176" x="10" height="44" width="20"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 44px; width: 20px;">0</span></foreignObject><foreignObject style="overflow: visible;" y="132" x="10" height="44" width="20"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 44px; width: 20px;">50</span></foreignObject><foreignObject style="overflow: visible;" y="88" x="10" height="44" width="20"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 44px; width: 20px;">100</span></foreignObject><foreignObject style="overflow: visible;" y="44" x="10" height="44" width="20"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 44px; width: 20px;">150</span></foreignObject><foreignObject style="overflow: visible;" y="0" x="10" height="44" width="20"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 44px; width: 20px;">200</span></foreignObject><foreignObject style="overflow: visible;" y="-30" x="10" height="30" width="20"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 30px; width: 20px;">250</span></foreignObject></g></svg></div>
            </div>
          </div>
          <!-- End Widget OVERALL VIEWS -->
        </div>
        
        <div class="col-xxl-6 col-lg-6" style="">
          <!-- Widget Stacked Bar -->
          <div class="card card-shadow" id="widgetStackedBar">
            <div class="card-block p-0">
              <div class="p-30 h-150">
                <p>MARKET DOW</p>
                <div class="red-600">
                  <i class="wb-triangle-up font-size-20 mr-5"></i>
                  <span class="font-size-30">26,580.62</span>
                </div>
              </div>
              <div class="counters pb-20 px-30" style="height:calc(100% - 350px);">
                <div class="row no-space">
                  <div class="col-4">
                    <div class="counter counter-sm">
                      <div class="counter-label text-uppercase">APPL</div>
                      <div class="counter-number-group text-truncate">
                        <span class="counter-number-related green-600">+</span>
                        <span class="counter-number green-600">82.24</span>
                        <span class="counter-number-related green-600">%</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="counter counter-sm">
                      <div class="counter-label text-uppercase">FB</div>
                      <div class="counter-number-group text-truncate">
                        <span class="counter-number-related red-600">-</span>
                        <span class="counter-number red-600">12.06</span>
                        <span class="counter-number-related red-600">%</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="counter counter-sm">
                      <div class="counter-label text-uppercase">GOOG</div>
                      <div class="counter-number-group text-truncate">
                        <span class="counter-number-related green-600">+</span>
                        <span class="counter-number green-600">24.86</span>
                        <span class="counter-number-related green-600">%</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="ct-chart h-200"><div class="chartist-tooltip" style="display: none; top: 200px; left: 318px;">684</div><svg xmlns:ct="http://gionkunz.github.com/chartist-js/ct" width="100%" height="100%" class="ct-chart-bar" style="width: 100%; height: 100%;"><g class="ct-grids"></g><g><g class="ct-series ct-series-a"><line x1="30.333333333333336" x2="30.333333333333336" y1="170" y2="83.54285714285714" class="ct-bar" ct:value="890"></line><line x1="51.00000000000001" x2="51.00000000000001" y1="170" y2="91.89714285714285" class="ct-bar" ct:value="804"></line><line x1="71.66666666666667" x2="71.66666666666667" y1="170" y2="91.02285714285715" class="ct-bar" ct:value="813"></line><line x1="92.33333333333333" x2="92.33333333333333" y1="170" y2="92.09142857142857" class="ct-bar" ct:value="802"></line><line x1="113" x2="113" y1="170" y2="88.20571428571428" class="ct-bar" ct:value="842"></line><line x1="133.66666666666669" x2="133.66666666666669" y1="170" y2="84.22285714285714" class="ct-bar" ct:value="883"></line><line x1="154.33333333333334" x2="154.33333333333334" y1="170" y2="86.74857142857142" class="ct-bar" ct:value="857"></line><line x1="175.00000000000003" x2="175.00000000000003" y1="170" y2="90.92571428571429" class="ct-bar" ct:value="814"></line><line x1="195.66666666666669" x2="195.66666666666669" y1="170" y2="84.90285714285714" class="ct-bar" ct:value="876"></line><line x1="216.33333333333334" x2="216.33333333333334" y1="170" y2="88.59428571428572" class="ct-bar" ct:value="838"></line><line x1="237.00000000000003" x2="237.00000000000003" y1="170" y2="84.22285714285714" class="ct-bar" ct:value="883"></line><line x1="257.6666666666667" x2="257.6666666666667" y1="170" y2="86.84571428571428" class="ct-bar" ct:value="856"></line><line x1="278.3333333333333" x2="278.3333333333333" y1="170" y2="88.20571428571428" class="ct-bar" ct:value="842"></line><line x1="299" x2="299" y1="170" y2="87.33142857142857" class="ct-bar" ct:value="851"></line><line x1="319.6666666666667" x2="319.6666666666667" y1="170" y2="86.94285714285714" class="ct-bar" ct:value="855"></line><line x1="340.3333333333333" x2="340.3333333333333" y1="170" y2="84.02857142857142" class="ct-bar" ct:value="885"></line><line x1="361" x2="361" y1="170" y2="86.94285714285714" class="ct-bar" ct:value="855"></line><line x1="381.6666666666667" x2="381.6666666666667" y1="170" y2="91.60571428571428" class="ct-bar" ct:value="807"></line><line x1="402.3333333333333" x2="402.3333333333333" y1="170" y2="89.66285714285715" class="ct-bar" ct:value="827"></line><line x1="423" x2="423" y1="170" y2="91.31428571428572" class="ct-bar" ct:value="810"></line><line x1="443.6666666666667" x2="443.6666666666667" y1="170" y2="88.78857142857143" class="ct-bar" ct:value="836"></line><line x1="464.3333333333333" x2="464.3333333333333" y1="170" y2="87.13714285714286" class="ct-bar" ct:value="853"></line><line x1="485" x2="485" y1="170" y2="90.53714285714285" class="ct-bar" ct:value="818"></line><line x1="505.6666666666667" x2="505.6666666666667" y1="170" y2="84.32" class="ct-bar" ct:value="882"></line><line x1="526.3333333333334" x2="526.3333333333334" y1="170" y2="86.74857142857142" class="ct-bar" ct:value="857"></line><line x1="547.0000000000001" x2="547.0000000000001" y1="170" y2="83.73714285714286" class="ct-bar" ct:value="888"></line><line x1="567.6666666666667" x2="567.6666666666667" y1="170" y2="83.93142857142857" class="ct-bar" ct:value="886"></line><line x1="588.3333333333334" x2="588.3333333333334" y1="170" y2="84.70857142857143" class="ct-bar" ct:value="878"></line><line x1="609.0000000000001" x2="609.0000000000001" y1="170" y2="83.83428571428571" class="ct-bar" ct:value="887"></line><line x1="629.6666666666667" x2="629.6666666666667" y1="170" y2="85.19428571428571" class="ct-bar" ct:value="873"></line></g><g class="ct-series ct-series-b"><line x1="30.333333333333336" x2="30.333333333333336" y1="83.54285714285714" y2="23.994285714285724" class="ct-bar" ct:value="613"></line><line x1="51.00000000000001" x2="51.00000000000001" y1="91.89714285714285" y2="24.965714285714284" class="ct-bar" ct:value="689"></line><line x1="71.66666666666667" x2="71.66666666666667" y1="91.02285714285715" y2="28.94857142857144" class="ct-bar" ct:value="639"></line><line x1="92.33333333333333" x2="92.33333333333333" y1="92.09142857142857" y2="27.005714285714276" class="ct-bar" ct:value="670"></line><line x1="113" x2="113" y1="88.20571428571428" y2="23.99428571428571" class="ct-bar" ct:value="661"></line><line x1="133.66666666666669" x2="133.66666666666669" y1="84.22285714285714" y2="24.86857142857143" class="ct-bar" ct:value="611"></line><line x1="154.33333333333334" x2="154.33333333333334" y1="86.74857142857142" y2="25.645714285714277" class="ct-bar" ct:value="629"></line><line x1="175.00000000000003" x2="175.00000000000003" y1="90.92571428571429" y2="31.474285714285713" class="ct-bar" ct:value="612"></line><line x1="195.66666666666669" x2="195.66666666666669" y1="84.90285714285714" y2="23.99428571428571" class="ct-bar" ct:value="627"></line><line x1="216.33333333333334" x2="216.33333333333334" y1="88.59428571428572" y2="26.42285714285714" class="ct-bar" ct:value="640"></line><line x1="237.00000000000003" x2="237.00000000000003" y1="84.22285714285714" y2="18.748571428571424" class="ct-bar" ct:value="674"></line><line x1="257.6666666666667" x2="257.6666666666667" y1="86.84571428571428" y2="20.39999999999999" class="ct-bar" ct:value="684"></line><line x1="278.3333333333333" x2="278.3333333333333" y1="88.20571428571428" y2="23.11999999999999" class="ct-bar" ct:value="670"></line><line x1="299" x2="299" y1="87.33142857142857" y2="24.480000000000004" class="ct-bar" ct:value="647"></line><line x1="319.6666666666667" x2="319.6666666666667" y1="86.94285714285714" y2="21.371428571428567" class="ct-bar" ct:value="675"></line><line x1="340.3333333333333" x2="340.3333333333333" y1="84.02857142857142" y2="24.86857142857143" class="ct-bar" ct:value="609"></line><line x1="361" x2="361" y1="86.94285714285714" y2="21.857142857142847" class="ct-bar" ct:value="670"></line><line x1="381.6666666666667" x2="381.6666666666667" y1="91.60571428571428" y2="31.862857142857152" class="ct-bar" ct:value="615"></line><line x1="402.3333333333333" x2="402.3333333333333" y1="89.66285714285715" y2="28.462857142857146" class="ct-bar" ct:value="630"></line><line x1="423" x2="423" y1="91.31428571428572" y2="32.34857142857143" class="ct-bar" ct:value="607"></line><line x1="443.6666666666667" x2="443.6666666666667" y1="88.78857142857143" y2="25.64571428571429" class="ct-bar" ct:value="650"></line><line x1="464.3333333333333" x2="464.3333333333333" y1="87.13714285714286" y2="27.685714285714283" class="ct-bar" ct:value="612"></line><line x1="485" x2="485" y1="90.53714285714285" y2="23.022857142857134" class="ct-bar" ct:value="695"></line><line x1="505.6666666666667" x2="505.6666666666667" y1="84.32" y2="16.90285714285713" class="ct-bar" ct:value="694"></line><line x1="526.3333333333334" x2="526.3333333333334" y1="86.74857142857142" y2="23.99428571428571" class="ct-bar" ct:value="646"></line><line x1="547.0000000000001" x2="547.0000000000001" y1="83.73714285714286" y2="21.75999999999999" class="ct-bar" ct:value="638"></line><line x1="567.6666666666667" x2="567.6666666666667" y1="83.93142857142857" y2="25.35428571428571" class="ct-bar" ct:value="603"></line><line x1="588.3333333333334" x2="588.3333333333334" y1="84.70857142857143" y2="23.799999999999997" class="ct-bar" ct:value="627"></line><line x1="609.0000000000001" x2="609.0000000000001" y1="83.83428571428571" y2="21.662857142857135" class="ct-bar" ct:value="640"></line><line x1="629.6666666666667" x2="629.6666666666667" y1="85.19428571428571" y2="25.45142857142858" class="ct-bar" ct:value="615"></line></g></g><g class="ct-labels"></g></svg></div>
            </div>
          </div>
          <!-- End Widget Stacked Bar -->
        </div>

        <div class="col-lg-3">
              <div class="card card-block p-25">
                <div class="counter counter-lg">
                  <span class="counter-number">5</span>
                  <div class="counter-label text-uppercase">RUTAS ACTIVAS</div>
                </div>
              </div>
            </div>

       <div class="col-lg-3">
              <div class="card card-block p-25">
                <div class="counter counter-lg">
                  <span class="counter-number">60.786</span>
                  <div class="counter-label text-uppercase">USUARIOS TRANSPORTADOS</div>
                </div>
              </div>
        </div>     

        <div class="col-lg-3">
              <div class="card card-block p-25">
                <div class="counter counter-lg">
                  <span class="counter-number">60</span>
                  <div class="counter-label text-uppercase">OFERTAS ACTIVAS</div>
                </div>
              </div>
        </div>

        <div class="col-lg-3">
              <div class="card card-block p-25">
                <div class="counter counter-lg">
                  <span class="counter-number">67.000</span>
                  <div class="counter-label text-uppercase">CLICS EN OFERTAS</div>
                </div>
              </div>
        </div>

        <div  class="col-lg-6">

          <div class="panel">
        <header class="panel-heading">
          <div class="panel-actions"></div>
          <h3 class="panel-title">TODAS LAS RUTAS</h3>
        </header>
        <div class="panel-body">
          <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
            <div class="row">              

                <div class="col-sm-12">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>Código</th>
                        <th>Vehículo</th>
                        <th>Opera desde</th>
                        <th>Pasajeros</th>
                        <th>Estado</th>
                        <th>Servicios</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>Teagan</td>
                        <td>Prohaska</td>
                        <td>@Elijah</td>
                        <td>
                          <span class="badge badge-danger">admin</span>
                        </td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>Andy</td>
                        <td>Gaylord</td>
                        <td>@Ramiro</td>
                        <td>
                          <span class="badge badge-info">member</span>
                        </td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>Veronica</td>
                        <td>Gusikowski</td>
                        <td>@Maxime</td>
                        <td>
                          <span class="badge badge-warning">developer</span>
                        </td>
                      </tr>
                      <tr>
                        <td>4</td>
                        <td>Bruce</td>
                        <td>Rogahn</td>
                        <td>@Maggio</td>
                        <td>
                          <span class="badge badge-success">supporter</span>
                        </td>
                      </tr>
                      <tr>
                        <td>5</td>
                        <td>Carolina</td>
                        <td>Hickle</td>
                        <td>@Hammes</td>
                        <td>
                          <span class="badge badge-info">member</span>
                        </td>
                      </tr>
                      <tr>
                        <td>6</td>
                        <td>Madaline</td>
                        <td>Eichmann</td>
                        <td>@Amaya</td>
                        <td>
                          <span class="badge badge-success">supporter</span>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- End Example Basic -->
            
               </div>

        </div>  
        
        
      </div>
    </div>  

      <div  class="col-lg-6">

          <div class="panel">
        <header class="panel-heading">
          <div class="panel-actions"></div>
          <h3 class="panel-title">TODAS LAS OFERTAS</h3>
        </header>
        <div class="panel-body">
          <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
            <div class="row">              

                <div class="col-sm-12">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>Fecha</th>
                        <th>Nombre</th>
                        <th>Clics</th>
                        <th>Estado</th>
                        <th>Tipo</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>Teagan</td>
                        <td>Prohaska</td>
                        <td>@Elijah</td>
                        <td>
                          <span class="badge badge-danger">admin</span>
                        </td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>Andy</td>
                        <td>Gaylord</td>
                        <td>@Ramiro</td>
                        <td>
                          <span class="badge badge-info">member</span>
                        </td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>Veronica</td>
                        <td>Gusikowski</td>
                        <td>@Maxime</td>
                        <td>
                          <span class="badge badge-warning">developer</span>
                        </td>
                      </tr>
                      <tr>
                        <td>4</td>
                        <td>Bruce</td>
                        <td>Rogahn</td>
                        <td>@Maggio</td>
                        <td>
                          <span class="badge badge-success">supporter</span>
                        </td>
                      </tr>
                      <tr>
                        <td>5</td>
                        <td>Carolina</td>
                        <td>Hickle</td>
                        <td>@Hammes</td>
                        <td>
                          <span class="badge badge-info">member</span>
                        </td>
                      </tr>
                      <tr>
                        <td>6</td>
                        <td>Madaline</td>
                        <td>Eichmann</td>
                        <td>@Amaya</td>
                        <td>
                          <span class="badge badge-success">supporter</span>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- End Example Basic -->
            
               </div>

        </div>  
        
        
      </div>
    </div>
    
    </div>
    </div>
    </div>
  

  




@endsection
