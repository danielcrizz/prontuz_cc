@extends('layouts.app')

@section('content')

<meta charset="utf-8">
<div class="page bg-white">
    <div class="page-aside">
      <!-- Contacts Sidebar -->
      <div class="page-aside-switch">
        <i class="icon wb-chevron-left" aria-hidden="true"></i>
        <i class="icon wb-chevron-right" aria-hidden="true"></i>
      </div>
      <div class="page-aside-inner page-aside-scroll">
        <div data-role="container">
          <div data-role="content">
            <div class="page-aside-section">
              <div class="list-group">
                <a class="list-group-item justify-content-between" id="listAllUsers" href="javascript:void(0)">
                  <span>
                    <i class="icon wb-inbox" aria-hidden="true"></i> Todos los usuarios
                  </span>
                  <span class="item-right" id="totalUsers"></span>
                </a>
              </div>
            </div>
            <div class="page-aside-section">
              <h5 class="page-aside-title">ROLES</h5>
              <div class="list-group">
                <a class="list-group-item justify-content-between" id="listAdminUsers" href="javascript:void(0)">
                  <span>
                    Administradores
                  </span>
                  <span class="item-right" id="numAdmin"></span>
                </a>
                <a class="list-group-item justify-content-between" id="listUsers" href="javascript:void(0)">
                  <span>
                    Usuarios
                  </span>
                  <span class="item-right" id="numUser"></span>
                </a>
                <a class="list-group-item justify-content-between" id="listDriverUsers" href="javascript:void(0)">
                  <span>
                    Conductores
                  </span>
                  <span class="item-right" id="numDriver"></span>
                </a>
                <a class="list-group-item justify-content-between" id="listDriverUsers" href="javascript:void(0)">
                  <span>
                    Wheel
                  </span>
                  <span class="item-right" id="numWheel"></span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Contacts Content -->
    <div class="page-main">
      <!-- Contacts Content Header -->
      <div class="page-header">
        <h1 class="page-title">Lista de Usuarios</h1>
      </div>
      <!-- Contacts Content -->
      <div id="contactsContent" class="page-content page-content-table" data-plugin="selectable">
        <!-- Contacts -->
        <table id='users_list' width="100%" class="table is-indent" data-plugin="animateList" data-animate="fade" data-child="tr"
        data-selectable="selectable">
          <thead>
            <tr>
              <th class="pre-cell"></th>
              <th class="cell-300" scope="col">Nombre y Apellido</th>
              <th class="cell-300" scope="col">Identificación</th>
              <th class="cell-300" scope="col">Telefono</th>
              <th scope="col">Email</th>
              <th scope="col">Rol</th>
              <th scope="col">Estatus</th>
            </tr>
          </thead>
          <tbody id="bodytable">
            <div id="loaderlist" style="display:none;">
            <center>
                <br><br><br><br>
                 <img width="48px" src="{{ asset('iconbar/loading.gif') }}" /> &nbsp; Cargando información ...
                <br><br>
            </center>
          </div>
          </tbody>
        </table>
        
      </div>
    </div>
  </div>
  <!-- Site Action -->
  <div class="site-action" data-plugin="actionBtn">
    <button type="button" id = "actionBtn" class="site-action-toggle btn-raised btn btn-success btn-floating">
      <i class="front-icon wb-plus animation-scale-up" aria-hidden="true"></i>
      <i class="back-icon wb-close animation-scale-up" aria-hidden="true"></i>
    </button>
    <div class="site-action-buttons">
      <button type="button" data-action="trash" class="btn-raised btn btn-success btn-floating animation-slide-bottom">
        <i class="icon wb-trash" aria-hidden="true"></i>
      </button>
      <button type="button" data-action="folder" class="btn-raised btn btn-success btn-floating animation-slide-bottom">
        <i class="icon wb-folder" aria-hidden="true"></i>
      </button>
    </div>
  </div>
  <!-- End Site Action -->


  <!-- Add User Form -->
  <div class="modal fade modal-3d-flip-vertical" id="addUserForm" aria-hidden="true" aria-labelledby="addUserForm" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
          <h4 class="modal-title">Crear Nuevo Usuario</h4>
        </div>
        <div class="modal-body">

        <div id="loader" style="display:none;">
          <center>
              <br><br>
               <img width="48px" src="{{ asset('iconbar/loading.gif') }}" /> &nbsp; Enviando ...
              <br><br>
          </center>
        </div>

        <form id="data" method="post" enctype="multipart/form-data" action="{{ url('users/store') }}">

          <div class="form-group">
                <div id="error">
                <!-- error will be shown here ! -->
                </div>
            </div>

          {{ csrf_field() }}

          <input type="hidden" id="rol" value="2">

          <div class="row">
            <div class="col-md-4">
                <span class="checkbox-custom checkbox-success checkbox-md">
                    <input type="radio" name="user_rolId" checked value="2"  class="contacts-radio" id="selusers"
                    />
                  <label for="selusers">Usuario</label>
                </span>
            </div>
            <div class="col-md-4">
                <span class="checkbox-custom checkbox-success checkbox-md">
                    <input type="radio" name="user_rolId" value="3" class="contacts-radio" id="seldrivers"
                    />
                  <label for="seldrivers">Conductor</label>
                </span>
            </div>
            <div class="col-md-4">
                <span class="checkbox-custom checkbox-success checkbox-md">
                    <input type="radio" name="user_rolId" value="1" class="contacts-radio" id="selmadmin"
                    />
                  <label for="selmadmin">Administrador</label>
                </span>
            </div>
          </div>
          
            <div class="form-group"> 
                <center>
                  <label for="userImage">
                      <img id="imgperfil" src="{{ asset('iconbar/assets/images/006-meta-A.jpg') }}" style="border-radius: 150px; width: 150px; height: 150px; background-repeat: no-repeat; background-position: 50%; border-radius: 50%; background-size: 100% auto; border:1px solid #999;" />
                  </label>
                  <input type="file" name="userImage" id="userImage" />
                  <p>* Clic para cargar imagen</p>
                </center>
            </div>
            
            <div class="form-group formdrivers"> 
              <div class="radio-custom radio-primary">
                <input id="driverType" name="driverType" type="radio" />
                <!--  checked="" -->
                <label for="driverType">Wheel</label>
              </div>
            </div>
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="text" required class="form-control" id="userFirstname" name="userFirstname" />
              <label class="floating-label">Nombres</label>
            </div>            
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="text" required class="form-control" id="userLastname" name="userLastname" />
              <label class="floating-label">Apellidos</label>
            </div>

            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="email" required class="form-control" id="userEmail" name="userEmail"  />
              <label class="floating-label">Email</label>
            </div>

            <div class="form-group form-material floating formadmin" data-plugin="formMaterial">
              <input title="8 a 16 characters" type="password" pattern=".{8,16}" class="form-control" id="password" name="password" maxlength="16" />
              <label class="floating-label">Contraseña</label>
            </div>

            <div class="row">
              <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label class="floating-label">Tipo de Documento de Identidad</label>
                    <select required class="form-control" id="user_doctypeId" name="user_doctypeId" data-plugin="select2" data-placeholder="Seleccione" data-allow-clear="true">
                    </select>
                  </div>
              </div>
              <div class="col-md-6 col-xs-12">
                  <div class="form-group form-material floating" data-plugin="formMaterial">
                    <input required type="text" class="form-control" id="userIdentificacion" name="userIdentificacion"  onkeypress='return event.charCode >= 31 && event.charCode <= 57 || event.charCode ==8 || event.charCode == 46  || event.charCode == 0'/>
                    <label class="floating-label">Nro Identificación</label>
                  </div>
              </div>
            </div>
            
            <div class="form-group form-material floating formdrivers" data-plugin="formMaterial">
              <input type="text" class="form-control" id="userLicence" name="userLicence" maxlength="8" />
              <label class="floating-label">Nro Licencia Conducción</label>
            </div>

            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input required type="text" class="form-control" id="userBorndate" name="userBorndate" data-plugin="datepicker" data-plugin-options='es' data-format="dd/mm/yyyy"/>
              <label class="floating-label">Fecha de Nacimiento</label>
            </div>

            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input required type="text" class="form-control" id="userPhone" name="userPhone" onkeypress='return event.charCode >= 31 && event.charCode <= 57 || event.charCode ==8 || event.charCode == 46  || event.charCode == 0' />
              <label class="floating-label">Telefono</label>
            </div>            

            <p class="formadmin">Secciones a administra:</p>
            <div class="row formadmin">
              <div class="col-md-3 col-xs-6">
                  <span class="checkbox-custom checkbox-success checkbox-md">
                      <input type="checkbox" name="user_menu[0]" class="contacts-checkbox" checked value="1" id="user_menu0"
                      />
                    <label for="user_menu0">Usuarios</label>
                  </span>
              </div>
              <div class="col-md-3 col-xs-6">
                  <span class="checkbox-custom checkbox-success checkbox-md">
                      <input type="checkbox" name="user_menu[1]" class="contacts-checkbox" checked value="2" id="user_menu1"
                      />
                    <label for="user_menu1">Vehículos</label>
                  </span>
              </div>
              <div class="col-md-3 col-xs-6">
                  <span class="checkbox-custom checkbox-success checkbox-md">
                      <input type="checkbox" name="user_menu[2]" class="contacts-checkbox" checked value="3" id="user_menu2"
                      />
                    <label for="user_menu2">Rutas</label>
                  </span>
              </div>
              <div class="col-md-3 col-xs-6">
                  <span class="checkbox-custom checkbox-success checkbox-md">
                      <input type="checkbox" name="user_menu[3]" class="contacts-checkbox" checked value="4" id="user_menu3"
                      />
                    <label for="user_menu3">Recargas</label>
                  </span>
              </div>
            </div>
            <div class="row formadmin">
              <div class="col-md-3 col-xs-6">
                  <span class="checkbox-custom checkbox-success checkbox-md">
                      <input type="checkbox" name="user_menu[4]" class="contacts-checkbox" checked value="5" id="user_menu4"
                      />
                    <label for="user_menu4">Transito</label>
                  </span>
              </div>
              <div class="col-md-3 col-xs-6">
                  <span class="checkbox-custom checkbox-success checkbox-md">
                      <input type="checkbox" name="user_menu[5]" class="contacts-checkbox" checked value="6" id="user_menu5"
                      />
                    <label for="user_menu5">Ofertas</label>
                  </span>
              </div>
              <div class="col-md-3 col-xs-6">
                  <span class="checkbox-custom checkbox-success checkbox-md">
                      <input type="checkbox" name="user_menu[6]" class="contacts-checkbox" checked value="7" id="user_menu6"
                      />
                    <label for="user_menu6">Noticias</label>
                  </span>
              </div>
              <div class="col-md-3 col-xs-6">
                  <span class="checkbox-custom checkbox-success checkbox-md">
                      <input type="checkbox" name="user_menu[7]" class="contacts-checkbox" checked value="8" id="user_menu7"
                      />
                    <label for="user_menu7">Pagos</label>
                  </span>
              </div>
            </div>
            
            
            <input type="hidden" name="userState" value="1">
            

            <br>

            <div class="form-group col-xl-12 text-right padding-top-m">
                <button class="btn btn-success" type="submit">Crear</button>
                <a class="btn btn-sm btn-white" data-dismiss="modal" href="javascript:void(0)">Cancelar</a>
            </div>

          </form>

        </div>
      </div>
    </div>
  </div>
  <!-- End Add User Form -->
  <script type="text/javascript">
    $(document).ready(function() {
        var token = $("input[name*='_token']").val();
        $('#userBorndate').datepicker({
          endDate: 'dateToday'
        });
        $("#data").trigger( "reset" );
        $('.formadmin').hide();
        $('.formdrivers').hide();

        list_user();
        totalKindUsers();
        if (window.location.host == 'localhost') {
            var baseurl = window.location.protocol +"//" + window.location.host + '/prontuz_backend/public';
        }else{
            var baseurl = window.location.protocol +"//" + window.location.host ;
        }


        $( "#user_menu0" ).change(function() {
             var value = $("#user_menu0").val();
             if (value == 0) {
                $("#user_menu0").val(1);
             }else if (value == 1){
                $("#user_menu0").val(0);
             }       
        });

        $( "#user_menu1" ).change(function() {
             var value = $("#user_menu1").val();
             if (value == 0) {
                $("#user_menu1").val(2);
             }else if (value == 2){
                $("#user_menu1").val(0);
             }       
        });

        $( "#user_menu2" ).change(function() {
             var value = $("#user_menu2").val();
             if (value == 0) {
                $("#user_menu2").val(3);
             }else if (value == 3){
                $("#user_menu2").val(0);
             }       
        });

        $( "#user_menu3" ).change(function() {
             var value = $("#user_menu3").val();
             if (value == 0) {
                $("#user_menu3").val(4);
             }else if (value == 4){
                $("#user_menu3").val(0);
             }       
        });

        $( "#user_menu4" ).change(function() {
             var value = $("#user_menu4").val();
             if (value == 0) {
                $("#user_menu4").val(5);
             }else if (value == 5){
                $("#user_menu4").val(0);
             }       
        });

        $( "#user_menu5" ).change(function() {
             var value = $("#user_menu5").val();
             if (value == 0) {
                $("#user_menu5").val(6);
             }else if (value == 6){
                $("#user_menu5").val(0);
             }       
        });

        $( "#user_menu6" ).change(function() {
             var value = $("#user_menu6").val();
             if (value == 0) {
                $("#user_menu6").val(7);
             }else if (value == 7){
                $("#user_menu6").val(0);
             }       
        });
        $( "#user_menu7" ).change(function() {
             var value = $("#user_menu7").val();
             if (value == 0) {
                $("#user_menu7").val(8);
             }else if (value == 8){
                $("#user_menu7").val(0);
             }       
        });
        $(".contacts-radio").change(function() {
            $('#rol').val($(this).val());
            switch($(this).val()) {
                case '1' :
                    $('.formadmin').show();
                    $('.formusers').hide();
                    $('.formdrivers').hide();
                    $('#userLicence').val('');
                    $('#userLicence').attr("required", false);
                    $('#password').attr("required", true);
                    break;
                case '2' :
                    $('.formadmin').hide();
                    $('.formusers').show();
                    $('.formdrivers').hide();
                    $('#userLicence').val('');
                    $('#userLicence').attr("required", false);
                    $('#userLicence').removeAttr("required");
                    $('#password').val('');
                    $('#password').attr("required", false);
                    $('#password').removeAttr("required");                    
                    break;
                case '3' :
                    $('.formadmin').hide();
                    $('.formusers').hide();
                    $('.formdrivers').show();
                    $('#userLicence').attr("required", true);
                    $('#password').val('');
                    $('#password').attr("required", false);
                    break;
            }            
        });

        $("form#data").submit(function() {
            var formData = new FormData($(this)[0]);
            var e = true;
            if($("#userImage").val() == '' && radioTU() == true)
            {
                e = false;
            }
            if (($("input[name*='user_menu[0]']").val() == 0) &&  
                ($("input[name*='user_menu[1]']").val() == 0) && 
                ($("input[name*='user_menu[2]']").val() == 0) &&
                ($("input[name*='user_menu[3]']").val() == 0) &&
                ($("input[name*='user_menu[4]']").val() == 0) &&
                ($("input[name*='user_menu[5]']").val() == 0) &&
                ($("input[name*='user_menu[6]']").val() == 0) &&
                ($("#rol").val() == 1)) {

              $("#error").fadeIn(1000, function(){      
              $("#error").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> Debe seleccionar al menos una sección para administrar</div>');
              });

            }else{
                swal({
                    title: "CONFIRMAR",
                    text: "Esta seguro de realizar la operación?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#2A67BD",
                    confirmButtonText: "Confirmar",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    showLoaderOnConfirm: true
                },
                function(isConfirm){    
                  if(isConfirm)
                  {
                    // console.log(formData);
                    // var token = $("input[name*='_token']").val()
                       $.ajax({
                        type: 'POST',
                        url: baseurl+'/users/store',
                        headers:{'x-csrf-token':token},
                        data: {_token:token},
                        data: formData,
                        dataType: 'JSON',
                        async: false,
                        cache: false,
                        contentType: false,
                        processData: false,
                        beforeSend: function(){ 
                          $("#error").fadeOut();
                          $("#data").fadeOut();
                          $("#loader").fadeIn();
                        },
                        success: function (response) {
                          $("#loader").fadeOut();
                          if(response.ResponseCode == 0){

                            $("#data").fadeIn();
                            $("#error").fadeIn(1000, function(){      
                            $("#error").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> '+response.ResponseMessage+'</div>');
                                 $("#btn-login").html('<span class="icon ion-android-lock"></span> &nbsp; Entrar');
                               });
                            swal({
                              title: "Error!",
                              text: response.ResponseMessage,
                              type: "Error",
                              timer: 1000,
                              showConfirmButton: false
                            }); 
                          }else{
                           $("#addUserForm").modal('hide');
                           $("#addUserForm input").val('');
                           $("#imgperfil").empty().attr('src',baseurl+'/iconbar/assets/images/006-meta-A.jpg');
                            swal({
                                      title: "ÉXITO!",
                                      text: "Operación Exitosa!",
                                      type: "success",
                                      timer: 1000,
                                      showConfirmButton: false
                                    }); 
                            setTimeout(' window.location.href = "'+baseurl+'/users"; ',1500);  
                          }

                        },
                        error: function (response) {
                            $("#error").fadeIn(1000, function(){      
                            $("#error").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> Error de comunicación, revise su conexion a internet.</div>');
                            });
                        }
                      });
                  }  
                  else
                  {
                    swal({
                              title: "Cancelada!",
                              text: "Operación Cancelada",
                              type: "success",
                              timer: 1000,
                              showConfirmButton: false
                        }); 
                  } 
                });
              }

            return false;
        });


        
        //tipos de documentos
        $.ajax({
            type: "GET",
            url: baseurl+'/users/DocumentKind',
            dataType: 'JSON',
            success: function (data) {
                

                var toAppend = '';
                $.each(data.ResponseData,function(k, v){
                    //console.log(v.doctypeName);
                    toAppend += '<option value="'+v.doctypeId+'">'+v.doctypeName+'</option>';
                });

                 $('#user_doctypeId').append(toAppend);
                 $('#user_doctypeId').trigger("select2:updated");

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });

        $("#bodytable").html('');
        var token = $("input[name*='_token']").val();

        $("#inputSearch").focusout(function() {
            $("#bodytable").html('');
            var value = $("#inputSearch").val();

            if (value == '') {
                $.ajax({
                    type: "POST",
                    url: baseurl+'/users/listing',
                    data: {userState: 1, _token: token},
                    dataType: 'JSON',
                    async: false,
                    beforeSend: function(){ 
                      $("#bodytable").fadeOut();
                      $("#loaderlist").fadeIn();
                    },
                    success: function (data) {
                        
                        $("#loaderlist").fadeOut();
                        var toAppend = '';
                        var numAdmin = 0;
                        var numUser = 0;
                        var numDriver = 0;
                        var totalUsers = 0;
                        $.each(data.ResponseData,function(k, v){
                            //console.log(v.userEmail);

                            if (v.user_rolId == 1) {
                                numAdmin = numAdmin + 1;
                            }else if(v.user_rolId == 2){
                                numUser = numUser + 1;
                            }else if(v.user_rolId == 3){
                                numDriver = numDriver + 1;
                            }else if(v.user_rolId == 4){
                                numWheel = numWheel + 1;
                            }

                            totalUsers = totalUsers + 1;

                            toAppend += '<tr data-url="users/'+v.userId+'/card" data-toggle="slidePanel"><td class="pre-cell"></td><td class="cell-300"><a class="avatar" href="javascript:void(0)"><img class="img-fluid" src="'+baseurl+'/'+v.userImage+'" alt="..."></a>'+v.userName+'</td><td class="cell-300">'+v.userPhone+'</td><td>'+v.userEmail+'</td><td><span class="badge badge-outline badge-success">'+v.rolName+'</span></td></tr>';
                        });

                         $('#bodytable').append(toAppend);

                         $('#totalUsers').html(totalUsers);
                         $('#numAdmin').html(numAdmin);
                         $('#numUser').html(numUser);
                         $('#numDriver').html(numDriver);
                         $('#numWheel').html(numWheel);

                         $("#bodytable").fadeIn();
                         

                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }else{
                $.ajax({
                    type: "GET",
                    url: baseurl+'/users/find',
                    data: {dataSearch: value},
                    dataType: 'JSON',
                    beforeSend: function(){ 
                      $("#bodytable").fadeOut();
                      $("#loaderlist").fadeIn();
                    },
                    success: function (data) {
                        
                        $("#loaderlist").fadeOut();
                        var toAppend = '';
                        var numAdmin = 0;
                        var numUser = 0;
                        var numDriver = 0;
                        var totalUsers = 0;
                        $.each(data.ResponseData,function(k, v){
                            //console.log(v.userEmail);

                            if (v.user_rolId == 1) {
                                numAdmin = numAdmin + 1;
                            }else if(v.user_rolId == 2){
                                numUser = numUser + 1;
                            }else if(v.user_rolId == 3){
                                numDriver = numDriver + 1;
                            }else if(v.user_rolId == 4){
                                numWheel = numWheel + 1;
                            }

                            totalUsers = totalUsers + 1;

                            toAppend += '<tr data-url="users/'+v.userId+'/card" data-toggle="slidePanel"><td class="pre-cell"></td><td class="cell-300"><a class="avatar" href="javascript:void(0)"><img class="img-fluid" src="'+baseurl+'/'+v.userImage+'" alt="..."></a>'+v.userName+'</td><td class="cell-300">'+v.userPhone+'</td><td>'+v.userEmail+'</td><td><span class="badge badge-outline badge-success">'+v.rolName+'</span></td></tr>';
                            
                        });

                         $('#bodytable').append(toAppend);

                         $('#totalUsers').html(totalUsers);
                         $('#numAdmin').html(numAdmin);
                         $('#numUser').html(numUser);
                         $('#numDriver').html(numDriver);
                         $('#numWheel').html(numWheel);

                         $("#bodytable").fadeIn();
                         

                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
           
        });  

        //validar email
        $("#userEmail").focusout(function() {
            var value = $(this).val();
            if (value != '') {
                $.ajax({
                  type: "POST",
                  url: baseurl+'/users/verifyEmail',
                  data: {userEmail: value, _token: token},
                  dataType: 'JSON',
                  success: function (response) {
                      
                      if (response > 0) {
                        $("#userEmail").val('');
                        $("#error").fadeIn(1000, function(){      
                        $("#error").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> Correo ya se registro anteriormente.</div>');
                        });

                      }else{
                        $("#error").fadeOut();
                        $("#error").html('');
                      }

                  },
                  error: function (data) {
                      console.log('Error:', data);
                  }
                });
            }

        });

        //validar identificacion
        $("#userIdentificacion").focusout(function() {
            var value = $(this).val();
            var value2 = $('#user_doctypeId').val();
            if (value != '') {
                $.ajax({
                  type: "POST",
                  url: baseurl+'/users/verifyIdentification',
                  data: {userIdentificacion: value, user_doctypeId: value2, _token: token},
                  dataType: 'JSON',
                  success: function (response) {
                      
                      if (response > 0) {
                        $("#userIdentificacion").val('');
                        $("#error").fadeIn(1000, function(){      
                        $("#error").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> Identificación ya se registro anteriormente.</div>');
                        });

                      }else{
                        $("#error").fadeOut();
                        $("#error").html('');
                      }

                  },
                  error: function (data) {
                      console.log('Error:', data);
                  }
                });
            }

        });
           
        

        $("#userImage").change(function(){
              var reader = new FileReader();
              reader.onload = function (e) {
               $('#imgperfil').attr('src', e.target.result);
              }
              reader.readAsDataURL(this.files[0]);
        });

        


    });
    var rowSelection='';
    function list_user()
    {

        rowSelection = $('#users_list').DataTable( {
                "oLanguage":
                {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                           "sNext":  '<span class="icon wb-chevron-right-mini"></span>',
                        "sPrevious": '<span class="icon wb-chevron-left-mini"></span>'
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                },
                "ajax":
                {
                    'url':'/users/listing',
                    'data': {userState: 1, _token: $("input[name*='_token']").val()},
                    "type": "POST",
                },
                "processing": true,
                "serverSide": true,
                'bLengthChange':true,
                "lengthMenu": [[50,75, 100, -1], [50,75, 100, "All"]],
                'order':[[1, 'asc']],
                'info':false,
                'createdRow': function( row, data, dataIndex ) 
                {
                    $(row).attr('data-url', 'users/'+data.userId+'/card');
                    $(row).attr('data-toggle', 'slidePanel');
                },
               "columnDefs": [
                    {
                      "targets": 0,
                      "data": "userImage",
                      "render" : function ( data) {
                        return '<img class="img-circle" style="max-width:60px;max-height:60px;background-repeat: no-repeat; background-position: 50%; border-radius: 50%;background-size: 100% auto;" alt="public/icon-user 2.png" src="'+data+'"/>';
                        },
                        className:'col-xs-1',
                        "orderable": false,
                    },
                    {
                      "targets": 1,
                      "data": "FullName"
                    },                     
                    {
                      "targets": 2,
                      "data": "FullId",
                    }, 
                    {
                      "targets": 3,
                      "data": "userPhone",
                      className:'col-lg-3',
                    },
                    {
                      "targets": 4,
                      "data": "userEmail",
                      className:'col-lg-3',
                    },
                    {
                      "targets": 5,
                      "data": "rolName",
                      "render" : function ( data) {
                        return '<span class="badge badge-outline badge-success">'+data+'</span>';
                        },
                      className:'col-lg-1'
                    },
                    {
                      "targets": 6,
                      "data": "userState",
                      "render" : function ( data) {
                            if(data==1)
                            {
                                return '<span class="badge badge-outline badge-success">Activo</span>';
                            }
                            else
                            {
                                return '<span class="badge badge-outline badge-danger">Inactivo</span>';
                            }
                        },
                      className:'col-lg-1',
                    },
                ], 
                destroy: true,
            "responsive": true,
        } );
    }

    function totalKindUsers() 
    {
       $.ajax({
            type: "POST",
            url: '/users/totalKindUsers',
            data: {userState: 1, _token: $("input[name*='_token']").val()},
            dataType: 'JSON',
            beforeSend: function(){ 
              $("#bodytable").fadeOut();
              $("#loaderlist").fadeIn();
            },
            success: function (data) {
                
                $("#loaderlist").fadeOut();  
                 $('#totalUsers').html(data.totalUsers);
                 $('#numAdmin').html(data.numAdmin);
                 $('#numUser').html(data.numUser);
                 $('#numDriver').html(data.numDriver);
                 $('#numWheel').html(data.numWheel);
                 $("#bodytable").fadeIn();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });

    }
    $('#actionBtn').click(function(event) {
        $('#data').show();
    });

    function radioTU()
    {
        var e = '';
        $('input[name="user_rolId"]:checked').each(function() 
        {
            switch(this.value) {
                case '1' :
                    e = false;
                    break;
                case '2' :
                     e = false;               
                    break;
                case '3' :
                     e = true;
                    break;
            }  
        });  
        return e;        
    }
  </script>

@endsection