

<header class="slidePanel-header overlay" style="height: 220px; background-image: url('iconbar/global/photos/placeholder.jpg');')">
<form id="dataPhoto" method='POST' action='users/updatePhoto'>
{{ csrf_field() }}
  <div class="overlay-panel overlay-background vertical-align">
    <div class="slidePanel-actions">
      <div class="btn-group">
        <!--button type="button" id="deleteUsers" class="btn btn-pure btn-inverse icon wb-trash" aria-hidden="true"></button-->
        <button type="button" class="btn btn-pure btn-inverse slidePanel-close icon wb-close"
          aria-hidden="true"></button>
    </div>
    </div>    
    <div class="vertical-align-middle" style="display:none;" >
        <a class="avatar" href="javascript:void(0)">
            <img id="userImageEdit" alt="...">
        </a> 
        <a class="userName1" href="javascript:void(0)" style="text-decoration: none; color: #fff;">
            <h3 class="name" id="userName1"></h3>  
        </a>       
    </div>
    <button type="button" class="edit btn btn-success btn-floating" data-toggle="edit">
      <i class="icon wb-pencil animation-scale-up" aria-hidden="true"></i>
      <i class="icon wb-close animation-scale-up" aria-hidden="true"></i>
    </button>
  </div>
    <input type="file" name="new_img" id = 'new_img' class='hidden'>
    <input type="hidden" id="userIdPhoto" name="userIdPhoto" value="{!! $userId !!}">
  </form>
</header>
<div class="slidePanel-inner">
  <br>
  <h4 class="modal-title">Editar Datos del Usuario</h4>
  <table class="user-info" style="display:none;">
    <tbody>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Rol de Usuario:</td>
        <td>
          <span id="user_rolId1"></span>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Nombre Apellido:</td>
        <td>
          <span id="userName2"></span>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Email:</td>
        <td>
          <span id="userEmail1"></span>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Identificación:</td>
        <td>
          <span id="userIdentificacion1"></span>
        </td>
      </tr>
      <tr class="formdriversEdit">
        <td  style="width: 120px !important;" class="info-label">Nro Lic. Conducción:</td>
        <td>
          <span id="userLicence1"></span>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Fecha de Nac.:</td>
        <td>
          <span id="userBorndate1"></span>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Telefono:</td>
        <td>
          <span id="userPhone1"></span>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Status:</td>
        <td>
          <span id="userState1"></span>
        </td>
      </tr>
      
      
    </tbody>
  </table>


  <div class="form-group">
      <div id="errorEdit">
      <!-- error will be shown here ! -->
      </div>
  </div>

  <div class="loaderSend">
    <center>
         <img width="48px" src="{{ asset('iconbar/loading.gif') }}" /> &nbsp; Cargando ...
    </center>
  </div>

  <div id="loaderEdit" style="display:none;">
    <center>
        <br><br>
         <img width="48px" src="{{ asset('iconbar/loading.gif') }}" /> &nbsp; Enviando ...
        <br><br>
    </center>
  </div>


  <form id="dataEdit" style="display:none;" method="PUT" action="{{ url('users/put') }}">
  <input type="hidden" id="userIdEdit" name="userId" value="{!! $userId !!}">

    {{ csrf_field() }}

    <input type="hidden" id="rolEdit" value="2">

    <div class="row">
      <div class="col-md-4">
          <span class="checkbox-custom checkbox-success checkbox-md">
              <input onchange='radio()' type="radio" name="user_rolId" value="2"  class="contacts-radio" id="selusersEdit"
              />
            <label for="selusersEdit">Usuario</label>
          </span>
      </div>
      <div class="col-md-4">
          <span class="checkbox-custom checkbox-success checkbox-md">
              <input onchange='radio()' type="radio" name="user_rolId" value="3" class="contacts-radio" id="seldriversEdit"
              />
            <label for="seldriversEdit">Conductor</label>
          </span>
      </div>
      <div class="col-md-4">
          <span class="checkbox-custom checkbox-success checkbox-md">
              <input onchange='radio()' type="radio" name="user_rolId" value="1" class="contacts-radio" id="seladminEdit"
              />
            <label for="seladminEdit">Administrador</label>
          </span>
      </div>
    </div>

  <table>
    <tbody>
      <tr class="formdriversEdit">
        <td colspan="2">
          <div class="form-group formdrivers"> 
              <div class="radio-custom radio-primary">
                <input id="driverTypeEdit" name="driverTypeEdit" type="radio" />
                <input id="driverTypeEdit2" name="driverTypeEdit2" type="hidden" value="0"/>
                <!--  checked="" -->
                <label for="driverTypeEdit">Wheel</label>
              </div>
            </div>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Nombre Apellido:</td>
        <td>
          <div class="form-group form-material floating">
            <input required type="text" class="form-control" name="userFirstname" id="userFirstnameEdit">
          </div>
        </td>
      </tr>      
      <tr>
        <td  style="width: 120px !important;" class="info-label"> Apellido:</td>
        <td>
          <div class="form-group form-material floating">
            <input required type="text" class="form-control" name="userLastname" id="userLastnameEdit">
          </div>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Email:</td>
        <td>
          <div class="form-group form-material floating">
            <input required type="email" class="form-control" name="userEmail" id="userEmailEdit">
          </div>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Tipo de Documento:</td>
        <td>
          <div class="form-group form-material floating">
            <select required class="form-control"  id="user_doctypeIdEdit" name="user_doctypeId" data-plugin="select2">
          </div>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Nro Identificación:</td>
        <td>
          <div class="form-group form-material floating">
            <input required type="text" class="form-control" name="userIdentificacion" id="userIdentificacionEdit" onkeypress='return event.charCode >= 31 && event.charCode <= 57 || event.charCode ==8 || event.charCode == 46  || event.charCode == 0'>
          </div>
        </td>
      </tr>
      <tr class="formdriversEdit">
        <td  style="width: 120px !important;" class="info-label">Nro Lic. Conducción:</td>
        <td>
          <div class="form-group form-material floating">
            <input  type="text" class="form-control" name="userLicence" id="userLicenceEdit" onkeypress='return event.charCode >= 31 && event.charCode <= 57 || event.charCode ==8 || event.charCode == 46  || event.charCode == 0'>
          </div>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Fecha Nacimiento:</td>
        <td>
          <div class="form-group form-material floating">
            <input required type="text" class="form-control" data-plugin="datepicker" data-format="dd/mm/yyyy" name="userBorndate" id="userBorndateEdit">
          </div>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Telefono:</td>
        <td>
          <div class="form-group form-material floating">
            <input required type="text" class="form-control" name="userPhone" id="userPhoneEdit" onkeypress='return event.charCode >= 31 && event.charCode <= 57 || event.charCode ==8 || event.charCode == 46  || event.charCode == 0'>
          </div>
        </td>
      </tr>
      <tr>
        <td  style="width: 120px !important;" class="info-label">Status:</td>
        <td>
          <div class="form-group form-material floating">
            <select required class="form-control form-select"  id="userStateEdit" name="userState" data-plugin="select2">

            <option value="0">Inactivo</option>
            <option value="1">Activo</option>

            </select>
          </div>
        </td>
      </tr>
    </tbody>
  </table>

  <p class="formadminEdit">Secciones a administra:</p>
      <div class="row formadminEdit">
        <div class="col-md-3 col-xs-6">
            <span class="checkbox-custom checkbox-success checkbox-md">
                <input type="checkbox" name="user_menu[0]" class="contacts-checkbox" value="0" id="user_menu0Edit"
                />
              <label for="user_menu0Edit">Usuarios</label>
            </span>
        </div>
        <div class="col-md-3 col-xs-6">
            <span class="checkbox-custom checkbox-success checkbox-md">
                <input type="checkbox" name="user_menu[1]" class="contacts-checkbox" value="0" id="user_menu1Edit"
                />
              <label for="user_menu1Edit">Vehículos</label>
            </span>
        </div>
        <div class="col-md-3 col-xs-6">
            <span class="checkbox-custom checkbox-success checkbox-md">
                <input type="checkbox" name="user_menu[2]" class="contacts-checkbox" value="0" id="user_menu2Edit"
                />
              <label for="user_menu2Edit">Rutas</label>
            </span>
        </div>
        <div class="col-md-3 col-xs-6">
            <span class="checkbox-custom checkbox-success checkbox-md">
                <input type="checkbox" name="user_menu[3]" class="contacts-checkbox" value="0" id="user_menu3Edit"
                />
              <label for="user_menu3Edit">Recargas</label>
            </span>
        </div>
      </div>
      <div class="row formadminEdit">
        <div class="col-md-3 col-xs-6">
            <span class="checkbox-custom checkbox-success checkbox-md">
                <input type="checkbox" name="user_menu[4]" class="contacts-checkbox" value="0" id="user_menu4Edit"
                />
              <label for="user_menu4Edit">Transito</label>
            </span>
        </div>
        <div class="col-md-3 col-xs-6">
            <span class="checkbox-custom checkbox-success checkbox-md">
                <input type="checkbox" name="user_menu[5]" class="contacts-checkbox" value="0" id="user_menu5Edit"
                />
              <label for="user_menu5Edit">Ofertas</label>
            </span>
        </div>
        <div class="col-md-3 col-xs-6">
            <span class="checkbox-custom checkbox-success checkbox-md">
                <input type="checkbox" name="user_menu[6]" class="contacts-checkbox" value="0" id="user_menu6Edit"
                />
              <label for="user_menu6Edit">Noticias</label>
            </span>
        </div>
        <div class="col-md-3 col-xs-6">
          <span class="checkbox-custom checkbox-success checkbox-md">
              <input type="checkbox" name="user_menu[7]" class="contacts-checkbox" checked value="8" id="user_menu7Edit"
              />
            <label for="user_menu7Edit">Pagos</label>
          </span>
      </div>
      </div>
      


  <hr>
  <center>
    <button id="guardar" class="btn btn-animate btn-animate-vertical btn-success" type="button" onclick="savePut()">
      <span>
      <i class="icon wb-download" aria-hidden="true"></i>
      Guardar Cambios
      </span>
    </button>
  </center>
  <br><br><br>
  </form>

</div>

<script type="text/javascript">
    $(document).ready(function() {
        doit1=false;
        $('#userBorndateEdit').datepicker({
          endDate: 'dateToday'
        });
        $('.formadminEdit').hide();
        $('.formdriversEdit').hide();
        $('.avatar').click(function(event) {
           $('#new_img').click();
        });

        $('.userName1').click(function(event) {
           $('#new_img').click();
        });

        //$('#driverTypeEdit').attr("checked", "checked");
        $('#driverTypeEdit').click(function(event) {
          if($('#driverTypeEdit').attr("checked") == "checked" || $('#driverTypeEdit').attr("checked") == true){
            $('#driverTypeEdit').attr("checked", false);
          }else{
            $('#driverTypeEdit').attr("checked",true);
          }
        });
        if (window.location.host == 'localhost') {
            var baseurl = window.location.protocol +"//" + window.location.host + '/prontuz_backend/public';
        }else{
            var baseurl = window.location.protocol +"//" + window.location.host ;
        }

        //tipos de documentos
        $.ajax({
            type: "GET",
            url: baseurl+'/users/DocumentKind',
            dataType: 'JSON',
            success: function (data) {
                

                var toAppend = '';
                $.each(data.ResponseData,function(k, v){
                    //console.log(v.doctypeName);
                    toAppend += '<option value="'+v.doctypeId+'">'+v.doctypeName+'</option>';
                });

                 $('#user_doctypeIdEdit').append(toAppend);
                 $('#user_doctypeIdEdit').trigger("select2:updated");

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });


        $(".edit").on("click", function() {
          var $button = $(this);
          if ($button[0].value == 0) {
              $('.user-info').fadeOut();
              $('#dataEdit').fadeIn();
              $button[0].value = 1;
          }else{
              $('#dataEdit').fadeOut();
              $('.user-info').fadeIn();
              $button[0].value = 0;
          }
        });

        $( "#user_menu0Edit" ).change(function() {
             var value = $("#user_menu0Edit").val();
             if (value == 0) {
                $("#user_menu0Edit").val(1);
             }else if (value == 1){
                $("#user_menu0Edit").val(0);
             }       
        });

        $( "#user_menu1Edit" ).change(function() {
             var value = $("#user_menu1Edit").val();
             if (value == 0) {
                $("#user_menu1Edit").val(2);
             }else if (value == 2){
                $("#user_menu1Edit").val(0);
             }       
        });

        $( "#user_menu2Edit" ).change(function() {
             var value = $("#user_menu2Edit").val();
             if (value == 0) {
                $("#user_menu2Edit").val(3);
             }else if (value == 3){
                $("#user_menu2Edit").val(0);
             }       
        });

        $( "#user_menu3Edit" ).change(function() {
             var value = $("#user_menu3Edit").val();
             if (value == 0) {
                $("#user_menu3Edit").val(4);
             }else if (value == 4){
                $("#user_menu3Edit").val(0);
             }       
        });

        $( "#user_menu4Edit" ).change(function() {
             var value = $("#user_menu4Edit").val();
             if (value == 0) {
                $("#user_menu4Edit").val(5);
             }else if (value == 5){
                $("#user_menu4Edit").val(0);
             }       
        });

        $( "#user_menu5Edit" ).change(function() {
             var value = $("#user_menu5Edit").val();
             if (value == 0) {
                $("#user_menu5Edit").val(6);
             }else if (value == 6){
                $("#user_menu5Edit").val(0);
             }       
        });

        $( "#user_menu6Edit" ).change(function() {
             var value = $("#user_menu6Edit").val();
             if (value == 0) {
                $("#user_menu6Edit").val(7);
             }else if (value == 7){
                $("#user_menu6Edit").val(0);
             }       
        });
        $( "#user_menu7Edit" ).change(function() {
             var value = $("#user_menu7Edit").val();
             if (value == 0) {
                $("#user_menu7Edit").val(8);
             }else if (value == 8){
                $("#user_menu7Edit").val(0);
             }       
        });

        var userId = $('#userIdEdit').val();
        var token = $("input[name*='_token']").val();

        $.ajax({
            type: "GET",
            url: baseurl+'/users/findOne',
            data: {userId: userId},
            dataType: 'JSON',
            success: function (data) {

                $('.vertical-align-middle').fadeIn();
                $('.user-info').fadeIn();
                $('.loaderSend').fadeOut();
                
                if (data.ResponseData[0].user_rolId != 3 || data.ResponseData[0].user_rolId != 4) {
                  $('.formdriversEdit').hide();
                }else if (data.ResponseData[0].user_rolId != 1) {
                  $('.formadminEdit').hide();
                }

                if (data.ResponseData[0].user_rolId == 1) {
                  $('#seladminEdit').attr("checked", "checked");
                  $('.formadminEdit').show();

                  $.each(data.ResponseData[0].permits,function(k, v){
                       if (v.menuName == 'Usuarios') {
                          $("#user_menu0Edit").attr("checked", "checked");
                          $("#user_menu0Edit").val(1);
                       }else if (v.menuName == 'Vehiculos') {
                          $("#user_menu1Edit").attr("checked", "checked");
                          $("#user_menu1Edit").val(2);
                       }else if (v.menuName == 'Rutas') {
                          $("#user_menu2Edit").attr("checked", "checked");
                          $("#user_menu2Edit").val(3);
                       }else if (v.menuName == 'Pagos') {
                          $("#user_menu3Edit").attr("checked", "checked");
                          $("#user_menu3Edit").val(4);
                       }else if (v.menuName == 'Transito') {
                          $("#user_menu4Edit").attr("checked", "checked");
                          $("#user_menu4Edit").val(5);
                       }else if (v.menuName == 'Ofertas') {
                          $("#user_menu5Edit").attr("checked", "checked");
                          $("#user_menu5Edit").val(6);
                       }else if (v.menuName == 'Noticias') {
                          $("#user_menu6Edit").attr("checked", "checked");
                          $("#user_menu6Edit").val(7);
                       }else if (v.menuName == 'Noticias') {
                          $("#user_menu7Edit").attr("checked", "checked");
                          $("#user_menu7Edit").val(8);
                       }
                  }); 
                }else if (data.ResponseData[0].user_rolId == 2) {
                  $('#selusersEdit').attr("checked", "checked");
                }else{
                  if(data.ResponseData[0].user_rolId == 4) {
                    $('#driverTypeEdit').attr("checked", "checked");
                    $('#driverTypeEdit2').val(1);
                  }
                  $('#seldriversEdit').attr("checked", "checked");
                }
                $('#rolEdit').val(data.ResponseData[0].user_rolId);
                    switch(parseInt(data.ResponseData[0].user_rolId)) 
                    {
                        case 1 :
                            $('.formadminEdit').show();
                            $('.formusersEdit').hide();
                            $('.formdriversEdit').hide();
                            $('#userLicenceEdit').attr("required", false);
                            break;
                        case 2:
                            $('.formadminEdit').hide();
                            $('.formusersEdit').show();
                            $('.formdriversEdit').hide();
                            $('#userLicenceEdit').attr("required", false);
                            break;
                        case 3 :
                        case 4 :
                            $('.formadminEdit').hide();
                            $('.formusersEdit').hide();
                            $('.formdriversEdit').show();
                            $('#userLicenceEdit').attr("required", true);
                            break;
                    } 
                
                $.each(data.ResponseData,function(k, v){
                     $('#user_rolId1').html(v.rolName);
                     $('#userName1').html(v.userFirstname+' '+v.userLastname);
                     $('#userName2').html(v.userFirstname+' '+v.userLastname);
                     $('#userFirstnameEdit').val(v.userFirstname);
                     $('#userLastnameEdit').val(v.userLastname);
                     $('#userEmail1').html(v.userEmail);
                     $('#userEmailEdit').val(v.userEmail);
                     $('#userPhone1').html(v.userPhone);
                     $('#userPhoneEdit').val(v.userPhone);
                     $('#userImageEdit').attr("src",v.userImage);

                     $('#userLicence1').html(v.userLicence);
                     $('#userLicenceEdit').val(v.userLicence);

                     $('#userBorndate1').html(v.userBorndate);
                     $('#userBorndateEdit').val(v.userBorndate);
                     $('#userIdentificacion1').html(v.doctypeInitial+' - '+v.userIdentificacion);
                     $('#user_doctypeIdEdit').val(v.user_doctypeId).trigger('change');
                     $('#userIdentificacionEdit').val(v.userIdentificacion);

                     if (v.userState == 1) {
                        $('#userState1').html('Activo');
                     }else{
                        $('#userState1').html('Inactivo');
                     }
                     $('#userStateEdit').val(v.userState).trigger('change');  
                });


            },
            error: function (data) {
                console.log('Error:', data);
            }
        });




        $('#deleteUsers').on("click", function() {
          swal({
              title: "Esta Seguro de Eliminar este Usuario?",
              text: "el status cambiara a inactivo!",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "btn-danger",
              confirmButtonText: 'Si, eliminar!',
              cancelButtonText: "No, cancelar!",
              closeOnConfirm: false,
              closeOnCancel: true
            },
            function(isConfirm) {
              if (isConfirm) {

                $.ajax({
                    type: "DELETE",
                    url: baseurl+'/users/destroy',
                    data: {userId: userId},
                    dataType: 'JSON',
                    success: function (data) {
                        
                        if (data.ResponseCode == 1) {
                            swal("Status Inactivo!", data.ResponseMessage, "success"); 
                            setTimeout(' window.location.href = "'+baseurl+'/users"; ',2000);
                        }else{
                            swal("Error!", data.ResponseMessage, "error"); 
                        }
                        


                    },
                    error: function (data) {
                        swal("Error!", data.ResponseMessage, "error"); 
                    }
                });


                
              }
            });
        });
        $('#new_img').change(function()
        {
             var reader = new FileReader();
            reader.onload = function (e) 
            {
                $('#userImageEdit').attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
            var formData = new FormData($("#dataPhoto")[0]);
            $.ajax({
                type: 'POST',
                url: 'users/updatePhoto',
                data: formData,
                contentType: false,
                processData: false,
                success: function(datos)
                {
                    data = $.parseJSON(datos);
                   if(parseInt(data.ResponseCode) ===1)
                   {
                        $("#dataEdit").fadeIn();
                        $("#errorEdit").fadeIn(1000, function()
                        {      
                            $("#errorEdit").html('<div role="alert" class="alert dark alert-success alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> '+data.ResponseMessage+'</div>');
                        });                  
                   }
                   else
                   {
                        $("#dataEdit").fadeIn();
                        $("#errorEdit").fadeIn(1000, function()
                        {      
                            $("#errorEdit").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> '+data.ResponseMessage+'</div>');
                        });
                   }
                }
            });          
        });
    });

    function radio()
    {

           $('input[name="user_rolId"]:checked').each(function() 
           {
            switch(this.value) {
                case '1' :
                    $('.formadminEdit').show();
                    $('.formusersEdit').hide();
                    $('.formdriversEdit').hide();
                    $('#userLicence').val('');
                    $('#userLicence').attr("required", false);
                    $('#password').attr("required", true);
                    break;
                case '2' :
                    $('.formadminEdit').hide();
                    $('.formusersEdit').show();
                    $('.formdriversEdit').hide();
                    $('#userLicence').val('');
                    $('#userLicence').attr("required", false);
                    $('#userLicence').removeAttr("required");
                    $('#password').val('');
                    $('#password').attr("required", false);
                    $('#password').removeAttr("required");                    
                    break;
                case '3' :
                    $('.formadminEdit').hide();
                    $('.formusersEdit').hide();
                    $('.formdriversEdit').show();
                    $('#userLicence').attr("required", true);
                    $('#password').val('');
                    $('#password').attr("required", false);
                    break;
            }  
        });          
    }
  /*******************************************/

  function savePut(){

    var doit = false;

    var driverTypeEdit2=$('#driverTypeEdit2').val(); 

    if(driverTypeEdit2 == 1)
    {
      if($('#driverTypeEdit').attr("checked") != "checked" || $('#driverTypeEdit').attr("checked") != true){
        doit1 = true;
        swal({
            title: "Cambio de Wheel!",
            text: "Al cambiar el tipo de usuario,se eliminara la asignacion y el vehiculo de este",
            type: "warning",
            timer: 5000,
            showConfirmButton: false
        });
      }else{
        var doit = true;
      }

    }else{
      var doit = true;
    }

    if(doit == true || doit1 == true){
      $('#guardar').attr('type','submit');  

        $("form#dataEdit").submit(function() {

          var formData = new FormData($(this)[0]);

            if (($("input[name*='user_menu[0]']").val() == 0) &&  
                ($("input[name*='user_menu[1]']").val() == 0) && 
                ($("input[name*='user_menu[2]']").val() == 0) &&
                ($("input[name*='user_menu[3]']").val() == 0) &&
                ($("input[name*='user_menu[4]']").val() == 0) &&
                ($("input[name*='user_menu[5]']").val() == 0) &&
                ($("input[name*='user_menu[6]']").val() == 0) &&
                ($("#rolEdit").val() == 1)) {

              $("#error").fadeIn(1000, function(){      
              $("#error").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> Debe seleccionar al menos una sección para administrar</div>');
              });

            }else{

              $.ajax({
                type: 'PUT',
                url: $(this).attr("action"),
                data: $(this).serialize(),
                dataType: 'JSON',
                beforeSend: function(){ 
                  $("#errorEdit").fadeOut();
                  $("#dataEdit").fadeOut();
                  $("#loaderEdit").fadeIn();
                },
                success: function (response) {
                  $("#loaderEdit").fadeOut();
                  if(response.ResponseCode == 0){

                    $("#dataEdit").fadeIn();
                    $("#errorEdit").fadeIn(1000, function(){      
                    $("#errorEdit").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> '+response.ResponseMessage+'</div>');
                       });
                  
                  }else{

                    if(doit1 == true){

                      var userIdEdit=$('#userIdEdit').val();
                      var token = $("input[name*='_token']").val();

                       $.ajax({
                              type: "DELETE",
                              url: baseurl+'/users/deleteAssignmentWheel',
                              data: {routeassignment_userId: userIdEdit, _token: token},
                              async: false,
                              dataType: 'JSON',
                              success: function (data) {

                                console.log('here2');

                                if(data.ResponseCode == 0){

                                $("#dataEdit").fadeIn();
                                $("#errorEdit").fadeIn(1000, function(){      
                                $("#errorEdit").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> '+data.ResponseMessage+'</div>');
                                     });
                                }
                           
                              },
                              error: function (data) {
                                  console.log('Error:', data);
                              }
                          });
                    }
                    setTimeout( function(){
                       swal({
                          title: "ÉXITO!",
                          text: "Operación Exitosa!",
                          type: "success",
                          timer: 1000,
                          showConfirmButton: false
                      });    
                      setTimeout(' window.location.href = "'+baseurl+'/users"; ',1500);                     
                    
                    },1000);
                     
                  }

                },
                error: function (response) {
                    $("#error").fadeIn(1000, function(){      
                    $("#error").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> Error de comunicación, revise su conexion a internet.</div>');
                    });
                }
              });

            }

         return false;
        });
      }
       
       
    }
  </script>