@extends('layouts.app')

@section('content')
  <link rel="stylesheet" href="{{ asset('classic/global/vendor/webui-popover/webui-popover.css') }}">
  <link rel="stylesheet" href="{{ asset('classic/global/vendor/toolbar/toolbar.css') }}">

  <script src="{{ asset('classic/global/vendor/webui-popover/jquery.webui-popover.min.js') }}"></script>
  <script src="{{ asset('classic/global/vendor/toolbar/jquery.toolbar.js') }}"></script>
  <script src="{{ asset('classic/global/js/Plugin/toolbar.js') }}"></script>
 

  <div class="page">
    <div class="page-content">
      <!-- Panel -->
      <div class="panel">
        <div class="panel-body">
          <div class="nav-tabs-horizontal nav-tabs-animate" data-plugin="tabs">
          <!-- Contacts Content -->
          <div id="contactsContent" class="page-content page-content-table" data-plugin="selectable">
            <!-- Contacts -->
            <table id='news_list' width="100%" class="table is-indent" data-plugin="animateList" data-animate="fade" data-child="tr"
            data-selectable="selectable">
              <thead>
                <tr>
                  <th width="25%" scope="col">Todas las noticias</th>
                  <th width="50%" scope="col"></th>
                  <th width="15%" scope="col"></th>
                  <th width="10%" scope="col"></th>
                </tr>
              </thead>
             </table>
            
          </div>
         
          </div>
        </div>
      </div>
      <!-- End Panel -->
    </div>
  </div>


  <!-- Site Action -->
  <div class="site-action" data-plugin="actionBtn">
    <button type="button" class="btn-raised btn btn-success btn-floating" onclick="openOpt()" value="0">
      <i class="front-icon wb-plus animation-scale-up" aria-hidden="true"></i>
      <i class="back-icon wb-close animation-scale-up" aria-hidden="true"></i>
    </button>
    <div class="site-action-buttons" id="Opt">
      <button type="button" data-action="sliders" class="btn-raised btn btn-success btn-floating animation-slide-bottom" onclick="openModal2()">
        <i class="icon ion-android-options" aria-hidden="true" style="font-size: 30px;"></i>
      </button>
      <button type="button" data-action="bus" class="btn-raised btn btn-success btn-floating animation-slide-bottom" onclick="openModal()">
        <i class="icon ion-android-bus" aria-hidden="true" style="font-size: 30px;"></i>
      </button>
    </div>
  </div>
  <!-- End Site Action -->


  <!-- Add User Form -->
  <div class="modal fade modal-3d-flip-vertical" id="addUserForm"  style="display:none" role="dialog" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
          <h4 class="modal-title">Crear Nueva noticia</h4>
        </div>
        <div class="modal-body">

        <div id="loader" style="display:none;">
          <center>
              <br><br>
               <img width="48px" src="{{ asset('iconbar/loading.gif') }}" /> &nbsp; Enviando ...
              <br><br>
          </center>
        </div>

        <form id="data" method="POST" action="{{ url('news/store') }}" >
            <div class="form-group">
                <div id="errorNews">
                <!-- error will be shown here ! -->
                </div>
            </div>
          {{ csrf_field() }}
            <div class="form-group"> 
              <center>
                <label for="newsImage">
                    <img id="imgnews" src="{{ asset('iconbar/assets/images/006-meta-A.jpg') }}" style="border-radius: 150px; width: 150px; height: 150px; background-repeat: no-repeat; background-position: 50%; border-radius: 50%; background-size: 100% auto; border:1px solid #999;" />
                </label>
                <input type="file" name="newsImage" id="newsImage" style="display:none"/>
                <p>* Clic para cargar imagen</p>
              </center>
            </div>
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="text" required class="form-control" id="newsTitle" name="newsTitle"/>
              <label class="floating-label" >Titulo</label>
            </div>
            <div class="example-wrap" data-plugin="formMaterial">
              <label class="label" >Contenido</label>
              <textarea rows="4" cols="50" required class="form-control" id="newsContent" name="newsContent" style="height:200px !important"></textarea>
              <!-- <input type="text" /> -->
              
            </div>
            <input type="hidden" name="newsState" value="1">
            <br>
            <div class="form-group col-xl-12 text-right padding-top-m">
                <button class="btn btn-success" type="button" onclick="crtNews();" id="crtNewsId">Crear</button>
                <a class="btn btn-sm btn-white" data-dismiss="modal" href="javascript:void(0)">Cancelar</a>
            </div>

          </form>

        </div>
      </div>
    </div>
  </div>

    @include('news.panel')
   
  <!-- End Add User Form -->
    <script type="text/javascript">
    token = $("input[name*='_token']").val();
    $(document).ready(function() {
      $('#userSlidePanel').hide();

      /****  Load Table****/
    print_datatable();
      /****  Load Table****/


        $("#data").trigger( "reset" );
        var toAppend = '';
        var y = 0;
        for (var i = 40; i > 0; i--) {
            y++;
            toAppend += '<option value="'+y+'">'+y+'</option>';
        }


        var token = $("input[name*='_token']").val();

        if (window.location.host == 'localhost') {
            var baseurl = window.location.protocol +"//" + window.location.host + '/prontuz_backend/public';
        }else{
            var baseurl = window.location.protocol +"//" + window.location.host ;
        }
   
    });

  /**************************************************/

  function print_datatable(){

      tableName='news_list';
      urlName='/news/listing';
      columnsName=[
              { data: 'newsImage',
                render: function (data){

                  console.log(data);
                  console.log(data.length);


                    if(data.length < 2){
                      var image="{{ ('Prontuz_files/icon-sin-foto.png') }}";
                      
                      return '<img width="150px"  style="max-height:130px !important" src="'+image+'" />';

                    }else{

                      return '<img width="150px"  style="max-height:130px !important" src="'+data+'" />';
                    }
                    
                  }
              },
              { data: 'news',
                render: function (data){
                        return data;
                  }
              },
              {
                //
                data: 'newsState2',
                render: function (data){
                        if(data == 'Activo'){
                          return '<span class="badge badge-outline badge-success">'+data+'</span>';
                        }else{
                          return '<span class="badge badge-outline badge-danger">'+data+'</span>';
                        }
                  }
                },
                {
                data: 'newsId',
                render: function (data){
                        return ' <button onclick="edtNews('+data+')" type="button" class="btnEditar btn btn-primary btn-sm">Editar</button>';
                  }
                },
           ];

      list_datatable(tableName,urlName,columnsName);
  }
  /**************************************************/
  
  $("#newsImage").change(function(){
      var reader = new FileReader();
      reader.onload = function (e) {
       $('#imgnews').attr('src', e.target.result);
      }
      reader.readAsDataURL(this.files[0]);
  });

/**************************************************/

  $("#newsImageEdit").change(function(){
      var reader = new FileReader();
      reader.onload = function (e) {
       $('#imgperfilEdit').attr('src', e.target.result);
      }
      reader.readAsDataURL(this.files[0]);
  });

/************************************/

function openOpt(){

    $('#addUserForm').modal('show');
  
}
/************************************/
  
  function edtNews(newsId){


    document.getElementById("newsImageEdit").value = "";
    $( "#charging" ).show();
    //$('#userSlidePanel').show();
    $( "#userSlidePanel" ).show( "slow" );

    $('#newsIdedit').val(newsId);

    $.ajax({
        type: 'GET',
        url: "news/find",
        data: {_token: token,newsId:newsId },
        dataType: 'JSON',
        beforeSend: function(){   
        },
        success: function (response) {

            if(response.ResponseCode == 0){
                $( "#charging" ).hide();
                $( "#userSlidePanel" ).hide( "slow" ); 
              setTimeout( function(){
                        swal({
                            title: "Error!",
                            text: response.ResponseMessage,
                            type: "danger",
                            timer: 3000,
                            showConfirmButton: false
                        });    
                    },1000); 


            }else{
          
              $('#newsIdEdit').val(newsId);
              $('#newsTitleEdit').val(response.ResponseData[0].newsTitle);
              $('#newsContentEdit').val(response.ResponseData[0].newsContent);
              $('#newsStateEdit option[value="'+response.ResponseData[0].newsState+'"]').attr("selected", "selected");
              $('#imgperfilEdit').attr("src",response.ResponseData[0].newsImage);
              //$('#companyImageEdit').attr("src",baseurl+'/'+data.ResponseData.companyImage);
              
              $( "#charging" ).hide();
              $( "#slideBody" ).show();
            }

        },error: function (response) {
              $("#errorEdit").fadeIn(1000, function(){      
              $("#errorEdit").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> Error de comunicación, revise su conexion a internet.</div>');
              });
          }
    });
     
  }
/************************************/

function closeSlide(){
  
  $( "#userSlidePanel" ).hide( "slow" );
  
}
/************************************/

function openModal(){
  
  openOpt();
  $('#addUserForm').modal('show');
  
}


/************************************/

  function crtNews(){

    var pass = false;
    $('#crtNewsId').attr('type','button');  


      $('#crtNewsId').attr('type','submit');  
      $("form#data").submit(function() {

        var formData = new FormData($(this)[0]);

        $('#crtNewsId').attr('disabled','disabled');  
        $.ajax({
            type: 'POST',
            url: $(this).attr("action"),
            headers:{'x-csrf-token':token},
            data: formData,
            dataType: 'JSON',
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){ 
              $("#error").fadeOut();
              $("#data").fadeOut();
              $("#loader").fadeIn();
            },
            success: function (response) {
                  $("#loader").fadeOut();
                  if(response.ResponseCode == 0){

                   setTimeout( function(){
                        swal({
                            title: "Error!",
                            text: response.ResponseMessage,
                            type: "danger",
                            timer: 3000,
                            showConfirmButton: false
                        });    
                    },1000);  
                  
                  }else{
                    $('#addUserForm').modal('hide');
                    print_datatable();

                    setTimeout( function(){
                        swal({
                            title: "Exito!",
                            text: "Noticia creada correctamente",
                            type: "success",
                            timer: 3000,
                            showConfirmButton: false
                        });    
                    },1000);

                  }
                },
                error: function (response) {
                    $("#errorNews").fadeIn(1000, function(){      
                    $("#errorNews").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> Error de comunicación, revise su conexion a internet.</div>');
                    });
                }
              });

            return false;
        });
     
    
  }
/**************************************************/

function validateImage(newsImageEdit){

  if(newsImageEdit.value != ''){
    var uploadFile = newsImageEdit.files[0];
  
    if (!(/\.(jpg|jpeg|png|gif)$/i).test(uploadFile.name)) {
        return false;
    }else{
        return true;
    }  
  }else{
    return true;
  }

                   
}

/**************************************************/


function savePut(){

    var newsImageEdit=document.getElementById('newsImageEdit');

    newsImageEdit=validateImage(newsImageEdit);

    if( newsImageEdit == true){
        $("#errorEdit").hide();
        $('#guardar').attr('type','submit');  

        $("form#dataEdit").submit(function() {

            var formData = new FormData($(this)[0]);
        
                $.ajax({
                    type: 'POST',
                    url: baseurl+'/news/put',
                    headers:{'x-csrf-token':token},
                    data: formData,
                    dataType: 'JSON',
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function(){ 
                      $("#errorEdit").fadeOut();
                      $("#slideBody").fadeOut();
                      $("#loaderEdit").fadeIn();
                    },
                    success: function (response) {
                        $("#loaderEdit").fadeOut();
                        if(response.ResponseCode == 0){

                            $("#slideBody").fadeIn();
                            $("#errorEdit").fadeIn(1000, function(){      
                            $("#errorEdit").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> '+response.ResponseMessage+'</div>');
                               });
                        }else{
                            setTimeout( function(){
                                swal({
                                    title: "ÉXITO!",
                                    text: "Operación Exitosa!",
                                    type: "success",
                                    timer: 3000,
                                    showConfirmButton: false
                                });    
                                setTimeout(function(){
                                  $( "#charging" ).hide();
                                  $( "#userSlidePanel" ).hide( "slow" );
                                  print_datatable();

                                },1500);                     
                            },1000);     
                       }

                    },
                    error: function (response) {
                        $("#error").fadeIn(1000, function(){      
                        $("#error").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button> Error de comunicación, revise su conexion a internet.</div>');
                        });
                    }
                });
            return false;
        });
    }else{
       $("#slideBody").fadeIn();
      $("#errorEdit").fadeIn(1000, function(){      
        $("#errorEdit").html('<div role="alert" class="alert dark alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" class="close" type="button"> <span aria-hidden="true">×</span> </button>Formato de Imagen no valido.</div>');
      });
    }
  
}
  

/************************************/





  </script>

@endsection
