//alert('included');

$(document).ready(function() {
        $('th.sorting').click(function() {

        	var th_sorting=$( "table.table" ).find( "th.sorting" );
        	
        	for (var i=0; i < th_sorting.length; i++){
        			
        			var th_sorting2=th_sorting[i];
	       			var th_sorting3=th_sorting2.getAttribute('class');

        			$('th.'+th_sorting3).css('border-bottom','1px solid #e4eaec');
        			$('th.sorting_asc').css('border-bottom','1px solid #e4eaec');
        			$('th.sorting_desc').css('border-bottom','1px solid #e4eaec');

        			th_sorting2="border-bottom:1px solid #e4eaec";
        	}

        	$(this).css('border-bottom','3px solid #4fb933');
			        	
        });
    });

/************************************/


function list_datatable(tableName,urlName,columnsName)
    {

        tableData=urlName.split('?');
       

        if(tableData.length == 1 ){
            var token={_token: $("input[name*='_token']").val()};
            var aLengthMenu=[50,75,100];
            var iDisplayLength=50;

        }else{
            var tableData2=tableData[1].split('=');
            var token={_token: $("input[name*='_token']").val(),datasearch:tableData2[1]};
            var aLengthMenu=[10,15,30];
            var iDisplayLength=10;
        }

        console.log(token);

        $('#'+tableName).DataTable( {
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "aLengthMenu": aLengthMenu,
            "iDisplayLength": iDisplayLength,
            "oLanguage":
                {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                           "sNext":  '<span class="icon wb-chevron-right-mini"></span>',
                        "sPrevious": '<span class="icon wb-chevron-left-mini"></span>'
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                },
            "ajax": {
                type:"POST",
                url:urlName,
                data:token
            },
            'info':false,
            columns: columnsName,
            'order':[[1, 'asc']]
        });
    }

