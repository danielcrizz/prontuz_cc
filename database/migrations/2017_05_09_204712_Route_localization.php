<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RouteLocalization extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('route_localization', function (Blueprint $table) {
            $table->integer('route_routeId');
            $table->integer('localization_localizationId');
            $table->integer('route_order');
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();

            $table->primary(['route_routeId', 'localization_localizationId']);
            $table->timestampsTz();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('route_localization');
    }
}
