<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutewheelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routewheel', function (Blueprint $table) {
            $table->increments('routewheelId');
            //$table->string('routewheelRoute',2000);
            $table->json('routewheelRoute');
            $table->string('routewheel_routeprincipalsId',20);
            $table->integer('routewheelState');
            $table->integer('routewheelDirection');
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();

            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routewheel');
    }
}
