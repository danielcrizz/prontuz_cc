<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Routeassignment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routeassignment', function (Blueprint $table) {
            $table->increments('routeassignmentId');
            $table->integer('routeassignmentFirst_routeId');
            $table->integer('routeassignment_userId');
            $table->integer('routeassignment_vehicleId');
            $table->integer('routeassignmentLast_routeId');
            $table->integer('routeassignmentActive');//0 ninguna, 1 first,2 last
            $table->integer('routeassignmentType');//0 ruta normal,1 ruta wheel
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();

           
            $table->foreign('routeassignment_userId')->references('userId')->on('user')->onDelete('set null');
            $table->foreign('routeassignment_vehicleId')->references('vehicleId')->on('vehicle')->onDelete('set null');
            // $table->foreign('routeassignmentFirst_routeId')->references('routeId')->on('route')->onDelete('set null');
            // $table->foreign('routeassignmentLast_routeId')->references('routeId')->on('route')->onDelete('set null');
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routeassignment');
    }
}
