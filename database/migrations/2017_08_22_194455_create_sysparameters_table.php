<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysparametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sysparameters', function (Blueprint $table) {
            $table->increments('sysparametersId');
            $table->string('sysparametersTitle');
            $table->string('sysparametersName');
            $table->string('sysparametersValue');
            $table->integer('sysparametersState');
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();

            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sysparameters');
    }
}
