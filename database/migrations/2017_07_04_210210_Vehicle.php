<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Vehicle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle', function (Blueprint $table) {
            $table->increments('vehicleId');
            $table->string('vehiclePlaque',30);
            $table->integer('vehicle_vehiclebrandId');
            $table->integer('vehicle_vehicletypeId');
            $table->string('vehicleModel',45);
            $table->dateTime('vehicleTecnomecanica');
            $table->string('vehicleNoperation',45)->unique();
            $table->string('vehicleNinside',45)->unique();
            $table->integer('vehicleCapacity');
            $table->string('vehicleNsoat',45)->unique();
            $table->dateTime('vehicleExpiration')->nullable();
            $table->integer('vehicle_lineId');
            $table->float('vehicleLatitude');
            $table->float('vehicleLongitude');
            $table->integer('vehicleOcupation');
            $table->integer('vehicleState');
            $table->integer('vehicle_locationId');
            $table->float('vehicleBearing');
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
                    
            $table->foreign('vehicle_lineId')->references('lineId')->on('line')->onDelete('set null');
            $table->foreign('vehicle_vehiclebrandId')->references('vehiclebrandId')->on('vehiclebrand')->onDelete('set null');
            $table->foreign('vehicle_vehicletypeId')->references('vehicletypeId')->on('vehicletype')->onDelete('set null');        
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle');
    }
}
