<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePayer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payer', function (Blueprint $table) {
            $table->increments('payerId');
            $table->string('payerName');//PAYER_NAME
            $table->string('payerEmail');//PAYER_EMAIL
            $table->string('payerPhone');//PAYER_CONTACT_PHONE
            $table->string('payerIdentification');//PAYER_DNI
            $table->string('payerCard');
            $table->integer('payer_userId');
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();

            $table->foreign('payer_userId')->references('userId')->on('user')->onDelete('set null');

            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payer');
    }
}
