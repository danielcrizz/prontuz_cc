<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Routes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // No permite crear archivo como route "palabra reservada"
        
        Schema::create('route', function (Blueprint $table) {
            $table->increments('routeId');
            $table->string('routeName',60)->unique();
            $table->string('route_routeprincipalsId',150);
            $table->float('routePrice');
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->integer('routeState');
            $table->integer('routeDirection');
            $table->timestampsTz();


            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('route');
    }
}
