<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Recharge extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('recharge', function (Blueprint $table) {
            $table->increments('rechargeId');
            $table->float('rechargeValue');
            $table->float('rechargeLoan');
            $table->integer('recharge_userId');
            $table->integer('created_by');
            $table->integer('updated_by')->nullable(); 

            $table->foreign('recharge_userId')->references('userId')->on('user')->onDelete('set null');     
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
