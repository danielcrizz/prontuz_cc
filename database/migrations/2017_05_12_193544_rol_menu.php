<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RolMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::create('rolmenu', function (Blueprint $table) {
            $table->integer('rolmenu_menuId');
            $table->integer('rolmenu_rolId');
            $table->integer('rolmenu_userId');
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
        
            $table->primary(['rolmenu_menuId', 'rolmenu_rolId','rolmenu_userId']);
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rolmenu');
    }
}
