<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer', function (Blueprint $table) {
            $table->increments('offerId');
            $table->string('offerTitle');
            $table->string('offerContent', 5000);
            $table->string('offerEmail');
            $table->string('offerImage');
            $table->float('offerValue');
            $table->string('offerCompany');
            $table->integer('offerQuantity');
            $table->integer('offerAvailable');
            $table->integer('offerState');
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();

            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer');
    }
}
