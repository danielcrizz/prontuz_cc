<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction', function (Blueprint $table) {
            $table->increments('transactionId');
            $table->string('transactionOrderid',50);
            $table->string('transactionrReferencecode',50);
            $table->string('transactionIdentification',50);
            $table->integer('transactionState');
            $table->dateTime('transactionOperationdate');
            $table->integer('transaction_userId');
            $table->integer('transaction_payerId')->default(0);
            $table->integer('transactionType')->comment = "0:Credicard,99:enCaja";
            $table->integer('transactionCategory');
            $table->float('transactionValue');
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();

            $table->foreign('transaction_userId')->references('userId')->on('user')->onDelete('set null');  
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction');
    }
}
