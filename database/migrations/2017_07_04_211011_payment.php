<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Payment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('payment', function (Blueprint $table) {
            $table->increments('paymentId');
            $table->float('paymentDebt');
            $table->float('paymentPay');
            $table->integer('payment_userId');
            $table->integer('payment_wheel_userId');   
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();

            $table->foreign('payment_userId')->references('userId')->on('user')->onDelete('set null');     
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment');
    }
}
