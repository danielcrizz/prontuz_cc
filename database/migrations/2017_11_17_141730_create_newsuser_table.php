<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsuserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('newsuser', function (Blueprint $table) {
            $table->increments('newsuserId'); 
            $table->integer('newsuser_newsId');
            $table->integer('newsuser_userId');
            $table->integer('newsuserState');
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();


            $table->foreign('newsuser_newsId')->references('newsId')->on('news')->onDelete('set null');
            $table->foreign('newsuser_userId')->references('userId')->on('user')->onDelete('set null');

            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('newsuser');
    }
}
