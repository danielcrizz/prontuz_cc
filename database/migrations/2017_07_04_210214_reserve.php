<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Reserve extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reserve', function (Blueprint $table) {
            $table->increments('reserveId');
            $table->integer('reserve_UserId');
            $table->integer('reservePayer_userId');
            $table->integer('reserve_vehicleId');
            $table->integer('reserveState');
            $table->string('reserveCode',50);
            $table->integer('reserve_route_localization');
            $table->integer('reserveType')->comment('(wheel => 1),ruta => 0 ');
            $table->float('reserveLatitude')->comment('0 si reserveType  = 0');
            $table->float('reserveLongitude')->comment('0 si reserveType  = 0');
            $table->float('reservePrice')->nullable(); 
            $table->float('reserveUtility')->nullable(); 
            
            $table->foreign('reserve_UserId')->references('userId')->on('user')->onDelete('set null');
            $table->foreign('reserve_vehicleId')->references('vehicleId')->on('vehicle')->onDelete('set null');
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reserve');
    }
}
