<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class User extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('userId');
            $table->string('userFirstname',45);
            $table->string('userLastname',45);
            $table->string('password');
            $table->string('userEmail',50)->unique();
            $table->string('userStreet',60)->nullable();
            $table->string('remember_token')->nullable();
            $table->string('userIdentificacion',50);
            $table->string('userPhone',45);
            $table->string('userLicence',45)->nullable();
            $table->dateTime('userBorndate');
            $table->integer('user_doctypeId');
            $table->integer('user_rolId');
            $table->integer('userState');
            $table->string('userImage');
            $table->string('userToken')->nullable();
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->timestamps();

            $table->foreign('user_doctypeId')->references('doctypeId')->on('doctype')->onDelete('set null');
            $table->foreign('user_rolId')->references('rolId')->on('rol')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
