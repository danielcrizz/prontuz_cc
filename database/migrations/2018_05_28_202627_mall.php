<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Mall extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mall', function (Blueprint $table) {
            $table->increments('id');
            $table->string('address');
            $table->string('nit');
            $table->string('nom_interventor');
            $table->string('tel_interventor');
            $table->string('city_id');
            $table->integer('user_id');
            $table->foreign('user_id')->references('userId')->on('user')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mall');
    }
}
