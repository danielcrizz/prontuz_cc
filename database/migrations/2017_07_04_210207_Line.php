<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Line extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('line', function (Blueprint $table) {
            $table->increments('lineId');
            $table->string('lineCode',45);
            $table->string('lineName',45);
            $table->integer('line_vehiclebrandId');
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->foreign('line_vehiclebrandId')->references('vehiclebrandId')->on('vehiclebrand')->onDelete('set null');
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('line');
    }
}
