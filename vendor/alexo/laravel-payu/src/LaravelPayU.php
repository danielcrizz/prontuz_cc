<?php

namespace Alexo\LaravelPayU;

use Exception;

class LaravelPayU
{
    /**
     * The payu merchant ID.
     *
     * @var string
     */
    protected static $merchantId;

    /**
     * The payu API login.
     *
     * @var string
     */
    protected static $apiLogin;

    /**
     * The payu API Key.
     *
     * @var string
     */
    protected static $apiKey;

    /**
     * The payu Account ID.
     *
     * @var string
     */
    protected static $accountId;

    /**
     * The country where the payment is processed.
     *
     * @var string
     */
    protected static $country;

    /**
     * The current currency.
     *
     * @var string
     */
    protected static $currency;

    /**
     * The current currency symbol.
     *
     * @var string
     */
    protected static $currencySymbol = '$';

    /**
     * The custom currency formatter.
     *
     * @var callable
     */
    protected static $formatCurrencyUsing;

    /**
     * Default validation rules (for testing purposes).
     *
     * @var array
     */
    protected static $rules;

    /**
     * The account testing state.
     *
     * @var bool
     */
    protected static $isTesting;

    /**
     * Set the currency to be used when billing users.
     *
     * @param  string  $currency
     * @param  string|null  $symbol
     * @return void
     */
    public static function useCurrency($currency, $symbol = null)
    {
        static::$currency = $currency;

        static::useCurrencySymbol($symbol ?: static::guessCurrencySymbol($currency));
    }

    /**
     * Guess the currency symbol for the given currency.
     *
     * @param  string  $currency
     * @return string
     */
    protected static function guessCurrencySymbol($currency)
    {
        switch (strtolower($currency)) {
        case 'usd':
        case 'clp':
        case 'cop':
        case 'mxn':
            return '$';
        case 'ars':
            return '$a';
        case 'pen':
            return 'S/';
        case 'brl':
            return 'R$';
        default:
            throw new Exception('Unable to guess symbol for currency. Please explicitly specify it.');
        }
    }

    /**
     * Get the currency currently in use.
     *
     * @return string
     */
    public static function usesCurrency()
    {
        return static::$currency;
    }

    /**
     * Set the currency symbol to be used when formatting currency.
     *
     * @param  string  $symbol
     * @return void
     */
    public static function useCurrencySymbol($symbol)
    {
        static::$currencySymbol = $symbol;
    }

    /**
     * Set the custom currency formatter.
     *
     * @param  callable  $callback
     * @return void
     */
    public static function formatCurrencyUsing(callable $callback)
    {
        static::$formatCurrencyUsing = $callback;
    }

    /**
     * Format the given amount into a displayable currency.
     *
     * @param  int  $amount
     * @return string
     */
    public static function formatAmount($amount)
    {
        if (static::$formatCurrencyUsing) {
            return call_user_func(static::$formatCurrencyUsing, $amount);
        }

        $amount = number_format($amount / 100, 2);

        if (starts_with($amount, '-')) {
            return '-'.static::usesCurrencySymbol().ltrim($amount, '-');
        }

        return static::usesCurrencySymbol().$amount;
    }

    /**
     * Get the Account ID.
     *
     * @return string
     */
    public static function getAccountId()
    {
        if (static::$accountId) {
            return static::$accountId;
        }

        if ($accountId = getenv('PAYU_ACCOUNT_ID')) {
            return $accountId;
        }

        return config('payu.payu_account_id');
    }

    /**
     * Get the Merchant ID.
     *
     * @return string
     */
    public static function getMerchantId()
    {
        if (static::$merchantId) {
            return static::$merchantId;
        }

        if ($merchantId = getenv('PAYU_MERCHANT_ID')) {
            return $merchantId;
        }

        return config('payu.payu_merchant_id');
    }

    /**
     * Get the API login.
     *
     * @return string
     */
    public static function getApiLogin()
    {
        if (static::$apiLogin) {
            return static::$apiLogin;
        }

        if ($apiLogin = getenv('PAYU_API_LOGIN')) {
            return $apiLogin;
        }

        return config('payu.payu_api_login');
    }

    /**
     * Get the API key.
     *
     * @return string
     */
    public static function getApiKey()
    {
        if (static::$apiKey) {
            return static::$apiKey;
        }

        if ($apiKey = getenv('PAYU_API_KEY')) {
            return $apiKey;
        }

        return config('payu.payu_api_key');
    }

    /**
     * Get the Account country.
     *
     * @return string
     */
    public static function getCountry()
    {
        if (static::$country) {
            return static::$country;
        }

        if ($country = getenv('PAYU_COUNTRY')) {
            return $country;
        }

        return config('payu.payu_country');
    }

    /**
     * Get the PSE redirect URL.
     *
     * @return string
     */
    public static function getRedirectPSE()
    {
        if ($pseRedirect = getenv('PSE_REDIRECT_URL')) {
            return $pseRedirect;
        }

        return config('payu.pse_redirect_url');
    }

    /**
     * Set the Account testing state (never use on production)
     *
     * @return string
     */
    public static function setAccountOnTesting($state)
    {
        static::$isTesting = $state;
    }

    /**
     * Get the account testing value.
     *
     * @return string
     */
    private static function isAccountInTesting()
    {
        if (!is_null(static::$isTesting)) {
            return static::$isTesting;
        }

        if ($isTesting = getenv('PAYU_ON_TESTING')) {
            return $isTesting;
        }

        return config('payu.payu_testing');
    }

    /**
     * Get the app testing value.
     *
     * @return string
     */
    private static function isAppInTesting()
    {
        if ($isLocal = getenv('APP_ENV')) {
            return $isLocal;
        }

        return config('app.env');
    }

   

    /**
     * Get array of available PSE banks.
     *
     * @return array
     */
    public static function getPSEBanks($onSuccess, $onError)
    {
        static::setPayUEnvironment();
        
        $response = false;

        try {
            $params[\PayUParameters::PAYMENT_METHOD] = 'PSE';
            $params[\PayUParameters::COUNTRY] = static::getCountry();

            $array = \PayUPayments::getPSEBanks($params);
            
            if ($array) {
                $response=$onSuccess($array->banks);
            }
            var_dump($onSuccess($array->banks));
        } catch (\PayUException $exc) {
            $response = $onError($exc);
            var_dump($response);
        } catch (ConnectionException $exc) {
            $response = $onError($exc);
            var_dump($response);
        } catch (RuntimeException $exc) {
            $response = $onError($exc);
            var_dump($response);
        } catch (InvalidArgumentException $exc) {
            $response = $onError($exc);
            var_dump($response);
        }
         return $response;
    }

    /**
     * Set PayU Environment for the account.
     *
     * @return void
     */
    public static function setPayUEnvironment()
    {
        \PayU::$apiKey = static::getApiKey();
        \PayU::$apiLogin = static::getApiLogin();
        \PayU::$merchantId = static::getMerchantId();
        \PayU::$isTest = static::isAccountInTesting();

        if (static::isAppInTesting() == 'local') {
            \Environment::setPaymentsCustomUrl(
                "https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi"
            );
            \Environment::setReportsCustomUrl(
                "https://sandbox.api.payulatam.com/reports-api/4.0/service.cgi"
            );
            \Environment::setSubscriptionsCustomUrl(
                "https://sandbox.api.payulatam.com/payments-api/rest/v4.3/"
            );
        } else {
            \Environment::setPaymentsCustomUrl(
                "https://api.payulatam.com/payments-api/4.0/service.cgi"
            );
            \Environment::setReportsCustomUrl(
                "https://api.payulatam.com/reports-api/4.0/service.cgi"
            );
            \Environment::setSubscriptionsCustomUrl(
                "https://api.payulatam.com/payments-api/rest/v4.3/"
            );
        }
    }

/***********************************************************/

    public static function doPing($onSuccess, $onError)
    {
        static::setPayUEnvironment();
        
        $response = false;

        try {

            $response = \PayUPayments::doPing();
        
            if ($response) {
                $response=$onSuccess($response);
            }
        } catch (\PayUException $exc) {
            
            $response=$onError($exc);
        }

        return $response;
    }

/***********************************************************/

    public static function getPaymentMethods($onSuccess, $onError)
    {
        static::setPayUEnvironment();

        $response = false;

        try {

            $response = \PayUPayments::getPaymentMethods();
        
            if ($response) {
                $response=$onSuccess($response);
            }
        } catch (\PayUException $exc) {
            
            $response=$onError($exc);
        }

        return $response;

    }

/***********************************************************/
    
    public static function CrediCardPayment($data,$onSuccess, $onError){
        
        static::setPayUEnvironment();
        $response = false;

        $parameters = array(
            //Ingrese aquí el identificador de la cuenta.
            \PayUParameters::ACCOUNT_ID => $data['ACCOUNT_ID'],
            //Ingrese aquí el código de referencia.
            \PayUParameters::REFERENCE_CODE => $data['REFERENCE_CODE'],
            //Ingrese aquí la descripción.
            \PayUParameters::DESCRIPTION => $data['DESCRIPTION'],
            
            // -- Valores --
            //Ingrese aquí el valor.        
            \PayUParameters::VALUE => $data['VALUE'],
            //Ingrese aquí la moneda.
            \PayUParameters::CURRENCY => $data['CURRENCY'],
            
            // -- Comprador 
            //Ingrese aquí el nombre del comprador.
            \PayUParameters::BUYER_NAME => $data['BUYER_NAME'],
            //Ingrese aquí el email del comprador.
            \PayUParameters::BUYER_EMAIL => $data['BUYER_EMAIL'],
            //Ingrese aquí el teléfono de contacto del comprador.
            \PayUParameters::BUYER_CONTACT_PHONE => $data['BUYER_CONTACT_PHONE'],
            //Ingrese aquí el documento de contacto del comprador.
            \PayUParameters::BUYER_DNI => $data['BUYER_CONTACT_PHONE'],          
            //Ingrese aquí la dirección del comprador.
            \PayUParameters::BUYER_STREET =>            $data['BUYER_STREET'],                    
            \PayUParameters::BUYER_STREET_2 =>          $data['BUYER_STREET_2'],           
            \PayUParameters::BUYER_CITY =>              $data['BUYER_CITY'],              
            \PayUParameters::BUYER_STATE =>             $data['BUYER_STATE'],  
            \PayUParameters::BUYER_COUNTRY =>           $data['BUYER_COUNTRY'],  
            \PayUParameters::BUYER_POSTAL_CODE =>       $data['BUYER_POSTAL_CODE'],  
            \PayUParameters::BUYER_PHONE =>             $data['BUYER_PHONE'],  
            
            // -- pagador --
            //Ingrese aquí el nombre del pagador.
            \PayUParameters::PAYER_NAME =>              $data['PAYER_NAME'],  
            //Ingrese aquí el email del pagador.
            \PayUParameters::PAYER_EMAIL =>             $data['PAYER_EMAIL'],  
            //Ingrese aquí el teléfono de contacto del pagador.
            \PayUParameters::PAYER_CONTACT_PHONE =>     $data['PAYER_CONTACT_PHONE'],  
            //Ingrese aquí el documento de contacto del pagador.
            \PayUParameters::PAYER_DNI =>               $data['PAYER_DNI'],  
            //Ingrese aquí la dirección del pagador.
            \PayUParameters::PAYER_STREET =>            $data['PAYER_STREET'],             
            \PayUParameters::PAYER_STREET_2 =>          $data['PAYER_STREET_2'],          
            \PayUParameters::PAYER_CITY =>              $data['PAYER_CITY'],           
            \PayUParameters::PAYER_STATE =>             $data['PAYER_STATE'],             
            \PayUParameters::PAYER_COUNTRY =>           $data['PAYER_COUNTRY'],        
            \PayUParameters::PAYER_POSTAL_CODE =>       $data['PAYER_POSTAL_CODE'],          
            \PayUParameters::PAYER_PHONE =>             $data['PAYER_PHONE'],            
            
            // -- Datos de la tarjeta de crédito -- 
            //Ingrese aquí el número de la tarjeta de crédito
            \PayUParameters::CREDIT_CARD_NUMBER =>      $data['CREDIT_CARD_NUMBER'],              
            //Ingrese aquí la fecha de vencimiento de la tarjeta de crédito
            \PayUParameters::CREDIT_CARD_EXPIRATION_DATE =>     $data['CREDIT_CARD_EXPIRATION_DATE'],  
            //Ingrese aquí el código de seguridad de la tarjeta de crédito
            \PayUParameters::CREDIT_CARD_SECURITY_CODE=>    $data['CREDIT_CARD_SECURITY_CODE'],  
            //Ingrese aquí el nombre de la tarjeta de crédito
            //VISA||MASTERCARD||AMEX||DINERS
            \PayUParameters::PAYMENT_METHOD =>          $data['PAYMENT_METHOD'],  
            
            //Ingrese aquí el número de cuotas.
            \PayUParameters::INSTALLMENTS_NUMBER =>     $data['INSTALLMENTS_NUMBER'],             
            //Ingrese aquí el nombre del pais.
            \PayUParameters::COUNTRY =>                 $data['COUNTRY'],           
            
            //Session id del device.
            \PayUParameters::DEVICE_SESSION_ID =>               "s6tvkcle931686k1900o6e1",
            //IP del pagadador
            \PayUParameters::IP_ADDRESS => "127.0.0.1",
            //Cookie de la sesión actual.
            \PayUParameters::PAYER_COOKIE=>"pt1t38347bs6jc9ruv2ecpv7o2",
            //Cookie de la sesión actual.        
            \PayUParameters::USER_AGENT=>"Mozilla/5.0 (Windows NT 5.1; rv:18.0) Gecko/20100101 Firefox/18.0"
        );
        
        try {
            $response = \PayUPayments::doAuthorizationAndCapture($parameters);
            $response2 = (array) $response;
        
            // if($response){
            //     $response->transactionResponse->orderId;
            //     $response->transactionResponse->transactionId;
            //     $response->transactionResponse->state;

            //     if($response->transactionResponse->state=="PENDING"){
            //         $response->transactionResponse->pendingReason;
            //         $response->transactionResponse->trazabilityCode;
            //         $response->transactionResponse->authorizationCode;      
            //         $response->transactionResponse->extraParameters->URL_PAYMENT_RECEIPT_HTML;
            //         $response->transactionResponse->extraParameters->REFERENCE;
            //         $response->transactionResponse->extraParameters->BAR_CODE;
            //     }
            //     $response->transactionResponse->responseCode;         
            // } 

            return $response2; 

        } catch (\PayUException $exc) {
            //var_dump($exc->message);
            
            $response=$onError($exc);
        }

        return $response;
    }

/***********************************************************/

    public static function getOrderDetail($data,$onSuccess, $onError)
    {
        static::setPayUEnvironment();

        $parameters = array( \PayUParameters::ORDER_ID => $data);
 
        
        $response = false;

        try {

            $response = \PayUReports::getOrderDetail($parameters);

            $response2 = (array) $response;    
        
            return $response2;

        } catch (\PayUException $exc) {
            
            $response=$onError($exc);
        }

        return $response;

    }

/***********************************************************/

 public static function getDetailByReferenceCode($data,$onSuccess, $onError)
    {
        static::setPayUEnvironment();

        $parameters = array(\PayUParameters::REFERENCE_CODE => $data);
       
        $response = false;

        try {

            $response = \PayUReports::getOrderDetailByReferenceCode($parameters);

            $response2 = (array) $response;    
        
            return $response2;

        } catch (\PayUException $exc) {
            
            $response=$onError($exc);
        }

        return $response;

    }

/***********************************************************/

 public static function getTransactionResponse($data,$onSuccess, $onError)
    {
        static::setPayUEnvironment();

        $parameters = array(\PayUParameters::TRANSACTION_ID => $data);
       
        $response = false;

        try {

            $response = \PayUReports::getTransactionResponse($parameters);

            $response2 = (array) $response;    
        
            return $response2;

        } catch (\PayUException $exc) {
            
            $response=$onError($exc);
        }

        return $response;

    }

/***********************************************************/

}
