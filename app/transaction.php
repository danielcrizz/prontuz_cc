<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class transaction extends Model
{
    //
    protected $table='transaction';
    protected $primaryKey ='transactionId';
	protected $fillable =['transactionOrderid','transactionrReferencecode','transactionIdentification','transactionState','transactionOperationdate','transaction_userId','transactionType','created_by','updated_by'];

	
	protected function store($data){

		$res=DB::table('transaction')->insert($data);
	    
		return $res;
	}
	
/**************************************************/

	protected function sumTransactions(){
		
		$data=array();

		$data['recharge']=DB::select('select CASE WHEN "transactionType" = 0 THEN count("transactionType")  ELSE(
                    CASE WHEN "transactionType" = 1 THEN  count("transactionType")   ELSE (
                        CASE WHEN "transactionType" = 2 THEN count("transactionType")  ELSE (
				CASE WHEN "transactionType" = 3 THEN count("transactionType")  ELSE count("transactionType")  END
			)END
                    )END
                )
                END  as "transactioncount", 
                CASE WHEN "transactionType" = 0 THEN  \'Tarjeta de Credito\'  ELSE(
                    CASE WHEN "transactionType" = 1 THEN  \'Pago efectivo(Baloto, efecty ..etc)\'  ELSE (
                        CASE WHEN "transactionType" = 2 THEN  \'Transferencias bancarias(PSE)\' ELSE (
				CASE WHEN "transactionType" = 3 THEN  \'Tarjetas débito\' ELSE \'Pago en Caja\' END
			)END
                    )END
                )
                END  as "transactionType2",
		CASE WHEN "transactionType" = 0 THEN sum("transactionValue")  ELSE(
                    CASE WHEN "transactionType" = 1 THEN  sum("transactionValue")  ELSE (
                        CASE WHEN "transactionType" = 2 THEN sum("transactionValue")  ELSE (
				CASE WHEN "transactionType" = 3 THEN sum("transactionValue")  ELSE sum("transactionValue")  END
			)END
                    )END
                )
                END  as "transactionGets", 
                "transactionType"
				from "transaction"
				WHERE "transactionState"=1
				AND "transactionCategory"=1
				group by "transactionType","transactionType"');

		$data['offer']=DB::select('select CASE WHEN "transactionType" = 0 THEN count("transactionType")  ELSE(
                    CASE WHEN "transactionType" = 1 THEN  count("transactionType")   ELSE (
                        CASE WHEN "transactionType" = 2 THEN count("transactionType")  ELSE (
				CASE WHEN "transactionType" = 3 THEN count("transactionType")  ELSE count("transactionType")  END
			)END
                    )END
                )
                END  as "transactioncount", 
                CASE WHEN "transactionType" = 0 THEN  \'Tarjeta de Credito\'  ELSE(
                    CASE WHEN "transactionType" = 1 THEN  \'Pago efectivo(Baloto, efecty ..etc)\'  ELSE (
                        CASE WHEN "transactionType" = 2 THEN  \'Transferencias bancarias(PSE)\' ELSE (
				CASE WHEN "transactionType" = 3 THEN  \'Tarjetas débito\' ELSE \'Pago en Caja\' END
			)END
                    )END
                )
                END  as "transactionType2",
		CASE WHEN "transactionType" = 0 THEN sum("transactionValue")  ELSE(
                    CASE WHEN "transactionType" = 1 THEN  sum("transactionValue")  ELSE (
                        CASE WHEN "transactionType" = 2 THEN sum("transactionValue")  ELSE (
				CASE WHEN "transactionType" = 3 THEN sum("transactionValue")  ELSE sum("transactionValue")  END
			)END
                    )END
                )
                END  as "transactionGets", 
                "transactionType"
				from "transaction"
				WHERE "transactionState"=1
				AND "transactionCategory"=0
				group by "transactionType","transactionType"');
		
	   
		return $data;
	}
	
/**************************************************/

}
