<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class routeprincipals extends Model
{
    //
    protected $table='routeprincipals';
    protected $primaryKey = 'routeprincipalsId';
    protected $fillable =['routeprincipalsName','created_by','updated_by'];
}
