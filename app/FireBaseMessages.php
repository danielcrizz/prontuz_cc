<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use PushNotification;
class FireBaseMessages extends Model
{
    protected function sendOne($user,$title,$msn,$data,$type='')
    {
		$token = $user['userToken'];
        $data['title']=$title;
        $data['body']=$msn;
        $data['sound']='default';
        $data['content_available']=true;
		$d = PushNotification::setService('fcm') 
    			->setMessage([
                                'content_available'=>true,
                                'data'=>$data
                             ])
                        ->setApiKey(env('FCM_SERVER_KEY'))
                        ->setDevicesToken($token)
                        ->send()
                        ->getFeedback();
    }
    protected function sendMany($users)
    {
    	$optionBuilder = new OptionsBuilder();
		$optionBuilder->setTimeToLive(60*20);

		$notificationBuilder = new PayloadNotificationBuilder('my title');
		$notificationBuilder->setBody('Hello world')
						    ->setSound('default');
						    
		$dataBuilder = new PayloadDataBuilder();
		$dataBuilder->addData(['a_data' => 'my_data']);

		$option = $optionBuilder->build();
		$notification = $notificationBuilder->build();
		$data = $dataBuilder->build();

		// You must change it to get your tokens
		$tokens = User::pluck('userToken')->toArray();

		$downstreamResponse = FCM::sendTo($tokens, $option, $notification);

		$downstreamResponse->numberSuccess();
		$downstreamResponse->numberFailure(); 
		$downstreamResponse->numberModification();

		//return Array - you must remove all this tokens in your database
		$downstreamResponse->tokensToDelete(); 

		//return Array (key : oldToken, value : new token - you must change the token in your database )
		$downstreamResponse->tokensToModify(); 

		//return Array - you should try to resend the message to the tokens in the array
		$downstreamResponse->tokensToRetry();

		// return Array (key:token, value:errror) - in production you should remove from your database the tokens present in this array 
		$downstreamResponse->tokensWithError(); 
    }
    protected function sendToDriver($user,$title,$msn,$data)
    {
        $token = $user['userToken'];
        $data['title']=$title;
        $data['body']=$msn;
        $data['sound']='default';
        $data['content_available']=true;
        $data['isOk']=1;
		$d = PushNotification::setService('fcm') 
    			->setMessage([
                                'content_available'=>true,
                                'data'=>$data
                             ])
                        ->setApiKey(env('FCM_SERVER_KEY_2'))
                        ->setDevicesToken($token)
                        ->send()
                        ->getFeedback();
    }
}
