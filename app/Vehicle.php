<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class Vehicle extends Model
{
    protected $table='vehicle';
    protected $primaryKey ='vehicleId';
	protected $fillable =['vehiclePlaque','vehicle_vehiclebrandId','vehicleModel','vehicle_vehicletypeId','vehicleTecnomecanica','vehicleNoperation','vehicleNinside','vehicleCapacity','vehicleNsoat','vehicleExpiration','vehicle_lineId','vehicleLatitude','vehicleLongitude','vehicleocupation','vehicleState','vehicle_locationId','vehicleBearing','created_by','updated_by'];


	protected function store($data)
	{
		$res=DB::table('vehicle')->insert($data);
		
		if($res){
			$res = DB::table('vehicle')->max('vehicleId');
		}
		
		return $res;
	}

/**************************************************/

	protected function getAll()
	{
		$res=Vehicle::select('vehicleId','vehiclePlaque','vehicle_vehiclebrandId','vehicleModel','vehicle_vehicletypeId','vehicleTecnomecanica','vehicleNoperation','vehicleNinside','vehicleCapacity','vehicleNsoat','vehicleExpiration','vehicle_lineId','vehicleLatitude','vehicleLongitude','vehicleOcupation','vehicleState','vehicle.created_by','vehicle.updated_by',
			'vehiclebrandName','vehicletypeName','lineName'
			)
           ->join('line', 'line.lineId','=', 'vehicle.vehicle_lineId')
           ->join('vehiclebrand', 'vehiclebrand.vehiclebrandId', '=', 'vehicle.vehicle_vehiclebrandId')
           ->join('vehicletype', 'vehicletype.vehicletypeId', '=', 'vehicle.vehicle_vehicletypeId')
           ->orderBy('vehicle.vehicleState','asc')
           ->orderBy('vehicle.vehiclePlaque','asc')
           ->get();

		return $res;
	}

/**************************************************/	
	
	protected function search($data)
	{
		$vehicleState=$data['vehicleState'];
		
		if($vehicleState==1){
			$order='ASC';
		}else{
			$order='DESC';
		}
		   $case='CASE WHEN "vehicleState" = 1 THEN  \'Activo\'  ELSE  \'Inactivo\'  END AS "vehicleState2"';
		   // Inner join "routeassignment" as "ra" on "routeassignment_vehicleId"="vehicleId"
		   $case2='CASE WHEN "routeassignment_vehicleId" is not null THEN 1 ELSE 0  END AS "routeassignment_vehicleId"';
		   $username='concat("userFirstname",'."' '".',"userLastname") AS "userName"';
	    /*

		$res=Vehicle::select('vehicleId','vehiclePlaque','vehicle_vehiclebrandId','vehicleModel','vehicle_vehicletypeId','vehicleTecnomecanica','vehicleNoperation','vehicleNinside','vehicleCapacity','vehicleNsoat','vehicleExpiration','vehicle_lineId','vehicleLatitude','vehicleLongitude','vehicleOcupation','vehicleState','vehicle.created_by','vehicle.updated_by',
			'vehiclebrandName','vehicletypeName','lineName'
			)*/
			
	      $res=Vehicle::select(DB::raw('"vehicleId","vehiclePlaque","vehicle_vehiclebrandId","vehicle_vehicletypeId","vehicleNoperation","vehicleNinside","vehicleExpiration","vehicle_lineId","vehicleLatitude","vehicleLongitude","vehicleOcupation","vehicleState",
			"vehiclebrandName","lineName","vehicleModel","vehicletypeName","vehicleCapacity","vehicleNsoat","vehicleTecnomecanica",'.$case
			))
           ->join('line', 'line.lineId','=', 'vehicle.vehicle_lineId')
           ->join('vehiclebrand', 'vehiclebrand.vehiclebrandId', '=', 'vehicle.vehicle_vehiclebrandId')
           ->join('vehicletype', 'vehicletype.vehicletypeId', '=', 'vehicle.vehicle_vehicletypeId')
			->orWhere('vehicle.vehiclePlaque','ilike','%'.$data['dataSearch'].'%')
           ->orWhere('vehiclebrand.vehiclebrandName','ilike','%'.$data['dataSearch'].'%')
           ->orWhere('vehicletype.vehicletypeName','ilike','%'.$data['dataSearch'].'%')
           ->orWhere('vehicle.vehicleNsoat','ilike','%'.$data['dataSearch'].'%')
           ->orderBy('vehicle.vehicleState',$order)
           ->get();

    	if(array_key_exists('vehicleId', $data)){
			
			$res=Vehicle::select(DB::raw('"vehicleId","vehiclePlaque","vehicle_vehiclebrandId","vehicleModel","vehicle_vehicletypeId","vehicleTecnomecanica","vehicleNoperation","vehicleNinside","vehicleCapacity","vehicleNsoat","vehicleExpiration","vehicle_lineId","vehicleLatitude","vehicleLongitude","vehicleOcupation","vehicleState","vehicle"."created_by","vehicle"."updated_by",
			"vehiclebrandName","vehicletypeName","lineName",'.$case2.','.$username))
           ->join('line', 'line.lineId','=', 'vehicle.vehicle_lineId')
           ->join('vehiclebrand', 'vehiclebrand.vehiclebrandId', '=', 'vehicle.vehicle_vehiclebrandId')
           ->leftjoin('routeassignment', 'routeassignment_vehicleId', '=', 'vehicleId')
           ->leftjoin('user', 'userId', '=', 'routeassignment_userId')
           ->join('vehicletype', 'vehicletype.vehicletypeId', '=', 'vehicle.vehicle_vehicletypeId')
           ->Where('vehicleId','=',$data['vehicleId'])
           ->orderBy('vehicle.vehicleState',$order)
           ->get();

		}
    
    	return $res;
	}

/**************************************************/

protected function listing($data)
    {

        $orden  = $data['order']['0']['column'];
        $ordenby= $data['order']['0']['dir'];
        $search = $data['columns'][$orden]['data'];

        switch($search){
       		
       		case 'vehiclePlaque':
       			$search='vehicle.vehiclePlaque';
       		break;
       		
       		case 'vehiclebrandName':
       			$search='vehiclebrand.vehiclebrandName';
       		break;
       		
       		case 'vehicletypeName':
       			$search='vehicletype.vehicletypeName';
       		break;
       		
       		case 'vehicleNsoat':
       			$search='vehicle.vehicleNsoat';
       		break;
       }
        
        $case='CASE WHEN "vehicleState" = 1 THEN  \'Activo\'  ELSE  \'Inactivo\'  END AS "vehicleState2"';
	    
	    $res=Vehicle::select(DB::raw('"vehicleId","vehiclePlaque","vehicle_vehiclebrandId","vehicle_vehicletypeId","vehicleNoperation","vehicleNinside","vehicleExpiration","vehicle_lineId","vehicleLatitude","vehicleLongitude","vehicleOcupation","vehicleState",
			"vehiclebrandName","lineName","vehicleModel","vehicletypeName","vehicleCapacity","vehicleNsoat","vehicleTecnomecanica",'.$case
			))
	       ->join('line', 'line.lineId','=', 'vehicle.vehicle_lineId')
	       ->join('vehiclebrand', 'vehiclebrand.vehiclebrandId', '=', 'vehicle.vehicle_vehiclebrandId')
	       ->join('vehicletype', 'vehicletype.vehicletypeId', '=', 'vehicle.vehicle_vehicletypeId')
	       //->Where('vehicle.vehicleState','=',1)
	       ->skip($data['start'])->take($data['length'])
	       ->orderBy($search,$ordenby)
	       ->get();
        
        return $res;
    }

/**************************************************/

	protected function validateVsoat($vehicleNsoat,$op='',$vehicleId=''){

		$data=NULL;

		if($op==''){
		
			if(!empty($vehicleNsoat))
	        {
	        	$data = DB::table('vehicle')->where('vehicleNsoat', $vehicleNsoat)->count();	

	        }
	    }else{
    		$data = DB::table('vehicle')
             ->where('vehicleNsoat', '=', $vehicleNsoat)
             ->where('vehicleId', '!=', $vehicleId)
             ->count();

	    }
		return $data;

	}

/**************************************************/

	protected function validateVtecnomecanica($vehicleTecnomecanica,$op='',$vehicleId=''){

		$data=NULL;

		if($op==''){
		
			if(!empty($vehicleTecnomecanica))
	        {
	        	$data = DB::table('vehicle')->where('vehicleTecnomecanica', $vehicleTecnomecanica)->count();		

	        }
		}else{
			$data = DB::table('vehicle')
                 ->where('vehicleTecnomecanica', '=', $vehicleTecnomecanica)
                 ->where('vehicleId', '!=', $vehicleId)
                 ->count();
		}
		return $data;

	}

/**************************************************/

	protected function validateVnoperation($vehicleNoperation,$op='',$vehicleId=''){

		$data=NULL;

		if($op==''){
		
			if(!empty($vehicleNoperation))
	        {
	        	$data = DB::table('vehicle')->where('vehicleNoperation', $vehicleNoperation)->count();		

	        }
		}else{
			$data = DB::table('vehicle')
                 ->where('vehicleNoperation', '=', $vehicleNoperation)
                 ->where('vehicleId', '!=', $vehicleId)
                 ->count();
		}
		return $data;

	}

/**************************************************/

	protected function validateVninside($vehicleNinside,$op='',$vehicleId=''){

		$data=NULL;
		
		if($op==''){
			if(!empty($vehicleNinside))
	        {
	        	$data = DB::table('vehicle')->where('vehicleNinside', $vehicleNinside)->count();	
	        }
        }else{
        	$data = DB::table('vehicle')
                 ->where('vehicleNinside', '=', $vehicleNinside)
                 ->where('vehicleId', '!=', $vehicleId)
                 ->count();
		}
		
		return $data;

	}

/**************************************************/

	protected function put($data)
    {
    	//var_dump($data);
    	
    	$res=Vehicle::where('vehicleId','=', $data['vehicleId'])->update($data);
    	
    	return $res; 
    }

/**************************************************/

     protected function destroyed($data)
    {
    	
    	$res=Vehicle::where('vehicleId','=', $data['vehicleId'])->update(['vehicleState'=>0],['updated_by'=>Auth::user()->userId]);
            
        return $res;
         
    }

/**************************************************/

     protected function bringVavailable()
    {
    	$vehicleId = DB::table('routeassignment')->pluck('routeassignment_vehicleId');

    	$res=Vehicle::select('vehicleId','vehiclePlaque')
           ->where('vehicleState','=',1)
           ->whereNotIn('vehicleId',$vehicleId)
           ->get();
            
       return $res; 
         
    }

/**************************************************/

    protected function refreshVlocation($data,$vehicleId){

    	$res=Vehicle::where('vehicleId','=', $vehicleId)->update($data);

    	return $res;
    }

/**************************************************/

	protected function bringOnelbyvehicle($data){

		$res=Vehicle::select('vehicleId','vehiclePlaque','vehicle_vehiclebrandId','vehicleModel','vehicle_vehicletypeId','vehicleTecnomecanica','vehicleNoperation','vehicleNinside','vehicleCapacity','vehicleNsoat','vehicleExpiration','vehicle_lineId','vehicleLatitude','vehicleLongitude','vehicleOcupation','vehicleState','vehicle.created_by','vehicle.updated_by',
			'vehiclebrandName','vehicletypeName','lineName'
			)
           ->join('line', 'line.lineId','=', 'vehicle.vehicle_lineId')
           ->join('vehiclebrand', 'vehiclebrand.vehiclebrandId', '=', 'vehicle.vehicle_vehiclebrandId')
           ->join('vehicletype', 'vehicletype.vehicletypeId', '=', 'vehicle.vehicle_vehicletypeId')
           ->orWhere('vehicle.vehiclePlaque','ilike','%'.$data['dataSearch'].'%')
           ->orWhere('vehiclebrand.vehiclebrandName','ilike','%'.$data['dataSearch'].'%')
           ->orWhere('vehicletype.vehicletypeName','ilike','%'.$data['dataSearch'].'%')
           ->orWhere('vehicle.vehicleNsoat','ilike','%'.$data['dataSearch'].'%')
           ->orderBy('vehicle.vehicleState',$order)
           ->get();

	    
	    return $res;
	}

/**************************************************/

	protected function bringVperfil($routeassignment_userId,$user_rolId){

		if($user_rolId == 4){

			$res= DB::select(DB::raw('select "vehicleId", "vehiclePlaque", "vehicleCapacity", "vehicleOcupation", "userFirstname", "userLastname","ra"."routeassignmentId","vehicleId",("vehicleCapacity"-"vehicleOcupation") as "vehicleDifference","user_rolId"
				from "vehicle" 
				Inner join "routeassignment" as "ra" on "routeassignment_vehicleId"="vehicleId"
				Inner join "user" on "userId" = "ra"."routeassignment_userId" 
				where "ra"."routeassignment_userId" ='.$routeassignment_userId.' and "vehicleState" = 1
				group by ("vehicleCapacity"-"vehicleOcupation"),"ra"."routeassignmentId","vehicleId", "vehiclePlaque", "vehicleCapacity", "vehicleOcupation", "vehicleState", "userFirstname", "userLastname","user_rolId"'));

		}else if($user_rolId == 3){
			
			$res= DB::select(DB::raw('select "vehicleId", "vehiclePlaque", "vehicleCapacity", "vehicleOcupation", "userFirstname", "userLastname","routeId","routeName","routeDirection","ra"."routeassignmentId","vehicleId",("vehicleCapacity"-"vehicleOcupation") as "vehicleDifference","user_rolId",
			trim(trailing from "route_routeprincipalsId") AS "route_routeprincipalsId"
			from "vehicle" 
			Inner join "routeassignment" as "ra" on "routeassignment_vehicleId"="vehicleId"
			Inner join "user" on "userId" = "ra"."routeassignment_userId" 
			Inner join "route"  on 
			"ra"."routeassignmentFirst_routeId" = "routeId" or "ra"."routeassignmentLast_routeId" = "routeId" 
			where "ra"."routeassignment_userId" ='.$routeassignment_userId.' and "vehicleState" = 1
			group by ("vehicleCapacity"-"vehicleOcupation"),"ra"."routeassignmentId","vehicleId", "vehiclePlaque", "vehicleCapacity", "vehicleOcupation", "vehicleState", "userFirstname", "userLastname","routeId","routeName","user_rolId"'));
		
		} 

		if(count($res) == 0){
			$res=false;
		}
	    
	    return $res;
	}

/**************************************************/

	protected function storeVbrand($data)
	{
		
		$res=DB::table('vehiclebrand')->insert($data);

		return $res;
	}

/**************************************************/

	protected function validateVbrand($vehiclebrandName)
    {
    	
    	$res=DB::table('vehiclebrand')->where('vehiclebrandName','=', $vehiclebrandName)->count();
    	
    	return $res; 
    }
}

