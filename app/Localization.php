<?php

namespace App;
use Auth;
use Illuminate\Database\Eloquent\Model;
use DB;

class Localization extends Model
{
    protected $table='localization';
    protected $primaryKey ='localizationId';
	protected $fillable =['localizationLatitude','localizationLongitude','localizationAddress','created_by','updated_by'];

	protected function bringlocalization(){

		$res1 = array();
		$res = false;
		
		$res0= DB::select('select max("route_order") as "route_order","route_routeId"
			from "localization"
			inner join "route_localization" on "localization_localizationId" = "localizationId"
			Inner join "routeassignment" as "ra" on "ra"."routeassignmentFirst_routeId" = "route_routeId" and "ra"."routeassignmentActive"=1 or "ra"."routeassignmentLast_routeId" = "route_routeId" and "ra"."routeassignmentActive"=2
			Inner join "vehicle" as "v" on "v"."vehicleId" = "ra"."routeassignment_vehicleId"
			where "localizationId" >= "vehicle_locationId"
			group by "route_routeId"');

		$i=0;
		
		foreach ($res0 as $k => $v) {
			
			$res1= DB::select('select "localizationId", "localizationAddress","localizationLatitude", "localizationLongitude"
			from "localization"
			inner join "route_localization" on "localization_localizationId" = "localizationId"
			Inner join "routeassignment" as "ra" on "ra"."routeassignmentFirst_routeId" = "route_routeId" and "ra"."routeassignmentActive"=1 or "ra"."routeassignmentLast_routeId" = "route_routeId" and "ra"."routeassignmentActive"=2
			Inner join "vehicle" as "v" on "v"."vehicleId" = "ra"."routeassignment_vehicleId"
			where "localizationId" >= "vehicle_locationId"
			and "route_order" !='.$v->route_order.'
			and "route_routeId" = '.$v->route_routeId.'
			order by "route_order"  asc');
			
			foreach ($res1 as $v1) {
				$res[$i]=$v1;
				$i++;
			}
			
		}
					
        return $res;
		
	}

/**************************************************/
    
    protected function store($data,$routeId){

    	$res=false;

    	for($i=0; $i < count($data); $i++){

    		$data2 = Localization::select(DB::raw('count(*) as count, "localizationId"'))
                     ->where('localizationLatitude', '=', $data[$i]['localizationLatitude'])
                     ->where('localizationLongitude', '=', $data[$i]['localizationLongitude'])
                     ->groupBy('localizationId')
                     ->get();
           
           $data2=$data2->toArray();

           if(!empty($data2)){

           	//print_r($data2);
           		$localizationIds=$data2[0]['localizationId'];
           }else{
           		$localizationIds = Localization::insertGetId([
	    		'localizationLatitude' => $data[$i]['localizationLatitude'],
	    		'localizationLongitude' => $data[$i]['localizationLongitude'],
	    		'localizationAddress' => $data[$i]['localizationAddress'],
	    		'created_by' => Auth::user()->userId
    			],'localizationId');
           }

           	
    		$res = route_localization::insert([
	    		'route_routeId' => $routeId,
	    		'localization_localizationId' => $localizationIds,
	    		'route_order' => ($i+1),
	    		'created_by' => Auth::user()->userId
    		]);

    	}

    	return $res;

    }

/**************************************************/

	protected function put($data,$routeId){

    	$res=false;
    	
    	$Lsecond=array();
    	$Lthird=array();
    	$Ldelete=array();

    	for($i=0; $i < count($data); $i++){

    		if(($data[$i] != '') || ($data[$i] != NULL)){
    		
    			if(($data[$i]['localizationId'] != '') || ($data[$i]['localizationId'] != NULL)){
    				$Lsecond[$i]=$data[$i]['localizationId'];

   					DB::table('localization')->where('localizationId', $data[$i]['localizationId'])->update(['localizationAddress' => $data[$i]['localizationAddress']]);
    			}
   				
     		} // else{
   //  			$j=1;
   //  			$Lthird[$j]=$data[$i];
   //  			$j++;
			// }
    		
    	
    	}

    	$Lfirst  = Routes::select('rl.localization_localizationId')
    				->join('route_localization as rl', 'rl.route_routeId', '=', 'routeId')
			   		->whereNotIn('rl.localization_localizationId', $Lsecond)
			   		->where('rl.route_routeId', '=', $routeId)
			   		->get();


		$Ldelete=$Lfirst->toArray();

		if(empty($Ldelete)){
			$res=true;
		}

		foreach($Ldelete as $k => $v){

			$res=route_localization::where('localization_localizationId', '=', $v['localization_localizationId'])
				->where('route_routeId', '=', $routeId)->delete();
		}

		

		if($res){

			foreach($data as $k => $v){

				
    			$Lcount = Routes::join('route_localization as rl', 'rl.route_routeId', '=', 'routeId')
			   		->where('rl.localization_localizationId', '=', $v['localizationId'])
			   		->where('rl.route_routeId', '=', $routeId)
			   		->count();

			 	if($Lcount == 0){

					//$Morder = Routes::join('route_localization as rl', 'rl.route_routeId', '=', 'routeId')
					$Morder = DB::table('route')->select(DB::raw('max("route_order") as "route_order"'))
					->join('route_localization as rl', 'rl.route_routeId', '=', 'routeId')
			   		->where('rl.route_routeId', '=', $routeId)
			   		->value('route_order');


			   		$Morder=intval($Morder);
					
			   		if(empty($Morder) || $Morder == 0){
			   			$Morder=1;
			   		}else{
			   			$Morder=($Morder+1);
			   		}

			   		if(($v['localizationId'] != '') || $v['localizationId']== NULL){

			   			$v['localizationId'] = Localization::insertGetId([
				    		'localizationLatitude' => $v['localizationLatitude'],
				    		'localizationLongitude' => $v['localizationLongitude'],
				    		'localizationAddress' => $v['localizationAddress'],
				    		'created_by' => Auth::user()->userId
			    			],'localizationId');
			   		}
			   			
		   			$res = route_localization::insert([
			    		'route_routeId' => $routeId,
			    		'localization_localizationId' => $v['localizationId'],
			    		'route_order' => $Morder,
			    		'created_by' => Auth::user()->userId
	    			]);
		   		
				}

    		}
		}
		
    	return $res;

    }

    /**************************************************/
	
	protected function bringLbyvehicle($data){
		

       	$res= DB::select('select "localizationId", "localizationLatitude", "localizationLongitude", 	"localizationAddress","vehicleBearing" 
			from "localization" 
			Inner join "route_localization" on "localization_localizationId" = "localizationId" 
			Inner join "routeassignment" as "ra" on "ra"."routeassignmentFirst_routeId" = "route_routeId" and "ra"."routeassignmentActive"=1 or "ra"."routeassignmentLast_routeId" = "route_routeId" and "ra"."routeassignmentActive"=2 
			Inner join "vehicle" on "vehicleId"="routeassignment_vehicleId"
			where "routeassignmentId" = '.$data['routeassignmentId'].' and "routeassignment_vehicleId" = '.$data['routeassignment_vehicleId'].'
			order by "route_order"  asc');
		
        return $res;
	}

	/**************************************************/

	
	protected function bringLbyroute($data){

		$res= DB::select('select DISTINCT "localizationId", "localizationAddress","localizationLatitude", "localizationLongitude","route_order"
			from "localization"
			Inner join "route_localization" on "localization_localizationId" = "localizationId" 
			left join "routeassignment" as "ra" on "ra"."routeassignmentFirst_routeId" = "route_routeId"
			left join "routeassignment" as "ra2" on "ra2"."routeassignmentLast_routeId" = "route_routeId" 
			where "route_routeId" = '.$data['routeId'].'
			order by "route_order" asc');
		
        return $res;
	}

	/**************************************************/

	
	protected function bringlby2l($vehicleId,$localization_localizationId2){
		/**/
		$res= DB::select('select "vehicleLatitude","vehicleLongitude","localizationLatitude", "localizationLongitude"
			from  "vehicle","localization"
			where "vehicleId"='.$vehicleId.' 
			and "localizationId" ='.$localization_localizationId2);
	
		return $res;
	}



}
