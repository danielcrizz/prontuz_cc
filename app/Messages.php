<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
	protected function message($value,$data='')
	{
		$msg=array();
		$msg[10000][0] = 1;//ResponseCode
		$msg[10000][1] = 'Operación realizada con éxito';//ResponseMessage
		$msg[10001][0] = 0;
		$msg[10001][1] = 'Pendiente envió de datos';
		$msg[10002][0] = 0;
		$msg[10002][1] = 'No se pudo realizar la consulta';
		$msg[10003][0] = 1;
		$msg[10003][1] = 'Señor(a) usuario(a) se le envió  un correo con un dígito de verificación para que por favor lo digite, el código tiene un tiempo de duración de 10 min.';
		$msg[10004][0] = 1;
		$msg[10004][1] = 'Señor(a) usuario(a) se le envió  un correo con un código de verificación para el cambio de contraseña, el código tiene un tiempo de duración de 10 min.';	
		$msg[10005][0] = 1;
		$msg[10005][1] = 'Señor(a) usuario(a) reserva cancelada';
		$msg[10006][0] = 1;
		$msg[10006][1] = 'Señor(a) usuario(a) cambio de contraseña realizado';		
		$msg[10007][0] = 0;
		$msg[10007][1] = 'El usuario no se ha encontrado';
		$msg[10008][0] = 0;
		$msg[10008][1] = 'Señor(a) usuario(a) la reserva fue cancelada por el conductor';
		$msg[10009][0] = 0;
		$msg[10009][1] = 'Contraseña errónea';
		$msg[10010][0] = 0;
		$msg[10010][1] = 'No se recibió un token valido';
		$msg[10011][0] = 0;
		$msg[10011][1] = 'Usuario no registrado';
		$msg[20000][0] = 0;
		$msg[20000][1] = 'Problema para guardar los datos';

		$msg[20001][0] = 0;
		$msg[20001][1] = 'El correo y la contraseña están en blanco';
		$msg[20002][0] = 0;
		$msg[20002][1] = 'La contraseña está en blanco';
		$msg[20003][0] = 0;
		$msg[20003][1] = 'El correo está en blanco';
		$msg[20004][0] = 0;
		$msg[20004][1] = 'El estatus del usuario es inactivo o no existe';
		$msg[20005][0] = 0;
		$msg[20005][1] = 'Correo o contraseña errónea';
		$msg[20006][0] = 0;
		$msg[20006][1] = 'Correo ya se registró anteriormente';
		$msg[20007][0] = 0;
		$msg[20007][1] = 'El número de identificación ya fue registrado anteriormente';
		$msg[20008][0] = 0;
		$msg[20008][1] = 'No hay coincidencia en la búsqueda';
		$msg[20009][0] = 0;
		$msg[20009][1] = 'No existen tipos de documentos disponibles';		
		$msg[20010][0] = 0;
		$msg[20010][1] = 'Especifiqué plataforma móvil o web (typePlataform)';
		$msg[20011][0] = 0;
		$msg[20011][1] = 'Código de verificación incorrecto';	
		$msg[20012][0] = 0;
		$msg[20012][1] = 'Los datos personales enviados no coinciden con ningún registro';		
		$msg[20013][0] = 0;
		$msg[20013][1] = 'Este usuario ya se encuentra activado';	
		$msg[20014][0] = 0;
		$msg[20014][1] = 'Los datos proporcionados son incorrectos';		
		$msg[20015][0] = 0;
		$msg[20015][1] = 'No hay menú registrado';
		$msg[20016][0] = 0;
		$msg[20016][1] = 'El usuario no está activo';
		$msg[20017][0] = 0;
		$msg[20017][1] = 'Señor(a) usuario(a) no posee reservas';
		$msg[20018][0] = 3;
		$msg[20018][1] = 'Señor(a) usuario(a) usted ya posee una reserva';
		$msg[20019][0] = 1;
		$msg[20019][1] = 'Esa reserva ya fue cancelada';
		$msg[20020][0] = 0;
		$msg[20020][1] = 'Código invalido o no existe';
		$msg[20021][0] = 0;
		$msg[20021][1] = 'No hay reservas registradas';		
		$msg[20022][0] = 0;
		$msg[20022][1] = 'Su rol no coincide con su perfil de usuario';		
		$msg[20023][0] = 0;
		$msg[20023][1] = 'No se a confirmado su ingreso al vehículo';		
		$msg[20024][0] = 0;
		$msg[20024][1] = 'No realizar la reserva ya que el vehículo ya paso por la localización seleccionada';		
		$msg[20025][0] = 0;
		$msg[20025][1] = 'Señor(a) usuario(a) aún no a realizado el registro en la aplicación del conductor';		
		$msg[20026][0] = 2;
		$msg[20026][1] = 'Actualmente hay una sesion activa, desea cerrar la sesion?';
		$msg[20027][0] = 0;
		$msg[20027][1] = 'Solo puede realizar maximo dos reservas por usuario';
		$msg[20028][0] = 0;
		$msg[20028][1] = 'Se ha llenado la capacidad del vehiculo, por favor selecciona otro';
		$msg[20029][0] = 0;
		$msg[20029][1] = 'Falta una reserva por confirmar';
		$msg[20030][0] = 2;
		$msg[20030][1] = 'Este usuario tiene reservada una ruta';
		$msg[20031][0] = 3;
		$msg[20031][1] = 'Este usuario tiene reservado un wheel';
		$msg[20032][0] = 0;
		$msg[20032][1] = 'Señor usuario actualmente tiene reservada una ruta';
		$msg[20033][0] = 0;
		$msg[20033][1] = 'Señor usuario actualmente tiene reservado un wheel';


		$msg[30000][0] = 0;
		$msg[30000][1] = 'No existen línea asociadas a la marca';
		$msg[30001][0] = 0;
		$msg[30001][1] = 'Placa ya asignada';
		$msg[30002][0] = 1;
		$msg[30002][1] = 'Placa disponible';
		$msg[30003][0] = 0;
		$msg[30003][1] = 'Soat previamente registrado';
		$msg[30004][0] = 0;
		$msg[30004][1] = 'Soat requerido';
		$msg[30005][0] = 0;
		$msg[30005][1] = 'Tecnomecanica previamente registrada';
		$msg[30006][0] = 0;
		$msg[30006][1] = 'Tecnomecanica requerida';
		$msg[30007][0] = 0;
		$msg[30007][1] = 'Número de operación previamente registrado';
		$msg[30008][0] = 0;
		$msg[30008][1] = 'Número de operación Requerido';
		$msg[30009][0] = 0;
		$msg[30009][1] = 'Número de interno previamente registrado';
		$msg[30010][0] = 0;
		$msg[30010][1] = 'Número de interno requerido';
		$msg[30011][0] = 0;
		$msg[30011][1] = 'Placa requerida';
		$msg[30012][0] = 0;
		$msg[30012][1] = 'Vehiculó requerido';
		$msg[30013][0] = 0;
		$msg[30013][1] = 'No existen coincidencias para el vehículo';
		$msg[30014][0] = 0;
		$msg[30014][1] = 'Nombre de marca ya existe';
		$msg[30015][0] = 0;
		$msg[30015][1] = 'Nombre de Linea ya existe';
		
		$msg[40001][0] = 0;
		$msg[40001][1] = 'Código de ruta ya asignado';
		$msg[40002][0] = 1;
		$msg[40002][1] = 'Código de ruta disponible';
		$msg[40003][0] = 0;
		$msg[40003][1] = 'Código de ruta requerido';
		$msg[40004][0] = 0;
		$msg[40004][1] = 'No existe rutas principales disponibles';
		$msg[40005][0] = 0;
		$msg[40005][1] = 'No existen rutas principales asociadas a la ruta';
		$msg[40006][0] = 0;
		$msg[40006][1] = 'Es requerido selección de al menos dos paradas';
		$msg[40007][0] = 0;
		$msg[40007][1] = 'Asignación de ruta-usuario-vehículo ya existe';
		$msg[40008][0] = 0;
		$msg[40008][1] = 'No se pudo eliminar la asignación de ruta-usuario-vehículo';
		$msg[40009][0] = 0;
		$msg[40009][1] = 'Pendiente envió asignación';
		$msg[40010][0] = 0;
		$msg[40010][1] = 'No existe vehículos asociados a esta ruta';
		$msg[40011][0] = 0;
		$msg[40011][1] = 'No existe rutas asociadas a esta parada';
		$msg[40012][0] = 0;
		$msg[40012][1] = 'Aún no te han asignado una ruta, comunícate con COOPAMER para ser asignado a una ruta';
		$msg[40013][0] = 0;
		$msg[40013][1] = 'No existen paradas asociadas a esta ruta';
		$msg[40014][0] = 0;
		$msg[40014][1] = 'No hay vehículos asignado a la ruta selecciona';		
		$msg[40015][0] = 0;
		$msg[40015][1] = 'El vehiculó llego a la capacidad máxima';
		$msg[40016][0] = 0;
		$msg[40016][1] = 'No existes asignaciones de esta ruta';
		$msg[40017][0] = 0;
		$msg[40017][1] = 'Nombre de parada ya asignado';
		$msg[40018][0] = 1;
		$msg[40018][1] = 'Nombre de parada disponible';
		$msg[40019][0] = 1;
		$msg[40019][1] = 'Latitud y longitud no existentes';
		$msg[40020][0] = 0;
		$msg[40020][1] = 'No existen rutas disponibles';		
		$msg[40021][0] = 0;
		$msg[40021][1] = 'No hay paradas asociadas';		
		$msg[40022][0] = 0;
		$msg[40022][1] = 'No hay poliLine disponible verifique las paradas de la ruta';
		$msg[40023][0] = 0;
		$msg[40023][1] = 'No se puso realizar la creación  de la ruta';

		
		$msg[50001][0] = 0;
		$msg[50001][1] = 'No se pudo dar inicio al recorrido, intente nuevamente';

		$msg[60000][0] = 1;
		$msg[60000][1] = 'Ping PayU exitoso';
		$msg[60001][0] = 0;
		$msg[60001][1] = 'Error de ping hacia PayU';


		$msg[70000][0] = 1;
		$msg[70000][1] = 'Ping PayU exitoso';
		$msg[70001][0] = 0;
		$msg[70001][1] = 'No posee saldo disponible';
		$msg[70002][0] = 1;
		$msg[70002][1] = 'Transaccion Pendiente en realizar';
		$msg[70003][0] = 1;
		$msg[70003][1] = 'Transaccion Aprobada';
		$msg[70004][0] = 1;
		$msg[70004][1] = 'Transaccion Declinada';
		$msg[70005][0] = 0;
		$msg[70005][1] = '¡Hola! Tienes un saldo pendiente de pago, para seguir usando ProntuzU debes recargar tu cuenta';
		$msg[70006][0] = 0;
		$msg[70006][1] = 'Error no se pudo realizar el descuento';
		$msg[70007][0] = 0;
		$msg[70007][1] = 'El usuario no existe en el sistema por favor verifique los datos o comuniquese con el administrador';
		$msg[70008][0] = 0;
		$msg[70008][1] = 'Transacción declinada por favor verifique la información de su tarjeta';
		$msg[70009][0] = 0;
		$msg[70009][1] = 'No posee una tarjeta registrada';

		$msg[80001][0] = 0;
		$msg[80001][1] = 'No existe ningun Wheel con las especificaciones indicadas disponible';
		$msg[80002][0] = 0;
		$msg[80002][1] = 'Ustes posee una reserva, se realizara la cancelación de esta';
		$msg[80003][0] = 0;
		$msg[80003][1] = 'Aun no tiene ningun viaje pendiente';


		$msg[90001][0] = 0;
		$msg[90001][1] = 'No  se pudo realizar la creación de la oferta';
		$msg[90002][0] = 0;
		$msg[90002][1] = 'No existen Ofertas disponibles';
		$msg[90003][0] = 0;
		$msg[90003][1] = 'No existen Noticias disponibles';

		
		



		return $this->responseMessage($msg[$value],$data);

	}

	protected function responseMessage($msg,$data)
	{
		$res=array();

		if(!empty($data)){
				$res = array(
					'ResponseCode'		=> $msg[0],
					'ResponseMessage'	=> $msg[1],
					'ResponseData'		=> $data
					);

		}else{
			$res = array(
					'ResponseCode'		=> $msg[0],
					'ResponseMessage'	=> $msg[1]
					);
		}

		return json_encode($res);
	}
}
