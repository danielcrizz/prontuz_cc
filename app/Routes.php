<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Appitventures\Phpgmaps\Phpgmaps;
use DB;
use App\Gmaps;
use Auth;

class Routes extends Model
{
    protected $table='route';
    protected $primaryKey ='routeId';
	protected $fillable =['routeName','route_routeprincipalsId','routeDirection','created_by','updated_by'];


	protected function validateRname($routeName,$op='',$routeId=''){

		$data=NULL;

		if($op==''){
			if(!empty($routeName))
	        {
	        	$data = DB::table('route')->where('routeName', $routeName)->count();	
	        }
        }else{
        	$data = DB::table('route')
                 ->where('routeName', '=', $routeName)
                 ->whereNotIn('routeId', array($routeId))
                 ->count();
		}
		
		return $data;

	}

	
	/**************************************************/

	protected function bringRbylocation($data){

		$add='';
		$localization_localizationId=$data['localization_localizationId'];

		if( $data['routeId'] > 0 ){
			
			$routeId=$data['routeId'];
			$add=' and "routeId" ='.$routeId;
		}
		
		$localization_localizationId=$data['localization_localizationId'];

		$concat="'".url('/').'/'."'";

		$res= DB::select('
				select 5 as "rTime","routeassignmentId",
				trim(trailing from "route_routeprincipalsId") AS "route_routeprincipalsId","route"."created_by","routeId","routeName","routeDirection",
				"v"."vehicleId",("v"."vehicleCapacity"-"v"."vehicleOcupation") as "vehicleDifference",
				"v"."vehicleOcupation","v"."vehiclePlaque","v"."vehicle_locationId","vehicleCapacity",
				concat('.$concat.',"u"."userImage") as "userImage",
				"rl"."localization_localizationId"
				
				from "route" 
				Inner join "routeassignment" as "ra" on "ra"."routeassignmentFirst_routeId" = "routeId" and "ra"."routeassignmentActive"=1 or "ra"."routeassignmentLast_routeId" = "routeId" and "ra"."routeassignmentActive"=2 
				inner join "user" as "u" on "u"."userId" = "ra"."routeassignment_userId" 
				left join "vehicle" as "v" on "ra"."routeassignment_vehicleId" = "v"."vehicleId"
				inner join "route_localization" as "rl" on "rl"."route_routeId" =  "routeId"  and "localization_localizationId" >= "v"."vehicle_locationId"
				inner join "localization" as "l" on "l"."localizationId" = "v"."vehicle_locationId"
				where
				"rl"."localization_localizationId" = '.$localization_localizationId.'
				'.$add.'
				and  ("v"."vehicleCapacity"-"v"."vehicleOcupation") > 0
				order by "routeName" asc');


		$res = $this->organicetime($res);
	
		if(count($res) == 0){
			$res=false;
		}
	    
	    return $res;
	}

/**************************************************/

	protected function store($data)
    {
		$routeId = DB::table('route')->insertGetId([
    		'routeName' => $data['routeName'],
    		'routeState' => 1,
    		'routePrice' => $data['routePrice'],
    		'route_routeprincipalsId' => implode(",",$data['route_routeprincipalsId']),
    		'routeDirection' => $data['routeDirection'],
    		'created_by' => Auth::user()->userId
    	],'routeId');
    	return $routeId;
    }		

/**************************************************/

	protected function storeRoutewheel($data)
    {
     
    	$res=DB::table('routewheel')->insert($data);
		
		if($res){
			$res = DB::table('routewheel')->max('routewheelId');
		}

		return $res;
    }

/**************************************************/

	protected function search($data='')
	{
		if(empty($data)){
			$res=false;

			$data= DB::select('select  5 as "rTime",
				count("routeId") as "numLocations",
				count(DISTINCT"routeassignment_vehicleId") as "numvehicle",
				trim(trailing from "route_routeprincipalsId") AS "route_routeprincipalsId","route"."created_by","routeId","routeName","routeDirection"
				from "route" 
				left join "route_localization" as "rl" on "rl"."route_routeId" = "routeId" 
				left join "routeassignment" as "ra" on "ra"."routeassignmentFirst_routeId" = "routeId" 
				left join "vehicle" as "v" on "ra"."routeassignment_vehicleId" = "v"."vehicleId" 
				group by "route_routeprincipalsId", "route"."created_by", "routeId", "routeName" 
				order by "routeName" asc');

			$res=$data;

			
		}else if((array_key_exists('typePlataform', $data)) && $data['typePlataform']=='movil'){
			//$order='ASC';
			
			$data= DB::select('select 
				count("routeId") as "numLocations",
				count(DISTINCT"routeassignment_vehicleId") as "numvehicle",
				trim(trailing from "route_routeprincipalsId") AS "route_routeprincipalsId","route"."created_by","routeId","routeName","routeDirection","routePrice"
				from "route" 
				left join "route_localization" as "rl" on "rl"."route_routeId" = "routeId" 
				Inner join "routeassignment" as "ra" on "ra"."routeassignmentFirst_routeId" = "routeId" and "ra"."routeassignmentActive"=1 or "ra"."routeassignmentLast_routeId" = "routeId" and "ra"."routeassignmentActive"=2 
				left join "vehicle" as "v" on "ra"."routeassignment_vehicleId" = "v"."vehicleId"
				where ("v"."vehicleCapacity"-"v"."vehicleOcupation") > 0 
				group by "route_routeprincipalsId", "route"."created_by", "routeId", "routeName" order by "routeName" asc');
			
			$res=$data;

			if(count($res) == 0){
				$res='no data';
			}

		}else if(array_key_exists('routeId', $data)){
			
			$res2=array();

			$routeId=$data['routeId'];
			
			$res= DB::table('route')->select(DB::raw('trim(trailing from "route_routeprincipalsId") AS "route_routeprincipalsId","route"."created_by","routeId","routeName","routeDirection","routeState","routePrice"'))
			  	->where('routeId','=',$routeId)
	           	->orderBy('routeId','asc')
		    	->get();
		    
		    foreach ($res as $k => $v) {
		    	
		    	$res2[$k]=$v;

		    		$localization_localizationId= DB::table('route')
		    		->select("localizationId", "localizationAddress","localizationLatitude", "localizationLongitude")
				  	->join('route_localization as rl', 'rl.route_routeId', '=', 'routeId')
				  	->join('localization as l', 'l.localizationId', '=', 'rl.localization_localizationId')
				  	->where('routeId','=',$v->routeId)
		           	->get();	

		           	$res2[$k]->localization=$localization_localizationId;
		

		    }
		    $res=array();

		    $res=$res2;
	    	
		}else if(array_key_exists('routeIdNot', $data)){
			
			$routeId=$data['routeIdNot'];
			
			$res= DB::table('route')->select("routeId","routeName")
			  	->whereNotIn('routeId',array($data['routeIdNot']))
			  	->whereNotIn('routeDirection',array($data['routeDirection']))
	           	->orderBy('routeName','asc')
		    	->get();	
	    	
		}else if(array_key_exists('dataSearch', $data)){
			
			$dataSearch="'%".$data['dataSearch']."%'";

			$case='CASE WHEN "routeState" = 1 THEN  \'Activo\'  ELSE  \'Inactivo\'  END AS "routeState2"';
        	$case2='CASE WHEN "routeDirection" = 0 THEN  \'Rumbo Universidad\'  ELSE  \'Desde universidad\'  END AS "routeDirection2"';
	    
	     $res=Routes::select(DB::raw('
				"route"."created_by","routeId","routeName","routeDirection","routeState",'.$case.','.$case2.',trim(trailing from "route_routeprincipalsId") AS "route_routeprincipalsId",
				"routePrice",count("routeId") as "numLocations",
				count(DISTINCT"routeassignment_vehicleId") as "numvehicle"'
			))
	    	->leftJoin('route_localization as  rl', 'rl.route_routeId', '=', "routeId")
	    	->leftJoin('routeassignment as ra', 'ra.routeassignmentFirst_routeId', '=',  "routeId")
	    	->leftJoin('vehicle as v', 'ra.routeassignment_vehicleId', '=', 'v.vehicleId')
	    	->where('routeName','ilike','%'.$data['dataSearch'].'%')
	    	->groupBy("route_routeprincipalsId", "route.created_by", "routeId", "routeName")
	    	->orderBy("routeName")
	       	->get();

		}

    	return $res;
	}
/**************************************************/

protected function listing($data)
    {
    	//routeState
        $orden  = $data['order']['0']['column'];
        $ordenby= $data['order']['0']['dir'];
        $search = $data['columns'][$orden]['data'];

        switch($search){
       		
       		case 'routeName':
       			$search='route.routeName';
       		break;
       		
       		case 'routeState':
       			$search='route.routeState';
       		break;
       		
       }
        
        $case='CASE WHEN "routeState" = 1 THEN  \'Activo\'  ELSE  \'Inactivo\'  END AS "routeState2"';
        $case2='CASE WHEN "routeDirection" = 0 THEN  \'Rumbo Universidad\'  ELSE  \'Desde universidad\'  END AS "routeDirection2"';
	    
	    $res=Routes::select(DB::raw('
				"route"."created_by","routeId","routeName","routeDirection","routeState",'.$case.','.$case2.',trim(trailing from "route_routeprincipalsId") AS "route_routeprincipalsId",
				"routePrice",count("routeId") as "numLocations",
				count(DISTINCT"routeassignment_vehicleId") as "numvehicle"'
			))
	    	->leftJoin('route_localization as  rl', 'rl.route_routeId', '=', "routeId")
	    	->leftJoin('routeassignment as ra', 'ra.routeassignmentFirst_routeId', '=',  "routeId")
	    	->leftJoin('vehicle as v', 'ra.routeassignment_vehicleId', '=', 'v.vehicleId')
	    	->groupBy("route_routeprincipalsId", "route.created_by", "routeId", "routeName")
	    	->skip($data['start'])->take($data['length'])
	       	->orderBy($search,$ordenby)
	       	->get();
        
        return $res;
    }

/**************************************************/

	protected function put($data)
    {
    	$res=Routes::where('routeId','=', $data['routeId'])->update($data);
    	
    	return $res; 
    }

/**************************************************/

    protected function validateRArelation($data,$two=false)
    {
        if($two ==  true){
	    	
	    	$data= DB::select('select "routeassignmentId" from "routeassignment" where  "routeassignmentId"!='.$data['routeassignmentId'].'  and "routeassignment_userId" ='.$data['routeassignment_userId'].' or "routeassignmentId"!='.$data['routeassignmentId'].'   and "routeassignment_vehicleId" = '.$data['routeassignment_vehicleId']);

	    	$res=count($data);
				
	          
	    }else{

    		$res = DB::table('routeassignment')->where('routeassignment_userId','=',$data['routeassignment_userId'])
    			->where('routeassignment_vehicleId','=',$data['routeassignment_vehicleId'])
    			->Where('routeassignmentFirst_routeId','=',$data['routeassignmentFirst_routeId'])
               	->count();
        }

       if($res > 0){
	    	return true;
	    }else{
	    	return $res;
	    }
    }

/**************************************************/

	protected function storeRArelation($data)
    {
	
    	$res=DB::table('routeassignment')->insert($data);
     
		return $res;
    }

/**************************************************/

	protected function destroyed($data){

		$res=$this->deleteRArelation($data);

		if($res){
			
			$res=DB::table('route')->where('routeId','=', $data['routeId'])
					->update(['routeState'=>0],['updated_by'=>Auth::user()->userId]);
		}

		return $res;
	}

/**************************************************/

protected function deleteRArelation($data){

		$data2=array();
		
		if(array_key_exists('routeassignmentId', $data)){
			$res = DB::table('routeassignment')
				->where('routeassignmentId','=',$data['routeassignmentId'])
				->delete();
		
		}else{
			$conteo = DB::table('routeassignment')
    			->where('routeassignmentLast_routeId','=',$data['routeId'])
    			->orWhere('routeassignmentFirst_routeId','=',$data['routeId'])
               	->count();

			if($conteo > 0){
				
				$res=DB::table('routeassignment')
				->where('routeassignmentLast_routeId','=',$data['routeId'])
				->orWhere('routeassignmentFirst_routeId','=',$data['routeId'])
				->delete();
			
			}else{
				$res=true;
			}
		}
		// var_dump($res);

		return $res;

	}

/**************************************************/

	protected function putAssignment($data)
	{
		$res=DB::table('routeassignment')->where('routeassignmentId','=', $data['routeassignmentId'])->update($data);

		return $res; 
   
    }


/**************************************************/

    protected function bringVbyroute($routeId){
		
    	$concat="'".url('/').'/'."'";

		$res= DB::select('
			select 5 as "rTime","routeassignmentId",
			trim(trailing from "route_routeprincipalsId") AS "route_routeprincipalsId","route"."created_by","routeId","routeName","routeDirection",
			"v"."vehicleId","v"."vehicleOcupation","v"."vehiclePlaque","v"."vehicle_locationId",
			--"u"."userImage",
			concat('.$concat.',"u"."userImage") as "userImage",
			--"l"."localizationAddress",
			"rl"."localization_localizationId" as "localization_localizationId_org",
			--"rl"."route_order",
			--"rl2"."route_order",
			"l2"."localizationAddress",
			"rl2"."localization_localizationId"
			from "route" 
			Inner join "routeassignment" as "ra" on "ra"."routeassignmentFirst_routeId" = "routeId" and "ra"."routeassignmentActive"=1 or "ra"."routeassignmentLast_routeId" = "routeId" and "ra"."routeassignmentActive"=2 
			inner join "user" as "u" on "u"."userId" = "ra"."routeassignment_userId" 
			left join "vehicle" as "v" on "ra"."routeassignment_vehicleId" = "v"."vehicleId"
			inner join "route_localization" as "rl" on "rl"."route_routeId" =  "routeId"  and "localization_localizationId" > "v"."vehicle_locationId"
			inner join "localization" as "l" on "l"."localizationId" = "v"."vehicle_locationId"

			left join "vehicle" as "v2" on "ra"."routeassignment_vehicleId" = "v2"."vehicleId" 
			inner join "route_localization" as "rl2" on "rl2"."route_routeId" =  "routeId" and cast("rl2"."route_order" as int) = (cast("rl"."route_order"as int) + 1)

			inner join "localization" as "l2" on "l2"."localizationId" = "rl2"."localization_localizationId"
			where "routeId" = '.$routeId.'  and "l2"."localizationAddress" != "l"."localizationAddress"
			order by "routeName" asc');

		$res = $this->organicetime($res);
	

		if(count($res) == 0){
			$res=false;
		}
	    
	    return $res;
	}

/**************************************************/

    protected function activateAssignment($data){

		$res = DB::table('routeassignment')->where('routeassignmentId',$data['routeassignmentId'])
            	->update(['routeassignmentActive' => $data['routeassignmentActive']]);

        return $res;
    }

/**************************************************/

    protected function organicetime($res,$localization_localizationId=false){

		$result=array();

		foreach($res as $k => $v){

			if($localization_localizationId == false){
				$result=Localization::bringlby2l($v->vehicleId,$v->localization_localizationId);

			}else if(is_array($localization_localizationId) && !empty($localization_localizationId)){

				$result[0]=(object) array(	'vehicleLatitude'=>$v->vehicleLatitude,
											'vehicleLongitude'=>$v->vehicleLongitude,
											'localizationLatitude'=>$localization_localizationId['localizationLatitude'],
											'localizationLongitude'=>$localization_localizationId['localizationLongitude']
										);
			}
			
			$rTime=Gmaps::get_time($result);

			

			$v->rTime=$rTime;
		}
		
        return $res;
    }

/**************************************************/

    protected function bringAssignment($routeId){

    	$username='concat("userFirstname",'."' '".',"userLastname") AS "userName"';
		$res= DB::table('routeassignment') 
			->join('vehicle', 'vehicleId', '=', 'routeassignment_vehicleId')
            ->join('user', 'userId', '=', 'routeassignment_userId')
            ->join('route', 'routeId', '=', 'routeassignmentLast_routeId')
            ->select(DB::raw('"routeassignment_vehicleId","routeassignment_userId","routeassignmentLast_routeId","routeassignmentFirst_routeId","routeName","vehiclePlaque",
            	'.$username.',"routeassignmentId","routeassignmentActive"'))
				->where('routeassignmentFirst_routeId','=',$routeId)
	           	->get();	
		
		return $res;
    }

	/**************************************************/

    protected function validatelocalization($localizationAddress,$localizationLatitude='',$localizationLongitude=''){

    	if(empty($localizationAddress)){

    		$res = Localization::select("localizationAddress")
    							->where('localizationLatitude', $localizationLatitude)
    							->where('localizationLongitude', $localizationLongitude)
    							->get();
    	
    	}else{

    		$res = Localization::where('localizationAddress', $localizationAddress)->count();


    	}

    	return $res;

    }

    /**************************************************/

    protected function validateAddress($data){

    	$res = Localization::where('localizationAddress', $data['localizationAddress'])
    					->whereNotIn('localizationId',array($data['localizationId']))
    					->count();

    	return $res;

    }
  	
  	/**************************************************/
    
    protected function localization_data($data,$order)
    {
       return Routes::select('localization.*')
       ->join('route_localization','route_localization.route_routeId','=','route.routeId')
       ->join('localization','localization.localizationId','=','route_localization.localization_localizationId')
       ->where('route.routeId','=',$data)
       ->orderBy('route_localization.route_order',$order);
    }

    /**************************************************/
    
    protected function beginRoute($data)
    {
    	$res=false;
    	
		$routeassignmentData= DB::table('routeassignment')->select(DB::raw('"routeassignment_vehicleId",
							CASE WHEN "routeassignmentFirst_routeId" = '.$data['routeId'].' THEN 1 ELSE ( CASE WHEN "routeassignmentLast_routeId" = '.$data['routeId'].' THEN 2  ELSE 0 END ) END  as "routeassignmentActive"'))
							->where('routeassignmentId', $data['routeassignmentId'])
							->first();
		

		if(!empty($routeassignmentData)){

			if($routeassignmentData->routeassignmentActive >0){
				$res=DB::table('routeassignment')->where('routeassignmentId','=', $data['routeassignmentId'])
					->update(	['routeassignmentActive'=>$routeassignmentData->routeassignmentActive],
								['updated_by'=>$data['userId']]
							);
				if($res){

					$res=DB::table('vehicle')->where('vehicleId','=', $routeassignmentData->routeassignment_vehicleId)
						->update(['vehicleLatitude'=>$data['vehicleLatitude'],
									'vehicleLongitude'=>$data['vehicleLongitude'],
									'vehicleBearing'=>$data['vehicleBearing'],
									'vehicle_locationId'=>$data['vehicle_locationId'],
									'updated_by'=>$data['userId']]
								);
				}

			
			}
		}

		return $res;
    }

    /**************************************************/

    protected function getAssignmentbyuser($routeassignment_userId){

    	$routeassignmentId = DB::table('routeassignment')->where('routeassignment_userId', $routeassignment_userId)->value('routeassignmentId');

		return $routeassignmentId;
    }

    /**************************************************/

    protected function findWheel($data){

    	$concat="'".url('/').'/'."'";

    	$res= DB::select(' select "userId","userFirstname","userLastname","userPhone","vehicleId","vehicleLatitude","vehicleLongitude","vehiclePlaque",("vehicleCapacity"-"vehicleOcupation") as 	"vehicleDifference","routewheelRoute","routewheel_routeprincipalsId","routeassignmentId",
    		concat('.$concat.',"userImage") as "userImage","routewheelId","routewheelDirection"
				from "routeassignment"
				inner join "routewheel" on "routeassignmentFirst_routeId"="routewheelId"
				inner join "vehicle" on "routeassignment_vehicleId"="vehicleId"
				inner join "user" on "routeassignment_userId"="userId"
				where "routeassignmentType" 
				=1
				and "routeassignmentActive"=1 
				and "routewheelDirection"='.$data['routewheelDirection'].'
				and (string_to_array("routewheel_routeprincipalsId",\',\')::int[]) && ARRAY['.$data['routewheel_routeprincipalsId'].']');

    	//$res = $this->organicetime($res,$data);
	

		if(count($res) == 0){
			$res=false;
		}
        

		return $res;
    }
}
