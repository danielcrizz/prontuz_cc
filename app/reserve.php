<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Appitventures\Phpgmaps\Phpgmaps;
use DB;
use App\Gmaps;
use App\Vehicle;
use App\User;
use App\Routes;
use App\recharge;
use App\FireBaseMessages;
use PushNotification;

class reserve extends Model
{ 
    
    protected $table='reserve';
    protected $primaryKey ='reserveId';
	protected $fillable =['reserve_UserId','reserve_vehicleId','reserveCode','reserve_route_localization','created_by','updated_by','reserveState','reserveId','reserveType'];
	/*0: cancelado; 1: Reservado; 2: confirmacion c; 3:Ocupacion; 4:Finalizado; 5:Pago saldado */
	protected function store($data)
	{
		

		return DB::transaction(function () use ($data) 
        {
        	//var_dump($data);

        	$data2=array();
        	$data2["reserve_UserId"]=$data["reserve_UserId"];
        	$data2["reserve_route_localization"]=$data["reserve_route_localization"];
        	$data2["reserve_vehicleId"]=$data["reserve_vehicleId"];
        	$data2["reserveState"]=$data["reserveState"];
        	$data2["reserveCode"]=$data["reserveCode"];
        	$data2["reservePayer_userId"]=$data["reservePayer_userId"];
        	$data2["reserveLatitude"]=floatval($data["reserveLatitude"]);
        	$data2["reserveLongitude"]=floatval($data["reserveLongitude"]);
        	$data2["reserveType"]=$data["reserveType"];

        	if($data["reserveType"] == 0){
        		$data2["reservePrice"] = DB::table('route')->where('routeId', $data['routeId'])->value('routePrice');
        		$data2["reserveUtility"] = 0;
        	}else{
        		$data2["reservePrice"] = DB::table('sysparameters')->where('sysparametersName', 'routewheelPrice')->value('sysparametersValue');
        		$data2["reserveUtility"] = DB::table('sysparameters')->where('sysparametersName', 'routewheelUtility')->value('sysparametersValue');
        	}
        	
        	$res=DB::table('reserve')->insert($data2);
		
			if($res){
				$reserveId = DB::table('reserve')->max('reserveId');

				$data2["reserveId"]=$reserveId;
			}

        	//$reserveId=reserve::fill($data)->create($data);

        	$data2=array();

            if($reserveId && Vehicle::where('vehicleId',$data['reserve_vehicleId'])->update(['vehicleOcupation'=>$data['vehicleOcupation']]))
            {
            	
            	$data2 = json_decode(json_encode((object) array(
           					'vehicleId' => $data['reserve_vehicleId'],
           					'localization_localizationId' => $data['localization_localizationId']
           					)));

           		$data3=array($data2);

           		if ($data['reserve_route_localization'] == 0){
    
           			$vehicle = Vehicle::select('vehicleLatitude','vehicleLongitude')->where('vehicleId',$data["reserve_vehicleId"])->get()->toArray();
           			$result = json_decode(json_encode((object) array('vehicleLatitude'=>$vehicle[0]['vehicleLatitude'],
						'vehicleLongitude'=>$vehicle[0]['vehicleLongitude'],
						'localizationLatitude'=>floatval($data["reserveLatitude"]),
						'localizationLongitude'=>floatval($data["reserveLongitude"])
					)));
					$result = array($result);
					$data['rTime'] = Gmaps::get_time($result);
           		}
           		else{
           			$res=Routes::organicetime($data3);
           			$data['rTime'] =$res[0]->rTime;
           		}
           		
				$data['reserveId']=$reserveId;

				if($data['reserveType'] ==1){
					$us=User::select('user.userId','user.userFirstname', 'user.userLastname', 'user.userPhone', 'user.userImage')->where('userId',$data['reserve_UserId'])->first();
					$user=$us->toArray();

					$user['reserveLatitude']=$data['reserveLatitude'];
					$user['reserveLongitude']=$data['reserveLongitude'];
	            	$user['notificationtype'] = '1';
	            	$user['userImage'] = url('/').'/'.$user['userImage'];
	            	$title='Estatus de Reserva';
	                $msn='Señor(a) conductor se ha creado una reserva';

	                $res=User::select('userId','userFirstname','userLastname', 'userEmail', 'password', 'userIdentificacion','remember_token', 'userPhone','userLicence','userBorndate','user_doctypeId','user_rolId','userState','userStreet','userImage','userToken')->join('routeassignment','user.userId', '=', 'routeassignment.routeassignment_userId')->where('routeassignmentId',$data['routeassignmentId'])->get();
	                $res1=$res->toArray();

	            	FireBaseMessages::sendToDriver($res1[0],$title,$msn,$user);

				}
				
            	return $data;
            }
        	else
        	{
        		return Messages::message(20000, $data);
        	}
        });
	}	
	protected function findReserveUserActive($data)
	{
		$reserveUser = DB::table('reserve')->select('*')->join('localization','localization.localizationId','=','reserve.reserve_route_localization')->where('reserve_UserId',$data['reserve_UserId'])->where('reserveState',1);
		if($reserveUser->count() > 0)
		{
			$resp=$reserveUser->get();
			return Messages::message(10000, $resp->toArray());
		}
		else
		{
			return Messages::message(20017);
		}
	}
	protected function listingReserveVehicle($data)
	{
		$reserveVehicle = DB::table('reserve')->select('reserve.*', 'user.userFirstname', 'user.userLastname','user.userToken','reservePayer_userId')->join('user','user.userId','=','reserve.reserve_UserId')->where('reserve_vehicleId',$data['vehicleId'])->where('reserveState',1)->where('reserve_route_localization',$data['reserve_route_localization'])->get();

		if($reserveVehicle->count() > 0)
		{
			$user=$reserveVehicle->toArray();
			$user=json_decode(json_encode($user), True);
			$title='Estatus de Reserva';
			$msn='Señor(a) usuario(a) el vehiculo esta llegando a la parada';
			for ($i=0; $i < $reserveVehicle->count(); $i++) 
			{ 
				FireBaseMessages::sendOne($user[$i],$title,$msn,array('isOk'=>1),1);
			}
			return Messages::message(10000, $user);
		}
		else
		{
			return Messages::message(20021);
		}
	}
/**************************************************/

	protected function clearReserveAll($data)
	{
		return DB::transaction(function () use ($data) 
        {
            if(Vehicle::where('vehicleId',$data['vehicleId'])->update(['vehicleOcupation'=>0,'vehicle_locationId'=>0]))
            {
            	$res=reserve::where('reserve_vehicleId',$data['vehicleId'])->whereBetween('reserveState',[2,3])->update(['reserveState'=>4]);
            	$res1=reserve::select('*')->join('user','user.userId', '=', 'reserve.reserve_UserId')->where('reserve_vehicleId',$data['vehicleId'])->where('reserveState',1);
            	if($res1->count()>0)
            	{
					$res1 = $res1->get();
					$res1 = $res1->toArray();
					reserve::where('reserve_vehicleId',$data['vehicleId'])->where('reserveState',1)->update(['reserveState'=>0]);
					$title='Estatus de Reserva';
	                $msn='Señor(a) usuario(a) la reserva fue cancelada por el conductor';
					for ($i=0; $i < count($res1); $i++) 
					{ 
		                FireBaseMessages::sendOne($res1[$i],$title,$msn,array('isCancel'=>1),1);
					}
            	}
            	$routeassignmentId = DB::table('routeassignment')
					->where('routeassignment_vehicleId', $data['vehicleId'])
					->pluck('routeassignmentId');

				if(!empty($routeassignmentId)){

					$res=DB::table('routeassignment')->where('routeassignmentId','=', $routeassignmentId)
							->update(['routeassignmentActive'=>0,
									'updated_by'=>$data['userId']]);
					
				}
            
            	return Messages::message(10000, $data);
            }
        	else
        	{
        		return Messages::message(20000, $data);
        	}
        });
	}	
	protected function closeReserveUser($data)
	{	
		return DB::transaction(function () use ($data) 
        {
        	$resp=DB::table('vehicle')->where('vehicleId',$data['vehicleId'])->get();
        	$resp1=$resp->toArray(); 
        	$total = (($resp1[0]->vehicleOcupation)-1);

        	$reserve=DB::table('reserve')->select('reserveType','reserve_UserId')->where('reserveId',$data['reserveId'])->first();

        	$res=DB::table('reserve')->where('reserveId',$data['reserveId'])->whereNotIn('reserveState',array(0))->where('reserve_vehicleId',$data['vehicleId'])->update(['reserveState'=>0]);
        	
        	$res2=DB::table('vehicle')->where('vehicleId',$data['vehicleId'])->update(['vehicleOcupation'=>$total]);

        	if($res && $res2)
            {
            	if($reserve->reserveType == 1){

            		$title='Estatus de Reserva';
	                $msn='Señor(a) conductor se ha cancelado una reserva';

	                $res=User::select('userId','userFirstname','userLastname', 'userEmail', 'password', 'userIdentificacion','remember_token', 'userPhone','userLicence','userBorndate','user_doctypeId','user_rolId','userState','userStreet','userImage','userToken')->join('routeassignment','routeassignment.routeassignment_userId','=','userId')->where('routeassignment_vehicleId',$data['vehicleId'])->get();
	                
	                $res1=$res->toArray();

	            	FireBaseMessages::sendToDriver($res1[0],$title,$msn,array('reserveId'=>$data['reserveId'], 'notificationtype' => '0', 'userId' => $reserve->reserve_UserId));
            	}
            	return Messages::message(10005);
            }
        	else
        	{
        		return Messages::message(20000);
        	}
        });
	}
	protected function historyReserveUser($data)
	{
		$reserveUser = reserve::where('reserve_UserId',$data['reserve_UserId']);
		if($reserveUser->count() > 0)
		{
			$resp=$reserveUser->get();
			return Messages::message(10000, $resp->toArray());
		}
		else
		{
			return Messages::message(20017);
		}
	}

	protected function confirmReserve($data)
	{
		$data2=array();
		return DB::transaction(function () use ($data) 
        {
        	if($data['user_rolId'] == 3)//
        	{
        		if($data['routeassignmentType'] == 0){

        			if(reserve::where('reserveCode',$data['reserveCode'])->where('reserve_route_localization',$data['reserve_route_localization'])->where('reserveState',1)->update(['reserveState'=>2]))
		            {
		            	if(array_key_exists('routeId', $data)){
		            		

		            		$reservePayer_userId=DB::table('reserve')->where('reserveCode',$data['reserveCode'])->where('reserveState',2)->value('reservePayer_userId');

		            		$data2=recharge::doDiscount($data['routeId'],$data['reserveCode'],$reservePayer_userId);
		            		
		            		$res=recharge::put($data2);

		            		#validar notificacion
		            		return Messages::message(10000,reserve::select('user.userFirstname', 'user.userLastname')->join('user','user.userId','=','reserve.reserve_UserId')->where('reserveCode',$data['reserveCode'])->where('reserve_route_localization',$data['reserve_route_localization'])->first());
		            	}
		            }
		        	else
		        	{
		        		return Messages::message(20000, $data);
		        	}
        		}else{
        			if(reserve::where('reserveCode',$data['reserveCode'])->where('reserveState',1)->update(['reserveState'=>2]))
		            {
		            	if(array_key_exists('routeId', $data)){

		            		$reservePayer_userId=DB::table('reserve')->where('reserveCode',$data['reserveCode'])->where('reserveState',2)->value('reservePayer_userId');
		            		
		            		$data2=recharge::doDiscountWheel($data['routeId'],$data['reserveCode'],$reservePayer_userId);
		            		
		            		$res=recharge::put($data2);

		            		#validar notificacion
		            		return Messages::message(10000,reserve::select('user.userFirstname', 'user.userLastname', 'user.userId')->join('user','user.userId','=','reserve.reserve_UserId')->where('reserveCode',$data['reserveCode'])->first());
		            	}
		            }
		        	else
		        	{
		        		return Messages::message(20000, $data);
		        	}
        		}
        		
        	}
        	if($data['user_rolId'] == 2)
        	{
        		if($data['routeassignmentType'] == 0){
        			if(reserve::where('reserveCode',$data['reserveCode'])->where('reserve_route_localization',$data['reserve_route_localization'])->where('reserveState',2)->count() > 0)
	        		{
			      		if(reserve::where('reserveCode',$data['reserveCode'])->where('reserve_route_localization',$data['reserve_route_localization'])->update(['reserveState'=>3]))
			            {
			            	return 1;
			            }
			        	else
			        	{
			        		return 0;
			        	}
	        		}
	        		//else if (reserve::where('reserveCode',$data['reserveCode'])->where('reserve_route_localization',$data['reserve_route_localization'])->where('reserveState',4)->count() > 0) 
	        		else if (reserve::where('reserveCode',$data['reserveCode'])->where('reserve_route_localization',$data['reserve_route_localization'])->whereIn('reserveState',array(3,4,5))->count() > 0) 
	        	
	        		{
		            	return 1;
		            }
		        	else
		        	{
		        		return 2;
		        	}
        		}else{
        			if(reserve::where('reserveCode',$data['reserveCode'])->where('reserveState',2)->count() > 0)
	        		{
			      		if(reserve::where('reserveCode',$data['reserveCode'])->update(['reserveState'=>4]))
			            {
			            	return 1;
			            }
			        	else
			        	{
			        		return 0;
			        	}
	        		}
	        		//else if (reserve::where('reserveCode',$data['reserveCode'])->where('reserveState',4)->count() > 0) 
	        		else if (reserve::where('reserveCode',$data['reserveCode'])->whereIn('reserveState',array(4,5))->count() > 0) 
	        		{
		            	return 1;
		            }
		        	else
		        	{
		        		return 2;
		        	}
        		}
        		
        	}
        });
	}
/**************************************************/

    protected function finishReserve($data){

   	$res = reserve::select('reserveId','reserveState', 'user.*')
   		->join('user', 'user.userId','=','reserve.reserve_UserId')
		->where('reserve.reserve_route_localization','=',$data['reserve_route_localization'])
		->Where('reserve.reserve_vehicleId','=',$data['reserve_vehicleId'])
		->Where('reserve.reserveState','=',1)
   		->get();

   		$vc=0;
	   	if($res->count() > 0){
	   		
	   		foreach ($res as $k => $v) {

   				$res=DB::table('reserve')
	   				->where('reserveId','=',$v['reserveId'])
	   				->update(['reserveState'=>0]);
	   			if($res){
					$vc++;
	   			}
                $title='Estatus de Reserva';
                $msn='Señor(a) usuario(a) la reserva fue cancelada por el conductor';
                FireBaseMessages::sendOne($v,$title,$msn,array('isCancel'=>1),1);

		   	}
		   	$vehicleOcupation = DB::table('vehicle')->where('vehicleId',$data['reserve_vehicleId'])->value('vehicleOcupation');
		   	$rest=intval($vehicleOcupation)-$vc;

		   	if($rest<0){
		   		$rest=0;
		   	}
		   	Vehicle::where('vehicleId',$data['reserve_vehicleId'])->update(['vehicleOcupation'=>$rest]);

	   	}else{
	   		$res=true;
	   	}

        return $res;
    }
    protected function alertVehicle($data)
    {
    	$res1=reserve::select('*')->join('user','user.userId', '=', 'reserve.reserve_UserId')->where('reserve_vehicleId',$data['vehicleId'])->where('reserveState',1)->get();
    	$res1=$res1->toArray();
    	for ($i=0; $i < count($res1); $i++) 
		{ 
			$title='Estatus del Vehiculo';
            $msn='El vehiculo ya se encuentra en la parada disponible para ser abordado';
            FireBaseMessages::sendOne($res1[$i],$title,$msn,array('isOk'=>1),1);
		}
    }
}
