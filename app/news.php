<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class news extends Model
{
    protected $table='news';
    protected $primaryKey ='newsId';
	protected $fillable =['newsTitle','newsContent','newsImage','newsState','created_by','updated_by'];

	/**************************************************/	
	
	protected function search($data)
	{
		$case='CASE WHEN "newsState" = 1 THEN  \'Activo\'  ELSE  \'Inactivo\'  END AS "newsState2"';
		$concat="'".url('/').'/'."'";
		$case2='CASE WHEN "newsImage" is null  or "newsImage" = \'\' THEN  \' \'  ELSE concat('.$concat.',"newsImage")  END AS "newsImage"';
		 

    	if(array_key_exists('newsId', $data)){

	    $res=DB::table('news')->select(DB::raw('
	    '.$case2.' ,"newsId","newsTitle","newsContent","newsState",'.$case
			))
	       ->Where('newsId','=',$data['newsId'])
	       ->get();

		}else if(array_key_exists('userId', $data)){

			$case='CASE WHEN "newsuserState" is null  THEN  1  ELSE "newsuserState"  END AS "newsState"';

	    	$res= DB::select('select '.$case2.',"newsId","newsTitle","newsContent",'.$case.',
               	CASE 
					WHEN EXTRACT(MONTH FROM "news"."created_at")=1 THEN 
					(EXTRACT(DAY FROM "news"."created_at")||\'-Ene-\'||EXTRACT(YEAR FROM "news"."created_at"))::VARCHAR
					WHEN EXTRACT(MONTH FROM "news"."created_at")=4 THEN
					(EXTRACT(DAY FROM "news"."created_at")||\'-Abr-\'||EXTRACT(YEAR FROM "news"."created_at"))::VARCHAR
					WHEN EXTRACT(MONTH FROM "news"."created_at")=8 THEN
					(EXTRACT(DAY FROM "news"."created_at")||\'-Ago-\'||EXTRACT(YEAR FROM "news"."created_at"))::VARCHAR
					WHEN EXTRACT(MONTH FROM "news"."created_at")=12 THEN
					(EXTRACT(DAY FROM "news"."created_at")||\'-Dic-\'||EXTRACT(YEAR FROM "news"."created_at"))::VARCHAR
					ELSE to_char("news"."created_at",\'dd-Mon-YYYY\')
				END
                from "news" 
                left join "newsuser"  on  "newsuser_newsId" = "newsId" and "newsuser_userId"='.$data['userId'].'
                where "news"."newsState" = 1
                order by "news"."created_at" asc');
		}else{
			$newsState=$data['newsState'];
		
			if($newsState==1){
				$order='ASC';
			}else{
				$order='DESC';
			}

				
	    	$res=DB::table('news')->select(DB::raw('
	    	'.$case2.',"newsId","newsTitle","newsContent","newsState",'.$case
			))
			->orWhere('newsTitle','ilike','%'.$data['dataSearch'].'%')
	       ->orderBy('newsState',$order)
	       ->get();
		}
    
    	return $res;
	}

	/**************************************************/	

    protected function listing($data)
    {

        $orden  = $data['order']['0']['column'];
        $ordenby= $data['order']['0']['dir'];
        $search = $data['columns'][$orden]['data'];

        switch($search){
       		
       		case 'newsState':
       			$search='news.newsState';
       		break;
       		
       		case 'newsTitle':
       			$search='news.newsTitle';
       		break;
       		
       }
        
       	$case='CASE WHEN "newsState" = 1 THEN  \'Activo\'  ELSE  \'Inactivo\'  END AS "newsState2"';
		$concat="'".url('/').'/'."'";
		$case2='CASE WHEN "newsImage" is null  or "newsImage" = \'\' THEN  \' \'  ELSE concat('.$concat.',"newsImage")  END AS "newsImage"';

	    $res=DB::table('news')->select(DB::raw(''.$case2.',"newsId","newsTitle","newsContent","newsState",'.$case
			))
	       //->Where('vehicle.vehicleState','=',1)
	       ->skip($data['start'])->take($data['length'])
	       ->orderBy($search,$ordenby)
	       ->get();
        
        return $res;
    }

/**************************************************/

	protected function store($data)
	{
		$res=DB::table('news')->insert($data);
		
		// if($res){
		// 	$res = DB::table('news')->max('newsId');
		// }
		
		return $res;
	}
    /**************************************************/

   	protected function put($data,$newsId)
    {
        return DB::table('news')->where('newsId', $newsId)->update($data);

    }
/**************************************************/
}
