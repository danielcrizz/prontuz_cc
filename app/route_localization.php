<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class route_localization extends Model
{
    protected $table='route_localization';
    protected $primaryKey = array('route_routeId', 'localization_localizationId');
    protected $fillable =['route_order','created_by','updated_by'];
}
