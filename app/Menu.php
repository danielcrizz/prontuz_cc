<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table='menu';
    protected $primaryKey ='menuId';
	protected $fillable =['menuName','menuPrefix','created_by','updated_by'];

	protected function listing_all()
	{
		return Menu::get();
	}
}
