<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolMenu extends Model
{
    protected $table = 'rolmenu';  
    protected $primaryKey ='rolmenu_rolId';
    protected $fillable = ['rolmenu_menuId','rolmenu_rolId', 'rolmenu_userId','created_by', 'updated_by'];
}
