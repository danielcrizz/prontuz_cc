<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Appitventures\Phpgmaps\Phpgmaps;

class Gmaps extends Model
{
    //
    protected function get_time($data)
    {
        $res=false;

        //print_r($data[0]);
        if(!empty($data)){

            //"vehicleLatitude","vehicleLongitude"
            $response = \GoogleMaps::load('distancematrix')
                ->setParam([
                   'origins'           => $data[0]->vehicleLatitude.','.$data[0]->vehicleLongitude,
                   "destinations"      => $data[0]->localizationLatitude.','.$data[0]->localizationLongitude
                          ])
                ->get(env('PUSHER_APP_KEY_MAP')); 
            if (array_key_exists('duration',$response['rows'][0]['elements'][0])) {
                $res=(($response['rows'][0]['elements'][0]['duration']['value'])/60); 
            }
            else{
                $res = 0;
            }
           
        }
                
        return $res;
    }	
    protected function getDirections($data,$orgin,$destination)
    {         
            $d =   \GoogleMaps::load('directions')
                    ->setParam([
                        'origin'          => $orgin,
                        'destination'     => $destination, 
                        'mode'            => 'via',
                        'waypoints'       => $data,                
                        'units'           => 'metric',
                        'region'          => 'CO',
                        'departure_time'  => 'now',
                    ])                   
                     ->getResponseByKey(env('PUSHER_APP_KEY_MAP'));
            if(count($d['routes'])>0)
            {
                return Messages::message(10000,$d['routes'][0]['overview_polyline']['points']);
            }
            else
            {
                return Messages::message(40021);
            }
    }  
    protected function getLocalization($lat='', $log='')
    {
        return \GoogleMaps::load('geocoding')
               ->setParamByKey('latlng', $lat.','.$log) 
               ->get('results.formatted_address'); 
    }
}