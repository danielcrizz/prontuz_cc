<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class routeassingment extends Model
{
    
    protected $table ='routeassignment';
    protected $primarykey ='routeassignmentId';
    protected $fillable = ['routeassignmentFirst_routeId','routeassignment_userId','routeassignment_vehicleId','routeassignmentLast_routeId','routeassignmentActive','created_by','updated_by'];

}
