<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocType extends Model
{
    protected $table='doctype';
    protected $primaryKey ='doctypeId';
	protected $fillable =['doctypeName','doctypeInitial','created_by','updated_by'];
}
