<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class offer extends Model
{
    protected $table='offer';
    protected $primaryKey ='offerId';
	protected $fillable =['offerTitle','offerContent','offerEmail','offerQuantity','offerValue','offerAvailable','offerImage','created_by','updated_by'];

	/**************************************************/	
	
	protected function search($data)
	{
		$case='CASE WHEN "offerState" = 1 THEN  \'Activo\'  ELSE  \'Inactivo\'  END AS "offerState2"';
		    $concat="'".url('/').'/'."'";

    	if(array_key_exists('offerId', $data)){

	    $res=DB::table('offer')->select(DB::raw('
	    	concat('.$concat.',"offerImage") as "offerImage","offerId","offerTitle","offerContent","offerCompany","offerEmail","offerValue","offerQuantity","offerAvailable","offerState",'.$case
			))
	       ->Where('offerId','=',$data['offerId'])
	       ->get();

		}else{
			$offerState=$data['offerState'];
		
			if($offerState==1){
				$order='ASC';
			}else{
				$order='DESC';
			}

				
	    	$res=DB::table('offer')->select(DB::raw('
	    	concat('.$concat.',"offerImage") as "offerImage","offerId","offerTitle","offerContent","offerCompany","offerEmail","offerValue","offerQuantity","offerAvailable","offerState",'.$case
			))
			->orWhere('offerTitle','ilike','%'.$data['dataSearch'].'%')
	       ->orderBy('offerState',$order)
	       ->get();
		}
    
    	return $res;
	}

	/**************************************************/	

    protected function listing($data)
    {

        $orden  = $data['order']['0']['column'];
        $ordenby= $data['order']['0']['dir'];
        $search = $data['columns'][$orden]['data'];

        switch($search){
       		
       		case 'offerState':
       			$search='offer.offerState';
       		break;
       		
       		case 'offerTitle':
       			$search='offer.offerTitle';
       		break;
       		
       }
        
        $case='CASE WHEN "offerState" = 1 THEN  \'Activo\'  ELSE  \'Inactivo\'  END AS "offerState2"';
	    $concat="'".url('/').'/'."'";

	    $res=DB::table('offer')->select(DB::raw('
	    	concat('.$concat.',"offerImage") as "offerImage","offerId","offerTitle","offerContent","offerEmail","offerValue","offerQuantity","offerAvailable","offerState",'.$case
			))
	       //->Where('vehicle.vehicleState','=',1)
	       ->skip($data['start'])->take($data['length'])
	       ->orderBy($search,$ordenby)
	       ->get();
        
        return $res;
    }

/**************************************************/

	protected function store($data)
	{
		$res=DB::table('offer')->insert($data);
		
		// if($res){
		// 	$res = DB::table('offer')->max('offerId');
		// }
		
		return $res;
	}
    /**************************************************/

   	protected function put($data,$offerId)
    {
        return DB::table('offer')->where('offerId', $offerId)->update($data);

    }
/**************************************************/
}
