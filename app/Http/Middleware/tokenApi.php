<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use App\Messages;
class tokenApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    protected $user;
    protected $messages;

        public function __construct(User $user,Messages $messages) {
            $this->user = $user;
            $this->messages = $messages;
        }
    public function handle($request, Closure $next,$ranks='')
    {
        $head=$request->header();        
        if(array_key_exists('token',$head))
        {
            $token = $head['token'][0];
            if($this->user->where('remember_token','=',$token)->count() > 0)
            {
                return $next($request);
            }
            else
            {
                $res = array(
                        'ResponseCode'      => 2,
                        'ResponseMessage'   => 'Por favor inicie sesion'
                        );
                return response()->json($res);
            }
        }
        else
        {
            $res = array(
                        'ResponseCode'      => 0,
                        'ResponseMessage'   => 'Error de autenticación Usuario no Autorizado'
                        );
            return response()->json($res);
        }
    }
}
