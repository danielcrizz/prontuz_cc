<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Redirect;
use Illuminate\Routing\UrlGenerator;
class menu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $url_comple = url()->current();
        $url = explode('/', $url_comple);
        $sesion = Session::get('userMenu');
        $a=0;
        for ($i=0; $i < count($sesion); $i++) 
        { 
            //if($url[5] === $sesion[$i]['menuPrefix'])
            if($url[3] === $sesion[$i]['menuPrefix'])
            {
                $a++;
            }            
        }
        if($a > 0)
        {
            return $next($request);            
        }
        else
        {
            return redirect()->to('404'); 
        }
    }
}
