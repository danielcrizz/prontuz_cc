<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use Redirect;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Messages;//messages
use Alexo\LaravelPayU\LaravelPayU;
use DB;

class PaymentsController extends Controller
{
    
    public function PayuPing(){

		$res=LaravelPayU::doPing(function($response) {
	        
	        //$code = $response->code;

	        //if($code == 'SUCCESS'){
	        	$res = Messages::message(60000,$response); 
	        //}
	      	return $res;
	    
	    }, function($error) {
	    
	    	//print_r($error);
	    	$res = Messages::message(60001,$error); 
	    	
	    	return $res;
	    });

    	return $res;
    
    }

/**********************************************/

    public function getPaymentMethods(){

		$res=LaravelPayU::getPaymentMethods(function($response) {
	        
	    
	        $res = Messages::message(10000,$response); 
	        
	      	return $res;
	    
	    }, function($error) {
	    
	    	//print_r($error);
	    	$res = Messages::message(10002,$error); 
	    	
	    	return $res;
	    });

    
    	return $res;
    
    }

    /**********************************************/

    public function getPSEBanks(){

		$res=LaravelPayU::getPSEBanks(function($response) {
	        
	        $res = Messages::message(10000,$response); 
	        
	      	return $res;
	    
	    }, function($error) {
	    
	    	//print_r($error);
	    	$res = Messages::message(10002,$error); 
	    	
	    	return $res;
	    });

    	return $res;
    
    }

    /**********************************************/

    static function CrediCardPayment($data){
    
 
    	$res=LaravelPayU::CrediCardPayment($data,function($response) {
	        
	        $res = $response; 
	        
	      	return $res;
	    
	    }, function($error) {
	    
	    	$res = array('error'=>$error); 
	    	
	    	return $res;
	    });

    	//print_r($res);
    	return $res;
    }

/**********************************************/

    public function getOrderDetail($data){

    	$parameters = $data;

		$res=LaravelPayU::getOrderDetail($parameters,function($response) {
	        
	    
	        $res = Messages::message(10000,$response); 
	        
	      	return $res;
	    
	    }, function($error) {
	    
	    	$res = Messages::message(10002,$error); 
	    	
	    	return $res;
	    });

    
    	return $res;
    
    }

/**********************************************/

    static function getDetailByReferenceCode($data){

    	$res=LaravelPayU::getDetailByReferenceCode($data,function($response) {
	        
	    
	        $res = Messages::message(10000,$response); 
	        
	      	return $res;
	    
	    }, function($error) {
	    
	    	$res = array('error'=>$error); 
	    	
	    	return $res;
	    });

    
    	return $res;
    
    }

/**********************************************/
	  
	 public function getTransactionResponse(Request $request){

	 	$data=$request->input();

    	$parameters = $data['transactionId'];

		$res=LaravelPayU::getTransactionResponse($parameters,function($response) {
	        
	    
	        $res = Messages::message(10000,$response); 
	        
	      	return $res;
	    
	    }, function($error) {
	    
	    	$res = Messages::message(10002,$error); 
	    	
	    	return $res;
	    });

    	
    	return $res;
    
    }
    
/**********************************************/
    
}
