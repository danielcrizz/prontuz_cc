<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use Redirect;
use Storage;
use App\news;
use App\Messages;//messages
use Cache;
use Carbon\Carbon;
use DB;


class NewsController extends Controller
{
    //

public function search(Request $request)
{
	if(!empty($request->input())){

		$data = $request->input();

		$data = news::search($data);

	}else{
	
		$data = DB::table('news')->select('*')->where('newsState',1)->get();

        if(empty($data)){
            
            return Messages::message(90003);
        }

	}

	$res = Messages::message(10000,$data);



	return $res;

    }

/**************************************************/

    public function store(Request $request)
    {
        $res=false;
        $newsValue2='';
        $newsImage='';
        $file=$request->file('newsImage');
    
        if(!empty($file)){
            $newsImage=Storage::disk('local')->put('tmp_files',$file);
        }
        
       
        $data=array(
            'newsTitle'        => $request->input('newsTitle'),
            'newsContent'      => $request->input('newsContent'),
            'newsImage'        => $newsImage,
            'newsState'        => $request->input('newsState'),
            'created_by'        => Auth::user()->userId,
            'created_at'        => date('Y-m-d H:i:s')
         );

         //return $data;
        $res=news::store($data);

        if($res){

            $res = Messages::message(10000,$res);
        }else{
            $res = Messages::message(90001);
        }

     return $res;
   
    }


/**************************************************/

public function listing(Request $request)
{
    $resp= array();
    $response= array();
    $response2= array();


    
    if($request['search']['value']!='' || $request['search']['value']!=null)
    {

        $resp=news::search(array(
                        'dataSearch'=>$request['search']['value'],
                        'newsState'=>1,
            )
        );
    
        $total=count($resp);
    }
    else
    {
        $resp=news::listing($request->input());
        $total=DB::table('news')->count();
 
    }

    foreach ($resp->toArray() as $k => $v) {
      
       foreach ($v as $k2 => $v2) {
            
            if($k2 == 'newsId'){
            
                $response[$k]['newsId']=$v->newsId;
            
            }if($k2 == 'newsImage'){
            
                $response[$k]['newsImage']=$v->newsImage;
            
            }if($k2 == 'newsState2'){
            
                $response[$k]['newsState2']=$v->newsState2;
            
            }else if($k2 == 'newsState'){
            
                $response[$k]['newsState']=$v->newsState;
            
            }else{
               
                switch ($k2) {
                    case 'newsTitle':
                        $response[$k]['news']='<div style="color:#000;font-size: 1.2rem;">'.$v2.'</div>';
                    break;
                    case 'newsContent':
                        $response[$k]['news']=$response[$k]['news'].substr($v2, 0, 80).'...';
                    break;
               }
            }
       }
 
    }   

    $response2['recordsTotal'] = count($resp);
    $response2['recordsFiltered'] = $total;
    $response2['data'] = $response;
       
    return $response2;
}

/**************************************************/
    
public function put(Request $request)
    {
        date_default_timezone_set('America/Bogota');
        $date=date('Y-m-d H:i:s');
        $res=false;

        if(!empty($request->file('newsImageEdit'))){

            $file=$request->file('newsImageEdit');
            $res=Storage::disk('local')->put('avatar',$file);

            $data=array(
                'newsTitle'        => $request->input('newsTitleEdit'),
                'newsContent'      => $request->input('newsContentEdit'),
                'newsImage'        => $res,
                'newsState'        => $request->input('newsStateEdit'),
                'updated_by'        =>  Auth::user()->userId,
                'updated_at'         =>  $date
            );
            

        }else{
            $data=array(
                'newsTitle'        => $request->input('newsTitleEdit'),
                'newsContent'      => $request->input('newsContentEdit'),
                'newsState'        => $request->input('newsStateEdit'),
                'updated_by'        =>  Auth::user()->userId,
                'updated_at'        =>  $date
            );
        }

            
        $res=news::put($data,$request->input('newsIdEdit'));


        if($res){
            return Messages::message(10000);
        }else{
            return Messages::message(20000);     
        }
        
    }  

/**************************************************/

     public function changeSeen(Request $request){

        date_default_timezone_set('America/Bogota');
        $date=date('Y-m-d H:i:s');

        
        $total=DB::table('newsuser')->where('newsuser_newsId', $request->input('newsId'))->where('newsuser_userId', $request->input('userId'))->count();
            
        if($total >= 1){
            $data=array(
                'newsuserState'       => 2,
                'newsuser_userId'      =>  $request->input('userId'),
                'newsuser_newsId'      =>  $request->input('newsId'),
                'updated_by'      =>  $request->input('userId'),
                'updated_at'      =>  $date
            );
            $res=DB::table('newsuser')->where('newsuser_newsId', $request->input('newsId'))->where('newsuser_userId', $request->input('userId'))->update($data);
        }else{
            $data=array(
                'newsuserState'       => 2,
                'newsuser_userId'      =>  $request->input('userId'),
                'newsuser_newsId'      =>  $request->input('newsId'),
                'created_by'      =>  $request->input('userId'),
                'created_at'      =>  $date
            );

            $res=DB::table('newsuser')->insert($data);
        }
        

        if($res){
            return Messages::message(10000);
        }else{
            return Messages::message(20000);     
        }

    }

}

