<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Messages;
class menuController extends Controller
{
    public function listing_all(Request $request)
    {
    	$menu = Menu::listing_all();
    	if(count($menu->toArray())>0)
    	{
    		return $menu->toArray();
    	}
    	else
    	{
    		return Messages::message(20015);
    	}
    }
}
