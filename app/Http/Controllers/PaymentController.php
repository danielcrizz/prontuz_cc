<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Messages;//messages
use App\Routes;
use App\Localization;
use App\route_localization;
use App\payment;
use App\User;
use Auth;
use DB;

class PaymentController extends Controller
{
    
    public function makeDiscount(Request $request){

      if(array_key_exists('paymentType', $request->input()) && array_key_exists('paymentValue', $request->input())){

      }

    
    }

/**************************************************/

    public function getRusers(){

        return DB::table('user')->where('user_rolId',4)->count();
    }

/**************************************************/

    public function getWheelBalance(Request $request){

        
        if(array_key_exists('userId', $request->input())){

            $res = payment::getWheelBalance($request->input());

        }else{
            $res = Messages::message(10001);

        }

        return $res;
    }

    /**************************************************/

    public function getWheelTrips(Request $request){

        if(array_key_exists('userId', $request->input())){

            $res = payment::getWheelTrips($request->input());

        }else{
            $res = Messages::message(10001);

        }

        return $res;
    }

/**************************************************/

    public function statistics(){

        $data=array();

        $query='select
                sum(CAST("r1"."reservePrice" AS INTEGER)) as "Income", 
                sum(CAST("r1"."reserveUtility" AS INTEGER)) as "Utility"
                from "user" 
                inner join "routeassignment" on "routeassignment_userId"="userId" 
                inner join "vehicle" on "routeassignment_vehicleId"="vehicleId" 
                inner join "reserve" as "r1" on "vehicleId"="r1"."reserve_vehicleId"
                WHERE "r1"."reserveState" >=4';
        $res= DB::select($query);

        $query2='select
                sum(CAST("r1"."reservePrice" AS INTEGER) - CAST("r1"."reserveUtility" AS INTEGER)) AS "Balance" 
                from "user" 
                inner join "routeassignment" on "routeassignment_userId"="userId" 
                inner join "vehicle" on "routeassignment_vehicleId"="vehicleId" 
                inner join "reserve" as "r1" on "vehicleId"="r1"."reserve_vehicleId"
                WHERE "r1"."reserveState" =4';

        $res2= DB::select($query2);

        $data=array(
            "Income"=>$res[0]->Income,
            "Utility"=>$res[0]->Utility,
            "Balance"=>(empty($res2[0]->Balance))? 0:$res2[0]->Balance
        );

        return $data;
    }

/**************************************************/


    public function getWheelinfo(){

        $query='select distinct("userId"), concat("userFirstname",\' \',"userLastname") AS "FullName","vehiclePlaque",
            sum(CAST("r1"."reservePrice" AS INTEGER)) as "Income", 
            sum(CAST("r1"."reserveUtility" AS INTEGER)) as "Utility"
            from "user" 
            inner join "routeassignment" on "routeassignment_userId"="userId" 
            inner join "vehicle" on "routeassignment_vehicleId"="vehicleId" 
            inner join "reserve" as "r1" on "vehicleId"="r1"."reserve_vehicleId"
            WHERE "r1"."reserveState" >=4
            group by "userId",concat("userFirstname",\' \',"userLastname") ,"vehiclePlaque"';

        $res= DB::select($query);

        foreach($res as $k => $v){

            $query1='select distinct("userId"),
                sum(CAST("r1"."reservePrice" AS INTEGER) - CAST("r1"."reserveUtility" AS INTEGER)) AS "Balance" 
                from "user" 
                inner join "routeassignment" on "routeassignment_userId"="userId" 
                inner join "vehicle" on "routeassignment_vehicleId"="vehicleId" 
                inner join "reserve" as "r1" on "vehicleId"="r1"."reserve_vehicleId" 
                WHERE "r1"."reserveState" =4
                and "userId"='.$v->userId.'
                group by "userId"';

            $res1= DB::select($query1);

            if(empty($res1[0])){
                $res[$k]->Balance=0;
            }else{
                $res[$k]->Balance=$res1[0]->Balance;
            } 

            $query2='select distinct("uw"."userId"), concat("ur"."userFirstname",\' \',"ur"."userLastname") AS "UserFullName",
            to_char("r1"."updated_at", \'DD Mon YYYY\') as "date"
            from "user" as "uw"
            inner join "routeassignment" on "routeassignment_userId"="userId"
            inner join "vehicle" on "routeassignment_vehicleId"="vehicleId"
            inner join "reserve" as "r1" on "vehicleId"="r1"."reserve_vehicleId"
            inner join "user" as "ur" on "ur"."userId" = "r1"."reserve_UserId"
            WHERE "r1"."reserveState" = 4
            and "uw"."userId"='.$v->userId.'
            group by "uw"."userId",concat("ur"."userFirstname",\' \',"ur"."userLastname") ,to_char("r1"."updated_at", \'DD Mon YYYY\')
            order by to_char("r1"."updated_at", \'DD Mon YYYY\') ASC';
            //(object) 
            $res2= DB::select($query2);    

            $res[$k]->data=$res2;           
        }

        return $res;
    
    }

    /**************************************************/


    public function getWheelHistory(){

        $query='select distinct("userId"), concat("userFirstname",\' \',"userLastname") AS "FullName","vehiclePlaque",
            sum(CAST("r1"."reservePrice" AS INTEGER)) as "Income", 
            sum(CAST("r1"."reserveUtility" AS INTEGER)) as "Utility"
            from "user" 
            inner join "routeassignment" on "routeassignment_userId"="userId" 
            inner join "vehicle" on "routeassignment_vehicleId"="vehicleId" 
            inner join "reserve" as "r1" on "vehicleId"="r1"."reserve_vehicleId"
            WHERE "r1"."reserveState" >=4
            group by "userId",concat("userFirstname",\' \',"userLastname") ,"vehiclePlaque"';

        $res= DB::select($query);

        foreach($res as $k => $v){

            $query1='select distinct("userId"),
                sum(CAST("r1"."reservePrice" AS INTEGER) - CAST("r1"."reserveUtility" AS INTEGER)) AS "Balance" 
                from "user" 
                inner join "routeassignment" on "routeassignment_userId"="userId" 
                inner join "vehicle" on "routeassignment_vehicleId"="vehicleId" 
                inner join "reserve" as "r1" on "vehicleId"="r1"."reserve_vehicleId" 
                WHERE "r1"."reserveState" =4
                and "userId"='.$v->userId.'
                group by "userId"';

            $res1= DB::select($query1);

            if(empty($res1[0])){
                $res[$k]->Balance=0;
            }else{
                $res[$k]->Balance=$res1[0]->Balance;
            } 

            $query2='select distinct("uw"."userId"), concat("ur"."userFirstname",\' \',"ur"."userLastname") AS "UserFullName",
            to_char("r1"."updated_at", \'DD Mon YYYY\') as "date"
            from "user" as "uw"
            inner join "routeassignment" on "routeassignment_userId"="userId"
            inner join "vehicle" on "routeassignment_vehicleId"="vehicleId"
            inner join "reserve" as "r1" on "vehicleId"="r1"."reserve_vehicleId"
            inner join "user" as "ur" on "ur"."userId" = "r1"."reserve_UserId"
            WHERE "r1"."reserveState" = 4
            and "uw"."userId"='.$v->userId.'
            group by "uw"."userId",concat("ur"."userFirstname",\' \',"ur"."userLastname") ,to_char("r1"."updated_at", \'DD Mon YYYY\')
            order by to_char("r1"."updated_at", \'DD Mon YYYY\') ASC';
            //(object) 
            $res2= DB::select($query2);    

            $res[$k]->data=$res2;           
        }


        return $res;
    
    }

/**************************************************/

    public  function store(Request $request){
        

        $data=array(
            'paymentPay'                => $request->input('paymentPay'),
            'payment_userId'            => Auth::user()->userId,
            'payment_wheel_userId'      => $request->input('payment_wheel_userId'),
            'created_by'                => Auth::user()->userId,
            'created_at'                => date('Y-m-d H:i:s')
        );

        $res=payment::store($data);


        if($res){
            
            $res = Messages::message(10000,$res);
        }else{
            $res = Messages::message(20000);
        }

        return $res; 
        
        
    }

/**************************************************/

  static function tokenPayment(){

    date_default_timezone_set('America/Bogota');

    $payment = new PaymentController();

    $part1 = rand(1,1000);
    $part2 = date('ymdHis');


    $part1=$payment::zerosPayment($part1,4);

      $code='ProntuzU_'.$part1.'_'.$part2;


      return $code;
    
    }

/**************************************************/

  static function zerosPayment($part1,$c){

    $size=intval(strlen($part1));

    if( $size < $c){
      $rest=$c-$size;

      for($i=0;$i < $rest;$i++){
        $part1='0'.$part1;
      }
        
    }

    return $part1;
  }
/**************************************************/


    public function listing(Request $request)
    {
        $resp= array();
        $response= array();
        $response2= array();
        
        if($request['search']['value']!='' || $request['search']['value']!=null)
        {

            $resp=payment::listing(array('dataSearch'=>$request['search']['value'],'datasearch'=>$request->input('datasearch')));
            $total=count($resp);
        }
        else
        {
            $resp=payment::listing($request->input());
            $total=count($resp);
        }

        if(!is_array($resp)){
            $response=$resp->toArray();
        }else{
            $response=$resp;
        }
    
        $response2['recordsTotal'] = count($response);
        $response2['recordsFiltered'] = $total;
        $response2['data'] = $response;
           
        return $response2;

    }
 /**************************************************/
    
  









}
