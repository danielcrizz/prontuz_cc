<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Messages;//messages
use App\transaction;//messages
use App\recharge;
use App\User;
use Auth;
use DB;
use App\FireBaseMessages;
use PushNotification;
use Mail;
use App\Mail\WelcomeMail;

class RechargeController extends Controller
{

    public function getRusers(){

        return DB::table('user')->count();
    }

/**************************************************/

    public function searchRuser(Request $request)
    {
        // if(array_key_exists('recharge_userId', $request->input())){
        //     $res=user::searchRuser($request->input('recharge_userId'));
            
        //     if(count($res)>0){
        //         return $res;
        //     }
        // }
        
        return $res;
    }
/**************************************************/

    public function getRcValue(Request $request){

        $res2=array();
        if(array_key_exists('recharge_userId', $request->input())){

            $data=recharge::getRcValue($request->input('recharge_userId'));

            $data2=$data->toArray();
            if(empty($data2)){
               
                
                $data=array();
                $data[0]['rechargeValue']=0;
                $data[0]['rechargeLoan']=0;
                $data[0]['recharge_userId']=$request->input('recharge_userId');
                $data[0]['created_at']='0000-00-00 00:00:00';
                $data[0]['updated_at']='0000-00-00 00:00:00';
                
            }

           $res = Messages::message(10000,$data); 

        }else{

            $res = Messages::message(10002); 
        }
        
        return $res;
    }
/**************************************************/

    public function getIduser(Request $request){
        
        $data=$request->input();

        $res = DB::table('user')->where('userIdentificacion', $data['userIdentificacion'])->where('user_rolId',2)->value('userId');  

        if(empty($res)){
            $res = Messages::message(10002); 
        }

        return $res;
           
    }

/**************************************************/
    
    public function store(Request $request/*  o  $data*/){ #Crear recarga y ajustar deuda



        date_default_timezone_set('America/Bogota');

        $crediCard = array("VISA", "MASTERCARD", "AMEX", "DINERS");
        $payment = new PaymentController();
        $PayU = new PaymentsController();
        $data=$request->input();
        $data2=array();
        $data3=array();
        $res = Messages::message(10002);

      

        if(array_key_exists('method', $data) && in_array($data['method'], $crediCard)) {

            if(array_key_exists('CREDIT_CARD_NUMBER', $data) && array_key_exists('CREDIT_CARD_EXPIRATION_DATE', $data) && array_key_exists('CREDIT_CARD_SECURITY_CODE', $data) && array_key_exists('recharge_userId', $data) && array_key_exists('rechargeValue', $data)
                && array_key_exists('PAYER_NAME', $data) && array_key_exists('PAYER_EMAIL', $data) && array_key_exists('PAYER_CONTACT_PHONE', $data) && array_key_exists('PAYER_DNI', $data) && array_key_exists('transactionTypeAd', $data) && array_key_exists('offerValue', $data)  && array_key_exists('offerId', $data)){

                $user = DB::table('user')->where('userId', $data['recharge_userId'])->first();
                $reference = $payment::tokenPayment();

                if(!empty($user)){

                    if($data['transactionTypeAd'] == 1){
                        $payValue=$data['rechargeValue'];
                    }else{
                        $payValue=$data['offerValue'];
                    }

                    $parameters = array(
                        //Ingrese aquí el identificador de la cuenta.
                        'ACCOUNT_ID' => env('PAYU_ACCOUNT_ID'),
                        //Ingrese aquí el código de referencia.
                        'REFERENCE_CODE' => $reference,
                        //Ingrese aquí la descripción.
                        'DESCRIPTION' => "payment test",
                        
                        // -- Valores --
                        //Ingrese aquí el valor.        
                        'VALUE' => $payValue,
                        //Ingrese aquí la moneda.
                        'CURRENCY' => env('PAYU_DFL_CURRENCY'),
                        
                        // -- Comprador 
                        //Ingrese aquí el nombre del comprador.
                        'BUYER_NAME' => $user->userFirstname.' '.$user->userLastname,
                        //Ingrese aquí el email del comprador.
                        'BUYER_EMAIL' => $user->userEmail,
                        //Ingrese aquí el teléfono de contacto del comprador.
                        'BUYER_CONTACT_PHONE' => $user->userPhone,
                        //Ingrese aquí el documento de contacto del comprador.
                        'BUYER_DNI' => $user->userIdentificacion,
                        //Ingrese aquí la dirección del comprador.
                        'BUYER_STREET' => $user->userStreet, #PENDIENTE EN AGREGAR EN LA ADMINISTRACION
                        'BUYER_STREET_2' => "0000",
                        'BUYER_CITY' => env('PAYU_DFL_CITY'),
                        'BUYER_STATE' => env('PAYU_DFL_STATE'),
                        'BUYER_COUNTRY' => env('PAYU_DFL_COUNTRY'),
                        'BUYER_POSTAL_CODE' => env('PAYU_DFL_POSTAL'),
                        'BUYER_PHONE' => $user->userPhone,
                        
                        // -- pagador --
                        //Ingrese aquí el nombre del pagador.
                        'PAYER_NAME' => $data['PAYER_NAME'],//"APPROVED"
                        //Ingrese aquí el email del pagador.
                        'PAYER_EMAIL' => $data['PAYER_EMAIL'],// (REQUERIR)
                        //Ingrese aquí el teléfono de contacto del pagador.
                        'PAYER_CONTACT_PHONE' => $data['PAYER_CONTACT_PHONE'],// (REQUERIR)
                        //Ingrese aquí el documento de contacto del pagador.
                        'PAYER_DNI' => $data['PAYER_DNI'],// (REQUERIR)
                        //Ingrese aquí la dirección del pagador.
                        'PAYER_STREET' => $user->userStreet,
                        'PAYER_STREET_2' => "125544",
                        'PAYER_CITY' => env('PAYU_DFL_CITY'),
                        'PAYER_STATE' => env('PAYU_DFL_STATE'),
                        'PAYER_COUNTRY' => env('PAYU_DFL_COUNTRY'),
                        'PAYER_POSTAL_CODE' => env('PAYU_DFL_POSTAL'),
                        'PAYER_PHONE' => $user->userPhone,
                        
                        // -- Datos de la tarjeta de crédito -- 
                        //Ingrese aquí el número de la tarjeta de crédito
                        'CREDIT_CARD_NUMBER' => $data['CREDIT_CARD_NUMBER'],
                        //Ingrese aquí la fecha de vencimiento de la tarjeta de crédito
                        'CREDIT_CARD_EXPIRATION_DATE' => $data['CREDIT_CARD_EXPIRATION_DATE'],
                        //Ingrese aquí el código de seguridad de la tarjeta de crédito
                        'CREDIT_CARD_SECURITY_CODE'=> $data['CREDIT_CARD_SECURITY_CODE'],
                        //Ingrese aquí el nombre de la tarjeta de crédito
                        //VISA||MASTERCARD||AMEX||DINERS
                        'PAYMENT_METHOD' => $data['method'],
                        
                        //Ingrese aquí el número de cuotas.
                        'INSTALLMENTS_NUMBER' => "1",
                        //Ingrese aquí el nombre del pais.
                        'COUNTRY' => env('PAYU_DFL_COUNTRY'),
                        
                        //Session id del device.
                        'DEVICE_SESSION_ID' => "vghs6tvkcle931686k1900o6e1",
                        //IP del pagadador
                        'IP_ADDRESS' => "127.0.0.1",
                        //Cookie de la sesión actual.
                        'PAYER_COOKIE'=>"pt1t38347bs6jc9ruv2ecpv7o2", //token

                        'TAX_RETURN_BASE'=>0, //token
                        
                        'TAX_VALUE'=>0, //token
                        //Cookie de la sesión actual.        
                        'USER_AGENT'=>"Mozilla/5.0 (Windows NT 5.1; rv:18.0) Gecko/20100101 Firefox/18.0"
                    );
                   
                    $res=$PayU::CrediCardPayment($parameters);

                    if(!array_key_exists('error', $res)){

                        $payerId=$this->savePayer($data);
                        
                        $data2['transactionType']=0;#crediCard
                        $data2['transactionrReferencecode']=$reference;
                        $data2['transactionOrderid']=$res['transactionResponse']->orderId;
                        $data2['transactionIdentification']=$res['transactionResponse']->transactionId;
                        $data2['transactionCategory']=$data['transactionTypeAd'];
                        $data2['transactionValue']=$payValue;
                        $operationDate=date('Y-m-d H:i:s');

                        if($res['transactionResponse']->state == 'DECLINED'){ //REJECTED
                            $state=0;
                        }
                        if($res['transactionResponse']->state == 'APPROVED'){
                            $state=1;
                            $operationDate=$res['transactionResponse']->operationDate;

                        }
                        if($res['transactionResponse']->state == 'PENDING'){
                            $state=2;
                        }
                        $state2=$res['transactionResponse']->state;

                        $data2['transactionState']=$state;
                        $data2['transactionOperationdate']=$operationDate;
                        $data2['transaction_userId']=$data['recharge_userId'];
                        $data2['transaction_payerId']=$payerId;
                        $data2['created_by']=$data['recharge_userId'];
                        $data2['created_at']=date('Y-m-d H:i:s');

                        $res=transaction::store($data2);

                        if($data['transactionTypeAd'] == 1){

                            $recharge = DB::table('recharge')->where('recharge_userId','=',$data['recharge_userId'])->count();

                            if($state == 1){
                                
                                if(empty($recharge) || $recharge == 0){

                                    $data3=$this->validateRecharge($data['recharge_userId'],$data['rechargeValue'],$state);

                                    $res=recharge::store($data3);

                                }else{
                                
                                    $data3=$this->validateRecharge($data['recharge_userId'],$data['rechargeValue'],$state);
                                
                                    $res=recharge::put($data3);
                                }

                                $obj = (object) array('transactionState' => $state);

                                if($res){
                                    $res = Messages::message(70003,$obj);
                                }else{
                                    $res = Messages::message(70008);
                                }

                            }else if($state == 0){

                                $obj = (object) array('transactionState' => $state);
                            
                               $res=Messages::message(70004,$obj); 
                            
                            }else if($state == 2){

                                $obj = (object) array('transactionState' => $state);
                            
                               $res=Messages::message(70002,$obj); 
                            
                            }
                        }else{

                            $obj = (object) array('transactionState' => $state);
                            
                            if($state == 1){
                            
                                if($res){
                                    $res = Messages::message(70003,$obj);
                                }else{
                                    $res = Messages::message(70008);
                                }

                            }else if($state == 0){
   
                               $res=Messages::message(70004,$obj); 
                            
                            }else if($state == 2){
                            
                               $res=Messages::message(70002,$obj); 
                            
                            }

                            $resu=User::select('userId','userFirstname','userLastname', 'userEmail', 'password', 'userIdentificacion','remember_token', 'userPhone','userLicence','userBorndate','user_doctypeId','user_rolId','userState','userStreet','userImage','userToken')->where('userId',$data['recharge_userId'])->get();
                            $resu1=$resu->toArray();
                            $user=$resu1[0];


                            $concat="'".url('/')."/'";
    
                            $offer=DB::table('offer')->select(DB::raw('
                                concat('.$concat.',"offerImage") as "offerImage","offerId","offerTitle","offerContent","offerCompany","offerEmail","offerValue","offerQuantity","offerAvailable","offerState"'))
                                ->Where('offerId',$data['offerId'])
                                ->first();

                            $data2=  array(
                                        'state'=>$state,
                                        'offerValue'=>number_format($offer->offerValue, 0, '.', '.'),
                                        'offerImage'=>$offer->offerImage,
                                        'offerContent'=>$offer->offerContent,
                                        'offerCompany'=>$offer->offerCompany,
                                        'offerTitle'=>$offer->offerTitle,
                                        'offerEmail'=>$offer->offerEmail,
                                        'userEmail'=>$user['userEmail'],
                                        'userFirstname'=>$user['userFirstname'],
                                        'userLastname'=>$user['userLastname']
                                    );



                            Mail::send('mail.buyEmail', ['data' => $data2], function ($m) use ($data2) {
                                $m->from(env('MAIL_USERNAME'), 'Prontuz');

                                $m->to($data2['userEmail'], $data2['userFirstname'].' '.$data2['userLastname'])->subject('Bienvenido!');
                            });

                            Mail::send('mail.buyEmail2', ['data' => $data2], function ($m) use ($data2) {
                                $m->from(env('MAIL_USERNAME'), 'Prontuz');

                                $m->to($data2['offerEmail'], $data2['offerCompany'])->subject('Bienvenido!');
                            });

                        }

                    }else{

                        $res=Messages::message(10002,$res['error']);
                    }
                }
            }
        }else if((array_key_exists('method', $data)) &&  ($data['method']  == 'cashPay') && array_key_exists('recharge_userId', $data) && array_key_exists('userId', $data) && array_key_exists('rechargeValue', $data) && array_key_exists('transactionTypeAd', $data) && array_key_exists('offerValue', $data) && array_key_exists('offerId', $data)){


            $state=1;
            $operationDate = date('Y-m-d H:i:s');
            $reference = $payment::tokenPayment();

            if($data['transactionTypeAd'] == 1){
                $payValue=$data['rechargeValue'];
                $payValue2=number_format($payValue, 0, '.', '.');
                $title='Estatus de saldo';
                $msn='Señor usuario a realizado una recarga en prontuz por el valor de $'.$payValue2;
            }else{
                $payValue=$data['offerValue'];
                $payValue2=number_format($payValue, 0, '.', '.');
                $title='Estatus de Compra';
                $msn='Señor usuario a realizado la compra de una oferta en prontuz por el valor de $'.$payValue2;
            }


            $data2['transactionType']=99;#cashPay
            $data2['transactionrReferencecode']=$reference;
            $data2['transactionOrderid']=0;
            $data2['transactionIdentification']=0;
            $data2['transactionValue']=$payValue;
            $data2['transactionState']=1;
            $data2['transaction_payerId']=$data['recharge_userId'];
            $data2['transactionOperationdate']=$operationDate;
            $data2['transaction_userId']=$data['recharge_userId'];
            $data2['created_by']=$data['userId'];
            $data2['created_at'] = date('Y-m-d H:i:s');

            $res=transaction::store($data2);

            if($data['transactionTypeAd'] == 1){

                $recharge = DB::table('recharge')->where('recharge_userId','=',$data['recharge_userId'])->count();
                            
                if(empty($recharge) || $recharge == 0){

                    $data3=$this->validateRecharge($data['recharge_userId'],$payValue,$state,$data['userId']);
                    
                    $res=recharge::store($data3);

                }else{
                
                    $data3=$this->validateRecharge($data['recharge_userId'],$payValue,$state,$data['userId']);
                
                    $res=recharge::put($data3);
                }

            }

            if($res){

                $res=User::select('userId','userFirstname','userLastname', 'userEmail', 'password', 'userIdentificacion','remember_token', 'userPhone','userLicence','userBorndate','user_doctypeId','user_rolId','userState','userStreet','userImage','userToken')->where('userId',$data['recharge_userId'])->get();
                $res1=$res->toArray();
                $user=$res1[0];

                if($data['offerId'] > 0){

                    $concat="'".url('/')."/'";
    
                    $offer=DB::table('offer')->select(DB::raw('
                        concat('.$concat.',"offerImage") as "offerImage","offerId","offerTitle","offerContent","offerCompany","offerEmail","offerValue","offerQuantity","offerAvailable","offerState"'))
                        ->Where('offerId',$data['offerId'])
                        ->first();

                    $data=  array(
                                'state'=>1,
                                'offerValue'=>number_format($offer->offerValue, 0, '.', '.'),
                                'offerImage'=>$offer->offerImage,
                                'offerContent'=>$offer->offerContent,
                                'offerCompany'=>$offer->offerCompany,
                                'offerTitle'=>$offer->offerTitle,
                                'offerEmail'=>$offer->offerEmail,
                                'userEmail'=>$user['userEmail'],
                                'userFirstname'=>$user['userFirstname'],
                                'userLastname'=>$user['userLastname']
                            );



                    Mail::send('mail.buyEmail', ['data' => $data], function ($m) use ($data) {
                        $m->from(env('MAIL_USERNAME'), 'Prontuz');

                        $m->to($data['userEmail'], $data['userFirstname'].' '.$data['userLastname'])->subject('Bienvenido!');
                    });
                    
                    Mail::send('mail.buyEmail2', ['data' => $data], function ($m) use ($data) {
                        $m->from(env('MAIL_USERNAME'), 'Prontuz');

                        $m->to($data['offerEmail'], $data['offerCompany'])->subject('Bienvenido!');
                    });
                }

                

                FireBaseMessages::sendOne($res1[0],$title,$msn,array('notificationType'=>3));

                $res = Messages::message(70003,$reference);
            }else{
                $res = Messages::message(10002);
            }
        }

        return $res;
    
    }

/**************************************************/

    public function validateRecharge($drecharge_userId,$drechargeValue,$state,$userId=''){

        date_default_timezone_set('America/Bogota');

        $data3=array();
        $res = false;

        $recharge = recharge::select('rechargeId','rechargeLoan','rechargeValue')
                                                    ->where('recharge_userId','=',$drecharge_userId)
                                                    ->first();
        if($state == 1){
                
                
            if(empty($recharge)){

                $data3['rechargeValue']=$drechargeValue;
                $data3['rechargeLoan']=0;
                $data3['recharge_userId']=$drecharge_userId;
                $data3['created_by'] = ($userId == '') ? $drecharge_userId : $userId;
                $data3['created_at'] = date('Y-m-d H:i:s');

                 $res=$data3;

            }else{

                $rechargeLoan=$recharge['rechargeLoan'];
                $rechargeValue=(intval($drechargeValue)+intval($recharge['rechargeValue']));
                
                if( $rechargeLoan > 0){

                    $rest=(intval($rechargeLoan)-intval($rechargeValue));
                
                    if($rest > 1){ //Deuda

                        $rechargeLoan=$rest;
                        $rechargeValue=0;

                    }else if($rest < 0){ //Recarga

                        $rechargeValue=($rest*(-1));
                        $rechargeLoan=0;

                    }else if($rest == 0){ 
                        $rechargeValue=0;
                        $rechargeLoan=0;
                    }
                }

                $data3['rechargeId']=$recharge['rechargeId'];
                $data3['rechargeValue']=$rechargeValue;
                $data3['rechargeLoan']=$rechargeLoan;
                $data3['recharge_userId']=$drecharge_userId;
                $data3['updated_by']=($userId == '') ? $drecharge_userId : $userId;
                $data3['updated_at']=date('Y-m-d H:i:s');
            
                $res=$data3; 

            }
      
        }

        return $res;
    }

    
/**************************************************/

    public function validateAllPending(){

        date_default_timezone_set('America/Bogota');
        $date=date('Y-m-d');
        $PayU = new PaymentsController();
        $validate=array();
        $data=array();
        $data3=array();
        $res=true;
        $res2=array();
        $verify=false;

        $transactionrReferencecodes = DB::select('select  distinct "transactionrReferencecode" from "transaction"
            where "transactionrReferencecode"
            in (
            select "transactionrReferencecode"
            from "transaction"
            where "transactionState" = 2) and date("transactionOperationdate") =\''.$date.'\'');

        $i=0;

        foreach($transactionrReferencecodes as $k => $v){
            
            $transactionrReferencecode = $v->transactionrReferencecode;

            $tres=DB::select('select "transactionType","transaction_userId","transactionValue","transactionState","transactionOperationdate","transactionOrderid","transactionIdentification","transaction_userId","transactionValue"
                from "transaction" 
                where "transactionOperationdate"=(
                    select max("transactionOperationdate")
                    from "transaction"  
                    where "transactionrReferencecode" = \''.$transactionrReferencecode.'\'
                ) and "transactionrReferencecode" = \''.$transactionrReferencecode.'\'');

            if(!empty($tres)){
                $timeFirst  = strtotime($tres[0]->transactionOperationdate);
                $timeSecond = strtotime(date('Y-m-d H:i:s'));
                $differenceInSeconds = $timeSecond - $timeFirst;

                if(($tres[0]->transactionState == 2) && ($tres[0]->transactionType != 1) /*&& ($differenceInSeconds > 601)*/){
                    $verify = true;
                }else if(($tres[0]->transactionState == 2) && ($tres[0]->transactionType == 1) /*&& ($differenceInSeconds > 3601)*/){
                    $verify = true;
                }else{
                    $verify = false;
                }
                if($verify == true){


                    
                    
                    $res2= $PayU->getDetailByReferenceCode($transactionrReferencecode);
                    
                    if(array_key_exists('error', $res2)){

                       return Messages::message(10002,$res2['error']);

                    }else{
                        
                        $res2 = $res2[0]->transactions;

                        foreach($res2 as $k2 => $v2){

                            if($v2->transactionResponse->state != 'PENDING'){

                                $transactionType=$tres[0]->transactionType;

                                switch($v2->paymentMethod){

                                    //case 2: CREDIT_CARD-Tarjetas de Crédito
                                    case "VISA": 
                                    case "MASTERCARD":
                                    case "AMEX": 
                                    case "DINERS": 
                                    case "CODENSA": 
                                    //case 8: REFERENCED-Pago Referenciado
                                    case "LENDING": 
                                        $transactionType = 0;
                                    break;


                                    //case 7: CASH-Pago en efectivo
                                    case 'BALOTO':
                                    case 'EFECTY':
                                    //case 5: ACH-Débitos ACH
                                    case 'ACH_DEBIT':
                                    //case 7: CASH-Pago en efectivo
                                    case 'OTHERS_CASH':
                                    //case 10: BANK_REFERENCED-Pago en bancos
                                    case 'BANK_REFERENCED':
                                        $transactionType = 1;
                                    break;
                                    
                                    //case 4: PSE-pse-Transferencias bancarias
                                    case 'PSE':
                                        $transactionType = 2;
                                    break;
                                
                                    //case 6: DEBIT_CARD-Tarjetas débito
                                    case 'VISA_DEBIT':
                                        $transactionType = 3;
                                    break;
                                }
                            
                                $data['transactionType'] = $transactionType;
                                $data['transactionrReferencecode'] = $transactionrReferencecode;
                                $data['transactionOrderid'] = $tres[0]->transactionOrderid;
                                $data['transactionIdentification'] = $v2->id;//$tres[0]->transactionIdentification;
                                $operationDate=date('Y-m-d H:i:s');

                                if($v2->transactionResponse->state == 'DECLINED'){ //REJECTED
                                    $state=0;
                                }
                                if($v2->transactionResponse->state == 'APPROVED'){
                                    $state=1;
                                }
                               
                                $data['transactionState']=$state;
                                $data['transactionOperationdate']=$operationDate;
                                $data['transactionCategory']=$tres[0]->transactionCategory;
                                $data['transaction_userId']=$tres[0]->transaction_userId;
                                $data['transactionValue']=$tres[0]->transactionValue;
                                $data['created_by']=0;
                                $data['created_at']=date('Y-m-d H:i:s');

                                $res=transaction::store($data);

                                if($tres[0]->transactionCategory == 1){

                                    $recharge = DB::table('recharge')->where('recharge_userId','=',$tres[0]->transaction_userId)->count();

                                    if($state == 1){
                                        
                                        if(empty($recharge) || $recharge == 0){

                                            $data3=$this->validateRecharge($tres[0]->transaction_userId,$tres[0]->transactionValue,$state);

                                            $res=recharge::store($data3);

                                        }else{
                                        
                                            $data3=$this->validateRecharge($tres[0]->transaction_userId,$tres[0]->transactionValue,$state);
                                        
                                            $res=recharge::put($data3);
                                  
                                        }
                                    }
                                }
                               
                            }
                        }
                    }

                    $verify = false;
                }
                
            }
        }
  
        return json_encode($res);
    }

/**************************************************/


    public function listing(Request $request)
    {
        $resp= array();
        $response= array();
        $response2= array();
        
        if($request['search']['value']!='' || $request['search']['value']!=null)
        {

            $resp=recharge::search(array('dataSearch'=>$request['search']['value'] ));
            $total=count($resp);
        }
        else
        {
            $resp=recharge::listing($request->input());
            $total = transaction::count();
        }

        if(!is_array($resp)){
            $response=$resp->toArray();
        }else{
            $response=$resp;
        }
    
        $response2['recordsTotal'] = count($response);
        $response2['recordsFiltered'] = $total;
        $response2['data'] = $response;
           
        return $response2;

    }

/**************************************************/

    public function getRallUsers(Request $request){

        $data=array();
        
        if(array_key_exists('userId', $request->input())){

            $data=User::findOne($request->input());
        
        }else{
            $data=User::getRallUsers();
        }
        
        return $data;
        
    }

/**************************************************/

    public function sumTransactions(){

        $data=array();
        
        $data=transaction::sumTransactions();
    
        return $data;
        
    }

/**************************************************/

    public function validateRuser(Request $request){

        $data=array();
        
        $data=User::validateRuser($request->input());

            
        if(empty($data)){
            $res = Messages::message(70007);
        }else{
            $payment = new PaymentController();

            $data->merchantId= env('PAYU_MERCHANT_ID');
            $data->accountId= env('PAYU_ACCOUNT_ID_WB');
            $data->description= 'Pago ProntuzU web';//Validar
            $data->referenceCode= $payment::tokenPayment();
            $data->test = env('PAYU_ON_TESTING');
            $data->currency= env('PAYU_DFL_CURRENCY');
            $data->responseUrl=url('/').'/';
            $data->signature= MD5(env('PAYU_API_KEY').'~'.env('PAYU_MERCHANT_ID').'~'.$data->referenceCode.'~'.$request->input('transactionValue').'~'.env('PAYU_DFL_CURRENCY'));

           // “ApiKey~merchantId~referenceCode~amount~currency”.
            
            
            $res = Messages::message(10000,$data);
        }
    
        return $res;
        
    }

/**************************************************/

    public function Rstore(Request $request){

        date_default_timezone_set('America/Bogota');

        $data2=array();
        $data=$request->input();
        $res = Messages::message(10002);

        if((array_key_exists('method', $data)) &&  ($data['method']  == 'webPay') && array_key_exists('userId', $data) && array_key_exists('transactionrReferencecode', $data) && array_key_exists('rechargeValue', $data)){
            $state=2;

            $operationDate = date('Y-m-d H:i:s');

            $data3 = DB::table('transaction')->where('transactionrReferencecode', $data['transactionrReferencecode'])->first();   

            if(empty($data3)){

                $data2['transactionType']=100;#webPay(credicard, pago en efectivo, consignacion)
                $data2['transactionrReferencecode']=$data['transactionrReferencecode'];
                $data2['transactionOrderid']=0;
                $data2['transactionIdentification']=0;
                $data2['transactionValue']=$data['rechargeValue'];
                $data2['transactionState']=$state;
                $data2['transactionOperationdate']=$operationDate;
                $data2['transaction_payerId']=0;
                $data2['transaction_userId']=$data['userId'];
                $data2['created_by']=$data['userId'];
                $data2['created_at']=date('Y-m-d H:i:s');

                $res=transaction::store($data2);

                if($res){
                    $res = Messages::message(70002);
                }else{
                    $res = Messages::message(10002);
                }
            }else{
                $res = Messages::message(70002);
            }
        
        }
        return $res;
    }

/**************************************************/

     public function validateRview(Request $request){

        date_default_timezone_set('America/Bogota');
        $data=$request->input();

        return DB::transaction(function () use ($data) 
        {
            $res = Messages::message(10002);

            if(!empty($data)){
                $data2=array();
                $data3=array();
                $data4=array();
                $operationDate = date('Y-m-d H:i:s');

                if(array_key_exists('referenceCode', $data) && array_key_exists('buyerEmail', $data) && array_key_exists('lapTransactionState', $data) && array_key_exists('polPaymentMethodType', $data) && array_key_exists('transactionId', $data) && array_key_exists('TX_VALUE', $data)){

                    $data2 = DB::table('user')->where('userEmail', $data['buyerEmail'])->where('user_rolId','=',2)->first();
                    $data3 = DB::table('transaction')->where('transactionrReferencecode', $data['referenceCode'])->first();

                    if(!empty($data2) && !empty($data3) ){

                        switch($data['polPaymentMethodType']){

                            case 2: //CREDIT_CARD-Tarjetas de Crédito
                            case 8://REFERENCED-Pago Referenciado
                                $transactionType = 0;
                            break;

                            case 5://ACH-Débitos ACH
                            case 7://CASH-Pago en efectivo
                            
                            case 10://BANK_REFERENCED-Pago en bancos
                                $transactionType = 1;
                            break;
                            case 4://PSE-pse-Transferencias bancarias
                                $transactionType = 2;
                            break;
                        
                            case 6://DEBIT_CARD-Tarjetas débito
                                $transactionType = 3;
                            break;
   
                        }

                        switch($data['lapTransactionState']){
                            case 'DECLINED':
                                $state=0;
                            break;
                            case 'APPROVED':
                                $state=1;
                            break;
                            case 'PENDING':
                                $state=2;
                            break;
                        }
                        

                        $data4['transactionType']=$transactionType;#cashPay
                        $data4['transactionrReferencecode']=$data['referenceCode'];
                        $data4['transactionOrderid']=0;
                        $data4['transactionIdentification']=$data['transactionId'];
                        $data4['transactionValue']=floatval($data['TX_VALUE']);
                        $data4['transactionState']=$state;
                        $data4['transactionOperationdate']=$operationDate;
                        $data4['transaction_userId']=$data2->userId;
                        $data4['created_by']=$data2->userId;
                        $data4['created_at']=date('Y-m-d H:i:s');

                        $res=transaction::store($data4);

                        if($state == 1){

                            $recharge = DB::table('recharge')->where('recharge_userId','=',$data2->userId)->count();
                                        
                            if(empty($recharge) || $recharge == 0){

                                $data3=$this->validateRecharge($data2->userId,$data['TX_VALUE'],$state);

                                $res=recharge::store($data3);

                            }else{
                            
                                $data3=$this->validateRecharge($data2->userId,$data['TX_VALUE'],$state);
                            
                                $res=recharge::put($data3);
                            }

                            if($res){
                                $res = Messages::message(70003);
                            }else{
                                $res = Messages::message(10002);
                            }

                        }
                    }
                }
                $tomorrow = date("Y-m-d", time() + 86400);
                $data=array('tomorrow'=> $tomorrow.' '.date('H:i:s'),
                            'response' => $res);
            
                return view('recharge/store',$data);
                    //buyerEmail
            }else{
                $tomorrow = date("Y-m-d", time() + 86400);
                $data=array('tomorrow'=> $tomorrow.' '.date('H:i:s'),
                            'response' => '');
                return view('recharge/store',$data);
            }
 
         });
           
    }

/**************************************************/

    public function savePayer($data){
    
        $payerId=0;
        $count = DB::table('payer')->where('payerIdentification', $data['PAYER_DNI'])->count();  
        
        if($count == 0){
            
            $data2=array(
                'payerName'         =>  $data['PAYER_NAME'],
                'payerEmail'        =>  $data['PAYER_EMAIL'],
                'payerPhone'        =>  $data['PAYER_CONTACT_PHONE'],
                'payerIdentification'=> $data['PAYER_DNI'],
                'payerCard'=> $data['CREDIT_CARD_NUMBER'],
                'payer_userId'=> $data['recharge_userId'],
                'created_by'=> $data['recharge_userId']
                );

            $res=DB::table('payer')->insert($data2);   

            if($res){
                $payerId = DB::table('payer')->max('payerId');
            }

        }else{

            $payerId = DB::table('payer')->where('payerIdentification', $data['PAYER_DNI'])->value('payerId');

        }

        return $payerId;
    }

/**************************************************/

    public function getPayer(Request $request){
    
        $payerId=0;
        $count = DB::table('payer')->where('payer_userId', $request->input('payer_userId'))->count();  
        
        if($count == 0 ){
            
            $res = Messages::message(70009);

        }else{
            $payerId = DB::table('payer')->where('payer_userId', $request->input('payer_userId'))->max('payerId');

            $res=DB::table('payer')->where('payerId', $payerId)->first();
           
            $res = Messages::message(10000, $res);
            

        }

        return $res;
    }

/**************************************************/

}
