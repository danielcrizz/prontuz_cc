<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Messages;//messages
use App\Routes;
use App\Localization;
use App\route_localization;
use Auth;
use DB;

class LocationController extends Controller
{
    
    public function bringlocalization(){

    	$data = Localization::bringlocalization();

    	if($data){

    		$res = Messages::message(10000,$data);

    	}else{//10002

    		$res = Messages::message(10002,$data);
    		
    	}

    	return $res;

    }

/**************************************************/
	
	public function bringLbyvehicle(Request $request){
		
		if(!empty ($request->input()) ){

			if(array_key_exists('routeassignmentId', $request->input())) {

				$res = DB::table('routeassignment')->where('routeassignmentId','=',$request->input('routeassignmentId'))
    			->Where('routeassignment_vehicleId','=',$request->input('routeassignment_vehicleId'))
               	->count();
	    		
	    		if($res > 0){
	    		
	    			$data = Localization::bringLbyvehicle($request->input());

	    			if($data){

    					$res = Messages::message(10000,$data);

			    	}else{//10002

			    		$res = Messages::message(40013);
			    		
			    	}
	    		
	    		}else{
	    			$res = Messages::message(40012);
	    		}

			}else{
				
				$res = Messages::message(40009);
			}

		}else{
            $res = Messages::message(10001);
        }

        return $res;
	}





}
