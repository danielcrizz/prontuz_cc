<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Messages;
use Storage;
use Cache;
use Carbon\Carbon;
use DB;
use Auth;
use File;
use RolMenu;
class UserController extends Controller
{
    public function store(Request $request)
    {
        if(!empty($request->input()))
        {
            $url='';
            $data=array();
            if($request->file('userImage')!='' )
            {
                $url=Storage::disk('local')->put('/avatar',$request->file('userImage'));
            }
            if($request->input('user_rolId')==1)//admin
            {
                $data=array(
                    'userFirstname'      => $request->input('userFirstname'),
                    'userLastname'       => $request->input('userLastname'),
                    'userEmail'          => $request->input('userEmail'),
                    'password'           => bcrypt($request->input('password')),
                    'userIdentificacion' => $request->input('userIdentificacion'),
                    'remember_token'     => '',
                    'userToken'          => '',
                    'userPhone'          => $request->input('userPhone'),
                    'userLicence'        => null,
                    'userBorndate'       => date('Y-m-d', strtotime(str_replace('/', '-', $request->input('userBorndate')))),
                    'user_doctypeId'     => $request->input('user_doctypeId'),
                    'user_rolId'         => $request->input('user_rolId'),
                    'userState'          => 1,
                    'userImage'          => $url,
                    'user_menu'          => $request->input('user_menu'),
                    'created_by'         => (Auth::user() == null) ? 0 : Auth::user()->userId
                );
            }

            if($request->input('user_rolId')==2)//normal user
            {
                $data=array(
                    'userFirstname'      => $request->input('userFirstname'),
                    'userLastname'       => $request->input('userLastname'),
                    'userEmail'          => $request->input('userEmail'),
                    'password'           => bcrypt($request->input('password')),
                    'userIdentificacion' => $request->input('userIdentificacion'),
                    'remember_token'     => '',
                    'userToken'          => '',
                    'userPhone'          => $request->input('userPhone'),
                    'userLicence'        => null,
                    'userBorndate'       => date('Y-m-d', strtotime(str_replace('/', '-', $request->input('userBorndate')))),
                    'user_doctypeId'     => $request->input('user_doctypeId'),
                    'user_rolId'         => $request->input('user_rolId'),
                    'userState'          => 0,
                    'userImage'          => $url,
                    'created_by'         => (Auth::user() == null) ? 0 : Auth::user()->userId
                );
            }
            
            if($request->input('user_rolId')==3)//driver
            {
                $user_rolId = $request->input('user_rolId');  
                
                if($request->input('driverType') == 'on'){
                  $user_rolId = 4;  
                   
                }

                $data=array(
                    'userFirstname'      => $request->input('userFirstname'),
                    'userLastname'       => $request->input('userLastname'),
                    'userEmail'          => $request->input('userEmail'),
                    'password'           => bcrypt($request->input('password')),
                    'userIdentificacion' => $request->input('userIdentificacion'),
                    'remember_token'     => '',
                    'userToken'          => '',
                    'userPhone'          => $request->input('userPhone'),
                    'userLicence'        => $request->input('userLicence'),
                    'userBorndate'       => date('Y-m-d', strtotime(str_replace('/', '-', $request->input('userBorndate')))),
                    'user_doctypeId'     => $request->input('user_doctypeId'),
                    'user_rolId'         => $user_rolId,
                    'userState'          => 0,
                    'userImage'          => $url,
                    'created_by'         => (Auth::user() == null) ? 0 : Auth::user()->userId
                );
            }

            if($request->input('user_rolId')==5)//Mall
            {
                $user_rolId = $request->input('user_rolId');  
                
                if($request->input('driverType') == 'on'){
                  $user_rolId = 5;  
                   
                }

                $data=array(
                    'userFirstname'      => $request->input('userFirstname'),
                    'userLastname'       => '',
                    'userEmail'          => $request->input('userEmail'),
                    'password'           => bcrypt($request->input('password')),
                    'userIdentificacion' => '',
                    'remember_token'     => '',
                    'userToken'          => '',
                    'userPhone'          => $request->input('userPhone'),
                    'userLicence'        => $request->input('userLicence'),
                    'userBorndate'       => date('Y-m-d', strtotime(str_replace('/', '-', $request->input('userBorndate')))),
                    'user_doctypeId'     => $request->input('user_doctypeId'),
                    'user_rolId'         => $user_rolId,
                    'userState'          => 0,
                    'userImage'          => $url,
                    'created_by'         => (Auth::user() == null) ? 0 : Auth::user()->userId
                );
            }

        	if(User::where('userIdentificacion',$request->input('userIdentificacion'))->where('user_doctypeId',$request->input('user_doctypeId'))->count()==0)
        	{
        		if(User::where('userEmail',$request->input('userEmail'))->count()==0)
        		{
                   return User::store($data);		
        		}
        		else
        		{
        			return Messages::message(20006);
        		}
        	}
        	else
        	{
        		return Messages::message(20007);
        	}
        }
        else
        {
        
            return Messages::message(10001);        
        }
    }

    public function put(Request $request)
    {
        $menu=null;
        if($request->input('user_rolId')==1)//admin
        {
            if($request->file('userImage'))
            {
                $menu = $request->input('user_menu');
                $data=array(
                    'userId'             => $request->input('userId'),
                    'userFirstname'          => $request->input('userFirstname'),
                    'userLastname'           => $request->input('userLastname'),
                    'userEmail'          => $request->input('userEmail'),
                    'userIdentificacion' => $request->input('userIdentificacion'),
                    'remember_token'     => '',
                    'userToken'          => '',
                    'userPhone'          => $request->input('userPhone'),
                    'userLicence'        => null,
                    'userBorndate'       => date('Y-m-d', strtotime(str_replace('/', '-', $request->input('userBorndate')))),
                    'user_doctypeId'     => $request->input('user_doctypeId'),
                    'user_rolId'         => $request->input('user_rolId'),
                    'userState'          => $request->input('userState'),
                    'userImage'          => Storage::disk('local')->put('/avatar',$request->file('userImage')),
                );                
            }
            else
            {
                $menu = $request->input('user_menu');
               $data=array(
                    'userId'             => $request->input('userId'),
                    'userFirstname'          => $request->input('userFirstname'),
                    'userLastname'           => $request->input('userLastname'),
                    'userEmail'          => $request->input('userEmail'),
                    'userIdentificacion' => $request->input('userIdentificacion'),
                    'remember_token'     => '',
                    'userToken'          => '',
                    'userPhone'          => $request->input('userPhone'),
                    'userLicence'        => null,
                    'userBorndate'       => date('Y-m-d', strtotime(str_replace('/', '-', $request->input('userBorndate')))),
                    'user_doctypeId'     => $request->input('user_doctypeId'),
                    'user_rolId'         => $request->input('user_rolId'),
                    'userState'          => $request->input('userState'),
                );   
            }
        }

        if($request->input('user_rolId')==2)//normal user
        {
            if($request->file('userImage'))
            {
                $data=array(
                    'userId'             => $request->input('userId'),
                    'userFirstname'          => $request->input('userFirstname'),
                    'userLastname'           => $request->input('userLastname'),
                    'userEmail'          => $request->input('userEmail'),
                    'userIdentificacion' => $request->input('userIdentificacion'),
                    'remember_token'     => '',
                    'userToken'          => '',
                    'userPhone'          => $request->input('userPhone'),
                    'userLicence'        => null,
                    'userBorndate'       => date('Y-m-d', strtotime(str_replace('/', '-', $request->input('userBorndate')))),
                    'user_doctypeId'     => $request->input('user_doctypeId'),
                    'user_rolId'         => $request->input('user_rolId'),
                    'userState'          => $request->input('userState'),
                    'userImage'          => Storage::disk('local')->put('/avatar',$request->file('userImage')),
                );
            }
            else
            {
                $data=array(
                    'userId'             => $request->input('userId'),
                    'userFirstname'          => $request->input('userFirstname'),
                    'userLastname'           => $request->input('userLastname'),
                    'userEmail'          => $request->input('userEmail'),
                    'userIdentificacion' => $request->input('userIdentificacion'),
                    'remember_token'     => '',
                    'userToken'          => '',
                    'userPhone'          => $request->input('userPhone'),
                    'userLicence'        => null,
                    'userBorndate'       => date('Y-m-d', strtotime(str_replace('/', '-', $request->input('userBorndate')))),
                    'user_doctypeId'     => $request->input('user_doctypeId'),
                    'user_rolId'         => $request->input('user_rolId'),
                    'userState'          => $request->input('userState'),
                );
            }
        }
        if($request->input('user_rolId')==3)//driver
        {
            $user_rolId = $request->input('user_rolId');  
                
            if($request->input('driverTypeEdit') == 'on'){
              $user_rolId = 4;  
            }

            if($request->file('userImage'))
            {
                $data=array(
                    'userId'             => $request->input('userId'),
                    'userFirstname'          => $request->input('userFirstname'),
                    'userLastname'          => $request->input('userLastname'),
                    'userEmail'          => $request->input('userEmail'),
                    'userIdentificacion' => $request->input('userIdentificacion'),
                    'remember_token'     => '',
                    'userToken'          => '',
                    'userPhone'          => $request->input('userPhone'),
                    'userLicence'        => $request->input('userLicence'),
                    'userBorndate'       => date('Y-m-d', strtotime(str_replace('/', '-', $request->input('userBorndate')))),
                    'user_doctypeId'     => $request->input('user_doctypeId'),
                    'user_rolId'         => $user_rolId,
                    'userState'          => $request->input('userState'),
                    'userImage'          => Storage::disk('local')->put('/avatar',$request->file('userImage')),
                );
            }
            else
            {
                $data=array(
                    'userId'             => $request->input('userId'),
                    'userFirstname'          => $request->input('userFirstname'),
                    'userLastname'           => $request->input('userLastname'),
                    'userEmail'          => $request->input('userEmail'),
                    'userIdentificacion' => $request->input('userIdentificacion'),
                    'remember_token'     => '',
                    'userToken'          => '',
                    'userPhone'          => $request->input('userPhone'),
                    'userLicence'        => $request->input('userLicence'),
                    'userBorndate'       => date('Y-m-d', strtotime(str_replace('/', '-', $request->input('userBorndate')))),
                    'user_doctypeId'     => $request->input('user_doctypeId'),
                    'user_rolId'         => $user_rolId,
                    'userState'          => $request->input('userState'),
                );
            }
        }
    	if(User::where('userIdentificacion',$request->input('userIdentificacion'))->where('user_doctypeId',$request->input('user_doctypeId'))->count() > 0)
    	{
    		if(User::where('userEmail',$request->input('userEmail'))->count() > 0)
    		{
    			return User::put($data, $menu);		
    		}
    		else
    		{
    			return Messages::message(20006);
    		}
    	}
    	else
    	{
    		return Messages::message(20007);
    	}
    }

    public function listing(Request $request)
    {
        $resp= array();
        $total = 0;
        if($request['search']['value']!='' || $request['search']['value']!=null)
        {
            $resp=User::search(array('dataSearch'=>$request['search']['value'], 'start'=>$request['start'],'length'=>$request['length']));
             $username='concat("userFirstname",'."' '".',"userLastname") AS "FullName", "userEmail","userPhone","userImage",concat("doctypeInitial",'."'-'".',"userIdentificacion") AS "FullId","rolName","userId","userState"';

            $total = User::select(DB::raw($username))
               ->join('rol', 'rol.rolId','=', 'user.user_rolId')
               ->join('doctype', 'doctype.doctypeId', '=', 'user.user_doctypeId')
               ->orWhere('user.userFirstname','ilike','%'.$request['search']['value'].'%')
               ->orWhere('user.userLastname','ilike','%'.$request['search']['value'].'%')
               ->orWhere('user.userEmail','ilike','%'.$request['search']['value'].'%')
               ->orWhere('user.userIdentificacion','ilike','%'.$request['search']['value'].'%')
               ->orWhere('user.userPhone','ilike','%'.$request['search']['value'].'%')
               ->orderBy('user.userIdentificacion','asc')
               ->orderBy('user.userIdentificacion','asc')
               ->count();
        }
        else
        {
            $resp=User::listing($request->input());
            $total = User::count();
        }    	
        $resp = $resp->toArray();
        for ($i=0; $i < count($resp); $i++)
        { 
            if (!File::exists($resp[$i]['userImage']))
            {
                $resp[$i]['userImage']='icon_user_2.png';
            }
            utf8_decode($resp[$i]['FullName'] = $resp[$i]['FullName']);
        }
        $response = array(
            "draw" => intval($request['draw']),
            "recordsTotal" => count($resp),
            "recordsFiltered" => $total,
            "data" => $resp
        );
        return $response;
    }

    public function search(Request $request)
    {
		$resp=User::search($request->input());
    	if(count($resp->toArray())>0)
    	{
    		return Messages::message(10000,$resp->toArray());
    	}
    	else
    	{
    		return Messages::message(20008);    		
    	}
    }    
    public function destroyed(Request $request)
    {
    	if(User::where('userId',$request->input('userId'))->count()>0)
    	{
    		return User::destroyed($request->input());
      	}
    	else
    	{
    		return Messages::message(20008);
    	}
    }

    /**************************************************/
    
    public function updateToken(Request $request)
    {
        if(!empty($request->input('userId')) && !empty($request->input('userToken')))
        {
            if (User::where('userId',$request->input('userId'))->count() > 0) 
            {
                $res = User::updateToken($request->input());
            }
            else
            {
                $res = Messages::message(10007); 
            }           
        }
        else
        {
            $res = Messages::message(10010); 
        }
        return $res;
    }

    public function verifyCode(Request $request)
    {
        if($request->input('userCode')==Cache::get($request->input('userEmail')))
        {
            if(User::where('userEmail',$request->input('userEmail'))->update(['userState'=>1]))
            {
                $data=User::where('userEmail',$request->input('userEmail'))->get();
                Cache::forget($request->input('userEmail'));
                $user=$data->toArray();
                return Messages::message(10000,$user[0]);
            }
            else
            {
                return Messages::message(20000);
            }
        }
        else
        {
            return Messages::message(20011);
        }
    }
    public function verifyUser(Request $request)
    {
        $validate = false;
        if(User::where('userEmail',$request->input('userEmail'))->count()>0)
        {   
            if(array_key_exists('user_rolId', $request->input())  && $request->input('user_rolId') == 2){
                if(User::where('userEmail',$request->input('userEmail'))->where('user_rolId',$request->input('user_rolId'))->count()>0)
                {
                    $validate = true;
                }
            }else{
                if(User::where('userEmail',$request->input('userEmail'))->count()>0 )
                {
                    $validate = true;
                }
                
            } 


            if($validate == true)
            {
                if(User::where('userIdentificacion',$request->input('userIdentificacion'))->where('user_doctypeId',$request->input('user_doctypeId'))->where('userEmail',$request->input('userEmail'))->count() > 0)
                {
                    if(User::where('userEmail',$request->input('userEmail'))->where('userState','0')->count() > 0)
                    {
                        $data=array('userPhone'=>$request->input('userPhone'),'userEmail'=>$request->input('userEmail'),'password'=>bcrypt($request->input('password')));
                        return DB::transaction(function () use ($data) 
                        {
                            if(User::where('userEmail','=', $data['userEmail'])->update($data))
                            {
                                 $data=User::where('userEmail',$data['userEmail'])->get();
                                if(count($data->toArray())>0)
                                {
                                    $user=$data->toArray();
                                    User::sendMail(array('mail'=>$user[0]['userEmail'],'view'=>'mail.holaProntuz', 'name'=>utf8_decode($data[0]['userFirstname'].' '.$data[0]['userLastname'])));
                                    return Messages::message(10003);                        
                                }
                                else
                                {
                                    return Messages::message(20000);
                                }
                            }
                            else
                            {
                                return Messages::message(20000);
                            }
                        });
                    }
                    else
                    {
                        return Messages::message(20013);
                    }
                }
                else
                {
                    return Messages::message(20014);
                }
            }
            else
            {
                return Messages::message(20022);
            }
        }
        else
        {
            return Messages::message(20014);
        }
    }


    public function findOne(Request $request)
    {

        $resp=User::findOne($request->input());

        //$res2=array();
        if(count($resp)>0)
        {
            $resp[0]->userBorndate=date('d/m/Y', strtotime(str_replace('-', '/', $resp[0]->userBorndate)));
            $resp[0]->userImage=url('/').'/'.$resp[0]->userImage;
            utf8_decode($resp[0]->userFirstname=$resp[0]->userFirstname);
            utf8_decode($resp[0]->userLastname=$resp[0]->userLastname);
            return Messages::message(10000, $resp);
        }
        else
        {
            return Messages::message(20008);            
        }
    }

    public function resetPassword(Request $request)
    {
        $user=User::where('userEmail',$request->input('userEmail'))->where('userState','1')->get();
        if(count($user->toArray())>0)
        {
            $data=$user->toArray();
            User::sendMail(array('mail'=>$data[0]['userEmail'],'view'=>'mail.recuperacionProntuz', 'name'=>utf8_decode($data[0]['userFirstname'].' '.$data[0]['userLastname'])));

            return Messages::message(10004);
        }
        else
        {
            return Messages::message(20014);
        }
    }

    public function newPassword(Request $request)
    {
        $user=User::where('userEmail',$request->input('userEmail'))->where('userState','1');
        if($user->count()>0)
        {
            if($request->input('userCode')==Cache::get($request->input('userEmail')))
            {
                if(User::where('userEmail',$request->input('userEmail'))->update(['password'=>bcrypt($request->input('password'))]))
                {
                    Cache::forget($request->input('userEmail'));
                    return Messages::message(10000);
                }
                else
                {
                    return Messages::message(20000);
                }
            }
            else
            {
                return Messages::message(20011);
            }
        }
        else
        {
            return Messages::message(20014);
        }
    }

    public  function verifyIdentification(Request $request)
    {
       return User::where('userIdentificacion',$request->input('userIdentificacion'))->where('user_doctypeId',$request->input('user_doctypeId'))->count();
    }

    public  function verifyEmail(Request $request)
    {
       return User::where('userEmail',$request->input('userEmail'))->count();
    }   

    public  function sendPassword(Request $request)
    {
        $data=array(
            'userId'     => $request->input('userId'),
            'password'   => bcrypt($request->input('password')),
        );
       return User::sendPassword($data);
    }

    public function totalKindUsers(Request $request)
    {
        $response = array(
            "numAdmin" => User::where('user_rolId',1)->count(),
            "numUser" => User::where('user_rolId',2)->count(),
            "numDriver" => User::where('user_rolId',3)->count(),
            "numWheel" => User::where('user_rolId',5)->count(),
            "totalUsers" => User::count()
        );
        return $response;
    }

    public  function updatePhoto(Request $request)
    {
        $data=array(
            'userId'     => $request->input('userIdPhoto'),
            'userImage'  => Storage::disk('local')->put('/avatar',$request->file('new_img')),
            'updated_by' => Auth::user()->userId
        );
        return User::updatePhoto($data);
    }

/**************************************************/

    public function deleteAssignmentWheel(Request $request){

        $res=false;

        $data = DB::table('routeassignment')
                ->select('routeassignmentId', 'routeassignment_vehicleId')
                ->where('routeassignment_userId',$request->input('routeassignment_userId'))
                ->first();
        $count = DB::table('routeassignment')
                ->select('routeassignmentId', 'routeassignment_vehicleId')
                ->where('routeassignment_userId',$request->input('routeassignment_userId'))
                ->count();
   
        if($count > 0){
            $res=DB::table('routeassignment')->where('routeassignment_userId', '=', $request->input('routeassignment_userId'))->delete();
            $res=DB::table('vehicle')->where('vehicleId', '=', $data->routeassignment_vehicleId)->delete();
        }
        
        if($res){
            $res = Messages::message(10000);
        }else{
            $res = Messages::message(20000);
        }

        return $res;     
    }

/**************************************************/

    public function bringParameters(){

        $res = DB::table('sysparameters')->get();

        if($res){
            $res = Messages::message(10000,$res);
        }else{
            $res = Messages::message(20000);
        }
        return $res;
    }

/**************************************************/

    public function putSysparameters(Request $request){

        $data=$request->input();

        if(count($data) > 1){

            foreach($data as $k => $v){
               
                if($k != '_token'){
                    $res=DB::table('sysparameters')->where('sysparametersName', $k)->update(['sysparametersValue' => $v]);
                }
            }

        }

        if($res){
            $res = Messages::message(10000,$res);
        }else{
            $res = Messages::message(20000);
        }

        return $res;
    }

/**************************************************/

    public function userbyIdentification(Request $request){

       if($request->input('userIdentificacion')){

            $res=User::userbyIdentification($request->input());

            if($res){
                $res = Messages::message(10000,$res);
            }else{
                $res = Messages::message(10011);
            }
        }else{
            $res = Messages::message(10002);  
        }

        return $res;   
    }

}
//