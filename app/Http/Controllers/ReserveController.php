<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Messages;
use App\User;
use App\recharge;
use App\reserve;
use App\Routes;
use App\routeassingment;
use App\FireBaseMessages;
use App\Vehicle;
use App\route_localization;
use App\Localization;
use App\routeprincipals;
use DB;

class ReserveController extends Controller
{
    public function store(Request $request)
    {
        $count=0;
        $vehicle='';
        $userIdpayer='';
        $vehicleOcupation=0;
        $data=array();

        if(array_key_exists('userData', $request->input())){

            $userdata0 = $request->input('userData');
            $userdata = json_decode(json_encode($userdata0));


            if(count($userdata) > 0 && count($userdata) < 3){
               
                $validate=false;
                $messege=10002;
                $vah="";


                foreach($userdata as $k => $v){
                    
                    if(property_exists($v, 'userId') && property_exists($v,'routeId') && property_exists($v,'routeassignmentId') && property_exists($v,'reserve_route_localization') && property_exists($v,'localization_localizationId') && property_exists($v,'userIdpayer') && property_exists($v,'routeassignmentType') && property_exists($v,'localizationLatitude') && property_exists($v,'localizationLongitude')){

                        if(User::where('userId',$v->userId)->where('userState',1)->count() > 0)
                        {
                            if((Routes::where('routeId',$v->routeId)->where('routeState',1)->count() > 0) ||
                                (DB::table('routewheel')->where('routewheelId',$v->routeId)->where('routewheelState',1)->count() > 0))
                            {
                                $vehicle = DB::table('routeassignment')->leftJoin('vehicle','vehicle.vehicleId','=','routeassignment.routeassignment_vehicleId')->where('routeassignment.routeassignmentId',$v->routeassignmentId)->first();
                            
                                if($v->reserve_route_localization >= $vehicle->vehicle_locationId)
                                {
                                    $reserve=reserve::where('reserve_UserId',$v->userId)->where('reserve_vehicleId',$vehicle->vehicleId)->where('reserveState',1);
                                   
                                    //ya existe una reserva
                                    if($reserve->count() === 0 && reserve::where('reserve_UserId',$v->userId)->where('reserveState',1)->count() === 0)
                                    { 
                                        if(routeassingment::where('routeassignmentFirst_routeId',$v->routeId)->where('routeassignmentActive',1)->count() > 0 || routeassingment::where('routeassignmentLast_routeId',$v->routeId)->where('routeassignmentActive',2)->count() > 0)
                                        {
                                            $userIdpayer=$v->userIdpayer;
                                            $validate=true;

                                        }else
                                        {
                                           $messege=40014;
                                        }
                                    }else
                                    {
                                        if(reserve::where('reserve_UserId',$v->userId)->where('reserveState',1)->value('reserveType') == $v->routeassignmentType){
                                            return $this->sendReserve($request, true);
                                        }else{
                                            
                                            if($v->routeassignmentType == 1){
                                                return Messages::message(20030,array('userPosition'=>$count));
                                            }else{
                                                return Messages::message(20031,array('userPosition'=>$count));
                                            }
                                            
                                        }

                                        
                                    }
                                   
                                }else
                                {
                                    $messege=20024;
                                }  
                            }else
                            {
                                $messege=40002;
                            }
                        }
                        else
                        {
                            $messege=20016;
                        }
                    }else{

                        if($validate == true){
                            $validate=false;
                        }
                    }

                    $count++;
                }


                if($validate == true){

                    $recharge=recharge::getRcValue($userIdpayer);
                    $recharge2=$recharge->toArray();

                    if(empty($recharge2)){
            
                        $validate=false;
                        $messege=70001;
                    
                    }else if($recharge2[0]->rechargeLoan > 0){
                    
                        $validate=false;
                        $messege=70005;
                    }

                    if($count == 1){
                        $vehicleOcupation=1;
                        if(!($vehicle->vehicleCapacity - $vehicle->vehicleOcupation > 0))
                        {
                            $validate=false;
                            $messege=20028;
                        }

                    }else if($count == 2){
                        $vehicleOcupation=2;
                        if(!($vehicle->vehicleCapacity - $vehicle->vehicleOcupation > 1))
                        {
                            $validate=false;
                            $messege=20028;
                        }
                    }

                    if($validate == true){
                        
                        $res2=array();
                        foreach($userdata as $k => $v){

                            $res=$this->storeR($v,$vehicle,$vehicleOcupation);
                           
                            if(!$res){

                                $validate == false;
                                
                            }else{
                                if($validate == true){
                                    $validate == true;
                                    $res2[count($res2)]=$res;
                                }
                            }

                        }
             

                        if($validate){

                            return Messages::message(10000,$res2);
                        }else{
                            return Messages::message(10002,$res2);   
                        }
                        

                    }else{
                       return Messages::message($messege); 
                    }
                }else{
                    return Messages::message($messege);
                }

            }else{
                return Messages::message(20027);
            }

        }else
        {
            return Messages::message(20016);
        }
    }

    public function storeR($data,$vehicle,$vehicleOcupation){

        
        $code=User::generateCode(false);
        if(reserve::where('reserveCode',$code)->where('reserve_route_localization',$data->reserve_route_localization)->where('reserveState',1)->count() > 0)
        {
            $code=User::generateCode(false);
        }

        $data=array(
            'routeId'              => $data->routeId,
            'reserve_UserId'              => $data->userId,
            'routeassignmentId'           => $data->routeassignmentId,
            'reserve_route_localization'  => $data->localization_localizationId,
            'reserve_vehicleId'           => $vehicle->vehicleId,
            'vehicleOcupation'            => $vehicle->vehicleOcupation+$vehicleOcupation,
            'reserveState'                => 1,
            'localization_localizationId' => $data->localization_localizationId,
            'reserveStatus'               => 'Pendiente',
            'reserveCode'                 => $code,
            'reserveType'                 => $data->routeassignmentType,
            'reservePayer_userId'         => $data->userIdpayer,
            'reserveLatitude'             => $data->localizationLatitude,
            'reserveLongitude'            => $data->localizationLongitude
        );
       
        $res=reserve::store($data);

        if($res){

            //reserve_vehicleId
            $vehiclebrandName = DB::table('vehicle')->join('vehiclebrand', 'vehiclebrandId', '=', 'vehicle_vehiclebrandId')->where('vehicleId',$data['reserve_vehicleId'])->value('vehiclebrandName');
            
            $res['vehiclebrandName']=$vehiclebrandName;

            return $res;
        }
        return reserve::store($data);
    }


    public function findReserveUserActive(Request $request)
    {
        return $this->sendReserve($request, false);
    }

    //Function that sends reservation in case it exists or the gate if it is no longer active
    private function sendReserve(Request $request, $boolean){
        $reserve = reserve::select('*')->where('reserve_UserId',$request['userId'])->where('reserveState',1);
        if ($reserve->count() > 0) {
            $reserve=$reserve->get();
            $reserve=$reserve->toArray();
            if ($reserve[0]['reserveType'] == 0)  {
                $reserve = reserve::select('*')->join('localization','localization.localizationId','=','reserve.reserve_route_localization')->where('reserve_UserId',$request['userId'])->where('reserveState',1);
                $reserve=$reserve->get();
                $reserve=$reserve->toArray();
                $routeassingment = routeassingment::select('*')->where('routeassignment_vehicleId',$reserve[0]['reserve_vehicleId'])->get();
                $routeassingment = $routeassingment->toArray();
                $route='';
                switch ($routeassingment[0]['routeassignmentActive']) {
                    case 1:
                        $route = Routes::select('*')->where('routeId', $routeassingment[0]['routeassignmentFirst_routeId'])->get();
                        break;
                    case 2:
                        $route = Routes::select('*')->where('routeId', $routeassingment[0]['routeassignmentLast_routeId'])->get();
                        break;
                    default:
                        if (reserve::where('reserveId',$reserve[0]['reserveId'])->update(['reserveState'=>0])) {
                            if ($boolean) {
                                return $this->store($request);
                            }
                            else{
                                return Messages::message(20017);
                            }
                            
                        }
                        else{
                            return Messages::message(10002);
                        }
                        break;
                }
                $route=$route->toArray();
                $vehicle = Vehicle::select('vehiclePlaque')->where('vehicleId', $reserve[0]['reserve_vehicleId'])->get();
                $vehicle = $vehicle->toArray();
                if (route_localization::select('*')->where('route_routeId', $route[0]['routeId'])->where('localization_localizationId', $reserve[0]['reserve_route_localization'])->count() == 0) {
                    if (reserve::where('reserveId',$reserve[0]['reserveId'])->update(['reserveState'=>0])) {
                            if ($boolean) {
                                return $this->store($request);
                            }
                            else{
                                return Messages::message(20017);
                            }
                        }
                    else{
                            return Messages::message(10002);
                        }
                }
                $data2 = json_decode(json_encode((object) array(
                    'vehicleId' => $reserve[0]['reserve_vehicleId'],
                    'localization_localizationId' => $reserve[0]['reserve_route_localization']
                    )));
                $data3=array($data2);
                $res=Routes::organicetime($data3);
                $localization = Localization::select('*')->where('localizationId', $reserve[0]['reserve_route_localization'])->get()->toArray();
                $routes_principals = explode(',', $route[0]['route_routeprincipalsId']);
                $routes_principalsId =array();
                foreach ($routes_principals as $id) {
                    array_push($routes_principalsId, routeprincipals::select('*')->where('routeprincipalsId', $id)->get()->toArray()[0]);
                }
                $reserve[0]['rTime'] =$res[0]->rTime;
                $reserve[0]['routeassignmentId'] = $routeassingment[0]['routeassignmentId'];
                $reserve[0]['routeDirection'] = $route[0]['routeDirection'];
                $reserve[0]['vehiclePlaque'] = $vehicle[0]['vehiclePlaque'];
                $reserve[0]['routeName'] = $route[0]['routeName'];
                $reserve[0]['localizationAddress'] = $localization[0]['localizationAddress'];
                $reserve[0]['route_routeprincipalsId'] = $routes_principalsId;
                $reserve[0]['routeId'] = $route[0]['routeId'];
                $reserve[0]['userImage'] = url('/').'/'.User::select('*')->where('userId', $routeassingment[0]['routeassignment_userId'])->get()->toArray()[0]['userImage'];
                return Messages::message(20018,$reserve[0]);
            }
            else{
                return Messages::message(20018,$reserve[0]);
            }
        }
        else
        {
            return Messages::message(20017);
        }
    }  

    public function listingReserveVehicle(Request $request)
    {
        return reserve::listingReserveVehicle(['vehicleId'=>$request->input('vehicleId'), 'reserve_route_localization'=>$request->input('reserve_route_localization')]);
    }  

    public function clearReserveAll(Request $request)
    {
        return reserve::clearReserveAll($request->input());
    }

    public function closeReserveUser(Request $request)
    {
        if(reserve::where('reserveId',$request->input('reserveId'))->where('reserveState',1)->count() > 0)
        {
            $reserve = reserve::closeReserveUser(['reserveId'=>$request->input('reserveId'),'vehicleId'=>$request->input('vehicleId')]);
            $reserv = json_decode($reserve,TRUE);
            if($reserv['ResponseCode']===1 && ($request->input('user_rolId')===3 || $request->input('user_rolId')==='3'))
            {
                $user = $this->usersreserve($request->input('reserveId'));
                $title='Estatus de Reserva';
                $msn='Señor(a) usuario(a) la reserva fue cancelada por el conductor';
                FireBaseMessages::sendOne($user->toArray(),$title,$msn,array('isCancel'=>1));
            }
            return $reserve;
        }
        else
        {
            return Messages::message(20019);
        }
    }  

    public function historyReserveUser(Request $request)
    {
        return reserve::historyReserveUser(['reserve_UserId'=>$request->input('userId')]);
    }  

    public function confirmReserve(Request $request)
    {
        if(($request->input('user_rolId') == 3) && array_key_exists('routeId', $request->input()))
        {
            if(reserve::where('reserveCode',$request->input('reserveCode'))->where('reserveState',1)->where('reserve_route_localization',$request->input('reserve_route_localization'))->count() > 0)
            {
                return reserve::confirmReserve($request->input());
            }
            else
            {
                return Messages::message(20020);
            }
        }
        if(array_key_exists('userData', $request->input()))
        {   
            $count=0;
            $userData=$request->input('userData');
            $validate=false;
            $res=array();
            $message='';

            foreach($userData as $k => $v){
                    
                if(array_key_exists('reserveCode', $v) && array_key_exists('reserve_route_localization', $v) && array_key_exists('user_rolId', $v) && array_key_exists('routeId', $v) && array_key_exists('routeassignmentType', $v)){

                    if($v['user_rolId'] == 2){
                       
                        //if(reserve::where('reserveCode',$v['reserveCode'])->where('reserve_route_localization',$v['reserve_route_localization'])->where('reserveState',2)->orWhere('reserveState', 4)->count() > 0){
                            $validate=true;
                        // }else{
                        //     $message=20025;
                        // }
                    }else{
                        $message=10002;
                    }

                }else{
                    $message=10001;
                }
                $count++;
            }
            
            if($validate == true){
                foreach($userData as $k => $v){
                    $res[count($res)]=reserve::confirmReserve($v);
                }

                if($count == 1){
                     if($res[0] == 1){

                    return Messages::message(10000); 

                    }else if($res[0] == 0){

                        return Messages::message(20000); 

                    }else if($res[0] == 2){

                        return Messages::message(20025); 
                    }
                }else{
                    if($res[0] == 1 && $res[1] == 1){

                    return Messages::message(10000); 

                    }else if( ($res[0] == 0 && $res[1] == 0) || ($res[0] == 1 && $res[1] == 0) || ($res[0] == 0 && $res[1] == 1) ){

                        return Messages::message(20000); 

                    }else if($res[0] == 1 && $res[1] == 2){

                        return Messages::message(20029,array('userPosition'=>0)); 

                        
                    }else if($res[0] == 2 && $res[1] == 1){

                        return Messages::message(20029,array('userPosition'=>1)); 
                    }
                    else if($res[0] == 2 && $res[1] == 2){

                        return Messages::message(20025); 
                    }
                }
                

                

            }else{
               return Messages::message($message); 
            }
          
        } 
        
        else{
            return Messages::message(10002);
        }

    }
/**************************************************/

    public function finishReserve(Request $request){

        if(array_key_exists('reserve_vehicleId', $request->input()) && array_key_exists('reserve_route_localization', $request->input())){

            $res=reserve::finishReserve($request->input());
                
            if($res){
                $res = Messages::message(10000);
            }else{
               $res = Messages::message(10002);
            }
          
        }else{
        
            $res = Messages::message(10001);
        }

        return $res;
    }
/**************************************************/

    public function usersreserve($id)
    {
        return reserve::select('user.*')->join('user','user.userId', '=', 'reserve.reserve_UserId')->where('reserveId',$id)->first();
    }

    public function alertVehicle(Request $request)
    {
        return reserve::alertVehicle(['vehicleId'=>$request->input('vehicleId')]);        
    }
}
