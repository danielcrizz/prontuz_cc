<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Messages;//messages
use App\Routes;
use App\route_localization;
use App\Localization;
use App\Vehicle;
use App\User;
use App\routeprincipals;
use App\routeassingment;
use Auth;
use App\Gmaps;
use DB;

class RoutesController extends Controller
{
    //
    public function bringRbylocation(Request $request){

        if(!empty($request->input('localization_localizationId'))){

            $data = Routes::bringRbylocation($request->input());

            if($data){
                
                $data = $this->prepareRprincipal($data,true);

                if($data){
                     $res = Messages::message(10000,$data); 
                }else{
                    $res = Messages::message(40010);
                }

               
            
            }else{
                $res = Messages::message(40010); 
            }

        }else{
        
            $res = Messages::message(10001);
        
        }

        return $res;
    }

/**************************************************/

    public function bringRprincipals(Request $request)//route_routeprincipalsId:1,2
    {

        $cont = DB::table('routeprincipals')->count();

        if($cont > 1){

            if(array_key_exists('route_routeprincipalsId', $request->input())){
            
                $route_routeprincipalsId=explode(',',$request->input('route_routeprincipalsId'));
                
                $data = DB::table('routeprincipals')->whereNotIn('routeprincipalsId',$route_routeprincipalsId)
                ->pluck('routeprincipalsId','routeprincipalsName'); 
                
            }else{
                 $data = DB::table('routeprincipals')->pluck('routeprincipalsId','routeprincipalsName');
            }
                    
            $res = Messages::message(10000,$data);
        
        }else{
             $res = Messages::message(40004);
        }
  
        return $res;
    }

/**************************************************/

 public function store(Request $request)
    {
        $prueba=array();
        $l=array();
        $l2=array();
        $l3=array();

     

        $localization=$request->input('localization_localizationId');

        //var_dump($request->input());
       
        //$localization=json_encode($prueba);

        //$l = json_decode($localization);
        $l = ($localization);
        //$l2 = json_decode(json_encode($l->localization_localizationId), True);
        
        //print_r($l);
        if(empty($localization) || $localization == null){
            $l3=array();   
        }else{
            foreach($l as $k => $v){

                $l3[count($l3)]=$v;
            }
        }
        


       if(count($l3) > 1){

           $res = $this->validateRn($request->input());
            
            $array = json_decode($res);
            
            if($array->ResponseCode == 1){

               $data=array(
                    'routePrice' => $request->input('routePrice'),
                    'routeName' => $request->input('routeName'),
                    'route_routeprincipalsId'=> $request->input('route_routeprincipalsId'),
                    'routeDirection'=> $request->input('routeDirection'),
                    'created_by'=>Auth::user()->userId
                    );

                $routeId=Routes::store($data);

                if(is_int($routeId) && $routeId > 0){
                    
                    $res = Localization::store($l3,$routeId);

                    if($res){
                        $res = Messages::message(10000);
                    }else{
                        $res = Messages::message(20000);
                    }
                }else{
                    $res = Messages::message(20000);
                }
             
            }
       }else{
            $res = Messages::message(40006);
       }

     

        
        return $res;
        
    }

/**************************************************/

   public function put(Request $request){

    $res = Messages::message(20000);

    if(!empty($request->input('routeId'))){

        // var_dump($request->input());
        $prueba=array();
        $l=array();
        $l2=array();
        $l3=array();


        $localization=$request->input('localization_localizationId');

        $l = ($localization);
        //var_dump($l);
        
        foreach($l as $k => $v){

            $l3[count($l3)]=$v;
        }


         if(count($l3) > 1){


            $res = $this->validateRn('',$request->input());

            $array = json_decode($res);

            if($array->ResponseCode == 1){
                    
                    $data=array(
                        'routeId'                   => $request->input('routeId'),
                        'routeName'                 => $request->input('routeName'),
                        'routeState'                => $request->input('routeState'),
                        'routePrice'                => $request->input('routePrice'),
                        'route_routeprincipalsId'   => implode(",",$request->input('route_routeprincipalsId')),
                        'routeDirection'            => $request->input('routeDirection'),
                        'updated_by'                => Auth::user()->userId
                    );

                    $res=Routes::put($data);

                    if($res){

                        if($request->input('routeState') == 0){

                            $res = Routes::deleteRArelation($request->input());
                        }
                        
                        $res = Localization::put($l3,$request->input('routeId'));

                        if($res){
                            $res = Messages::message(10000);
                        }else{
                             $res = Messages::message(20000);
                        }
                    }else{
                        $res = Messages::message(20000);
                    }

                    
            }else{
                 $res = Messages::message(10001);
            }
                        
           }

    }else{
         $res = Messages::message(10001);
    }

        return $res;
    }




/**************************************************/

    public function validateRname(Request $request)
    {
        if(array_key_exists('op', $request->input())){
             return $this->validateRn($request->input(),$request->input());
        }else{
             return $this->validateRn($request->input());
        }
       
    }

/**************************************************/
    
    public function validateRn($data='',$data2='')
    {

    	if($data2 == ''){
    		
    		if(empty($data)){
    			$res=Messages::message(10001);
    		}else{
    			$routeName=$data['routeName'];

		    	if(empty($routeName)){
		    		$res=Messages::message(10001);
		    	}else{
		    	
		    		$data=Routes::validateRname($routeName);

		    		if($data > 0){	
		        		$res=Messages::message(40001);
		    		}else{
		    			$res=Messages::message(40002);
		    		}
		    	}
    		}
    		
    	}else{
    		$routeName = $data2['routeName'];
    		$routeId = $data2['routeId'];


    		$data = Routes::validateRname($routeName,1,$routeId);

    		if($data > 0){	
        		$res=Messages::message(40001);
    		}else{
    			$res=Messages::message(40002);
    		}
    	} 
       	return $res;
    	
	}

/**************************************************/

    public function prepareRprincipal($data,$a=false){

        $data3=array();
        $data4=array();

            foreach($data as $k => $v){


                foreach($v as $k2 => $v2){


                     if($k2 == 'route_routeprincipalsId'){

                        
                        if(!empty($v2)){


                            $data3=explode(',',$v2);

                            for ($i=0;$i<count($data3);$i++){
                                
                                $routeprincipalsName = DB::table('routeprincipals')->where('routeprincipalsId',$data3[$i])->value('routeprincipalsName');
                                $data4[$i] = array(
                                                'routeprincipalsId'=>$data3[$i],
                                                'routeprincipalsName'=>$routeprincipalsName
                                                );
                            }
                             if($a==true){
                                 $data[$k]->route_routeprincipalsId=$data4;
                             }else{
                                 $data[$k]['route_routeprincipalsId']=$data4;
                             }
                             //print_r($data[$k]);
                          
                        }
                        
                    }
                }
                     
            }
        
        
     
        return $data;
    }
/**************************************************/

public function listing(Request $request)
{
    $resp= array();
    $response= array();
    $response2= array();
    
    if($request['search']['value']!='' || $request['search']['value']!=null)
    {

        $resp=Routes::search(array(
                        'dataSearch'=>$request['search']['value']
            )
        );
        $total=count($resp);
    }
    else
    {
        $resp=Routes::listing($request->input());
        $total = Routes::count();
    }

    if(!is_array($resp)){
        $resp=$resp->toArray();
    }
    foreach ($resp as $k => $v) {
      
       foreach ($v as $k2 => $v2) {
            
            if($k2 == 'routeId'){
            
                $response[$k]['routeId']=$v['routeId'];
            
            }if($k2 == 'routeState2'){
            
                $response[$k]['routeState2']=$v['routeState2'];
            
            }else if($k2 == 'routeState'){
            
                $response[$k]['routeState']=$v['routeState'];
            
            }else{
               
                switch ($k2) {
                    case 'routeName':
                        $response[$k]['route']='<div style="color:#000;font-size: 1.2rem;">'.$v2;
                    break;
                    case 'routeDirection2':
                        $response[$k]['route']=$response[$k]['route'].' <small>'.$v2.'</small></div>';
                    break;
                    case 'route_routeprincipalsId':
                        
                        $v4='';
                        $dat=explode(',',$v2);
                        
                        for ($i=0; $i < count($dat) ; $i++) {
                            
                            $v3 = DB::table('routeprincipals')->where('routeprincipalsId',$dat[$i])->pluck('routeprincipalsName');
                            
                            if(count($v3)> 0 ){
                                if(!empty($v3[0])){
                                    if(!empty($v4)){
                                        $v4=$v4.'-'.$v3[0];
                                    }else{
                                        $v4=$v3[0];
                                    }
                                }
                            }
                        }
                        $response[$k]['route']=$response[$k]['route'].'<div style="color:#4fb933">'.$v4.'</div>';
                        
                    break;
                    case 'routePrice':
                        $response[$k]['route']=$response[$k]['route'].'Precio: <small style="color:#ff7a36">$ '.$v2.' '.env('PAYU_DFL_CURRENCY').'</small><BR>';
                    break;
                    case 'numLocations':
                        $response[$k]['route']=$response[$k]['route'].'Paradas: <small style="color:#ff7a36">'.$v2.' Paradas</small>';
                    break;
                    case 'numvehicle':
                        $response[$k]['route']=$response[$k]['route'].'<br>Asignados: <small style="color:#ff7a36">'.$v2.' Vehiculos</small>';
                    break;

                }
            }
       }
 
    }   
    $response2['recordsTotal'] = count($response);
    $response2['recordsFiltered'] = $total;
    $response2['data'] = $response;
       
    return $response2;

}

/**************************************************/

    public function search(Request $request){

        if(!empty($request->input())){

            $data = $request->input();

            $data = Routes::search($data);

        }else{
            
            $data = Routes::search();
        }
        if($data == 'no data'){
            return Messages::message(40020);  
        }

        if(is_array($data)){
               $data = $this->prepareRprincipal($data,true);
        }else{
             $data = $this->prepareRprincipal($data->toArray(),true);
        }
       

        if($data){
            $res = Messages::message(10000,$data);
        }else{
            $res = Messages::message(10002);  
        }

        

        return $res;
    }

/**************************************************/

    public function bringRlast(Request $request){
        
        if(!empty($request->input('routeId'))){

            $data['routeIdNot']=$request->input('routeId');
            $data['routeDirection']=$request->input('routeDirection');

            $data = Routes::search($data);

            if($data){
                $res = Messages::message(10000,$data);
            }else{
                $res = Messages::message(10002);  
            }

        }else{
            $res = Messages::message(10001);   
        }

        return $res;
        
    }

/**************************************************/

    public function saveAssignment(Request $request){

        if($request->input()){

            $validateRArelation=Routes::validateRArelation($request->input());

            if($validateRArelation == true){

               $res = Messages::message(40007);  
            }else{

                $data=array(

                    'routeassignmentFirst_routeId' => $request->input('routeassignmentFirst_routeId'),
                    'routeassignment_userId'=> $request->input('routeassignment_userId'),
                    'routeassignment_vehicleId'=> $request->input('routeassignment_vehicleId'),
                    'routeassignmentLast_routeId'=> $request->input('routeassignmentLast_routeId'),
                    'routeassignmentActive'=> 0,
                    'routeassignmentType'=> 0,
                    'created_by'=>Auth::user()->userId//sesion
                );

                $res=Routes::storeRArelation($data);

                if($res){
                    $res = Messages::message(10000);
                }else{
                    $res = Messages::message(20000);
                }
            }

        }else{
            $res = Messages::message(10001);  
        }

        return $res;     
    }

/**************************************************/

    public function destroy(Request $request)
    {
        if(!empty($request->input())){

            if(Routes::where('routeId',$request->input('routeId'))->count()>0)
            {
                $res = Routes::destroyed($request->input());

                if($res){
                    $res = Messages::message(10000);
                }else{
                    $res = Messages::message(40008);
                }
            }else
            {
                $res = Messages::message(20008);
            }
        }else{
            $res = Messages::message(10001);
        }
        
        return $res;
    }

/**************************************************/

    public function destroyAssignment(Request $request)
    {
        if(!empty($request->input())){

            $res = Routes::deleteRArelation($request->input());

            if($res){
                $res = Messages::message(10000);
            }else{
               $res = Messages::message(40012); 
            }
        
        }else{
            $res = Messages::message(10001);
        }

        return $res;
    }

/**************************************************/

    public function putAssignment(Request $request)
    {
        if(!empty($request->input())){

            if(array_key_exists('routeassignmentId', $request->input())) {

                $data=array(

                    'routeassignmentId' => $request->input('routeassignmentId'),
                    'routeassignment_userId'=> $request->input('routeassignment_userId'),
                    'routeassignment_vehicleId'=> $request->input('routeassignment_vehicleId'),
                    'routeassignmentLast_routeId'=> $request->input('routeassignmentLast_routeId'),
                    'updated_by'=>Auth::user()->userId//sesion
                    );

                $res=Routes::validateRArelation($data,true);
               
                if($res != true){

                    $res=Routes::putAssignment($data);

                    if($res){
                        $res = Messages::message(10000);
                    }else{
                        $res = Messages::message(10002);
                    }
                }else{
                    $res=Messages::message(40007);
                }
            
            }else{
                $res=Messages::message(40009);
            }
            
        }else{
            $res = Messages::message(10001);
        }

        return $res;

    }

/**************************************************/

    public function bringVbyroute(Request $request){

        if(!empty($request->input('routeId'))){

            $data = Routes::bringVbyroute($request->input('routeId'));

            if($data){
                
                $data = $this->prepareRprincipal($data,true);

                if($data){
                     $res = Messages::message(10000,$data); 
                }else{
                    $res = Messages::message(40010);
                }

            
            }else{
                $res = Messages::message(40011); 
            }

        }else{
        
            $res = Messages::message(10001);
        
        }

        return $res;

    }

/**************************************************/

    public function bringLbyroute(Request $request){

        if(!empty($request->input('routeId'))){

            $data = Localization::bringLbyroute($request->input());

            $data = $this->prepareRprincipal($data,true);

            if($data){
                $res = Messages::message(10000,$data); 
            }else{
                $res = Messages::message(40010);
            }


        }else{
        
            $res = Messages::message(10002);
        }

        return $res;


    }

/**************************************************/

    public function activateAssignment(Request $request){

        if(!empty($request->input('routeassignmentId'))){
        
            $res=Routes::activateAssignment($request->input());

            if($res){
                $res = Messages::message(10000);
            }else{
                $res = Messages::message(10002);
            }

        }else{
        
            $res = Messages::message(10001);
        }

        return $res;
    }

/**************************************************/

    public function bringAssignment(Request $request){

        if(!empty($request->input('routeId'))){

            $data = Routes::bringAssignment($request->input('routeId'));

            if($data){
                $res = Messages::message(10000,$data); 
            }else{
                $res = Messages::message(40016);
            }


        }else{
        
            $res = Messages::message(10001);
        }

        return $res;
    }
/**************************************************/

    public function validatelocalization(Request $request){

        if(array_key_exists('localizationAddress', $request->input())){

            $res=Routes::validatelocalization($request->input('localizationAddress'));

            if($res == 0){

                $res = Messages::message(40018);
            
            }else{
                $res = Messages::message(40017);
            }

        }else if(array_key_exists('localizationLatitude', $request->input()) && array_key_exists('localizationLongitude', $request->input())){
            
            $res=Routes::validatelocalization('',$request->input('localizationLatitude'),$request->input('localizationLongitude'));

            if($res->count() > 0){
               
                 $res = Messages::message(40018,$res);
            
            }else{
                $res = Messages::message(40019);
            }

            
                    
        }else{
            $res = Messages::message(10001);
        }

        return $res;
    
    }
/**************************************************/

    public function validateAddress(Request $request){

        if(array_key_exists('localizationAddress', $request->input())){

            $res=Routes::validateAddress($request->input());

            if($res == 0){

                $res = Messages::message(40018);
            
            }else{
                $res = Messages::message(40017);
            }

        }else{
            $res = Messages::message(10001);
        }

        return $res;
    
    }

/**************************************************/

    public function bringVavailable()
    {
        $data=Vehicle::bringVavailable();
        
        if($data){
            $res = Messages::message(10000,$data);
        }else{
            $res = Messages::message(10002,$data);
        }
        return $res; 
    }

/**************************************************/
    
    public function bringDrivers(){

        $data=User::bringDrivers();
        
        if($data){
            $res = Messages::message(10000,$data);
        }else{
            $res = Messages::message(10002);  
        }

        return $res;
    }

    /**************************************************/
    
    public function beginRoute(Request $request){

        if(array_key_exists('routeassignmentId', $request->input())){
            
            $res=Routes::beginRoute($request->input());
        
            if($res){
                $res = Messages::message(10000);
            }else{
                $res = Messages::message(50001);  
            }
        }else{
            $res = Messages::message(50001);
        }

        return $res;
    }

    /**************************************************/

    public function getWayPoint(Request $request)
    {

        $data  = Routes::localization_data($request->input('route_routeId'),'asc');
        if($data->count()>0)
        {
            $firts = Routes::localization_data($request->input('route_routeId'),'asc');
            $last = Routes::localization_data($request->input('route_routeId'),'desc');
            $data = $data->get(); 
            $data = $data->toArray(); 
            $firts = $firts->first();
            $firts = Gmaps::getLocalization($firts['localizationLatitude'], $firts['localizationLongitude']);
            $last  = $last->first();
            $last  = Gmaps::getLocalization($last['localizationLatitude'], $last['localizationLongitude']);
            $dir=array();
            $e=0;
            for ($i=0; $i < count($data); $i++) 
            { 
                $d = Gmaps::getLocalization($data[$i]['localizationLatitude'], $data[$i]['localizationLongitude']);
                $dir[$e]=$d['results'][1]['formatted_address'];
                $e++;
            }
            $f = $firts['results'][1]['formatted_address'];
            $l = $last['results'][1]['formatted_address'];
            return Gmaps::getDirections($dir, $f, $l);
        }
        else
        {
            return Messages::message(40021);

        }
    }
    /**************************************************/

    public function getRprincipals(Request $request)//route_routeprincipalsId:1,2
    {

        $cont = DB::table('routeprincipals')->count();

        if($cont > 1){
            
            $data = DB::table('routeprincipals')->select('routeprincipalsId','routeprincipalsName')->get();
                    
            $res = Messages::message(10000,$data);
        
        }else{
             $res = Messages::message(40004);
        }
  
        return $res;
    }
    /**************************************************/  

    public function findWheel(Request $request)
    {
        
        if(array_key_exists('routewheelDirection', $request->input()) && array_key_exists('routewheel_routeprincipalsId', $request->input())
            && array_key_exists('localizationLatitude', $request->input()) && array_key_exists('localizationLongitude', $request->input())){

           $res=Routes::findWheel($request->input());
           

            if($res!=false && count($res) > 0 ){

                $sort=array();
                $res2=array();
                $res3=array();
                
                foreach ($res as $k => $v) {
                    
                    $v->routewheelRoute=json_decode($v->routewheelRoute);
                    $porcentage=similar_text($request->input('routewheel_routeprincipalsId'),$v->routewheel_routeprincipalsId);
                    
                    if(array_key_exists($porcentage, $sort)){

                        $i=(0.1);
                        
                        while (array_key_exists(strval($porcentage), $sort)):

                            $porcentage=($porcentage+$i);
                            $i=$i+(0.1);
                          
                            

                        endwhile;

                        $sort[strval($porcentage)]=$res[$k];
                       
                    }else{
                        
                        $sort[strval($porcentage)]=$res[$k]; 
                    }
                    

                }
                krsort($sort);

                foreach ($sort as $k => $v) {
                    $object = (object) $sort[$k];
                    $res2[count($res2)]=$object;
                }
                
            
                
                $res = Messages::message(10000,$res2);

            }else{
                $res = Messages::message(80001);  
            }
        }else{
            $res = Messages::message(10002);
        }

        return $res;
    }


    /**************************************************/  

    public function findRoutes()
    {

        $username='concat("userFirstname",'."' '".',"userLastname")';
        $concat="'".url('/').'/'."'";


        $sql='select "userFirstname","userLastname",
            concat('.$concat.',"u"."userImage") as "userImage",
            count("routeId") as "numLocations",
            count(DISTINCT"routeassignment_vehicleId") as "numvehicle",
            "v"."vehiclePlaque",("v"."vehicleCapacity"-"v"."vehicleOcupation") as "vehicleDifference",
            "v"."vehicleLatitude","v"."vehicleLongitude",
            trim(trailing from "route_routeprincipalsId") AS "route_routeprincipalsId","route"."created_by","routeId","routeName","routeDirection","routePrice"
            from "route" 
            left join "route_localization" as "rl" on "rl"."route_routeId" = "routeId" 
            Inner join "routeassignment" as "ra" on "ra"."routeassignmentFirst_routeId" = "routeId" and "ra"."routeassignmentActive"=1 or "ra"."routeassignmentLast_routeId" = "routeId" and "ra"."routeassignmentActive"=2 
            left join "vehicle" as "v" on "ra"."routeassignment_vehicleId" = "v"."vehicleId"
            inner join "user" as "u" on "u"."userId" = "ra"."routeassignment_userId" 
            where ("v"."vehicleCapacity"-"v"."vehicleOcupation") > 0 
            group by "v"."vehicleLatitude","v"."vehicleLongitude",concat('.$concat.',"u"."userImage"),"v"."vehiclePlaque",("v"."vehicleCapacity"-"v"."vehicleOcupation"),"userFirstname","userLastname" ,"route_routeprincipalsId", "route"."created_by", "routeId", "routeName" order by "routeName" asc';

        $res= DB::select($sql);
 
       
        $res = Messages::message(10000,$res);
       

        return $res;
    }

/**************************************************/

 public function storeRoutewheel(Request $request)
    {
         date_default_timezone_set('America/Bogota');

        if(array_key_exists('routewheelRoute', $request->input()) && array_key_exists('userId', $request->input()) && array_key_exists('routewheel_routeprincipalsId', $request->input()) && array_key_exists('routewheelDirection', $request->input())
            && array_key_exists('vehicleOcupation', $request->input()) && array_key_exists('vehicleId', $request->input())){

            $data=array(
                    'routewheelRoute' => json_encode($request->input('routewheelRoute')),
                    'routewheel_routeprincipalsId' => $request->input('routewheel_routeprincipalsId'),
                    'routewheelState' => 1,
                    'routewheelDirection' => $request->input('routewheelDirection'),
                    'created_by'    => $request->input('userId'),
                    'created_at'    => date('Y-m-d H:i:s')
                    );

            $routewheelId=Routes::storeRoutewheel($data);

            $routeassignmentId=Routes::getAssignmentbyuser($request->input('userId'));

            if((is_int($routewheelId) && $routewheelId > 0) && (is_int($routewheelId) && $routewheelId > 0)){

                $data=array(
                    'vehicleId'                 => $request->input('vehicleId'),
                    'vehicleOcupation'          => $request->input('vehicleOcupation'),
                    'updated_at'                => date('Y-m-d H:i:s')
                );

              $res = Vehicle::put($data); 

                $routeassignmentFirst_routeId=$routewheelId;
                $routeassignmentLast_routeId=0;
               
                $data=array(

                    'routeassignmentId' => $routeassignmentId,
                    'routeassignmentFirst_routeId' => $routeassignmentFirst_routeId,
                    'routeassignmentLast_routeId'=> $routeassignmentLast_routeId,
                    'routeassignmentActive'=> 1,
                    'routeassignmentType'=> 1,
                    'updated_by'    =>$request->input('userId'),
                    'updated_at'    => date('Y-m-d H:i:s')
                );

                $res=Routes::putAssignment($data);

                if($res){
                    $res = Messages::message(10000,$data);
                }else{
                    $res = Messages::message(10002);
                }
                
            }else{
                $res = Messages::message(40023);
            }
           

        }else{
            $res = Messages::message(40023);
        }
         
        return $res;
        
    }

    /**************************************************/

     public function bringlocalization(){

        $data = Localization::bringlocalization();

        if($data){

            $res = Messages::message(10000,$data);

        }else{//10002

            $res = Messages::message(10002,$data);
            
        }

        return $res;

    }

/**************************************************/


}