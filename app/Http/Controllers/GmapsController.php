<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Appitventures\Phpgmaps\Phpgmaps;
use App\Gmaps;

class GmapsController extends Controller
{
   

    public function index(){

        $data['origin'][0]          =4.6880573;
        $data['origin'][1]          =-74.0664687;
        $data['destinations'][0]    =4.7036638;
        $data['destinations'][1]    =-74.0709033;
        
        $res=Gmaps::get_time($data);

        return $res;
    }

}
