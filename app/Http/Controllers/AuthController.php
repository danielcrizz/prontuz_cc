<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use Redirect;
use App\RolMenu;
use App\User;
use App\Messages;
use App\Http\Requests;
use App\Http\Requests\AuthRequest;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    public function getView(){
        return view('login');
    }

    public function logIn(Request $request)
    {
        $empty=''; 
        $validate = false;
        if($request['userEmail']=='' && $request['password']=='')
        {
            return Messages::message(20001);
        }
        if($request['userEmail']!='' && $request['password']=='')
        {
            return Messages::message(20002);
        }
        if($request['userEmail']=='' && $request['password']!='')
        {
            return Messages::message(20003);
        }
        if(User::where(['userEmail'=>$request['userEmail']])->where(['userState'=>1])->count()>0)
        {
            if($request['typePlataform']!=null || $request['typePlataform']!='')
            {
                if($request['typePlataform']=='web')
                {
                    if(Auth::attempt(['userEmail'=>$request['userEmail'],'password'=>$request['password'],'userState'=>1,'user_rolId'=>1],true) && $request['typePlataform']=='web')
                    {
                           $menu=RolMenu::select('menu.menuName', 'menu.menuPrefix', 'menu.menuIcon')->join('menu', 'menu.menuId','=','rolmenu.rolmenu_menuId')->where("rolmenu.rolmenu_userId",'=',Auth::user()->userId)
                                 ->where('rolmenu.rolmenu_rolId','=',1)->orderBy('menu.menuId', 'Asc')->get();
                            Session::put(['userMenu'=>$menu->toArray()]);
                            return Messages::message(10000,Auth::user());  
                    }
                    else
                    {
                        return Messages::message(20005);
                    }
                }
                else
                {
                    
                    $user = User::where(['userEmail'=>$request['userEmail']])->where(['userState'=>1])->get();
                    $user = $user->toArray();
                    $user = $user[0];
                    if (Hash::check($request['password'],$user['password'])) 
                    {
                      
                        if(array_key_exists('user_rolId', $request->input())  && ($request->input('user_rolId') == 2 || $request->input('user_rolId') == 4)){
                            
                            if(User::where(['userEmail'=>$request['userEmail']])->where(['user_rolId'=>$request['user_rolId']])->count() > 0)
                            {
                                if(User::where(['userEmail'=>$request['userEmail']])->where(['user_rolId'=>$request['user_rolId']])->where('remember_token','=',$empty)->count() > 0)
                                {
                                    $validate = true;
                                
                                }else{
                                    return Messages::message(20026);
                                        
                                }
                                
                            }else{
                                return Messages::message(20022);
                            }  
                        }else{

                           if(User::where(['userEmail'=>$request['userEmail']])->whereIn('user_rolId',array(3,4))->count() > 0)
                            {

                                if(User::where(['userEmail'=>$request['userEmail']])->whereIn('user_rolId',array(3,4))->where('remember_token','=',$empty)->count() > 0)
                                {
                                    $validate = true;   
                                
                                }else{
                                    return Messages::message(20026);
                                      
                                }
                           
                            }else{
                                return Messages::message(20022);
                            }
                            
                        } 

                        if($validate == true){
                            if(Auth::attempt(['userEmail'=>$request['userEmail'],'password'=>$request['password'],'userState'=>1],true) && $request['typePlataform']=='movil')
                            {
                                Auth::user()->userImage = url('/').'/'.Auth::user()->userImage;
                                return Messages::message(10000,Auth::user());
                            }
                            else
                            {
                                return Messages::message(20005);
                            }  
                        }else{
                            return Messages::message(20022);
                        } 
                    }
                    else
                    {
                        return Messages::message(10009);
                    }
                
                }
            }
            else
            {
                return Messages::message(20010);                    
            }
        }
        else
        {
            return Messages::message(20004);
        }
       
    }
    public function logOut(Request $request)
    {
        $empty='';
        if($request['userId']>0)
        {
            if(User::where('userId',$request->input('userId'))->count() > 0)
            {
                
                User::where('userId',$request->input('userId'))->update(['remember_token'=>$empty, 'userToken'=>$empty]);
                Auth::logout();
                Session::flush();
                if($request['typePlataform']=='movil')
                {
                    return Messages::message(10000);
                }
                if($request['typePlataform']=='web')
                {
                    return Messages::message(10000);
                }
                else
                {
                    return Messages::message(20010);                    
                }
            }
            else
            {
                return Messages::message(10007);
            }
        }
        else
        {
            if(User::where('userEmail',$request->input('userEmail'))->count() > 0)
            {
                User::where('userEmail',$request->input('userEmail'))->update(['remember_token'=>$empty, 'userToken'=>$empty]);
                Auth::logout();
                Session::flush();
                if($request['typePlataform']=='movil')
                {
                    return Messages::message(10000);
                }
                if($request['typePlataform']=='web')
                {
                    return Messages::message(10000);
                }
                else
                {
                    return Messages::message(20010);                    
                }
            }
            else
            {
                return Messages::message(10007);
            }
        }
    }
}
