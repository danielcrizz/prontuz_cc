<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use Redirect;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Gmaps;
use App\Vehicle;
use App\Localization;
use App\Line;
use App\Routes;
use App\routeassingment;
use App\Messages;//messages
use DB;
use App\Http\Controllers\RoutesController;


class VehiclesController extends Controller
{
    //

    public function search(Request $request)
    {
    	if(!empty($request->input())){

    		$data = $request->input();

    		$data = Vehicle::search($data);

    	}else{
    	
    		$data = Vehicle::getAll();
	
    	}

    	$res = Messages::message(10000,$data);

		return $res;

    }

/**************************************************/

    public function store(Request $request)
    {
        date_default_timezone_set('America/Bogota');
    	#Validations
    	$a=false;
    	$b=false;
    	$c=false;
    	$d=false;

    	$vehicleNsoat=$request->input('vehicleNsoat');
    	$vehicleTecnomecanica=$request->input('vehicleTecnomecanica');
    	$vehicleNoperation=$request->input('vehicleNoperation');
    	$vehicleNinside=$request->input('vehicleNinside');

    	

    	if(empty($vehicleNsoat)){
			
			$res=Messages::message(30004);

    	}else{

			$data=Vehicle::validateVsoat($vehicleNsoat);	
			
			if($data > 0){	//if(($data->count()) > 0){	
        		$res=Messages::message(30003);
    		}else{
    			$a=true;
    		}
		}

		// if(empty($vehicleTecnomecanica)){
		// 	$res=Messages::message(30006);
  //   	}else{

		// 	$data=Vehicle::validateVtecnomecanica($vehicleTecnomecanica);	
			
		// 	if($data > 0){	//if(($data->count()) > 0){	
  //       		$res=Messages::message(30005);
  //   		}else{
    			$b=true;
  //   		}
		// }

		if(empty($vehicleNoperation)){
			$res=Messages::message(30008);
    	}else{

			$data=Vehicle::validateVnoperation($vehicleNoperation);	
			
			if($data > 0){	//if(($data->count()) > 0){	
        		$res=Messages::message(30007);
    		}else{
    			$c=true;
    		}
		}
		

		if(empty($vehicleNinside)){
			$res=Messages::message(30010);
    	}else{

			$data=Vehicle::validateVninside($vehicleNinside);	
			
			if($data > 0){	//if(($data->count()) > 0){	
        		$res=Messages::message(30009);
    		}else{
    			$d=true;
    		}
		}

		
    	
    	if(($a==true) && ($b==true) && ($c==true) && ($d==true)){

			$data=array(
	    		'vehicle_vehiclebrandId'	=> $request->input('vehicle_vehiclebrandId'),
		    	'vehicleModel'				=> $request->input('vehicleModel'),
		    	'vehicle_vehicletypeId'		=> $request->input('vehicle_vehicletypeId'),
		    	'vehicleTecnomecanica'		=> date('Y-m-d', strtotime(str_replace('/', '-', $request->input('vehicleTecnomecanica')))),
                //$request->input('vehicleTecnomecanica'),
		    	'vehicleNoperation'			=> $request->input('vehicleNoperation'),//
		    	'vehicleNinside'			=> $request->input('vehicleNinside'),//
		    	'vehicleCapacity'			=> $request->input('vehicleCapacity'),
		    	'vehicleNsoat'				=> $request->input('vehicleNsoat'),
                // 'vehicleExpiration'         => $request->input('vehicleExpiration'),
		    	'vehicleExpiration'			=> date('Y-m-d', strtotime(str_replace('/', '-', $request->input('vehicleExpiration')))),
		    	'vehicle_lineId'			=> $request->input('vehicle_lineId'),
		    	'vehicleLatitude'			=> 0,
		    	'vehicleLongitude'			=> 0,
		    	'vehicleOcupation'			=> $request->input('vehicleOcupation'),
		    	'vehiclePlaque'				=> $request->input('vehiclePlaque'),
                'vehicleState'              => $request->input('vehicleState'),
                'vehicle_locationId'        => 0,
		    	'vehicleBearing'     		=> 0,
	    		'created_by'				=> Auth::user()->userId,
                'created_at'                => date('Y-m-d H:i:s')
    		);

	    	//return $data;
	    	$res=Vehicle::store($data);

	    	if($res){
				$res = Messages::message(10000,$res);
	    	}else{
				$res = Messages::message(20000);
	    	}

    	}

    	

    	return $res;
    }

/**************************************************/

public function listing(Request $request)
{
    $resp= array();
    $response= array();
    $response2= array();
    
    if($request['search']['value']!='' || $request['search']['value']!=null)
    {

        $resp=Vehicle::search(array(
                        'dataSearch'=>$request['search']['value'],
                        'vehicleState'=>1,
            )
        );
        //$total = Vehicle::count();
        $total=count($resp);
    }
    else
    {
        $resp=Vehicle::listing($request->input());
        $total=Vehicle::count();
        //$total=count($resp);
    }

    foreach ($resp->toArray() as $k => $v) {
      
       foreach ($v as $k2 => $v2) {
            
            if($k2 == 'vehicleId'){
            
                $response[$k]['vehicleId']=$v['vehicleId'];
            
            }if($k2 == 'vehicleState2'){
            
                $response[$k]['vehicleState2']=$v['vehicleState2'];
            
            }else if($k2 == 'vehicleState'){
            
                $response[$k]['vehicleState']=$v['vehicleState'];
            
            }else{
               
                switch ($k2) {
                    case 'vehiclePlaque':
                        $response[$k]['vehicle']='<div style="color:#000;font-size: 1.2rem;">'.$v2.'</div>';
                    break;
                    case 'vehiclebrandName':
                        $response[$k]['vehicle']=$response[$k]['vehicle'].$v2;
                    break;
                    case 'lineName':
                        $response[$k]['vehicle']=$response[$k]['vehicle'].'-'.$v2;
                    break;
                    case 'vehicleModel':
                        $response[$k]['vehicle']=$response[$k]['vehicle'].'-'.$v2;
                    break;
                    case 'vehicletypeName':
                        $response[$k]['vehicle']=$response[$k]['vehicle'].'<br><div style="color:#4fb933">'.$v2;
                    break;
                    case 'vehicleCapacity':
                        $response[$k]['vehicle']=$response[$k]['vehicle'].'-'.$v2.'</div>';
                    break;
                    case 'vehicleNsoat':
                        $response[$k]['vehicle']=$response[$k]['vehicle'].'SOAT:'.$v2;
                    break;
                    case 'vehicleTecnomecanica':
                        $response[$k]['vehicle']=$response[$k]['vehicle'].'<BR>TÉCNOMECANICA:'.$v2;
                    break;
               }
            }
       }
 
    }   

    $response2['recordsTotal'] = count($resp);
    $response2['recordsFiltered'] = $total;
    $response2['data'] = $response;
       
    return $response2;
}

/**************************************************/

    public function bringVBrand(){

    	$data = DB::table('vehiclebrand')->pluck('vehiclebrandId', 'vehiclebrandName');

    	$res = Messages::message(10000,$data);

    	return $res;
    } 

/**************************************************/

    public function bringVType(){

		$data = DB::table('vehicletype')->pluck('vehicletypeId', 'vehicletypeName');
    	
    	$res = Messages::message(10000,$data);
    	
    	return $res;
    }

/**************************************************/

	public function bringVLine(Request $request){

		$res=false;
		
		$line_vehiclebrandId=$request->input('line_vehiclebrandId');

		if(!empty($line_vehiclebrandId))
        {
    		$data = line::select('lineId', 'lineName')->where('line_vehiclebrandId',$request['line_vehiclebrandId'])->get();

    		if($data == null){
				$res = Messages::message(30000);
    		}else{
	    		if(($data->count()) > 0){
	    			$res = Messages::message(10000,$data);
	    		}else{
	    			$res = Messages::message(30000);
				}
			}
    		
        }else{
        	$res = Messages::message(10001);
        }
		
		return $res;
	}

/**************************************************/

	public function validateVplaque(Request $request){

		$res=false;
		
		$vehiclePlaque=$request->input('vehiclePlaque');

		if(!empty($vehiclePlaque))
        {
            $res = DB::table('vehicle')->where('vehiclePlaque', $vehiclePlaque)->count();
        	
        	
        }else{
        	$res = Messages::message(10001);
    	}
		
		return $res;

	}

/**************************************************/

	public function put(Request $request)
    {

    	$z=false;
    	$a=false;
    	$b=false;
    	$c=false;
    	$d=false;
    	$res=false;

    	$vehiclePlaque=$request->input('vehiclePlaque');
    	$vehicleId=$request->input('vehicleId');
    	$vehicleNsoat=$request->input('vehicleNsoat');
    	$vehicleTecnomecanica=$request->input('vehicleTecnomecanica');
    	$vehicleNoperation=$request->input('vehicleNoperation');
    	$vehicleNinside=$request->input('vehicleNinside');

    	

    	if(empty($vehiclePlaque)){
			$res=Messages::message(30011);

    	}else{

    		$data = DB::table('vehicle')
                 ->where('vehiclePlaque', '=', $vehiclePlaque)
                 ->where('vehicleId', '!=', $vehicleId)
                 ->count();

            if($data == 0){	
        			$z=true;
    		}else{
    			$res=Messages::message(30001);
    		}
		}
		if(empty($vehicleNsoat)){
			$res=Messages::message(30004);

    	}else{
    			$data = Vehicle::validateVsoat($vehicleNsoat,1,$vehicleId);
		
            if($data == 0){	
        			$a=true;
    		}else{
    			$res=Messages::message(30003);
    		}
		}

		// if(empty($vehicleTecnomecanica)){
		// 	$res=Messages::message(30006);
  //   	}else{

		// 	$data=Vehicle::validateVtecnomecanica($vehicleTecnomecanica,1,$vehicleId);

  //          	if($data == 0){	
        			$b=true;
  //   		}else{
  //   			$res=Messages::message(30005);
  //   		}
		// }

		if(empty($vehicleNoperation)){
			$res=Messages::message(30008);
    	}else{

			$data = Vehicle::validateVnoperation($vehicleNoperation,1,$vehicleId);
			
           	if($data == 0){	
        			$c=true;
    		}else{
    			$res=Messages::message(30007);
    		}

		}
		
		if(empty($vehicleNinside)){
			$res=Messages::message(30010);
    	}else{

			$data = Vehicle::validateVninside($vehicleNinside,1,$vehicleId);

           	if($data == 0){	
        			$d=true;
    		}else{
    			$res=Messages::message(30009);
    		}
		}

		
    	if(($z==true) && ($a==true) && ($b==true) && ($c==true) && ($d==true)){

                
     
			$data=array(
	    		'vehicleId'					=> $request->input('vehicleId'),
	    		'vehicle_vehiclebrandId'	=> $request->input('vehicle_vehiclebrandId'),
		    	'vehicleModel'				=> $request->input('vehicleModel'),
		    	'vehicle_vehicletypeId'		=> $request->input('vehicle_vehicletypeId'),
                'vehicleTecnomecanica'      => date('Y-m-d', strtotime(str_replace('/', '-', $request->input('vehicleTecnomecanica')))),
                //'vehicleTecnomecanica'		=> $request->input('vehicleTecnomecanica'),//
		    	'vehicleNoperation'			=> $request->input('vehicleNoperation'),//
		    	'vehicleNinside'			=> $request->input('vehicleNinside'),//
		    	'vehicleCapacity'			=> $request->input('vehicleCapacity'),
		    	'vehicleNsoat'				=> $request->input('vehicleNsoat'),
		    	'vehicleExpiration'         => date('Y-m-d', strtotime(str_replace('/', '-', $request->input('vehicleExpiration')))),
                //'vehicleExpiration'			=> $request->input('vehicleExpiration'),
		    	'vehicle_lineId'			=> $request->input('vehicle_lineId'),
		    	'vehicleOcupation'			=> $request->input('vehicleOcupation'),
		    	'vehiclePlaque'				=> $request->input('vehiclePlaque'),
		    	'vehicleState'				=> $request->input('vehicleState'),
	    		'updated_by'				=> Auth::user()->userId,
                'created_at'                => date('Y-m-d H:i:s')
    		);

	    	//return $data;
	    	$res = Vehicle::put($data);	


	    	if($res){
				$res = Messages::message(10000);
	    	}else{
				$res = Messages::message(20000);
	    	}

    	}

    	return $res;

    }

/**************************************************/

    public function destroy(Request $request)
    {
    	if(Vehicle::where('vehicleId',$request->input('vehicleId'))->count()>0)
    	{
    		$res=Vehicle::destroyed($request->input());

    		if($res){
				$res = Messages::message(10000);
	    	}else{
				$res = Messages::message(20000);
	    	}
      	}
    	else
    	{
    		$res=Messages::message(20008);
    	}
    	return $res;
    }



/**************************************************/

    public function refreshVlocation(Request $request){

        if(!empty ($request->input())){

            if(array_key_exists('vehicleId', $request->input()))
            
                if(array_key_exists('user_rolId', $request->input()) && $request->input('user_rolId') == 3) {
                    $data=array(
                        'vehicleLatitude'           => $request->input('vehicleLatitude'),
                        'vehicleLongitude'          => $request->input('vehicleLongitude'),
                        'vehicle_locationId'        => $request->input('vehicle_locationId'),
                        'vehicleBearing'            => $request->input('vehicleBearing'),
                        'updated_by'                => $request->input('userId')
                    );

                }elseif(array_key_exists('user_rolId', $request->input()) && $request->input('user_rolId') == 4) {
                    
                    $data=array(
                        'vehicleLatitude'           => $request->input('vehicleLatitude'),
                        'vehicleLongitude'          => $request->input('vehicleLongitude'),
                        'vehicleBearing'            => $request->input('vehicleBearing'),
                        'updated_by'                => $request->input('userId')
                    );
                }

                $res=Vehicle::refreshVlocation($data,$request->input('vehicleId'));

                if($res){
                    $res = Messages::message(10000);
                }else{
                    $res = Messages::message(20000);
                }
           

        }else{

            $res = Messages::message(10001);
        }

        return $res;
    }

/**************************************************/
    
     public function bringOnelbyvehicle(Request $request){
        
        if(!empty ($request->input()) ){

            if(array_key_exists('vehicleId', $request->input())) { 

                 $res = Vehicle::select('vehicleId','vehicleLatitude','vehicleLongitude','vehicle_locationId','vehicleBearing')->where('vehicleId', $request->input('vehicleId'))->get()->toArray();

                 // DB::select('select"vehicleId","vehicleLatitude","vehicleLongitude","vehicle_locationId","vehicleBearing",
                 //                from "vehicle" 
                 //                where "vehicleId" = '.$request->input('vehicleId'));
                
                if(count($res) == 0){
                    $res = Messages::message(30013);
                }else{
                    $res = Messages::message(10000,$res[0]);
                }

            }else{

                $res = Messages::message(30012);
            }
        }else{

            $res = Messages::message(10001);
        }

        return $res;
    } 

    /**************************************************/
    
     public function bringTvehicle(Request $request){
        
        if(!empty ($request->input()) ){

            if(array_key_exists('vehicleId', $request->input())) { 
                $res = '';
                if ($request->input('localizationId') == 0) {
                    $vehicle = Vehicle::select('vehicleLatitude','vehicleLongitude')->where('vehicleId',$request->input('vehicleId'))->get()->toArray();
                    $res = json_decode(json_encode((object) array('vehicleLatitude'=>$vehicle[0]['vehicleLatitude'],
                        'vehicleLongitude'=>$vehicle[0]['vehicleLongitude'],
                        'localizationLatitude'=>floatval($request->input('localizationLatitude')),
                        'localizationLongitude'=>floatval($request->input('localizationLongitude'))
                    )));
                    $res = array($res);
                    $res = Messages::message(10000,array('rTime'=>Gmaps::get_time($res)));
                }
                else{
                    $vehicle = Vehicle::select('vehicleLatitude','vehicleLongitude')->where('vehicleId',$request->input('vehicleId'))->get()->toArray();
                    $location = Localization::select('localizationLatitude', 'localizationLongitude')->where('localizationId', $request->input('localizationId'))->get()->toArray();
                    $res = json_decode(json_encode((object) array('vehicleLatitude'=>$vehicle[0]['vehicleLatitude'],
                        'vehicleLongitude'=>$vehicle[0]['vehicleLongitude'],
                        'localizationLatitude'=>floatval($location[0]['localizationLatitude']),
                        'localizationLongitude'=>floatval($location[0]['localizationLongitude'])
                    )));
                    $res = array($res);
                    $res = Messages::message(10000,array('rTime'=>Gmaps::get_time($res)));
                }

            }else{

                $res = Messages::message(30012);
            }
        }else{

            $res = Messages::message(10001);
        }

        return $res;
    }   	

/**************************************************/

    public function bringVperfil(Request $request)
    {
        $RoutesController= New RoutesController();
        
        if(!empty($request->input('routeassignment_userId')) && !empty($request->input('user_rolId'))){

            if((DB::table('routeassignment')->where('routeassignment_userId', $request->input('routeassignment_userId'))->count()) > 0){

                $data=Vehicle::bringVperfil($request->input('routeassignment_userId'),$request->input('user_rolId'));

                if(is_array($data)){
                    $res = $RoutesController->prepareRprincipal($data,true);
                }else{
                    $res = $RoutesController->prepareRprincipal($data->toArray(),true);
                }

                if(count($res) == 0){
                    $res = Messages::message(10002);
                }else{
                    $res = Messages::message(10000,$res);
                }

            }else{
            
                $res = Messages::message(40012);
            }
            //;
        }else{
            $res = Messages::message(10001);
        }

        return $res;
    }

/**************************************************/

    public function validateVbrand(Request $request){

        $res=Vehicle::validateVbrand($request->input('vehiclebrandName'));
        
        if($res == 0){
            $res = Messages::message(10000);
        }else{
            $res = Messages::message(30014);
        }
        return $res; 

    }
/**************************************************/

    public function storeVbrand(Request $request){

        $data=array();

        $data['created_by']=Auth::user()->userId;
        $data['created_at']=date('Y-m-d H:i:s');
        $data['vehiclebrandName']=$request->input('vehiclebrandName');

        $data=Vehicle::storeVbrand($data);
        
        if($data){
            $res = Messages::message(10000);
        }else{
            $res = Messages::message(10002,$data);
        }
        return $res; 

    }
/**************************************************/

    public function validateVLine(Request $request){

        $res=Line::validateVLine($request->input('lineName'));
        
        if($res == 0){
            $res = Messages::message(10000);
        }else{
            $res = Messages::message(30015);
        }
        return $res; 

    }

/**************************************************/

    public function storeVline(Request $request){
        
        $vehiclebrandId=$request->input('vehiclebrandId');
        $lineName=$request->input('lineName');

        $created_at = DB::table('line')->where('line_vehiclebrandId', $vehiclebrandId)->max('created_at');
        $lineCode = DB::table('line')->where('line_vehiclebrandId', $vehiclebrandId)->where('created_at', $created_at)->value('lineCode');
      
        $data['lineCode']=($lineCode+1);
        $data['line_vehiclebrandId']=$vehiclebrandId;
        $data['lineName']=$lineName;
        $data['created_at']=date('Y-m-d H:i:s');
        $data['created_by']=Auth::user()->userId;
        
        $res=Line::storeVline($data);
        
        if($res){
            $res = Messages::message(10000);
        }else{
            $res = Messages::message(10002);
        }
        return $res; 

    }
/**************************************************/

    public function getWheel(Request $request){
        
        $data=$request->input();

        $routeassignment_userId = routeassingment::select('routeassignment_userId')->get();

        $res = DB::table('user')->where('userIdentificacion', $data['userIdentificacion'])
        ->where('user_rolId',4)
        ->whereNotIn('userId',$routeassignment_userId)->get();  
        
        $res2=$res->toArray();
        
        if(!empty($res2)){
            $res = Messages::message(10000,$res);
        }else{
            $res = Messages::message(10007); 
        }

        return $res;
    }

/**************************************************/

    public function storeAssignmentWheel(Request $request){

       if($request->input()){

            $data=array(

                'routeassignmentFirst_routeId' => 0,
                'routeassignment_userId'=> $request->input('routeassignment_userId'),
                'routeassignment_vehicleId'=> $request->input('routeassignment_vehicleId'),
                'routeassignmentLast_routeId'=> 0,
                'routeassignmentActive'=> 0,
                'routeassignmentType'=> 1,
                'created_by'=>Auth::user()->userId//sesion
            );

            $res=Routes::storeRArelation($data);

            if($res){
                $res = Messages::message(10000);
            }else{
                $res = Messages::message(20000);
            }
        }else{
            $res = Messages::message(10001);  
        }

        return $res;     
    }


}

