<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\FireBaseMessages;
use App\Messages;
class FireBaseMessageseController extends Controller
{
    public function sendOne(Request $request)
    {
    	$user=User::where('userId','=',$request->input('userId'));
    	if($user->count()>0)
    	{
    		$user=$user->get();
    		$user=$user->toArray();
    		$user=$user[0];
    		return FireBaseMessages::sendOne($user);
    	}
    	else
    	{
    		return Messages::message(10007);
    	}
    }
    public function sendMany(Request $request)
    {
    	return FireBaseMessages::sendMany($request);
    }
}
