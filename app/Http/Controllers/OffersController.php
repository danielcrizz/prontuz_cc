<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use Redirect;
use Storage;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\offer;
use App\Vehicle;
use App\Localization;
use App\Line;
use App\Routes;
use App\routeassingment;
use App\Messages;//messages
use DB;
use App\Http\Controllers\RoutesController;


class OffersController extends Controller
{
    //

public function search(Request $request)
{
	if(!empty($request->input())){

		$data = $request->input();

		$data = offer::search($data);

	}else{
        $concat="'".url('/').'/'."'";
	
		$data=DB::table('offer')->select(DB::raw('
            concat('.$concat.',"offerImage") as "offerImage","offerId","offerTitle","offerContent","offerCompany","offerEmail","offerValue","offerQuantity","offerAvailable","offerState"'))
            ->Where('offerState','=',1)
            ->Where('offerAvailable','>=',1)
            ->get();

        if(empty($data)){
            
            return Messages::message(90002);
        }

	}

	$res = Messages::message(10000,$data);

	return $res;

    }

/**************************************************/

    public function store(Request $request)
    {
        $res=false;
        $offerValue2='';
        $file=$request->file('offerImage');
    
        if(!empty($file)){
            $offerImage=Storage::disk('local')->put('tmp_files',$file);
        }
        
        $offerValue=explode('.',$request->input('offerValue'));

        for($i=0;$i<count($offerValue);$i++){
            $offerValue2=$offerValue2.$offerValue[$i];
        }

        $data=array(
            'offerTitle'        => $request->input('offerTitle'),
            'offerContent'      => $request->input('offerContent'),
            'offerCompany'      => $request->input('offerCompany'),
            'offerEmail'        => $request->input('offerEmail'),
            'offerQuantity'     => $request->input('offerQuantity'),
            'offerValue'        => floatval($offerValue2),
            'offerImage'        => $offerImage,
            'offerAvailable'    => $request->input('offerQuantity'),
            'offerState'        => $request->input('offerState'),
            'created_by'        => Auth::user()->userId,
            'created_at'        => date('Y-m-d H:i:s')
         );

         //return $data;
        $res=offer::store($data);

        if($res){
            $res = Messages::message(10000,$res);
        }else{
            $res = Messages::message(90001);
        }

     return $res;
   
    }


/**************************************************/

public function listing(Request $request)
{
    $resp= array();
    $response= array();
    $response2= array();


    
    if($request['search']['value']!='' || $request['search']['value']!=null)
    {

        $resp=offer::search(array(
                        'dataSearch'=>$request['search']['value'],
                        'offerState'=>1,
            )
        );
    
        $total=count($resp);
    }
    else
    {
        $resp=offer::listing($request->input());
        $total=DB::table('offer')->count();
 
    }

    foreach ($resp->toArray() as $k => $v) {
      
       foreach ($v as $k2 => $v2) {
            
            if($k2 == 'offerId'){
            
                $response[$k]['offerId']=$v->offerId;
            
            }if($k2 == 'offerImage'){
            
                $response[$k]['offerImage']=$v->offerImage;
            
            }if($k2 == 'offerState2'){
            
                $response[$k]['offerState2']=$v->offerState2;
            
            }else if($k2 == 'offerState'){
            
                $response[$k]['offerState']=$v->offerState;
            
            }else{
               
                switch ($k2) {
                    case 'offerTitle':
                        $response[$k]['offer']='<div style="color:#000;font-size: 1.2rem;">'.$v2.'</div>';
                    break;
                    case 'offerContent':
                        $response[$k]['offer']=$response[$k]['offer'].substr($v2, 0, 80);
                    break;
                    case 'offerValue':
                        $response[$k]['offer']=$response[$k]['offer'].'<br><div style="color:#4fb933"> VALOR: $'.$v2.'</div>';
                    break;
                    case 'offerQuantity':
                        $response[$k]['offer']=$response[$k]['offer'].'Cantidad:'.$v2;
                    break;
               }
            }
       }
 
    }   

    $response2['recordsTotal'] = count($resp);
    $response2['recordsFiltered'] = $total;
    $response2['data'] = $response;
       
    return $response2;
}

/**************************************************/
    
public function put(Request $request)
    {
        date_default_timezone_set('America/Bogota');
        $date=date('Y-m-d H:i:s');
        $res=false;
        $offerValue2='';

        $offerValue=explode('.',$request->input('offerValueEdit'));

        for($i=0;$i<count($offerValue);$i++){
            $offerValue2=$offerValue2.$offerValue[$i];
        }

        if(!empty($request->file('offerImageEdit'))){

            $file=$request->file('offerImageEdit');
            $res=Storage::disk('local')->put('avatar',$file);

            $data=array(
                'offerTitle'        => $request->input('offerTitleEdit'),
                'offerContent'      => $request->input('offerContentEdit'),
                'offerCompany'      => $request->input('offerCompanyEdit'),
                'offerEmail'        => $request->input('offerEmailEdit'),
                'offerQuantity'     => $request->input('offerQuantityEdit'),
                'offerValue'        => $offerValue2,
                'offerImage'        => $res,
                'offerAvailable'    => $request->input('offerQuantityEdit'),
                'offerState'        => $request->input('offerStateEdit'),
                'updated_by'        =>  Auth::user()->userId,
                'updated_at'         =>  $date
            );
            

        }else{
            $data=array(
                'offerTitle'        => $request->input('offerTitleEdit'),
                'offerContent'      => $request->input('offerContentEdit'),
                'offerCompany'      => $request->input('offerCompanyEdit'),
                'offerEmail'        => $request->input('offerEmailEdit'),
                'offerQuantity'     => $request->input('offerQuantityEdit'),
                'offerValue'        => $offerValue2,
                'offerAvailable'    => $request->input('offerQuantityEdit'),
                'offerState'        => $request->input('offerStateEdit'),
                'updated_by'        =>  Auth::user()->userId,
                'updated_at'        =>  $date
            );
        }

            
        $res=offer::put($data,$request->input('offerIdEdit'));


        if($res){
            return Messages::message(10000);
        }else{
            return Messages::message(20000);     
        }
        
    }  

}

