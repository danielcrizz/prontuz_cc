<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DocType;
use App\Messages;//messages
use DB;

class CommonController extends Controller
{
    //
    public function DocumentKind(Request $request){
    	
    	$res=false;

    	$data = DocType::get();

    	if($data == null){
  			$res = Messages::message(30000);
  		}else{
      		if(($data->count()) > 0){
      			$res = Messages::message(10000,$data);
      		}else{
      			$res = Messages::message(20009);
  			}
  		}
    
    return  $res;
    }
}
