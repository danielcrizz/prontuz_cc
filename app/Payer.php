<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payer extends Model
{
    protected $table='payer';
    protected $primaryKey ='payerId';
	protected $fillable =['payerName','payerEmail','payerPhone','payerIdentification','created_by','updated_by'];

}
