<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;

use Jenssegers\Mongodb\Eloquent\Model as Model;

use Carbon\Carbon;
use DateTime;

class Rol extends Model
{
    protected $table='rol';
    protected $primaryKey ='rolId';
	protected $fillable =['rolName','created_by','updated_by'];

	protected function getDateFormat()
    {
    return 'd.m.Y H:i:s';
     }
}
