<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\User;
use App\FireBaseMessages;
use PushNotification;

class payment extends Model
{
    //
    protected $table='payment';
    protected $primaryKey ='paymentId';
    protected $fillable =['paymentDebt','paymentPay','payment_userId','payment_wheel_userId','created_by','updated_by'];


    protected function store($data)
    {
        return DB::transaction(function () use ($data) 
        {
            $res=DB::table('payment')->insert($data);
            
            if($res){
                $vehicleId=0;
                $query='select distinct("uw"."userId"),"r1"."reserveId","vehicleId"
                from "user" as "uw"
                inner join "routeassignment" on "routeassignment_userId"="userId"
                inner join "vehicle" on "routeassignment_vehicleId"="vehicleId"
                inner join "reserve" as "r1" on "vehicleId"="r1"."reserve_vehicleId"
                inner join "user" as "ur" on "ur"."userId" = "r1"."reserve_UserId"
                WHERE "r1"."reserveState" = 4
                and "uw"."userId"='.$data['payment_wheel_userId'].'
                group by "uw"."userId","r1"."reserveId","vehicleId"';
                
                $query= DB::select($query);

                foreach($query as $k => $v){
                    $vehicleId=$v->vehicleId;
                    $res=DB::table('reserve')->where('reserveId',$v->reserveId)->update(['reserveState'=>5]);
                }

                $title='Estatus de Pagos';
                $msn='Señor(a) conductor se ha realizado un pago por el valor de $'.$data['paymentPay'];

                $res=User::select('userId','userFirstname','userLastname', 'userEmail', 'password', 'userIdentificacion','remember_token', 'userPhone','userLicence','userBorndate','user_doctypeId','user_rolId','userState','userStreet','userImage','userToken')->join('routeassignment','routeassignment.routeassignment_userId','=','userId')->where('routeassignment_vehicleId',$vehicleId)->get();
                    
                $res1=$res->toArray();

                FireBaseMessages::sendToDriver($res1[0],$title,$msn,array('reserveId'=>0, 'notificationtype' => '0', 'userId' => $data['payment_wheel_userId']));
            }


            return $res;
        });
    }

/**************************************************/

	protected function listing($data)
    {
    	

       //  switch($search){
       		
       // 		case 'routeName':
       // 			$search='route.routeName';
       // 		break;
       		
       // 		case 'routeState':
       // 			$search='route.routeState';
       // 		break;
       		
       // }

        if(array_key_exists('dataSearch', $data)){


           $dataSearch="'%".$data['dataSearch']."%'";

           $query='select distinct("user"."userId"),concat("ur"."userFirstname",\' \',"ur"."userLastname") AS "UserFullName","reserveState", to_char("r1"."updated_at", \'DD Mon YYYY\') as "date", CASE WHEN "routewheelDirection" = 0 THEN \'Rumbo Universidad\' ELSE \'Desde universidad\' END AS "routeDirection2", CASE WHEN "reserveState" = 4 THEN \'Pendiente\' ELSE \'Cancelado\' END AS "reserveState2","reservePrice"
       		from "user" 
       		inner join "routeassignment" on "routeassignment_userId" = "user"."userId" 
       		inner join "routewheel" on "routewheelId" = "routeassignmentFirst_routeId" and "routeassignmentType" = 1 
       		inner join "vehicle" on "routeassignment_vehicleId" = "vehicleId" 
       		inner join "reserve" as "r1" on "vehicleId" = "r1"."reserve_vehicleId" 
       		inner join "user" as "ur" on "ur"."userId" = "r1"."reserve_UserId" 
       		where "reserveState" > 3 and "user"."userId" = '.$data['datasearch'].' 
       		and concat("ur"."userFirstname",\' \',"ur"."userLastname") like '.$dataSearch.'
       		group by "user"."userId", concat("ur"."userFirstname",\' \',"ur"."userLastname"), to_char("r1"."updated_at", \'DD Mon YYYY\'), "reserveState", "routewheelDirection","reservePrice" order by "date" desc ';

         
        }else{
        	$orden  = $data['order']['0']['column'];
        	$ordenby= $data['order']['0']['dir'];
        	$search = $data['columns'][$orden]['data'];

        	$query='select distinct("user"."userId"),concat("ur"."userFirstname",\' \',"ur"."userLastname") AS "UserFullName","reserveState", to_char("r1"."updated_at", \'DD Mon YYYY\') as "date", CASE WHEN "routewheelDirection" = 0 THEN \'Rumbo Universidad\' ELSE \'Desde universidad\' END AS "routeDirection2", CASE WHEN "reserveState" = 4 THEN \'Pendiente\' ELSE \'Cancelado\' END AS "reserveState2","reservePrice" 
       		from "user" 
       		inner join "routeassignment" on "routeassignment_userId" = "user"."userId" 
       		inner join "routewheel" on "routewheelId" = "routeassignmentFirst_routeId" and "routeassignmentType" = 1 
       		inner join "vehicle" on "routeassignment_vehicleId" = "vehicleId" 
       		inner join "reserve" as "r1" on "vehicleId" = "r1"."reserve_vehicleId" 
       		inner join "user" as "ur" on "ur"."userId" = "r1"."reserve_UserId" 
       		where "reserveState" > 3 and "user"."userId" = '.$data['datasearch'].' 
       		group by "user"."userId", concat("ur"."userFirstname",\' \',"ur"."userLastname"), to_char("r1"."updated_at", \'DD Mon YYYY\'), "reserveState", "routewheelDirection","reservePrice" order by "date" desc limit '.$data['length'].' offset '.$data['start'];
        }
     
                
            $res=DB::select($query);

  
        return $res;
    }

/**************************************************/
	
	protected function getWheelBalance($data)
    {

    	$res1=User::select(DB::raw('distinct("userId"),
                sum(CAST("reservePrice" AS INTEGER) - CAST("reserveUtility" AS INTEGER)) AS "Balance"'))
            ->join('routeassignment', 'routeassignment_userId', '=', "userId")
            ->join('vehicle', 'routeassignment_vehicleId', '=',  "vehicleId")
            ->join('reserve', 'vehicleId', '=',  "reserve_vehicleId")
            ->where('reserveState',4)
            ->where('userId',$data['userId'])
            ->groupBy('userId')
            ->first();

        if($res1){
            
            $res = Messages::message(10000,$res1);
        }else{
            $res = Messages::message(10000,array('userId'=>$data['userId'],'Balance'=>0));
        }
        return $res;
    }

/**************************************************/

	protected function getWheelTrips($data)
    {

	    $query='select distinct("user"."userId"),count("ur"."userId") as "countUsers", to_char("r1"."updated_at", \'DD Mon YYYY\') as "date", 
             sum(CAST("r1"."reservePrice" AS INTEGER) - CAST("r1"."reserveUtility" AS INTEGER)) AS "Balance" ,"routewheelDirection"
   		from "user" 
   		inner join "routeassignment" on "routeassignment_userId" = "user"."userId" 
   		inner join "routewheel" on "routewheelId" = "routeassignmentFirst_routeId" and "routeassignmentType" = 1 
   		inner join "vehicle" on "routeassignment_vehicleId" = "vehicleId" 
   		inner join "reserve" as "r1" on "vehicleId" = "r1"."reserve_vehicleId" 
   		inner join "user" as "ur" on "ur"."userId" = "r1"."reserve_UserId" 
   		where "reserveState" =4 and "user"."userId" = '.$data['userId'].'
   		group by "user"."userId", to_char("r1"."updated_at", \'DD Mon YYYY\'),"routewheelDirection"
   		order by "date" desc';

        $res= DB::select($query);

        if($res){
            
            $res = Messages::message(10000,$res);
        }else{
            $res = Messages::message(80003);
        }
        return $res;
    }

/**************************************************/

}
