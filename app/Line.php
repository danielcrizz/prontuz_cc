<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class Line extends Model
{
    protected $table='line';
    protected $primaryKey ='lineId';
	protected $fillable =['lineCode','lineName','created_by','updated_by']; 

/**************************************************/

	protected function storeVline($data)
	{
		$res=DB::table('line')->insert($data);

		return $res;
	}

/**************************************************/

	protected function validateVLine($lineName)
    {
    	
    	$res=DB::table('line')->where('lineName','=', $lineName)->count();
    	
    	return $res; 
    }

}
