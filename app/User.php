<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;
use Mail;
use App\Mail\Confirmed;
use Cache;
use Carbon\Carbon;
use Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'user';  
    protected $primaryKey ='userId';
    protected $fillable = [
        'userFirstname','userLastname', 'userEmail', 'password', 'userIdentificacion','remember_token', 'userPhone','userLicence','userBorndate','user_doctypeId','user_rolId','userState','userStreet','userImage','created_by', 'updated_by','userId', 'userToken'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    protected function store($data)
    {
        return DB::transaction(function () use ($data) 
        {
            if(Auth::user() !=null || Auth::user()!='')
            {
                $data['created_by']=Auth::user()->userId;
            }  
            else
            {
                 $data['created_by']=1;
            }
            if($data['user_rolId']==1)
            {
                $user=User::fill($data)->create($data);
                $menu=array();
                sort($data['user_menu']);
                for ($i=0; $i < count($data['user_menu']); $i++)
                { 
                   $menu[$i]=RolMenu::create(array('rolmenu_menuId'=>$data['user_menu'][$i],'rolmenu_rolId'=>$data['user_rolId'],'rolmenu_userId'=>$user->userId,'created_by'=>$data['created_by']));
                }
                if(count($user)>0&& count($menu)>0)
                {
                    return Messages::message(10000);
                }
                else
                {
                    return Messages::message(20000);
                }
            }
            else
            {
                if(User::fill($data)->create($data))
                {
                    $this->sendMail(array('mail'=>$data['userEmail'],'view'=>'mail.welcomeProntuz', 'name'=>utf8_decode($data['userFirstname'].' '.$data['userLastname'])));
                    return Messages::message(10000);
                }
                else
                {
                    return Messages::message(20000);
                }
            }
        });
    }    

    protected function put($data,$menu='')
    {
        return DB::transaction(function () use ($data,$menu) 
        {
            if(Auth::user()->userId!='' || Auth::user()->userId !=null)
            {
                $data['created_by']=Auth::user()->userId;
            }  
            else
            {
                 $data['created_by']=1;
            }
            if($data['user_rolId']==1)
            {   
               if(count($menu) > 0)
                {

                    $user=User::where('userId','=', $data['userId'])->update($data);
                    $menu1=array();
                    sort($menu);
                    RolMenu::where('rolmenu_userId',$data['userId'])->delete();
                    for ($i=0; $i < count($menu); $i++)
                    { 
                        $menu1[$i]=RolMenu::create(array('rolmenu_menuId'=>$menu[$i],'rolmenu_rolId'=>$data['user_rolId'],'rolmenu_userId'=> $data['userId'],'created_by'=>$data['created_by']));
                    }
                    if(count($user)>0&& count($menu1)>0)
                    {
                        return Messages::message(10000);
                    }
                    else
                    {
                        return Messages::message(20000);
                    }
                }
                else
                {
                    if(User::where('userId','=', $data['userId'])->update($data) && RolMenu::where('rolmenu_userId',$data['userId'])->delete())
                    {
                        return Messages::message(10000);
                    }
                    else
                    {
                        return Messages::message(20000);
                    }
                }
            }
            else
            {
                if(User::where('userId','=', $data['userId'])->update($data))
                {
                    return Messages::message(10000);
                }
                else
                {
                    return Messages::message(20000);
                }
            }
        });
    }

    protected function listing($data)
    {
        $orden  = $data['order']['0']['column'];
        $ordenby= $data['order']['0']['dir'];
        $search = $data['columns'][$orden]['data'];
        $searching=$data['search']['value'];
        if($searching!='' || $searching !=null)
        {
            $data['dataSearch']=$searching;
            return $this->search($data);
        }
        else
        {        
            $username='concat("userFirstname",'."' '".',"userLastname") AS "FullName", "userEmail","userPhone","userImage",concat("doctypeInitial",'."'-'".',"userIdentificacion") AS "FullId","rolName","userId","userState"';
                if($search=='rolName')
                {
                    $search = 'rol.rolName';
                }
                else
                {
                    if($search == 'FullId')
                    {
                        $search='userIdentificacion';
                    }
                    if($search == 'FullName')
                    {
                        $search = 'user.userFirstname';
                    }
                    else
                    {
                        $search = 'user.'.$search;
                    }
                    
                }
                if($data['userState']=='')
                {
                    return User::select(DB::raw($username))
                           ->join('rol', 'rol.rolId','=', 'user.user_rolId')
                           ->join('doctype', 'doctype.doctypeId', '=', 'user.user_doctypeId')
                           ->orderBy($search,$ordenby)->get();
                }        
                else
                {
                    return User::select(DB::raw($username))
                           ->join('rol', 'rol.rolId','=', 'user.user_rolId')
                           ->join('doctype', 'doctype.doctypeId', '=', 'user.user_doctypeId')
                           ->skip($data['start'])->take($data['length'])
                           ->orderBy($search,$ordenby)
                           ->get();            
                }
        }
    }

    protected function search($data)
    {
            $username='concat("userFirstname",'."' '".',"userLastname") AS "FullName", "userEmail","userPhone","userImage",concat("doctypeInitial",'."'-'".',"userIdentificacion") AS "FullId","rolName","userId","userState"';

            return User::select(DB::raw($username))
               ->join('rol', 'rol.rolId','=', 'user.user_rolId')
               ->join('doctype', 'doctype.doctypeId', '=', 'user.user_doctypeId')
               ->orWhere('user.userFirstname','ilike','%'.$data['dataSearch'].'%')
               ->orWhere('user.userLastname','ilike','%'.$data['dataSearch'].'%')
               ->orWhere('user.userEmail','ilike','%'.$data['dataSearch'].'%')
               ->orWhere('user.userIdentificacion','ilike','%'.$data['dataSearch'].'%')
               ->orWhere('user.userPhone','ilike','%'.$data['dataSearch'].'%')
               ->skip($data['start'])->take($data['length'])
               ->orderBy('user.userIdentificacion','asc')
               ->orderBy('user.userIdentificacion','asc')
               ->get();
    }

    protected function destroyed($data)
    {
        return DB::transaction(function () use ($data) 
        {
            if(User::where('userId','=', $data['userId'])->update(['userState'=>'0','updated_by'=>Auth::user()->userId]))
            {
                return Messages::message(10000);
            }
            else
            {
                return Messages::message(20000);
            }
        }); 
    }

    /**************************************************/

    protected function updateToken($data)
    {
        return DB::transaction(function () use ($data) 
        {
            $userId=$data['userId'];
            $userToken=$data['userToken'];
            if(User::where('userId','=',$userId)->update(['userToken'=>$userToken]))
            {
                return Messages::message(10000);
            }
            else
            {
                return Messages::message(20000);
            }
        }); 
    }

    /**************************************************/

    protected function sendMail($data)
    {
        $view=$data['view'];
        $mail=$data['mail'];
        $name=$data['name'];
        $subject='';
        $code=$this->generateCode();
        Cache::put([$mail=>$code],Carbon::now()->addMinutes(10));
        if($view!='mail.welcomeProntuz')
        {
            $subject='Codigo de verificacion de Prontuz ('.$code.')';
            if($mail==Cache::get($mail))
            {
                Cache::forget($request->input('userEmail'));
            }
        }
        if($view == 'mail.welcomeProntuz')
        {
            $subject='Bienvenido a Prontuz';
            Cache::forget($mail);
        }
        Mail::send($view, ['name'=>$name, 'code'=>$code],function ($message) use ($mail,$subject)
        {
            $message->to($mail);
            $message->subject($subject);
        });
    }

/**************************************************/

    protected function bringDrivers()
    {

     $userId = DB::table('routeassignment')->pluck('routeassignment_userId');

        $username='concat("userFirstname",'."' '".',"userLastname") AS "userName"';
       
        $data=User::select(DB::raw('"userId",'.$username))
           ->where('user_rolId','=',3)
           ->where('userState','=',1)
           ->whereNotIn('userId',$userId)
           ->get();
        
        return $data;
    }

/**************************************************/

     protected function findOne($data)
    {
        $user = DB::table('user')->select('*')
               ->join('rol', 'rol.rolId','=', 'user.user_rolId')
               ->join('doctype', 'doctype.doctypeId', '=', 'user.user_doctypeId')
               ->where('user.userId',$data['userId'])
               ->get();
        $user = $user->toArray();

        if($user[0]->rolId === 1)
        {
            $per=DB::table('rolmenu')->select('menu.*')
               ->join('menu', 'menu.menuId','=', 'rolmenu.rolmenu_menuId')
               ->where('rolmenu.rolmenu_userId','=',$data['userId'])
               ->get();
               $user[0]->permits=$per->toArray();
            return $user;

        }
        else
        {
            return $user;
        }
    }

    protected function generateCode($short=true)
    {
        if($short == false){
            
            $code = rand (0001 ,9999);
            if(strlen($code) === 4)
            {
                return $code;
            }
            else
            {
                return '0'.$code;
            } 
        }else{
            $code = rand (000001 ,999999);
            if(strlen($code) === 6)
            {
                return $code;
            }
            else
            {
                return '0'.$code;
            } 
        }
        
    }

    protected function updatePhoto($data)
    {
        return DB::transaction(function () use ($data) 
        {
            if(User::where('userId','=', $data['userId'])->update($data))
            {
                return Messages::message(10000);
            }
            else
            {
                return Messages::message(20000);
            }
        }); 
    }  
    protected function sendPassword($data)
    {
        return DB::transaction(function () use ($data) 
        {
            if(User::where('userId','=', $data['userId'])->update($data))
            {
                return Messages::message(10000);
            }
            else
            {
                return Messages::message(20000);
            }
        }); 
    }  
/**************************************************/

    protected function getRallUsers()
    {
        $users = User::where('user_rolId',2)->select('*')->get();

        return $users;
    }
    
/**************************************************/

    protected function validateRuser($data)
    {
        $users = DB::table('user')
        ->where('userIdentificacion',$data['userIdentificacion'])
        ->where('userEmail',$data['userEmail'])
        ->where('user_rolId',2)
        ->first();

        return $users;
    }
    
/**************************************************/

    protected function userbyIdentification($data)
    {
        $concat="'".url('/').'/'."'";
        
        $users=User::select(DB::raw('"userFirstname","userLastname", "userEmail", "password", "userIdentificacion","remember_token", "userPhone","userLicence","userBorndate","user_doctypeId","user_rolId","userState","userStreet",
            concat('.$concat.',"userImage") as "userImage",
            "created_by", "updated_by","userId", "userToken"'
            ))
        ->where('userIdentificacion',$data['userIdentificacion'])
        ->where('user_rolId',2)
        ->first();

        return $users;
    }
    
/**************************************************/
           
}
