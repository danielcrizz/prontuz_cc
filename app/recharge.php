<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class recharge extends Model
{
    //
    protected $table='recharge';
    protected $primaryKey ='rechargeId';
    protected $fillable =['rechargeValue','rechargeLoan','recharge_userId','created_by','updated_by'];
    

    protected function getRcValue($recharge_userId){

        $data = DB::table('recharge')->where('recharge_userId', $recharge_userId)->get();   
        
        return $data;
    }
    
/**************************************************/
    
    protected function store($data){

        $res=DB::table('recharge')->insert($data);
        
        return $res;
    }

/**************************************************/
    
    protected function put($data){

        date_default_timezone_set('America/Bogota');

        $res=DB::table('recharge')->where('rechargeId', $data['rechargeId'])
                                  ->update(['rechargeLoan'  => $data['rechargeLoan'],
                                            'rechargeValue' => $data['rechargeValue'],
                                            'updated_by'    => $data['updated_by'],
                                            'updated_at'    => date('Y-m-d H:i:s')
                                        ]);
        
        return $res;
    }

/**************************************************/

    protected function doDiscount($drouteId,$reserveCode,$userIdpayer){

        $data3=array();
        $res = false;

        $recharge = recharge::select('rechargeId','rechargeLoan','rechargeValue')
                                                    ->where('recharge_userId','=',$userIdpayer)
                                                    ->first();
        $routePrice = DB::table('route')->where('routeId', $drouteId)->value('routePrice');

        $rechargeLoan =0;
        $rechargeValue=0;
        if($recharge['rechargeValue'] > $routePrice){

            $rechargeValue = (intval($recharge['rechargeValue'])-intval($routePrice));

        }else{

            $rechargeLoan = (intval($routePrice)-intval($recharge['rechargeValue']));
        }

        $data3['rechargeId']=$recharge['rechargeId'];
        $data3['rechargeValue']=$rechargeValue;
        $data3['rechargeLoan']=$rechargeLoan;
        $data3['updated_by']=$userIdpayer;
    
        $res=$data3;
        
     
        return $res;
    }

    /**************************************************/

    protected function doDiscountWheel($drouteId,$reserveCode,$userIdpayer){

        $data3=array();
        $res = false;

        $recharge = recharge::select('rechargeId','rechargeLoan','rechargeValue')
                                                    ->where('recharge_userId','=',$userIdpayer)
                                                    ->first();
        $routePrice = DB::table('sysparameters')->where('sysparametersName', 'routewheelPrice')->value('sysparametersValue');

        $rechargeLoan =0;
        $rechargeValue=0;
        if($recharge['rechargeValue'] > intval($routePrice)){

            $rechargeValue = (intval($recharge['rechargeValue'])-intval($routePrice));

        }else{

            $rechargeLoan = (intval($routePrice)-intval($recharge['rechargeValue']));
        }

        $data3['rechargeId']=$recharge['rechargeId'];
        $data3['rechargeValue']=$rechargeValue;
        $data3['rechargeLoan']=$rechargeLoan;
        $data3['updated_by']=$userIdpayer;
    
        $res=$data3;
        
     
        return $res;
    }
/**************************************************/

    protected function listing($data)
    {
        //routeState
        
        $concat='concat("userFirstname",'."' '".',"userLastname")';
        $orden  = $data['order']['0']['column'];
        $ordenby= $data['order']['0']['dir'];
        $search = $data['columns'][$orden]['data'];

        switch($search){
            
            case 'userName':
                $search='userName';
            break;
            
            // case 'routeState':
            //     $search='route.routeState';
            // break;
            
       }

        $res=recharge::select(DB::raw('concat("userFirstname",'."' '".',"userLastname") as "userName", "transactionType","transactionValue","transactionOperationdate","rechargeValue","rechargeLoan", 
            CASE WHEN "transactionType" = 0 THEN \'Tarjeta de Credito\' ELSE( CASE WHEN "transactionType" = 1 THEN \'Pago efectivo(Baloto, efecty ..etc)\' ELSE ( CASE WHEN "transactionType" = 2 THEN \'Transferencias bancarias(PSE)\' ELSE ( CASE WHEN "transactionType" = 3 THEN \'Tarjetas débito\' ELSE \'Pago en Caja\' END )END )END ) END as "transactionType2",
            CASE WHEN "transactionCategory" = 0 THEN  \'Oferta\'   ELSE \'Recarga\' END as "transactionCategory2",
                "transactionCategory",
            CASE WHEN "transactionState" = 1 THEN \'Aprobada\' ELSE ( CASE WHEN "transactionState" = 2 THEN \'Pendiente\' ELSE \'Declinada\' END )END  as "transactionState2"'))
            ->join('user', 'userId', '=', "recharge_userId")
            ->join('transaction', 'transaction_userId', '=',  "recharge_userId")
            ->where("user_rolId",2)
            ->skip($data['start'])->take($data['length'])
            ->orderBy($search,$ordenby)
            ->get();
        
        return $res;
    }
/**************************************************/

    protected function search($data='')
    {
        $res=false;
         
        if(array_key_exists('dataSearch', $data)){
        
           $dataSearch="'%".$data['dataSearch']."%'";

             $res= DB::select('select concat("userFirstname",'."' '".',"userLastname")  as  "userName",
                "transactionType","transactionValue","transactionOperationdate","rechargeValue","rechargeLoan",
                CASE WHEN "transactionType" = 0 THEN  \'Tarjeta de Credito\'  ELSE(
                    CASE WHEN "transactionType" = 1 THEN  \'Pago efectivo(Baloto, efecty ..etc)\'  ELSE (
                        CASE WHEN "transactionType" = 2 THEN  \'Transferencias bancarias(PSE)\' ELSE \'Pago en Caja\' END
                    )END
                )
                END  as "transactionType2",
                CASE WHEN "transactionCategory" = 0 THEN  \'Oferta\'   ELSE \'Recarga\' END as "transactionCategory2",
                "transactionCategory",
                CASE WHEN "transactionState" = 1 THEN \'Aprobada\' ELSE ( CASE WHEN "transactionState" = 2 THEN \'Pendiente\' ELSE \'Declinada\' END )END  as "transactionState2"
                from recharge
                inner join "user" on  ("userId" = "recharge_userId")
                inner join "transaction" on  ("transaction_userId" = "recharge_userId")
                where "user_rolId"=2 and concat("userFirstname",'."' '".',"userLastname") like '.$dataSearch);
         
        }
        

        return $res;
    }
/**************************************************/


}
