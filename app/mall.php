<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mall extends Model
{
    protected $table='mall';
    protected $primaryKey ='id';
	protected $fillable =['address','nit','nom_interventor','tel_interventor','city_id'];
}
