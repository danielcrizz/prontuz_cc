<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/logIn', 'AuthController@logIn');
Route::post('/resetPassword', 'UserController@resetPassword');
Route::post('/newPassword', 'UserController@newPassword');
Route::post('/verifyCode', 'UserController@verifyCode');
Route::post('/verifyUser', 'UserController@verifyUser');
Route::post('/updateToken', 'UserController@updateToken');
Route::post('/users/updateToken', 'UserController@updateToken');
Route::group(['prefix'=>'users','middleware' => ['verifyToken']],function()
//Route::group(['prefix'=>'users'],function()
{
	Route::post('/store', 'UserController@store');
	Route::get('/find', 'UserController@search');
	Route::put('/put', 'UserController@put');
	Route::post('/listing', 'UserController@listing');
	Route::delete('/destroy', 'UserController@destroyed');
	Route::post('/userbyIdentification', 'UserController@userbyIdentification');
});

//Route::group(['prefix'=>'common','middleware' => ['verifyToken']],function()
Route::group(['prefix'=>'common'],function()
{
	Route::post('/logOut', 'AuthController@logOut');
	Route::post('/sendOne', 'FireBaseMessageseController@sendOne');
	Route::get('/DocumentKind', 'CommonController@DocumentKind');
	Route::post('/sendToDriver', 'ReserveController@sendToDriver');	
});

Route::group(['prefix'=>'drivers','middleware' => ['verifyToken']],function()
//Route::group(['prefix'=>'drivers'],function()
{
	Route::post('/store', 'DriversController@store');
	Route::get('/find', 'DriversController@search');
	Route::put('/put', 'DriversController@put');
	Route::delete('/destroy', 'DriversController@destroy');
	Route::get('/listing', 'DriversController@destroy');
});

Route::group(['prefix'=>'vehicles','middleware' => ['verifyToken']],function()
//Route::group(['prefix'=>'vehicles'],function()
{
	Route::post('/refreshVlocation', 'VehiclesController@refreshVlocation');//*
	Route::post('/bringOnelbyvehicle', 'VehiclesController@bringOnelbyvehicle');//*
	Route::post('/bringVperfil', 'VehiclesController@bringVperfil');//*
	Route::post('/bringTvehicle', 'VehiclesController@bringTvehicle');//*

	Route::post('/store', 'VehiclesController@store');
	Route::get('/find', 'VehiclesController@search');
	Route::put('/put', 'VehiclesController@put');
	Route::delete('/destroy', 'VehiclesController@destroy');
	Route::get('/listing', 'VehiclesController@destroy');
});

Route::group(['prefix'=>'routes','middleware' => ['verifyToken']],function()
//Route::group(['prefix'=>'routes'],function()
{
	Route::post('/bringRbylocation', 'RoutesController@bringRbylocation');//*
	Route::post('/bringVbyroute', 'RoutesController@bringVbyroute');//*
	Route::post('/bringLbyroute', 'RoutesController@bringLbyroute');//*
	Route::post('/activateAssignment', 'RoutesController@activateAssignment');//*	
	
	Route::post('/store', 'RoutesController@store');
	Route::get('/find', 'RoutesController@search');//*
	Route::put('/put', 'RoutesController@put');
	Route::delete('/destroy', 'RoutesController@destroy');
	Route::post('/listing', 'RoutesController@listing');
	Route::post('/getWayPoint', 'RoutesController@getWayPoint');
	Route::post('/beginRoute', 'RoutesController@beginRoute');
	Route::get('/getRprincipals', 'RoutesController@getRprincipals');//*
	Route::post('/storeRoutewheel', 'RoutesController@storeRoutewheel');//*
	Route::post('/findWheel', 'RoutesController@findWheel');//*
	Route::post('/findRoutes', 'RoutesController@findRoutes');//*
	Route::get('/bringlocalization', 'RoutesController@bringlocalization');//*


});
Route::group(['prefix'=>'location','middleware' => ['verifyToken']],function()
//Route::group(['prefix'=>'location'],function()
{
	Route::get('/bringlocalization', 'LocationController@bringlocalization');//*
	Route::post('/bringLbyvehicle', 'LocationController@bringLbyvehicle');//*
	
	Route::post('/store', 'LocationController@store');
	Route::get('/find', 'LocationController@search');
	Route::put('/put', 'LocationController@put');
	Route::delete('/destroy', 'LocationController@destroy');
	Route::get('/listing', 'LocationController@destroy');
});
Route::group(['prefix'=>'notification','middleware' => ['verifyToken']],function()
//Route::group(['prefix'=>'notification'],function()
{
	Route::post('/store', 'NotificationController@store');
	Route::get('/find', 'NotificationController@search');
	Route::put('/put', 'NotificationController@put');
	Route::delete('/destroy', 'NotificationController@destroy');
	Route::get('/listing', 'NotificationController@listing');
});


Route::group(['prefix'=>'reserve','middleware' => ['verifyToken']],function()
//Route::group(['prefix'=>'reserve'],function()
{
	Route::post('/store', 'ReserveController@store');
	Route::post('/findReserveUserActive', 'ReserveController@findReserveUserActive');
	Route::post('/listingReserveVehicle', 'ReserveController@listingReserveVehicle');
	Route::put('/clearReserveAll', 'ReserveController@clearReserveAll');
	Route::put('/closeReserveUser', 'ReserveController@closeReserveUser');
	Route::get('/historyReserveUser', 'ReserveController@historyReserveUser');
	Route::post('/confirmReserve', 'ReserveController@confirmReserve');
	Route::post('/finishReserve', 'ReserveController@finishReserve');//*	
	Route::post('/alertVehicle', 'ReserveController@alertVehicle');//*	

});

// Route::group(['prefix'=>'recharge'],function()
Route::group(['prefix'=>'recharge','middleware' => ['verifyToken']],function()
{
	Route::post('/getRcValue', 'RechargeController@getRcValue');
	Route::post('/store', 'RechargeController@store');//*
	Route::put('/put', 'RechargeController@put');//*
	Route::post('/getPayer', 'RechargeController@getPayer');	

});

Route::group(['prefix'=>'payments','middleware' => ['verifyToken']],function()
{
	Route::post('/getWheelBalance', 'PaymentController@getWheelBalance');//////***//////	
	Route::post('/getWheelTrips', 'PaymentController@getWheelTrips');//////***//////	
});

Route::group(['prefix'=>'news','middleware' => ['verifyToken']],function()
{
	Route::get('/find', 'NewsController@search');//*
	Route::post('/changeSeen', 'NewsController@changeSeen');	
	

});

Route::group(['prefix'=>'offers','middleware' => ['verifyToken']],function()
{
	Route::get('/find', 'OffersController@search');//*
});


Route::post('/store', 'RechargeController@store');//*
