<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//rutas forntend web alexis


Route::get('login', ['as' => 'login', 'uses' => 'AuthController@getView']);


Route::post('/singUp', 'UserController@store');
Route::post('/logIn', 'AuthController@logIn');
Route::post('user/resetPassword', 'UserController@resetPassword');
Route::post('user/newPassword', 'UserController@newPassword');
Route::post('user/sendPassword', 'UserController@sendPassword');
Route::post('user/bringParameters', 'UserController@bringParameters');
Route::post('user/putSysparameters', 'UserController@putSysparameters');

Route::group(['middleware'=> ['auth','verifyUser']],function()
{
	Route::group(['prefix'=>'users'	],function()
	{
		Route::post('/store', 'UserController@store');
		Route::get('/find', 'UserController@search');
		Route::put('/put', 'UserController@put');
		Route::delete('/destroy', 'UserController@destroyed');
		Route::post('/listing', 'UserController@listing');
		Route::post('/totalKindUsers', 'UserController@totalKindUsers');
		
		Route::get('/findOne', 'UserController@findOne');
		Route::post('/resetPassword', 'UserController@resetPassword');
		Route::post('/newPassword', 'UserController@newPassword');
		Route::post('/verifyIdentification', 'UserController@verifyIdentification');
		Route::post('/verifyEmail', 'UserController@verifyEmail');
		Route::post('/updatePhoto', 'UserController@updatePhoto');
		Route::post('/sendPassword', 'UserController@sendPassword');
		Route::delete('/deleteAssignmentWheel', 'UserController@deleteAssignmentWheel');
		

		Route::get('/DocumentKind', 'CommonController@DocumentKind');//*
		
		//vistas front alexis
		Route::get('/', function () {
			return view('users/listing');
		});
		Route::get('/{userId}/card', function ($userId) {
		    return view('users/panel')->with('userId', $userId);
		});
	});

	Route::group(['prefix'=>'vehicles'],function()
	{
		//peticion formulario de creacion vehiculo
		Route::get('/bringVBrand', 'VehiclesController@bringVBrand');//*
		Route::get('/bringVType', 'VehiclesController@bringVType');//*
		Route::post('/bringVLine', 'VehiclesController@bringVLine');//*--
		Route::post('/validateVplaque', 'VehiclesController@validateVplaque');//
		
		Route::post('/store', 'VehiclesController@store');//*
		Route::get('/find', 'VehiclesController@search');//*
		Route::put('/put', 'VehiclesController@put');//*
		Route::delete('/destroy', 'VehiclesController@destroy');//*
		Route::post('/listing', 'VehiclesController@listing');
		Route::post('/validateVbrand', 'VehiclesController@validateVbrand');
		Route::post('/storeVbrand', 'VehiclesController@storeVbrand');
		Route::post('/validateVLine', 'VehiclesController@validateVLine');
		Route::post('/storeVline', 'VehiclesController@storeVline');
		Route::post('/getWheel', 'VehiclesController@getWheel');
		Route::post('/storeAssignmentWheel', 'VehiclesController@storeAssignmentWheel');
	
		//vistas front alexis
		Route::get('/', function () {
		
			$data=array(
				'moretoday'=>(intval(date('Y'))+1),
				'leasttoday'=>(intval(date('Y'))- 25)
			);
		    return view('vehicles/listing',$data);
		});

		Route::get('/{vehicleId}/card', function ($vehicleId) {
			$data=array(
				'moretoday'=>(intval(date('Y'))+1),
				'leasttoday'=>(intval(date('Y'))- 25)
			);

		    return view('vehicles/panel',$data)->with('vehicleId', $vehicleId);
		});
	});

	Route::group(['prefix'=>'routes'],function()
	{
		Route::get('/bringRprincipals', 'RoutesController@bringRprincipals');//*
		Route::post('/validateRname', 'RoutesController@validateRname');//*
		Route::get('/bringRlast', 'RoutesController@bringRlast');//* ---
		Route::post('/saveAssignment', 'RoutesController@saveAssignment');//*	
		Route::post('/putAssignment', 'RoutesController@putAssignment');//*	
		Route::post('/bringAssignment', 'RoutesController@bringAssignment');//*	
		Route::post('/bringLbyroute', 'RoutesController@bringLbyroute');//*
		Route::post('/validatelocalization', 'RoutesController@validatelocalization');//*
		Route::post('/validateAddress', 'RoutesController@validateAddress');
		Route::get('/bringVavailable', 'RoutesController@bringVavailable');//* --			
		Route::get('/bringDrivers', 'RoutesController@bringDrivers');//*
		Route::post('/destroyAssignment', 'RoutesController@destroyAssignment');//*
		Route::get('/getRprincipals', 'RoutesController@getRprincipals');//*
		Route::post('/findWheel', 'RoutesController@findWheel');//*
		Route::post('/findRoutes', 'RoutesController@findRoutes');//*
		Route::get('/bringlocalization', 'RoutesController@bringlocalization');//*
	

		
		Route::post('/store', 'RoutesController@store');//*
		Route::get('/find', 'RoutesController@search');//*
		Route::put('/put', 'RoutesController@put');//*
		Route::delete('/destroy', 'RoutesController@destroy');//*
		Route::post('/listing', 'RoutesController@listing');	


		//vistas front alexis
		Route::get('/', function () {
		    return view('routes/listing',['googleApiKey' => 'IzaSyCAcMphZ5OU_4iZl30i-gjD_NDmZsZ0TrQ']);
		});


	});

	Route::group(['prefix'=>'recharge'],function()
	{
		

		Route::post('/store', 'RechargeController@store');//*
		Route::post('/getRusers', 'RechargeController@getRusers');
		Route::post('/getRallUsers', 'RechargeController@getRallUsers');
		Route::post('/getRcValue', 'RechargeController@getRcValue');
		Route::post('/sumTransactions', 'RechargeController@sumTransactions');
		Route::post('/getIduser', 'RechargeController@getIduser');
		// Route::delete('/destroy', 'RoutesController@destroy');//*
		Route::post('/listing', 'RechargeController@listing');	
		Route::get('/', function () {
			
			$userId = Auth::user()->userId;
		    return view('recharge/listing',['userId'=> $userId]);
		});
	});

	Route::group(['prefix'=>'payments'],function()
	{
		

		Route::post('/store', 'PaymentController@store');//*
		Route::post('/getWheelinfo', 'PaymentController@getWheelinfo');
		Route::post('/getRusers', 'PaymentController@getRusers');
		Route::post('/statistics', 'PaymentController@statistics');
		
		Route::post('/listing', 'PaymentController@listing');	
		Route::get('/', function () {
			
			$userId = Auth::user()->userId;
		    return view('payments/listing',['userId'=> $userId]);
		});
	});

	Route::group(['prefix'=>'offers'],function()
	{
		

		Route::post('/store', 'OffersController@store');//*
		Route::get('/find', 'OffersController@search');//*
		Route::post('/put', 'OffersController@put');//*
		Route::post('/listing', 'OffersController@listing');	
	
			
		Route::get('/', function () {
		
		    return view('offers/listing');
		});

		Route::get('/{offerId}/card', function ($offerId) {
		
		    return view('offers/panel',$data)->with('offerId', $offerId);
		});
		
	});

	Route::group(['prefix'=>'news'],function()
	{
		

		Route::post('/store', 'NewsController@store');//*
		Route::get('/find', 'NewsController@search');//*
		Route::post('/put', 'NewsController@put');//*
		Route::post('/listing', 'NewsController@listing');	
			
		Route::get('/', function () {
		
		    return view('news/listing');
		});

		Route::get('/{newsId}/card', function ($newsId) {
		
		    return view('news/panel',$data)->with('newsId', $newsId);
		});
		
	});

	Route::group(['prefix'=>'transit'],function()
	{
		

		Route::get('/bringRprincipals', 'RoutesController@bringRprincipals');//*
		Route::post('/validateRname', 'RoutesController@validateRname');//*
		Route::get('/bringRlast', 'RoutesController@bringRlast');//* ---
		Route::post('/saveAssignment', 'RoutesController@saveAssignment');//*	
		Route::post('/putAssignment', 'RoutesController@putAssignment');//*	
		Route::post('/bringAssignment', 'RoutesController@bringAssignment');//*	
		Route::post('/bringLbyroute', 'RoutesController@bringLbyroute');//*
		Route::post('/validatelocalization', 'RoutesController@validatelocalization');//*
		Route::post('/validateAddress', 'RoutesController@validateAddress');
		Route::get('/bringVavailable', 'RoutesController@bringVavailable');//* --			
		Route::get('/bringDrivers', 'RoutesController@bringDrivers');//*
		Route::post('/destroyAssignment', 'RoutesController@destroyAssignment');//*
	

		
		Route::post('/store', 'RoutesController@store');//*
		Route::get('/find', 'RoutesController@search');//*
		Route::put('/put', 'RoutesController@put');//*
		Route::delete('/destroy', 'RoutesController@destroy');//*
		Route::post('/listing', 'RoutesController@listing');	


		//vistas front alexis
		Route::get('/', function () {
		    return view('transit/listing',['googleApiKey' => 'IzaSyCAcMphZ5OU_4iZl30i-gjD_NDmZsZ0TrQ']);
		});
		
	});


	
	Route::group(['prefix'=>'menu'],function()
	{
		Route::get('/listing_all', 'menuController@listing_all');
		
	});
	Route::get('/dashboard', function () {
	    return view('transit/listing');
	});
});

Route::get('/mail', function () {

	return view('mails/buyEmail');//	
});

Route::get('/logOut', 'AuthController@logOut');

Route::get('404', function () 
{
    return view('404');		
});

/*verificacion de sesion*/
Route::get('/dashboard', function () 
{
	if(Auth::check())
	{
    	return view('dahsboard/listing');
	}
	else
	{
    	return view('login');		
	}
});

Route::get('/index', function () {
    return redirect('/dashboard');
});
		
/** Recharge **/
Route::get('/validateAllPending', 'RechargeController@validateAllPending');
Route::post('/validateRuser', 'RechargeController@validateRuser');	
Route::post('/Rstore', 'RechargeController@Rstore');
Route::get('/Ffind', 'OffersController@search');//*	
Route::get('/','RechargeController@validateRview');
/***/

Route::post('/PayuPing', 'PaymentsController@PayuPing');	
Route::post('/getPaymentMethods', 'PaymentsController@getPaymentMethods');	

//Route::post('/getOrderDetail', 'PaymentsController@getOrderDetail');
//Route::post('/getTransactionResponse', 'PaymentsController@getTransactionResponse');
